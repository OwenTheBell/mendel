﻿using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(
    fileName = "Quotes",
    menuName = "Mendel/Quotes")]
public class Quotes : ScriptableObject {

    [System.Serializable]
    public struct Quote {
        public string quote;
        public string author;
    }

    [SerializeField]
    private List<Quote> All;

    public void GetQuote(out string quote, out string author) {
        var index = Random.Range(0, All.Count);
        quote = All[index].quote;
        author = All[index].author;
    }

}
