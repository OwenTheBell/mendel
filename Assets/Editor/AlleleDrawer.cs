using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(Allele))]
public class AlleleDrawer : PropertyDrawer {
	
	public override void OnGUI(Rect position,
								SerializedProperty property,
								GUIContent label) {

		label = EditorGUI.BeginProperty(position, label, property);

		var rect = position;

		var allele1 = property.FindPropertyRelative("allele1").intValue;
		var allele2 = property.FindPropertyRelative("allele2").intValue;

		// EditorGUIUtility.labelWidth = 14f;

		var contentPos = EditorGUI.PrefixLabel(position, label);
		contentPos.width = 50;
		EditorGUIUtility.labelWidth = 0f;
		var bool1 = EditorGUI.Toggle(contentPos, (allele1 == 1));
		contentPos.x += contentPos.width;
		var bool2 = EditorGUI.Toggle(contentPos, (allele2 == 1));
		contentPos.x += 20;
		contentPos.width = 200;
		// var bool2 = true;

		property.FindPropertyRelative("allele1").intValue = (bool1) ? 1 : -1;
		property.FindPropertyRelative("allele2").intValue = (bool2) ? 1 : -1;

		rect.width = EditorGUIUtility.labelWidth;
		if (bool1 == bool2 && bool1) {
			EditorGUI.LabelField(contentPos, "Dominant");
		}
		else if (bool1 == bool2 && !bool1) {
			EditorGUI.LabelField(contentPos, "Recessive");
		}
		else if (bool1 != bool2) {
			EditorGUI.LabelField(contentPos, "Partial Dominant");
		}

		EditorGUI.EndProperty();
    }
}