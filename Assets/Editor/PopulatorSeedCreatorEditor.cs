using UnityEngine;
using UnityEditor;
using System;

[CustomEditor(typeof(PopulatorSeedCreator))]
public class PopulatorSeedCreatorEditor : Editor {

	public override void OnInspectorGUI() {

		// DrawDefaultInspector();

		serializedObject.Update();

		var property = serializedObject.GetIterator();
		property.Next(true);
		while (property.NextVisible(false)) {
			if (property.type.Contains("ValuesCreator")) {
				DisplayValuesCreator(property);
			}
			else {
				EditorGUILayout.PropertyField(property);
			}
		}

		serializedObject.ApplyModifiedProperties();
	}

	private void DisplayValuesCreator(SerializedProperty property) {
		var lowerProperty = property.FindPropertyRelative("LowerValues");
		var upperProperty = property.FindPropertyRelative("UpperValues");

		var name = property.displayName;
		// var expanded = property.isExpanded;
		property.isExpanded = EditorGUILayout.Foldout(property.isExpanded, name);

		if (!property.isExpanded) {
			return;
		}

		EditorGUI.indentLevel++;

		var lowerContent = new GUIContent("lower");
		var upperContent = new GUIContent("upper");

		var lowerEnd = lowerProperty.GetEndProperty();
		var upperEnd = upperProperty.GetEndProperty();

		lowerProperty.Next(true);
		upperProperty.Next(true);

		do {
		// while (lowerProperty.NextVisible(false) && upperProperty.NextVisible(false)) {

			EditorGUILayout.LabelField(lowerProperty.displayName);

			EditorGUI.indentLevel++;
			EditorGUILayout.PropertyField(lowerProperty, lowerContent);
			EditorGUILayout.PropertyField(upperProperty, upperContent);
			EditorGUI.indentLevel--;

			lowerProperty.NextVisible(false);
			upperProperty.NextVisible(false);
		} while (!SerializedProperty.EqualContents(lowerProperty, lowerEnd) &&
				!SerializedProperty.EqualContents(upperProperty, upperEnd));

		EditorGUI.indentLevel--;
	}

}