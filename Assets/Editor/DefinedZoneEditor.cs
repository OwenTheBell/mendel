﻿using UnityEngine;
using UnityEditor;
using UnityEngine.SceneManagement;
using NUnit.Framework;
using System;

[CustomEditor(typeof(DefinedZone))]
public class DefinedZoneEditor : Editor {

	const float LINE_WIDTH = 20f;
	const float DOT_RADIUS = 10f;
	const float DOT_SIZE = 0.1f;

	private Vector3[] positions;

	private bool _MouseDown = false;
	private int _ManipulatingIndex = -1;
	private Vector3 _AlteredPoint;

	void OnSceneGUI() {

		var t = target as DefinedZone;

		var pointsProperty = serializedObject.FindProperty("Points");

		positions = new Vector3[pointsProperty.arraySize];
		for (var i = 0; i < positions.Length; i++) {
			positions[i] = pointsProperty.GetArrayElementAtIndex(i).vector3Value;
		}

		for (var i = 0; i < positions.Length; i++) {
			positions[i] = t.transform.TransformPoint(positions[i]);
		}

		var linePositions = new Vector3[positions.Length + 1];
		Array.Copy(positions, linePositions, positions.Length);
		linePositions[linePositions.Length - 1] = positions[0];

		var red = t.Color;
		Handles.color = t.Color;
		var lineSize = LINE_WIDTH * HandleUtility.GetHandleSize(t.transform.position);
		Handles.DrawAAPolyLine(LINE_WIDTH, linePositions);

		foreach (var position in positions) {
			var dotSize = DOT_SIZE * HandleUtility.GetHandleSize(position);
			Handles.DrawSolidDisc(position, Vector3.up, dotSize);
		}

		var controlID = GUIUtility.GetControlID(FocusType.Passive);

		var e = Event.current;
		if (e.type == EventType.MouseDown && e.button == 0) {
			var mouse = GUIUtility.GUIToScreenPoint(e.mousePosition);

			var index = 0;
			foreach (var position in positions) {
				var dotSize = DOT_SIZE * HandleUtility.GetHandleSize(position);
				var distance = HandleUtility.DistanceToCircle(position, dotSize);
				if (distance == 0) {
					GUIUtility.hotControl = controlID;
					e.Use();
					_MouseDown = true;
					_ManipulatingIndex = index;
					break;
				}
				index++;
			}

			for (var i = 0; i < linePositions.Length && !_MouseDown; i++) {
				var j = (i >= linePositions.Length) ? 0 : i+1;
				var pos1 = linePositions[i];
				var pos2 = linePositions[j];
				var distanceToLine = HandleUtility.DistanceToLine(pos1, pos2);
				if (distanceToLine < LINE_WIDTH/2) {
					var newPoint = HandleUtility.ClosestPointToPolyLine(linePositions);
					_ManipulatingIndex = i+1;
					pointsProperty.InsertArrayElementAtIndex(_ManipulatingIndex);
					_AlteredPoint = newPoint;
					GUIUtility.hotControl = controlID;
					e.Use();
					_MouseDown = true;
					break;
				}
			}
		}
		else if (e.type == EventType.MouseUp && e.button == 0) {
			if (_MouseDown) {
				GUIUtility.hotControl = 0;
				_MouseDown = false;
				e.Use();
			}
		}
		else if (e.type == EventType.MouseDrag && _MouseDown) {
			GUIUtility.hotControl = controlID;
			e.Use();

			var ray = HandleUtility.GUIPointToWorldRay(e.mousePosition);
			var origin = new Vector3(ray.origin.x, t.transform.position.y, ray.origin.z);
			_AlteredPoint = origin;
			// positions[_ManipulatingIndex] = origin;
		}
		else if (e.type == EventType.MouseDown && e.button == 1) {
			var index = 0;
			foreach (var position in positions) {
				var dotSize = DOT_SIZE * HandleUtility.GetHandleSize(position);
				var distance = HandleUtility.DistanceToCircle(position, dotSize);
				if (distance == 0) {
					GUIUtility.hotControl = controlID;
					e.Use();
					break;
				}
				index++;
			}
			pointsProperty.DeleteArrayElementAtIndex(index);
			serializedObject.ApplyModifiedProperties();
		}

		if (_MouseDown && _ManipulatingIndex >= 0 && _ManipulatingIndex < pointsProperty.arraySize) {
			var point = t.transform.InverseTransformPoint(_AlteredPoint);
			pointsProperty.GetArrayElementAtIndex(_ManipulatingIndex).vector3Value = point; 
			serializedObject.ApplyModifiedProperties();
		}
	}

	public override void OnInspectorGUI() {
		serializedObject.Update();
		DrawDefaultInspector();
	}

}
