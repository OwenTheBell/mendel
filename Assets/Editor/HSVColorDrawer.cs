using UnityEngine;
using UnityEditor;

[CustomPropertyDrawer(typeof(HSVColor))]
public class HSVColorDrawer : PropertyDrawer {
	
	public override void OnGUI(Rect position, SerializedProperty property, GUIContent label) {
		label = EditorGUI.BeginProperty(position, label, property);
		Rect contentPos = EditorGUI.PrefixLabel(position, label);

		var savedIndent = EditorGUI.indentLevel;

		if (position.height > 16f) {
			position.height = 16f;
			EditorGUI.indentLevel++;
			contentPos = EditorGUI.IndentedRect(position);
			contentPos.y += 18f;
		}

		EditorGUIUtility.labelWidth = 14f;

		EditorGUI.indentLevel = 0;
		/* remove this to allow for the spacing between properties */
		contentPos.width -= 3;
		contentPos.width *= 0.25f;

		var h = property.FindPropertyRelative("h").floatValue;
		var s = property.FindPropertyRelative("s").floatValue;
		var v = property.FindPropertyRelative("v").floatValue;
		
		h = Mathf.Clamp01(EditorGUI.FloatField(contentPos, "H", h));
		contentPos.x += contentPos.width;
		s = Mathf.Clamp01(EditorGUI.FloatField(contentPos, "S", s));
		contentPos.x += contentPos.width;
		v = Mathf.Clamp01(EditorGUI.FloatField(contentPos, "V", v));
		contentPos.x += contentPos.width + 3;

		var hsvColor = new HSVColor(h, s, v);
		var rgbColor = EditorGUI.ColorField(contentPos, hsvColor.RGBColor);

		if (rgbColor != hsvColor.RGBColor) {
			hsvColor = new HSVColor(rgbColor);
			h = hsvColor.h;
			s = hsvColor.s;
			v = hsvColor.v;
		}

		property.FindPropertyRelative("h").floatValue = h;	
		property.FindPropertyRelative("s").floatValue = s;	
		property.FindPropertyRelative("v").floatValue = v;	

		if (position.height > 16f) {
			EditorGUI.indentLevel--;
		}

		EditorGUI.indentLevel = savedIndent;

		EditorGUI.EndProperty();
	}

	/* in the event the column is below a width of 333, claim an extra line of space
	* in the inspector window */
	public override float GetPropertyHeight(SerializedProperty property, GUIContent label) {
		/* this line accounts for retina screens (maybe). It works for my 15" retina
		* macbook pro */
		var dpiRatio = Screen.dpi / 73.5;
		return Screen.width < (333 * dpiRatio) ? (16f + 18f) : 16f;
	}

}