/*
* Users of this interface will be checked for by a Gene Managre for assembling
* the first genetics of a plant. Their only purpose is to expose parameters in
* the inspector for setting the values of a specific gene, and then making
* that new gene accessible to the GeneManager.
*/

public interface IGeneCreator {

	// GeneTypeEnum GeneType {
	// 	get;
	// }

	IGene Gene {
		get;
	}
}