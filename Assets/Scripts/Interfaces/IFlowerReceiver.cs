public interface IFlowerReceiver {

    ISeed Seed { get; }

    void SetSeed(UnityEngine.GameObject seedObject);
}
