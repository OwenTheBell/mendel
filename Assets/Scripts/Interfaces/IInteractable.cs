public interface IInteractable {

    bool Usable { get; }

    void Interact();
}
