using UnityEngine;
using System.Collections;

// public enum GeneTypeEnum {LSystemGene, FlowerGene, TestGene};

public interface IGene {

	System.Type SortingType { get; }
	bool Comparable { get; }

	bool MixWith(IGene otherGene, out IGene babyGene, bool mutate);
	GeneValues GeneValues { get; set; }

	/*
	* The gene should return the state of its values as a string
	*/
	string ToString();

	/* return true if the genes are considered to match each other phenotypically */
	bool CompareWith(IGene gene);
}

public struct GeneComparisonStruct {
	public float Similarity;
	public float Weight;

	public float Percent { get { return Similarity/Weight; }}

	public GeneComparisonStruct(float similarity, float weight) {
		// Similarity = Mathf.Clamp01(similarity);
		Similarity = similarity;
		Weight = weight;
	}
}