public interface ISeed {

	System.Collections.Generic.Dictionary<System.Type, IGene> Genes {
		get;
		set;
	}

	bool CompareWith(ISeed seed);
	GeneComparisonStruct WeightedCompareWith(ISeed seed);
}
