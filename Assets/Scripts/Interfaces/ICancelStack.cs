﻿public interface ICancelStack {
    bool Occupied { get; }
    bool Locked { get; set; }
    void Add(ICancelReciever reciever, bool fade = true);
    void Remove();
    void Remove(ICancelReciever reciever);
    void RemoveAll();
}