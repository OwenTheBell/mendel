using UnityEngine;

public interface IFlowerMeshBuilder {
	
	Mesh Mesh {
		get;
	}

    Mesh MeshForHeight(float height);

}