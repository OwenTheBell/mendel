﻿using UnityEngine;
using System.Collections;

public class PositionIndicatorGizmo : MonoBehaviour {

	public float radius = 0.2f;
	public Color color = Color.red;

	void OnDrawGizmos() {
		Gizmos.color = color;	
		Gizmos.DrawSphere(transform.position, radius);
	}
}
