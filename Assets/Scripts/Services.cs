using UnityEngine.Assertions;
using UnityEngine;

public class Services {
	private static SpeciesCatalog _SpeciesCatalog;
	public static SpeciesCatalog SpeciesCatalog {
		get {
			if (_SpeciesCatalog == null) { _SpeciesCatalog = new SpeciesCatalog(); }
			return _SpeciesCatalog;
		}
		set { _SpeciesCatalog = value; }
	}

	private static SaveLoadController _SaveLoad;
	public static SaveLoadController SaveLoad {
		get {
            if (_SaveLoad == null) { _SaveLoad = new SaveLoadController(); }
			return _SaveLoad;
		}
		set { _SaveLoad = value; }
	}

    private static SeedVault _SeedVault;
    public static SeedVault SeedVault {
        get {
            if (_SeedVault == null) { _SeedVault = new SeedVault(); }
            return _SeedVault;
        }
        set { _SeedVault = value; }
    }

    private static MeshVault _MeshVault;
    public static MeshVault MeshVault {
        get {
            if (_MeshVault == null) { _MeshVault = new MeshVault(); }
            return _MeshVault;
        }
        set { _MeshVault = value; }
    }

    private static PlantInfoCatalog _PlantInfoCatalog;
    public static PlantInfoCatalog PlantInfoCatalog {
        get {
            if (_PlantInfoCatalog == null) { _PlantInfoCatalog = new PlantInfoCatalog(); }
            return _PlantInfoCatalog;
        }
        set { _PlantInfoCatalog = value; }
    }

    public static bool Loaded;

    public static WindControl _WindControl;
    public static WindControl WindControl {
        get {
            Assert.IsNotNull(_WindControl, "WindControl services has not been set");
            return _WindControl;
        }
        set { _WindControl = value; }
    }

    public static ICancelStack CancelStack { get; set; }

    public static Transform _PlayerTransform;
    public static Transform PlayerTransform {
        get {
            return _PlayerTransform;
        }
        set {
            _PlayerTransform = value;
        }
    }
}