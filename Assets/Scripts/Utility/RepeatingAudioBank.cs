﻿using UnityEngine;
using System.Collections;

public class RepeatingAudioBank : MonoBehaviour, IOnGrow, IOnDoneGrowing {

    public float TransitionTime;
    public Vector2 TransitionRange;
    public AudioClip[] Clips;

    private AudioSource[] _Sources;
    private int _SourceIndex;
    private int _ClipIndex;

    private void Awake() {
        _Sources = GetComponents<AudioSource>();
        _SourceIndex = 0;
        _ClipIndex = Clips.Length;
    }

    public void OnGrow() {
        Play();
    }

    public void OnDoneGrowing() {
        Stop();
    }

    public void Play() {
        enabled = true;
        StartCoroutine(CrossFade());
    }

    public void Stop() {
        StopAllCoroutines();
        if (_ClipIndex < Clips.Length) {
            StartCoroutine(Fade());
        }
    }

    IEnumerator CrossFade() {
        var nextIndex = (_SourceIndex == 0) ? 1 : 0;
        var nextSource = _Sources[nextIndex];
        var currentSource = _Sources[_SourceIndex];

        var nextClipIndex = Random.Range(0, Clips.Length - 2);
        if (nextClipIndex >= _ClipIndex) nextClipIndex++;
        nextSource.clip = Clips[nextClipIndex];
        nextSource.volume = 0f;
        nextSource.Play();

        var time = 0f;
        while (time < TransitionTime) {
            time += Time.deltaTime;
            var percent = Easing.EaseInOut(time / TransitionTime, 2);
            currentSource.volume = 1f - percent;
            nextSource.volume = percent;
            yield return null;
        }
        currentSource.Stop();

        _SourceIndex = nextIndex;
        _ClipIndex = nextClipIndex;

        var gap = Random.Range(TransitionRange.x, TransitionRange.y);
        gap += Clips[_ClipIndex].length - TransitionTime * 2 + gap;
        yield return new WaitForSeconds(gap);
        StartCoroutine(CrossFade());
    }

    IEnumerator Fade() {
        var difference = Clips[_ClipIndex].length - _Sources[_SourceIndex].time;
        var timeRemainging = (difference < TransitionTime) ? difference : TransitionTime;
        var time = 0f;
        var startingVolumes = new float[_Sources.Length];
        for (var i = 0; i < _Sources.Length; i++) {
            startingVolumes[i] = _Sources[i].volume;
        }
        while (time < timeRemainging) {
            time += Time.deltaTime;
            var percent = 1f - Easing.EaseInOut(time / timeRemainging, 2);
            for (var i = 0; i < _Sources.Length; i++) {
                _Sources[i].volume = startingVolumes[i] * percent;
            }
            yield return null;
        }
        yield return null;
        enabled = false;
    }
}