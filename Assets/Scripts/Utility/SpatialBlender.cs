﻿using UnityEngine;
using System.Collections.Generic;

public class SpatialBlender : MonoBehaviour {

    private List<float> _SavedMax;
    private List<AudioSource> _Sources;

    private void Awake() {
        _Sources = new List<AudioSource>(GetComponents<AudioSource>());
        _SavedMax = new List<float>();
        _Sources.Act(s => _SavedMax.Add(s.maxDistance));
    }

    public void SetSpatialBlend(float blend, float radius) {
        for (var i = 0; i < _Sources.Count; i++) {
            _Sources[i].minDistance = radius;
            _Sources[i].maxDistance = radius + _SavedMax[i];
            _Sources[i].spatialBlend = blend;
        }
    }
}