using UnityEngine;
using System.Collections.Generic;

public class Events {
	private static Events eventsInstance = null;
	public static Events instance {
		get {
			if (eventsInstance == null) {
				eventsInstance = new Events();
			}

			return eventsInstance;
		}
	}

	public delegate void EventDelegate<T> (T e) where T : GameEvent;

	private Dictionary<System.Type, System.Delegate> delegates =
									new Dictionary<System.Type, System.Delegate>();

	public void AddListener<T>(EventDelegate<T> del) where T : GameEvent {
		if (delegates.ContainsKey(typeof(T))) {
			System.Delegate tempDel = delegates[typeof(T)];
			delegates[typeof(T)] = System.Delegate.Combine(tempDel, del);
		} else {
			delegates[typeof(T)] = del;
		}
	}

	public void RemoveListener<T>(EventDelegate<T> del) where T : GameEvent {
		if (delegates.ContainsKey(typeof(T))) {
			var currentDel = System.Delegate.Remove(delegates[typeof(T)], del);
			if (currentDel == null) {
				delegates.Remove(typeof(T));
			} else {
				delegates[typeof(T)] = currentDel;
			}
		}
	}

	public void Raise (GameEvent e) {
		if (e == null) {
			Debug.Log("Invalid event arugment: " + e.GetType().ToString());
			return;
		}

		if (delegates.ContainsKey(e.GetType())) {
			delegates[e.GetType()].DynamicInvoke(e);
		}
	}

}


/************************
* All GameEvent classes *
************************/

public class GameEvent {};

/* Used to change the crosshair */
public class SetCrosshairEvent : GameEvent {
	public readonly Sprite sprite;

	public SetCrosshairEvent(Sprite sprite) {
		this.sprite = sprite;
	}
}

/* Undoes previous */
public class UnsetCrosshairEvent : GameEvent {
	public readonly Sprite sprite;

	public UnsetCrosshairEvent(Sprite sprite) {
		this.sprite = sprite;
	}
}

public class ResetCrosshairEvent : GameEvent {}

public class TintGroundEvent : GameEvent {
	public readonly Vector3 position;
	public readonly HSVColor hsvColor;
	public readonly float radius;

	public TintGroundEvent(Vector3 position, HSVColor color, float radius) {
		this.position = position;
		this.hsvColor = color;
		this.radius = radius;
	}
}

public class HideAlertEvent : GameEvent { }

public class UnHideAlertEvent : GameEvent { }

public class SetPlayerActiveEvent : GameEvent {
	public readonly bool Enabled;

	public SetPlayerActiveEvent(bool enabled) {
		this.Enabled = enabled;
	}
}

public class PlayerStartedEvent : GameEvent {}
public class NoAvailablePickupEvent : GameEvent {}

public class GroundTargetEvent : GameEvent {
	public readonly Vector3 target;

	public GroundTargetEvent(Vector3 target) {
		this.target = target;
	}
}

public class ClearGroundTargetEvent : GameEvent {}

public class NewGameEvent : GameEvent {}

public class NewPlantEvent : GameEvent {
    public readonly GameObject gameObject;

    public NewPlantEvent(GameObject gameObject) {
        this.gameObject = gameObject;
    }
}

public class PlantUprootedEvent : GameEvent {
    public readonly GameObject plant;

    public PlantUprootedEvent(GameObject plant) {
        this.plant = plant;
    }
}

public class PlantReplantedEvent : GameEvent {
    public readonly GameObject plant;

    public PlantReplantedEvent(GameObject plant) {
        this.plant = plant;
    }
}

public class FadeEvent : GameEvent { }

public class RefocusEvent : GameEvent { }

public class ShowTaxonomyPromptEvent : GameEvent { }

public class ShowSpeciesEntryEvent : GameEvent {
    public string name;
    public int identity;

    public ShowSpeciesEntryEvent(string name, int identity) {
        this.name = name;
        this.identity = identity;
    }
}

public class HybridizePlantsEvent : GameEvent {
    readonly public int id1;
    readonly public int id2;
    readonly public Vector3 position1;
    readonly public Vector3 position2;
    readonly public bool immediate;

    public HybridizePlantsEvent(int id1, int id2, Vector3 position1, Vector3 position2, bool immediate = false) {
        this.id1 = id1;
        this.id2 = id2;
        this.position1 = position1;
        this.position2 = position2;
        this.immediate = immediate;
    }
}

public class PromptTextEvent : GameEvent {
    readonly public string text;
    readonly public float duration;

    public PromptTextEvent(string text, float duration = Mathf.Infinity) {
        this.text = text;
        this.duration = duration;
    }
}

public class HidePromptTextEvent : GameEvent {
    readonly public string text;

    public HidePromptTextEvent(string text = "") {
        this.text = (text == "") ? string.Empty : text;
    }
}

public class ScreenshotPromptEvent : GameEvent { }

public class RegisterAmbientSwayEvent : GameEvent {
    readonly public AmbientSway Sway;

    public RegisterAmbientSwayEvent(AmbientSway sway) {
        Sway = sway;
    }
}

public class UnregisterAmbientSwayEvent : GameEvent {
    readonly public AmbientSway Sway;
    public UnregisterAmbientSwayEvent(AmbientSway sway) {
        Sway = sway;
    }
}