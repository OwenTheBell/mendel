using UnityEngine;

public class PlayAudioOnStartEvent : MonoBehaviour {
	
	void OnEnable() {
		Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnDisable() {
		Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnDestroy() {
		Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnPlayerStartedEvent(PlayerStartedEvent e) {
		Debug.Log("yep, I heard you");
		GetComponent<AudioSource>().Play();
	}

}