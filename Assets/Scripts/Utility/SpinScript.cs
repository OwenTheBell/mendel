using UnityEngine;

public class SpinScript : MonoBehaviour {

	public float Speed;

	void FixedUpdate() {
		var euler = transform.rotation.eulerAngles;
		euler.y += 360 / Speed * Time.deltaTime;
		if (euler.y > 360) euler.y -= 360;
		transform.rotation = Quaternion.Euler(euler);
	}
	
}