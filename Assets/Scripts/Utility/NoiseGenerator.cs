using UnityEngine;

public class NoiseGenerator : MonoBehaviour {

	public Vector2 Dimensions;
	public float Scale;
	public Vector2 Scroll;

	private Texture2D mTexture;

	void Start() {
		mTexture = new Texture2D((int)Dimensions.x, (int)Dimensions.y);

		CalculateTexture(0);
	}

	void Update() {
		if (Scroll.magnitude > 0) {
			CalculateTexture(Time.timeSinceLevelLoad);
		}
	}

	void CalculateTexture(float time) {
		// Resources.UnloadUnusedAssets();

		// mTexture = new Texture2D((int)Dimensions.x, (int)Dimensions.y);

		var offset = Scroll * time;

		var width = (int)Dimensions.x;
		var height = (int)Dimensions.y;
		// var map = Noise.SimpleNoiseMap((int)Dimensions.x, (int)Dimensions.y, Scale);
		var map = Noise.GenerateNoiseMap(width, height, 1, Scale, offset);

		var pixels = new Color[width * height];
		for (var y = 0; y < height; y++) {
			for (var x = 0; x < width; x++) {
				pixels[y * width + x] = Color.Lerp(Color.white, Color.black, map[x, y]);
			}
		}

		mTexture.SetPixels(pixels);
		mTexture.Apply();

		GetComponent<MeshRenderer>().material.SetTexture("_NoiseMap", mTexture);
	}
	
}