﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(AudioSource))]
public class AudioFader : MonoBehaviour {

    public float Duration;
    public bool Playing { get; private set; }

    private List<AudioSource> _Sources;
    private List<float> _StartingVolumes;

    private void Awake() {
        _Sources = new List<AudioSource>(GetComponents<AudioSource>());
        _StartingVolumes = new List<float>(_Sources.Count);
        for (var i = 0; i < _Sources.Count; i++) {
            _StartingVolumes.Add(_Sources[i].volume);
            _Sources[i].volume = (_Sources[i].playOnAwake) ? _StartingVolumes[i] : 0f;
        }
    }

    public void Play() {
        StopAllCoroutines();
        _Sources.Act(s => {
            s.enabled = true;
            s.Play();
        });
        Playing = true;
        StartCoroutine(ChangeVolume(1f));
    }

    public void Stop() {
        StopAllCoroutines();
        Playing = false;
        StartCoroutine(ChangeVolume(-1f));
    }

    IEnumerator ChangeVolume(float direction) {
        var percent = _Sources[0].volume / _StartingVolumes[0];
        do {
            percent += Time.deltaTime / Duration * direction;
            var index = 0;
            _Sources.Act(s => s.volume = Easing.EaseInOut(percent, 2) * _StartingVolumes[index++]);
            yield return null;
        } while (percent > 0f && percent < 1f);

        if (percent <= 0f) {
            _Sources.Act(s => {
                s.Stop();
                s.enabled = false;
            });
        }
    }
}