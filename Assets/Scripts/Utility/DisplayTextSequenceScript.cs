﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DisplayTextSequenceScript : MonoBehaviour {

	public float StartDelay;

    public bool Done {
        get {
            var returnValue = true;
            foreach (var text in _TextDisplay) {
                if (!text.Visible) {
                    returnValue = false;
                    break;
                }
            }
            return returnValue;
        }
    }

	private List<UIText> _TextDisplay;

	void Start () {
		_TextDisplay = new List<UIText>(GetComponentsInChildren<UIText>());
		//foreach (var displayer in _TextDisplay) {
		//	displayer.InstantHide();
		//}
	}

    public void Display() {
		StartCoroutine(DisplayTest());
    }

	IEnumerator DisplayTest() {
		yield return new WaitForSeconds(StartDelay);
		var index = 0;

		do {
            _TextDisplay[index].Show();
            while (!_TextDisplay[index].Visible) {
                yield return null;
            }
            index++;
			yield return 0;
		} while (index < _TextDisplay.Count);

		yield return null;
	}
}
