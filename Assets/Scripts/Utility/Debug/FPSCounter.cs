﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FPSCounter : MonoBehaviour {

    public float UpdateGap;

    private float _TimeSinceUpdate;
    private int _Ticks;

	// Use this for initialization
	void Start () {
        _TimeSinceUpdate = 0f;
        _Ticks = 0;
	}
	
	// Update is called once per frame
	void Update () {
        _TimeSinceUpdate += Time.deltaTime;
        _Ticks++;
        
        if (_TimeSinceUpdate >= UpdateGap) {
            var message = "FPS: " + (int)(1 / (_TimeSinceUpdate / _Ticks));
            //var message = "FPS: " + Time.deltaTime;
            GetComponent<Text>().text = message;
            _TimeSinceUpdate = 0f;
            _Ticks = 0;
        }	
	}
}
