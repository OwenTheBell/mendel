﻿using UnityEngine;
using System.Collections;

public class FadeOut : MonoBehaviour {

	public float delay;
	public float duration;

	// Use this for initialization
	void Start () {
		StartCoroutine(Fade());
	}

	IEnumerator Fade() {
		yield return new WaitForSeconds(delay);

		float time = 0;
		while (time < duration) {
			time += Time.deltaTime;
			float percent = time/duration;
			float easeInOut = Easing.EaseInOut(percent, 2);
			foreach (CanvasRenderer renderer in GetComponentsInChildren<CanvasRenderer>()) {
				renderer.SetAlpha(1f - easeInOut);
			}
			yield return 0;
		}

		yield return null;

	}
}
