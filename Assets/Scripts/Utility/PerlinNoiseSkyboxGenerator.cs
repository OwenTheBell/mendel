﻿using UnityEngine;
using System.Collections;

public class PerlinNoiseSkyboxGenerator : MonoBehaviour {

	public int mapWidth;
	public int mapHeight;
	public float noiseScale;

	public Color BaseColor = Color.black;
	public Color CloudColor = Color.white;

	public bool autoUpdate;


	void Awake() {
		GenerateMap();
	}

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}


	public void GenerateMap() {
		float[,] noiseMap = Noise.GenerateNoiseMap(mapWidth*3, mapHeight*3, noiseScale);

		int width = noiseMap.GetLength (0);
		int height = noiseMap.GetLength (1);

		// Texture2D texture = new Texture2D(width/3, height/3);

		Texture2D[] textures = new Texture2D[9];

		Color[] colourMap = new Color[width * height];
		for (int index = 0; index < 9; index++) {
			Texture2D newTexture = new Texture2D(mapWidth, mapHeight);
			int startHeight = mapHeight * (index/3), endHeight = startHeight + mapHeight;
			int startWidth = mapWidth * (index/3 % 3), endWidth = startWidth + mapWidth;
			for (int y = startHeight; y < endHeight; y++) {
				for (int x = startWidth; x < endWidth; x++) {
					colourMap[(y - startHeight) * width + (x - startWidth)] = Color.Lerp(BaseColor, CloudColor, noiseMap[x, y]);
				}
			}
			newTexture.SetPixels(colourMap);
			newTexture.Apply();
		}

		Shader skyboxShader = Shader.Find("Skybox/6 Sided");
		Material skyboxMaterial = new Material(skyboxShader);
		skyboxMaterial.SetTexture("_FrontTex", textures[1]);
		skyboxMaterial.SetTexture("_BackTex", textures[7]);
		skyboxMaterial.SetTexture("_LeftTex", textures[3]);
		skyboxMaterial.SetTexture("_RightTex", textures[5]);
		skyboxMaterial.SetTexture("_UpTex", textures[4]);

		Skybox skybox = gameObject.AddComponent<Skybox>();
		skybox.material = skyboxMaterial;

		// MapDisplay display = FindObjectOfType<MapDisplay> ();
		// display.DrawNoiseMap (noiseMap);
	}
}
