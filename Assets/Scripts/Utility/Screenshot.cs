﻿using UnityEngine;

public class Screenshot : MonoBehaviour {

	public KeyCode ScreenshotKey;
	public int SuperSize;
    public AudioSource ShutterNoise;
	
	private int _AntiAliasing;
	private bool _CaptureNextFrame;
	private bool _ResetAliasingNextFrame;

	void Start () {
		_AntiAliasing = QualitySettings.antiAliasing;	
	}
	
	void Update () {
	}

     void LateUpdate() {
		if (Input.GetKeyDown(ScreenshotKey)) {
            if (ShutterNoise != null) {
                ShutterNoise.Play();
            }
			var camera = GetComponent<Camera>();
			var width = Screen.width * SuperSize;
			var height = Screen.height * SuperSize;
			var rt = new RenderTexture(width, height, 24);
			camera.targetTexture = rt;
			var screenShot = new Texture2D(width, height, TextureFormat.RGB24, false);
			camera.Render();
			RenderTexture.active = rt;
			screenShot.ReadPixels(new Rect(0, 0, width, height), 0, 0);
			camera.targetTexture = null;
			RenderTexture.active = null;
			Destroy(rt);
			byte[] bytes = screenShot.EncodeToPNG();
			var path = "";
			var number = 0;
			var date = System.DateTime.Now.ToString("_yyyy-MM-dd");
			do {
				var name = "Mendel" + date;
				name += ((number != 0) ? "_(" + number + ")" : "") + ".png";
                path = System.Environment.GetFolderPath(System.Environment.SpecialFolder.Desktop);
				path = System.IO.Path.Combine(path, name);
				number++;
			} while (System.IO.File.Exists(path));
			System.IO.File.WriteAllBytes(path, bytes);
		}
	}
}
