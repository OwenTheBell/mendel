﻿using UnityEngine;
using System.Collections;

public class DisableOnButtonPress : MonoBehaviour {

	public KeyCode button;
    public GameObject[] ObjectsToDisable;

	private bool activeState = true;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(button)) {
			activeState = !activeState;
            foreach (var gObject in ObjectsToDisable) {
                gObject.SetActive(activeState);
            }
        }
	}
}
