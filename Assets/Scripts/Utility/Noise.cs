using UnityEngine;

public static class Noise {

	public static float[,] GenerateNoiseMap(
												int mapWidth,
												int mapHeight,
												float scale) {
		return GenerateNoiseMap(mapWidth, mapHeight, 1, scale, Vector2.zero);
	}

	public static float[,] GenerateNoiseMap(
												int mapWidth,
												int mapHeight,
												float scale,
												int seed) {
		return GenerateNoiseMap(mapWidth, mapHeight, seed, scale, Vector2.zero);
	}

	public static float[,] SimpleNoiseMap(int mapWidth, int mapHeight, float scale) {
		var noiseMap = new float[mapWidth,mapHeight];

		if (scale <= 0) {
			scale = 0.0001f;
		}

		for (var y = 0; y < mapHeight; y++) {
			for (var x = 0; x < mapWidth; x++) {
				var sampleX = x / scale;
				var sampleY = y / scale;

				var perlinValue = Mathf.PerlinNoise(sampleX + 0.01f, sampleY + 0.01f);
				noiseMap[x, y] = perlinValue;
			}
		}

		return noiseMap;
	}	

	public static float[,] GenerateNoiseMap(
											int mapWidth,
											int mapHeight,
											int seed,
											float scale,
											Vector2 offset,
											int octaves = 1,
											float persistance = 1f,
											float lacunarity = 1f
										) {

		var noiseMap = new float[mapWidth,mapHeight];

		/* create array of seed values to control the generation */
		var random = new System.Random(seed);
		var offsets = new Vector2[octaves];
		for (int i = 0; i < octaves; i++) {
			var offsetX = random.Next(-10000, 100000) + offset.x;
			var offsetY = random.Next(-10000, 100000) + offset.y;
			offsets[i] = new Vector2(offsetX, offsetY);
		}

		if (scale <= 0) {
			scale = 0.0001f;
		}

		/* use a really really big number so that everything the noise
		* generates will be less than this */
		var minNoiseHeight = 0f;
		var maxNoiseHeight = 0f;

		var halfWidth = mapWidth/2;
		var halfHeight = mapHeight/2;

		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {

				var amplitude = 1f;
				var frequencey = 1f;
				var noiseHeight = 0f;

				for (int i = 0; i < octaves; i++) {

					var sampleX = (x-halfWidth) / scale * frequencey + offsets[i].x;
					var sampleY = (y-halfHeight) / scale * frequencey + offsets[i].y;

					/* adjust Perlin Noise to range -1 to 1 */
					var perlinValue = Mathf.PerlinNoise(sampleX, sampleY) * 2 - 1;
					noiseHeight += perlinValue * amplitude;

					amplitude *= persistance;
					frequencey *= lacunarity;
				}
				if (noiseHeight > maxNoiseHeight) {
					maxNoiseHeight = noiseHeight;
				}
				else if (noiseHeight < minNoiseHeight) {
					minNoiseHeight = noiseHeight;
				}
				noiseMap [x, y] = noiseHeight;
			}
		}

		/* normalize all noiseMap values to the range 0 -> 1 */
		for (int y = 0; y < mapHeight; y++) {
			for (int x = 0; x < mapWidth; x++) {
				noiseMap[x, y] = Mathf.InverseLerp(
												minNoiseHeight,
												maxNoiseHeight,
												noiseMap[x, y]
											);
			}
		}

		return noiseMap;
	}

    public static float [,] GenerateTilableNoise(int width, int height, int seed) {
        var noiseMap = new float[width, height];
        var subMap = GenerateNoiseMap((width+2) / 2, (height+2) / 2, seed);
        for (var x = 0; x < width; x++) {
            for (var y = 0; y < height; y++) {
                var x2 = (x < width / 2) ? x : width - x;
                var y2 = (y < height / 2) ? y : height - y;
                noiseMap[x, y] = subMap[x2, y2];
            }
        }
        return noiseMap;
    }

}