﻿using UnityEngine;

public class DisableGameObjectInBuild : MonoBehaviour {

    public void Awake() {
        if (!Application.isEditor) {
            gameObject.SetActive(false);
        }
    }
}
