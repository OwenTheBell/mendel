public class PlantIDIssuer {

	private int _currentID = 0;
	
	/***************
	* Constructors *
	***************/	

	private static PlantIDIssuer mIinstance = null;
	public static PlantIDIssuer instance {
		get {
			if (mIinstance == null) {
				mIinstance = new PlantIDIssuer();
			}

			return mIinstance;
		}
	}

	/*******************
	* Member Functions *
	*******************/	

	public int IssueID() {
		return _currentID++;
	}

}