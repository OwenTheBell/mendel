using UnityEngine;

public class PointAtTransform : MonoBehaviour {

	public Transform PointAt;
	public float RotationDelay;
    public bool Invert;

	private Quaternion _LastRotation;
	private float _RotationTime = 0f;
	private Transform LastTarget;

	public AxisBools FreezeRotationOn;
	private Vector3 _SavedRotation;

	void Start() {
		_LastRotation = transform.rotation;	
		_SavedRotation = transform.rotation.eulerAngles;
	}

	void Update() {
		if (PointAt == null) {
			return;
		}

		// var lookRotation = Quaternion.LookRotation(PointAt.position - transform.position);
		// if (lookRotation != _LastRotation) {
		// 	_RotationTime = 0f;
		// 	_LastRotation = lookRotation;
		// }
		// _RotationTime += Time.deltaTime;
		// /* if I have a new target, don't bother rotation into position */
		// if (LastTarget != PointAt) {
		// 	LastTarget = PointAt;
		// 	_RotationTime = RotationDelay;
		// }
		// var t = _RotationTime/RotationDelay;
		// if ( t != t ) t = 1f;
		// transform.rotation = Quaternion.Lerp(transform.rotation, lookRotation, t);

		// var prerotation = transform.rotation.eulerAngles;
		_SavedRotation = transform.rotation.eulerAngles;

		transform.LookAt(PointAt);

		var rotation = transform.rotation.eulerAngles;
		if (FreezeRotationOn.X) {
			rotation.x = _SavedRotation.x;
		}
		if (FreezeRotationOn.Y) {
			rotation.y = _SavedRotation.y;
		}
		if (FreezeRotationOn.Z) {
			rotation.z = _SavedRotation.z;
		}
		transform.rotation = Quaternion.Euler(rotation);

        if (Invert) {
            transform.forward = transform.forward * -1f;
        }
	}
}

[System.Serializable]
public struct AxisBools {
	public bool X;
	public bool Y;
	public bool Z;
}
