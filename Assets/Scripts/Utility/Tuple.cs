using System.Xml.Serialization;

public static class Tuple {
	public static Tuple<T1, T2> New<T1, T2>(T1 first, T2 second) {
		return new Tuple<T1, T2>(first, second);
	}
}

public struct Tuple<T1, T2> {
    [XmlAttribute("First")]
	public T1 First;
    [XmlAttribute("Second")]
	public T2 Second;

	internal Tuple(T1 first, T2 second) {
		First = first;
		Second = second;
	}
}