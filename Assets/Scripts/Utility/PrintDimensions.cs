using UnityEngine;

public class PrintDimensions : MonoBehaviour {
	
	public void OnDrawGizmo() {
		if (GetComponent<Renderer>() != null) {
			Debug.Log(GetComponent<Renderer>().bounds.size);
		}
	}

	public void Start() {
		if (GetComponent<Renderer>() != null) {
			Debug.Log(gameObject.name + " " + GetComponent<Renderer>().bounds.size);
		}
	}

}