﻿using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(MeshFilter))]
public class CircularMeshGenerator : MonoBehaviour {

    public int Spokes;
    public float Rings;
    public float Radius;

	// Use this for initialization
	void Start () {
        var mesh = new Mesh();

        var spacing = Radius / Rings;

        var vertices = new List<Vector3>();
        var colors = new List<Color>();
        var uvs = new List<Vector2>();
        vertices.Add(Vector3.zero);
        colors.Add(Color.black);
        uvs.Add(new Vector2(0, 0));

        for (var radius = spacing; radius < Radius; radius += spacing) {
            for (var radians = 0f; radians < Mathf.PI*2; radians += Mathf.PI * 2/Spokes) {
                var x = radius * Mathf.Cos(radians);
                var z = radius * Mathf.Sin(radians);
                vertices.Add(new Vector3(x, 0f, z));
                colors.Add(Color.black);
                uvs.Add(new Vector2(radius / Radius, 0));
            }
        }

        var triangles = new List<int>();
        for (var i = 1; i < vertices.Count; i++) {
            int[] indices;
            if (i < Spokes + 1) {
                var j = (i < Spokes) ? i + 1 : 1;
                indices = new int[3] { i, 0, j };
            }
            else {
                var j = (i % Spokes != 0) ? i + 1 : i - (Spokes - 1);
                var i2 = i - Spokes;
                var j2 = j - Spokes;
                indices = new int[6] { i, j2, j, j2, i, i2 };
            }
            triangles.AddRange(indices);
        }

        mesh.SetVertices(vertices);
        mesh.SetTriangles(triangles, 0);
        mesh.SetColors(colors);
        mesh.SetUVs(0, uvs);

        GetComponent<MeshFilter>().sharedMesh = mesh;

        if (GetComponent<MeshCollider>() != null) {
            GetComponent<MeshCollider>().sharedMesh = mesh;
        }
	}
}