using UnityEngine;
using System.Collections.Generic;

public class MeshDistortions {

	private static MeshDistortions _instance;
	private static MeshDistortions Instance {
		get
		{
			if (_instance == null) {
				_instance = new MeshDistortions();
			}
			return _instance;
		}
	}

	public static void RandomizeVertices(ref Mesh mesh, Vector3 variance) {
		Vector3[] vertices = mesh.vertices;
		Dictionary<Vector3, Vector3> placedVertices =
											new Dictionary<Vector3, Vector3>();
		// foreach (Vector3 vector in vertices) {
		for (int i = 0; i < vertices.Length; i++) {
			if (placedVertices.ContainsKey(vertices[i])) {
				vertices[i] = placedVertices[vertices[i]];
				continue;
			}
			Vector3 key = vertices[i];
			vertices[i] += new Vector3(
									Random.Range(-variance.x, variance.x),
									Random.Range(-variance.y, variance.y),
									Random.Range(-variance.z, variance.z)
								);
			placedVertices.Add(key, vertices[i]);
		}
		mesh.vertices = vertices;

		mesh.RecalculateBounds();
		mesh.RecalculateNormals();
	}
}