﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TwoToneSkybox : MonoBehaviour {

    public float CycleTime = 0f;
    private float _CurrentTime;

	// Use this for initialization
	void Start () {
        Mesh mesh = GetComponent<MeshFilter>().sharedMesh;
        var uvs = new Vector2[mesh.vertexCount];
        var maxDistance = 0f;
        var peak = Vector3.zero;
        foreach (var vertex in mesh.vertices) {
            var point = transform.TransformPoint(vertex);
            if (point.y > maxDistance) {
                maxDistance = point.y;
                peak = vertex;
            }
        }
        maxDistance = 0f;
        foreach (var vertex in mesh.vertices) {
            var distance = Vector3.Distance(vertex, peak);
            if (distance > maxDistance) {
                maxDistance = distance;
            }
        }
        for (var i = 0; i < mesh.vertexCount; i++) {
            if (mesh.vertices[i] == peak) {
                uvs[i] = new Vector2(0.5f, 0.5f);
                continue;
            }
            var distance = Vector3.Distance(peak, mesh.vertices[i]) / maxDistance * 0.5f;
            var angle = Mathf.Atan2(mesh.vertices[i].y - peak.y, mesh.vertices[i].x - peak.x);
            uvs[i] = new Vector2(Mathf.Cos(angle) * distance + 0.5f, Mathf.Sin(angle) * distance + 0.5f);
        }
        mesh.uv = uvs;

        GetComponent<MeshFilter>().sharedMesh = mesh;
    }
}