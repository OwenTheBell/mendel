using UnityEngine;
using System.Collections.Generic;

public class PlaneMeshGenerator : MonoBehaviour {

	public Vector2 Dimensions;
	public Vector2 Divisions;

	private Mesh mMesh;
	
	void Awake() {

		var vertices = new Vector3[(int)(Divisions.x+1) * (int)(Divisions.y+1)];
		var uv = new Vector2[vertices.Length];
		var triangles = new List<int>();

		var width = Dimensions.x/Divisions.x;
		var height = Dimensions.y/Divisions.y;

		var topLeft = new Vector3(-Dimensions.x/2, 0f, -Dimensions.y/2);

		var vertIndex = 0;

		for (var y = 0; y <= Divisions.y; y++) {
			for (var x = 0; x <= Divisions.x; x++) {
				vertices[vertIndex] = new Vector3(
												topLeft.x + x * width,
												0f,
												topLeft.z + y * height
											);
				uv[vertIndex] = new Vector2(x/Divisions.x, y/Divisions.y);
				if (x < Divisions.x && y < Divisions.y) {
					int i = vertIndex;
					triangles.AddRange(new int[]{i, 
												i + (int)Divisions.x + 1,
												i + (int)Divisions.x + 2,
												i,
												i + (int)Divisions.x + 2,
												i + 1});
				}
				vertIndex++;
			}
		}

		mMesh = new Mesh();
        mMesh.vertices = vertices;
        mMesh.triangles = triangles.ToArray();
        mMesh.uv = uv;
        mMesh.RecalculateNormals();
        mMesh.RecalculateBounds();
	}

	void Start() {
		GetComponent<MeshFilter>().sharedMesh = mMesh;
		if (GetComponent<MeshCollider>() != null) {
			GetComponent<MeshCollider>().sharedMesh = mMesh;
		}
	}


	void OnValidate() {
		Divisions.x = (int)Divisions.x;
		Divisions.y = (int)Divisions.y;
	}

}