using UnityEngine;
using System.Xml.Serialization;

[System.Serializable]
public class HSVColor {

	/*******************
	* Member Variables *
	*******************/
	[XmlAttribute("h")]
	public float h;
	[XmlAttribute("s")]
	public float s;
	[XmlAttribute("v")]
	public float v;

	public UnityEngine.Color RGBColor {
		get {
			return Color.HSVToRGB(h, s, v);
		}
	}

	/***************
	* Constructors *
	***************/

	/* parameterless constructor for serialization */
	private HSVColor() {}

	public HSVColor(float[] values) {
		if (values.Length < 3) {
			h = 0f;
			s = 0f;
			v = 0f;
		}
		else {
			h = values[0];
			s = values[1];
			v = values[2];
		}
	}

	public HSVColor(float h, float s, float v) {
		this.h = ClampHSVFloat(h);
		this.s = ClampHSVFloat(s);
		this.v = ClampHSVFloat(v);
	}

	public HSVColor(UnityEngine.Color color) {
		Color.RGBToHSV(color, out h, out s, out v);
	}

	/*******************
	* Member Functions *
	*******************/

	/* returns an HSVColor with hsv values equal to the difference between
	• the values of the instance and arguments hsv values */
	public HSVColor Difference(HSVColor otherColor) {
		var h1 = this.h;
		var h2 = otherColor.h;
		if (UnityEngine.Mathf.Abs(h1 - h2) > 0.5f) {
			if (h1 < h2) {
				h1 += 1;
			} else {
				h2 += 1;
			}
		}
		var hDiff = Mathf.Abs(h1 - h2);
		var sDiff = Mathf.Abs(this.s - otherColor.s);
		var vDiff = Mathf.Abs(this.v - otherColor.v);
		return new HSVColor(hDiff, sDiff, vDiff);
	}

	public HSVColor MixWith(HSVColor otherColor) {
		float new_h = AverageHSVFloats(h, otherColor.h);
		float new_s = (s + otherColor.s) / 2;
		float new_v = (v + otherColor.v) / 2;
		return new HSVColor(new_h, new_s, new_v);
	}

	private float AverageHSVFloats(float x, float y) {
		x = ClampHSVFloat(x);
		y = ClampHSVFloat(y);
		if (UnityEngine.Mathf.Abs(x - y) > 0.5f) {
			if (x < y) {
				x += 1;
			} else {
				y += 1;
			}
		}
		return (x + y) / 2f % 1f;
	}

	private float ClampHSVFloat(float x) {
		return (x > 1f || x < 0f) ? x % 1f : x;
	}

	public static HSVColor Lerp(HSVColor color1, HSVColor color2, float t) {
		t = Mathf.Clamp(t, 0f, 1f);
		float h1 = color1.h, h2 = color2.h;
		if (Mathf.Abs(h1 - h2) > 0.5f) {
			if (h1 < h2) {
				h1 += 1;
			}
			else {
				h2 += 1;
			}
		}
		float h = (h2 - h1) * t + h1;
		float s = (color2.s - color1.s) * t + color1.s;
		float v = (color2.v - color1.v) * t + color1.v;
		return new HSVColor(h, s, v);
	}

	public static float InverseLerp(HSVColor start, HSVColor end, HSVColor test) {
		float sH = start.h, eH = end.h, tH = test.h;

		/* adjust all the H values so they are in order least to greatest */
		if (Mathf.Abs(sH - eH) > 0.5f) {
			if (sH < eH) {
				/* if tH is in this position, then test is not between 
				* start & end*/
				if (tH > sH && tH < eH) {
					return -1f;
				}
				sH++;
				if (tH < eH) {
					tH++;
				}
			}
			else { 
				if (tH > eH && tH < sH) {
					return -1f;
				}
				eH++;
				if (tH < sH) {
					tH ++;
				}
			}
		}

		/* compare the three percent differences by turning them into ints to
		* avoid errors caused by floating point math. Adjust the precision
		* variable to get finer results if needed. */
		var precision = 100000;
		var percentH = (tH - sH) / (eH - sH) * precision;
		var percentS = (test.s - start.s) / (end.s - start.s) * precision;
		var percentV = (test.v - start.v) / (end.v - start.v) * precision;

		/* all percent changes must be equal for test to be valid */
		if ((int)percentH == (int)percentS && (int)percentH == (int)percentV) {
			return percentH / precision;
		}

		return -1f;
	}

	public string ToString() {
		return "HSVColor: h: " + h + " s: " + s + " v: " + v;
	}

	static public float PercentDifference(HSVColor color1, HSVColor color2) {
		var h = CompareHFloats(color1.h, color2.h);
		var s = 1f - Mathf.Abs(color1.s - color2.s);
		var v = 1f - Mathf.Abs(color1.v - color2.v);
		return (h + s + v)/3f;
	}

	/* this only works for float values in the range 0->1 as this is only for
	* the use of comparing h, s, & v values */
	static private float CompareHFloats(float h, float h2) {
		var difference = h - h2;
		if (Mathf.Abs(difference) > 0.5) {
			if (h > h2) {
				h2 += 1f;
			}
			else {
				h += 1f;
			}
		}

		difference = 1f - (Mathf.Abs(h - h2) / 0.5f);
		return difference;
	}
}
