using UnityEngine;

public class GeometryHelper {

	public static bool GetLineSegmentIntersection(
													Vector2 p1,
													Vector2 p2,
													Vector2 q1,
													Vector2 q2,
													out Vector2 intersect) {
		
		var denominator = (q2.y-q1.y)*(p2.x-p1.x) - (q2.x-q1.x)*(p2.y-p1.y);

		/* the lines are parallel if this happens */
		if (denominator == 0f) {
			intersect = Vector2.zero;
			return false;
		}

		var a = ((q2.x-q1.x)*(p1.y-q1.y) - (q2.y-q1.y)*(p1.x-q1.x)) / denominator;
		var b = ((p2.x-p1.x)*(p1.y-q1.y) - (p2.y-p1.y)*(p1.x-q1.x)) / denominator;

		/* if both a & b are withing range 0..1, then the lines' intersection is
		* within the provided segments */
		if (0 <= a && a <= 1 && 0 <= b && b <= 1) {
			intersect = new Vector2(
										p1.x + a * (p2.x - p1.x),
										p1.y + b * (p2.y - p1.y)
									);
			return true;
		}

		intersect = Vector2.zero;
		return false;
	}

	public static bool DoLineSegementsIntersect(
													Vector2 p1,
													Vector2 p2,
													Vector2 q1,
													Vector2 q2) {
		var output = Vector2.zero;
		return GetLineSegmentIntersection(p1, p2, q1, q2, out output);
	}

	public static bool DoesPolygonContainPoints(Vector2[] polygon, Vector2 point) {

		/* find the max & min points of the polygon */
		var min = Vector2.zero;
		var max = Vector2.zero;

		foreach (var vertex in polygon) {
			if (vertex.x < min.x) {
				min.x = vertex.x;
			}
			if (vertex.y < min.y) {
				min.y = vertex.y;
			}
			if (vertex.x > max.x) {
				max.x = vertex.x;
			}
			if (vertex.y > max.y) {
				max.y = vertex.y;
			}
		}

		var lowerPoint = new Vector3(point.x, 0f, min.y);

		var intersections = 0;
		for (var i = 0; i < polygon.Length; i++) {
			var j = (i >= polygon.Length - 1) ? 0 : i + 1;
			var p1 = new Vector2(point.x, point.y);
			var p2 = new Vector2(lowerPoint.x, lowerPoint.z);
			var q1 = new Vector2(polygon[i].x, polygon[i].y);
			var q2 = new Vector2(polygon[j].x, polygon[j].y);
			if (GeometryHelper.DoLineSegementsIntersect(p1, p2, q1, q2)) {
				intersections++;
			}
		}

		return (intersections != 0 && intersections % 2 == 1);
	}

	public static Vector2[] ConvertVector3XZToVector2(Vector3[] array) {
		var output = new Vector2[array.Length];
		for (var i = 0; i < output.Length; i++) {
			output[i] = new Vector2(array[i].x, array[i].z);
		}
		return output;
	}
}