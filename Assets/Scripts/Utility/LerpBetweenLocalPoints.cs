﻿using UnityEngine;
using System.Collections;

public class LerpBetweenLocalPoints : MonoBehaviour {

    public Vector3 Target;
    public float Duration;

    private Vector3 _Starting;

    private void Awake() {
        _Starting = transform.localPosition;
    }

    public void Go() {
        StopAllCoroutines();
        StartCoroutine(Move(Target, _Starting));
    }

    public void Return() {
        StartCoroutine(Move(_Starting, Target));
    }

    IEnumerator Move(Vector3 target, Vector3 origin) {
        var distance = Vector3.Distance(target, origin);
        var currentDistance = Vector3.Distance(transform.localPosition, origin);
        var elapsed = currentDistance / distance * Duration;
        while (elapsed < Duration) {
            elapsed += Time.deltaTime;
            var easeInOut = Easing.EaseInOut(elapsed / Duration, 2);
            transform.localPosition = Vector3.Lerp(origin, target, easeInOut);
            yield return null;
        }
    }
}
