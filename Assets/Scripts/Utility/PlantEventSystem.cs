using UnityEngine;
using System.Collections.Generic;

public class PlantEventSystem : MonoBehaviour {

	public delegate void EventDelegate<T> (T e) where T : GameEvent;

	private Dictionary<System.Type, System.Delegate> delegates =
									new Dictionary<System.Type, System.Delegate>();

	/*******************
	* Member Functions *
	*******************/
	public void AddListener<T>(EventDelegate<T> del) where T : GameEvent {
		if (delegates.ContainsKey(typeof(T))) {
			System.Delegate tempDel = delegates[typeof(T)];
			delegates[typeof(T)] = System.Delegate.Combine(tempDel, del);
		} else {
			delegates[typeof(T)] = del;
		}
	}

	public void RemoveListener<T>(EventDelegate<T> del) where T : GameEvent {
		if (delegates.ContainsKey(typeof(T))) {
			var currentDel = System.Delegate.Remove(delegates[typeof(T)], del);
			if (currentDel == null) {
				delegates.Remove(typeof(T));
			} else {
				delegates[typeof(T)] = currentDel;
			}
		}
	}

	public void Raise (GameEvent e) {
		if (e == null) {
			Debug.Log("Invalid event arugment: " + e.GetType().ToString());
			return;
		}

		if (delegates.ContainsKey(e.GetType())) {
			delegates[e.GetType()].DynamicInvoke(e);
		}
	}
}

public class PlaceFlowerEvent : GameEvent {
	public readonly Vector3 position;
    public readonly float height;
	public readonly Quaternion rotation;
    public readonly float time;

	public PlaceFlowerEvent(Vector3 position, Quaternion rotation, float height, float time = 0f) {
		this.position = position;
		this.rotation = rotation;
        this.height = height;
        this.time = time;
	}
}

public class DoneGrowingEvent : GameEvent { }

public class SwitchToAmbientEvent : GameEvent { }

public class ChangeFlowerMaterialEvent : GameEvent {
    public readonly Material material;

    public ChangeFlowerMaterialEvent(Material material) {
        this.material = material;
    }
}

public class DoneBloomingEvent : GameEvent { }