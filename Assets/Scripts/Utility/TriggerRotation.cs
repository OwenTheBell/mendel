﻿using UnityEngine;
using System.Collections;

public class TriggerRotation : MonoBehaviour {

    public float Speed;

    private float _SpeedPercent;

    private void Awake() {
        enabled = false;
    }

    // Update is called once per frame
    void LateUpdate() {
        var amount = Speed  * _SpeedPercent * Time.deltaTime;
        var rotation = transform.rotation;
        //rotation *= Quaternion.AngleAxis(amount, transform.InverseTransformDirection(Vector3.up));
        rotation *= Quaternion.AngleAxis(amount, Vector3.up);
        transform.rotation = rotation;
    }

    public void StartRotating() {
        enabled = true;
        StartCoroutine(BeginRotation());
    }

    IEnumerator BeginRotation() {
        var time = _SpeedPercent * 0.5f;
        while (time < 0.5f) {
            time += Time.deltaTime;
            _SpeedPercent = Easing.EaseInOut(time / 0.5f, 2);
            yield return null;
        }
    }
}