﻿using UnityEngine;
using System.Collections;

public class WaitForPlayerStarted : MonoBehaviour {

    private void Awake() {
        Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStarted);
        gameObject.SetActive(false);
    }

    private void OnDestroy() {
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStarted);
    }

    void OnPlayerStarted(PlayerStartedEvent e) {
        gameObject.SetActive(true);
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStarted);
    }
}