﻿using UnityEngine;

public class OrbitTransform : MonoBehaviour {

    public float Height;
    public float Radius;
    public Transform Target;
    public float DegreesPerSecond;

    // Use this for initialization
    void Start() {
        transform.position = new Vector3(0f, Height, Radius);
    }

    // Update is called once per frame
    void Update() {
        transform.RotateAround(Target.position, Vector3.up, DegreesPerSecond * Time.deltaTime);
        transform.LookAt(Target);
        var position = transform.position;
        position.y = Height;
        transform.position = position;
    }

    private void OnEnable() {
        Services.PlayerTransform = transform;
    }
}
