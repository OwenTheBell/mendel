﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TranslationTester : MonoBehaviour {

    [Range(0, 1)]
    public float Intensity;
    public float Speed;
    public float Count;
    public float Spacing;

    private List<Transform> children = new List<Transform>();

	// Use this for initialization
	void Start () {
	    for (var i = 0; i < Count; i++) {
            var child = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            child.transform.position = Vector3.right * Spacing * i;
            child.transform.parent = transform;
            children.Add(child.transform);
        }	
	}
	
	// Update is called once per frame
	void Update () {
        var cos = Mathf.Cos(Time.time * Speed) * Intensity;
        var translate = Vector3.forward * cos;
        foreach (var child in children) {
            child.position += translate;
        }
	}
}