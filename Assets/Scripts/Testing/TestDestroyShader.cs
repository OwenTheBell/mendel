﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestDestroyShader : MonoBehaviour {

    public KeyCode TriggerKey;
    public Material DestroyMaterial;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
        if (Input.GetKeyDown(TriggerKey)) {
            var renderer = GetComponent<MeshRenderer>();
            renderer.material = DestroyMaterial;
            renderer.material.SetFloat("_StartTime", Time.time);
        }		
	}
}
