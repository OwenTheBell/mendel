﻿using UnityEngine;
using TMPro;

public class PlantCounter : MonoBehaviour {

    private TMP_Text _Text;
    private int _Count;

    private void Awake() {
        _Text = GetComponent<TMP_Text>();
    }

    private void OnEnable() {
        Events.instance.AddListener<NewPlantEvent>(OnNewPlantEvent); 
    }

    private void OnDisable() {
        Events.instance.RemoveListener<NewPlantEvent>(OnNewPlantEvent); 
    }

    void OnNewPlantEvent(NewPlantEvent e) {
        _Count++;
        _Text.text = "Plant count: " + _Count;
    }
}