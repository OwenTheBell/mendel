﻿using System;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class FPSDisplay : MonoBehaviour {

    public float Interval;

    private float _TimeToUpdate;
    private TMP_Text _Text;

    private List<float> _IntervalFrames;
    private int _IntervalIndex;
    private float _Average;
    private float _Current;
    private float _Min;
    private int _TotalFrames;

    private void Awake() {
        _TimeToUpdate = Interval;
        _Text = GetComponent<TMP_Text>();
        _IntervalFrames = new List<float>();
        _Min = Mathf.Infinity;
    }

    private void Start() {
        _Text.text = "";
    }

    private void Update() {
        var fps = 1 / Time.deltaTime;

        if (_IntervalIndex < _IntervalFrames.Count) {
            _IntervalFrames[_IntervalIndex] = fps;
        }
        else {
            _IntervalFrames.Add(fps);
        }
        _IntervalIndex++;

        _TimeToUpdate -= Time.deltaTime;
        if (_TimeToUpdate < 0f) {
            _Current = 0f;
            for (var i = 0; i < _IntervalIndex; i++) {
                _Current += _IntervalFrames[i] / _IntervalIndex;
            }
            _IntervalIndex = 0;
            _TimeToUpdate = Interval;
        }

        if (fps < _Min) {
            _Min = fps;
        }

        _TotalFrames++;
        _Average = _Average * (_TotalFrames - 1) / _TotalFrames + (fps * 1 / _TotalFrames);

        _Text.text = "Average: " + Math.Round(_Average, 1) +
                        " Current: " + Math.Round(_Current, 1) +
                        " Min: " + Math.Round(_Min, 1);

        if (Input.GetKeyUp(KeyCode.Tab)) {
            _Average = 0f;
            _TotalFrames = 0;
            _Current = 0f;
            _Min = Mathf.Infinity;
        }
    }
}