﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;

public class TimeComparison : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var date = DateTime.Now;
        var plusDays = date.AddDays(40.0);
        var difference = plusDays.Subtract(date);
        Debug.Log("hour difference: " + (int)difference.TotalHours);
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
