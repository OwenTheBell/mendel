﻿using UnityEngine;
using System.Collections.Generic;

public class AutomatedHybridizing : MonoBehaviour {

    public float Interval;
    public int PerInterval;
    public int Maximum;
    public int Increment;
    public KeyCode Key;

    private int _ToSpawnCount;
    private float _TimeToSpawn;
    private List<PlantInfo> _PlantInfos;

    private void Awake() {
        _ToSpawnCount = 0;
        _TimeToSpawn = Interval;
        _PlantInfos = new List<PlantInfo>();
        _ToSpawnCount = Maximum;
    }

    void Update() {
        if (Input.GetKeyUp(Key)) {
            if (_ToSpawnCount < 0) {
                _ToSpawnCount = 0;
            }
            _ToSpawnCount += Increment;
        }
        if (_ToSpawnCount > 0) {
            _TimeToSpawn -= Time.deltaTime;
            if (_TimeToSpawn <= 0f) {
                for (var i = 0; i < PerInterval && _ToSpawnCount > 0; i++) {
                    _ToSpawnCount--;
                    var info1 = _PlantInfos[Random.Range(0, _PlantInfos.Count)];
                    var info2 = _PlantInfos[Random.Range(0, _PlantInfos.Count)];

                    Events.instance.Raise(new HybridizePlantsEvent(
                        info1.Identity,
                        info2.Identity,
                        info1.transform.position,
                        info2.transform.position
                    ));
                }
                _TimeToSpawn = Interval;
            }
        }
    }

    private void OnEnable() {
        Events.instance.AddListener<NewPlantEvent>(OnNewPlantEvent);
    }

    private void OnDisable() {
        Events.instance.RemoveListener<NewPlantEvent>(OnNewPlantEvent);
    }

    private void OnNewPlantEvent(NewPlantEvent e) {
        var plantInfo = e.gameObject.GetComponent<PlantInfo>();
        if (plantInfo != null) {
            _ToSpawnCount--;
            _PlantInfos.Add(plantInfo);
        }
    }
}