﻿using UnityEngine;
using UnityEngine.UI;

public class FamilyTreeButton : MonoBehaviour {

    public ScrollRect Rect;
    public GameObject Plant;
    public int Identity {
        get {
            return _PlantIdentity;
        }
        set {
            if (value == -1) {
                _PlantIdentity = -1;
                _Text.Text = string.Empty;
            }
            else if (_PlantIdentity != value) {
                var name = Services.SpeciesCatalog.GetNameForIdentity(value, true);
                _Text.Text = (name != string.Empty) ? name : PROPERTIES.NoNameMessage;
                NamedSpecies = (name != string.Empty);
                _PlantIdentity = value;
            }
        }
    }
    public bool NamedSpecies { get; private set; }

    private UIText _Text;
    private int _PlantIdentity;

    private void Awake() {
        _Text = GetComponentInChildren<UIText>();
    }

    public void Show() {
        _Text.Show();
    }

    public void Hide() {
        _Text.Hide();
    }

    public void OnClick() {

    }

    public void SetPlant(GameObject plant, int identity) {
        Plant = plant;
        Identity = identity;
    }
}