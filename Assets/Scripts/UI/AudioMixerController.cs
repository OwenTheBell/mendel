﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using System;
using System.Linq;

public enum AudioBanks {
    Player,
    Music,
    Ambient,
    Interface,
}

public class AudioMixerController : MonoBehaviour {

    private const string SNAPSHOTS_KEY = "Active Mixer Snapshots";

    public AudioMixer Mixer;

    public bool DoNotLoadFromPrefs;

    private float _SavedMusic;
    private float _SavedPlayer;
    private float _SavedInterface;
    private float _SavedAmbient;
    private float _SavedPlant;
    private float _SavedDrilling;

    private bool _Music = true;
    public bool Music {
        get {
            return _Music;
        }
        set {
            _Music = value;
            if (_Music) {
                Mixer.SetFloat("Music Volume", _SavedMusic);
            }
            else {
                Mixer.SetFloat("Music Volume", -100f);
            }
        }
    }

    private bool _Player = true;
    public bool Player {
        get {
            return _Player;
        }
        set {
            _Player = value;
            if (_Player) {
                Mixer.SetFloat("Player Volume", _SavedPlayer);
                Mixer.SetFloat("Drilling Volume", _SavedDrilling);
            }
            else {
                Mixer.SetFloat("Player Volume", -100f);
                Mixer.SetFloat("Drilling Volume", -100f);
            }
        }
    }

    private bool _Interface = true;
    public bool Interface {
        get {
            return _Interface;
        }
        set {
            _Interface = value;
            if (_Interface) {
                Mixer.SetFloat("Interface Volume", _SavedInterface);
            }
            else {
                Mixer.SetFloat("Interface Volume", -100f);
            }
        }
    }

    private bool _Ambient = true;
    public bool Ambient {
        get {
            return _Ambient;
        }
        set {
            _Ambient = value;
            if (_Ambient) {
                Mixer.SetFloat("Ambient Volume", _SavedAmbient);
                Mixer.SetFloat("Plants Volume", _SavedPlant);
            }
            else {
                Mixer.SetFloat("Ambient Volume", -100f);
                Mixer.SetFloat("Plants Volume", -100f);
            }
        }
    }

    private void Awake() {
        Mixer.GetFloat("Music Volume", out _SavedMusic);
        Mixer.GetFloat("Player Volume", out _SavedPlayer);
        Mixer.GetFloat("Interface Volume", out _SavedInterface);
        Mixer.GetFloat("Ambient Volume", out _SavedAmbient);
        Mixer.GetFloat("Plants Volume", out _SavedPlant);
        Mixer.GetFloat("Drilling Volume", out _SavedDrilling);

        if (DoNotLoadFromPrefs && Application.isEditor) {
            return;
        }

        var bankValues = Enum.GetValues(typeof(AudioBanks)).Cast<AudioBanks>().ToArray();
        if (PlayerPrefs.HasKey(SNAPSHOTS_KEY)) {
            var snapsAcive = PlayerPrefs.GetInt(SNAPSHOTS_KEY);
            for (var i = 0; i < bankValues.Length; i++) {
                if ((snapsAcive & (1 << i)) > 0) {
                    ToggleBank(bankValues[i]);
                }
            }
        }
    }

    private void Start() {
        Ambient = Ambient;
        Player = Player;
        Music = Music;
        Interface = Interface;
    }

    public bool GetBank(AudioBanks bank) {
        switch (bank) {
            case AudioBanks.Player:
                return Player;
            case AudioBanks.Ambient:
                return Ambient;
            case AudioBanks.Interface:
                return Interface;
            case AudioBanks.Music:
                return Music;
            default:
                return false;
        }
    }

    public void ToggleBank(AudioBanks bank) {
        switch (bank) {
            case AudioBanks.Player:
                Player = !Player;
                return;
            case AudioBanks.Ambient:
                Ambient = !Ambient;
                return;
            case AudioBanks.Interface:
                Interface = !Interface;
                return;
            case AudioBanks.Music:
                Music = !Music;
                return;
        }
    }

    private void OnDisable() {
        var disabledBanks = 0;
        var bankValues = Enum.GetValues(typeof(AudioBanks)).Cast<AudioBanks>().ToArray();
        for (var i = 0; i < bankValues.Length; i++) {
            if (!GetBank(bankValues[i])) {
                disabledBanks += (int)Mathf.Pow(2, i);
            }
        }
        PlayerPrefs.SetInt(SNAPSHOTS_KEY, disabledBanks);
        PlayerPrefs.Save();
    }
}