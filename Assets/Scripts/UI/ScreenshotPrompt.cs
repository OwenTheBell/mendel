﻿using UnityEngine;

public class ScreenshotPrompt : MonoBehaviour {

    public string Message;
    [Range(0, 1)]
    public float Frequency;
    public float Duration;

    private int _NextPrompt = 0;

    private void OnEnable() {
        Events.instance.AddListener<ScreenshotPromptEvent>(OnScreenshotPromptEvent);
    }

    private void OnDisable() {
        Events.instance.RemoveListener<ScreenshotPromptEvent>(OnScreenshotPromptEvent);
    }

    void OnScreenshotPromptEvent(ScreenshotPromptEvent e) {
        // skip the first 5 seconds to avoid displaying this on initial generation
        if (Time.time < 5f) return;
        if (_NextPrompt <= 0) {
            Events.instance.Raise(new PromptTextEvent(Message, Duration));
            _NextPrompt = (int)(10 * (1f - Frequency)) + 2;
        }
    }
}
