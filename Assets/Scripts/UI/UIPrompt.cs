using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

public class UIPrompt : MonoBehaviour, IUIPrompt {

	private Sprite mSprite;
	public Sprite Sprite {
		get {
			return mSprite;
		}
		set {
			mSprite = value;
			_Image.sprite = value;
			if (!mIsHidden) {
				Appear();
			}
		}
	}

	private string mMessage;
	public string Message {
		get {
			return mMessage;
		}
		set {
			mMessage = value;
			_Text.text = value;
			if (!mIsHidden) {
				Appear();
			}
		}
	}

	public bool Visible {
		get {
			return HasMessage && !mIsHidden;
		}
	}

	public bool HasMessage {
		get {
			return (Sprite != null && Message != System.String.Empty);
		}
	}

	private bool mIsHidden;

	[SerializeField]
	protected Image _Image;
	[SerializeField]
	protected Text _Text;

	protected virtual void Awake() {
		mIsHidden = true;

		Assert.IsNotNull(_Image, gameObject.name + " does not have an Image object");
		Assert.IsNotNull(_Text, gameObject.name + " does not have a Text object");

		mSprite = _Image.sprite;
		// mSprite = null;
		// mMessage = System.String.Empty;
		mMessage = _Text.text;
	}

	protected virtual void Start() {
		Hide();
	}

	public virtual void Clear() {
		Sprite = null;
		Message = System.String.Empty;
		Hide();		
	}
	
	public virtual void Hide() {
		foreach (var uiPrompt in GetComponentsInChildren<IShowHideUIPiece>()) {
			uiPrompt.Hide();
		}
		mIsHidden = true;
	}

	public virtual void Appear() {
		if (!HasMessage) {
			return;
		}

		foreach (var uiPrompt in GetComponentsInChildren<IShowHideUIPiece>()) {
			uiPrompt.Show();
		}
		mIsHidden = false;
	}
}