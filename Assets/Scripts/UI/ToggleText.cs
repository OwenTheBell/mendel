﻿using UnityEngine;

public class ToggleText : MonoBehaviour {

    public bool StartOn = true;
    public string OnText;
    public string OffText;
    public bool IsOn { get { return _Toggle; } }

    private bool _Toggle;
    private UIText _Text;
    private string _DefaultText;
    private Animator _Animator;

    void Awake() {
        _Text = GetComponentInChildren<UIText>();
        _Animator = GetComponent<Animator>();
        _Toggle = StartOn;
    }

	void Start () {
        _DefaultText = _Text.Text;
        SetTextForToggle();
	}

    public void ToggleState() {
        _Toggle = !_Toggle;
        SetTextForToggle();
    }

    void SetTextForToggle() {
        if (_Text == null || _DefaultText == default(string)) return;
        if (_Toggle) {
            _Text.Text = _DefaultText.Replace("xxx", OnText);
        }
        else {
            _Text.Text = _DefaultText.Replace("xxx", OffText);
        }
    }
}
