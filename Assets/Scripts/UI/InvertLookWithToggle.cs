﻿using UnityEngine;

public class InvertLookWithToggle : MonoBehaviour {

	public MendelMouseLook Look;

    private ToggleText _ToggleText;

    private void Awake() {
        _ToggleText = GetComponent<ToggleText>();
    }

    private void OnEnable() {
		if (Look.InvertY != _ToggleText.IsOn) {
			_ToggleText.ToggleState();
		}
    }

    public void Toggle() {
		Look.InvertY = !Look.InvertY;
    }
}
