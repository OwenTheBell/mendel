﻿using UnityEngine;

public class LoadingDisplay : MonoBehaviour, IUIElement {

    public bool Available { get; set; }
    public bool Visible {
        get {
            return Quote.Visible && Loading.Visible;
        }
    }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }

    public QuoteDisplay Quote;
    public UIText Loading;

    // Use this for initialization
    public void Show() {
        Quote.Show();
        foreach (var element in Loading.GetComponents<IUIElement>()) { element.Show(); }
    }

    public void Hide() {
        Quote.Hide();
        foreach (var element in Loading.GetComponents<IUIElement>()) { element.Hide(); }
    }
}
