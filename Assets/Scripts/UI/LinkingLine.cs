﻿using UnityEngine;
using System.Collections;

public class LinkingLine : MonoBehaviour {

	public RectTransform Origin;
	public RectTransform End;
	public float BeginningOffset = 0f;
	public float EndingOffset = 0f;

	public Canvas Canvas;

	private RectTransform LeftEnd;
	private RectTransform RightEnd;
	private RectTransform Line;

	private Quaternion LeftRotation;
	private Quaternion RightRotation;
	private Quaternion LineRotation;

	// Use this for initialization
	void Start () {
		LeftEnd = transform.Find("Left End").GetComponent<RectTransform>();	
		RightEnd = transform.Find("Right End").GetComponent<RectTransform>();	
		Line = transform.Find("Line").GetComponent<RectTransform>();	

		LeftRotation = LeftEnd.rotation;
		RightRotation = RightEnd.rotation;
		LineRotation = Line.rotation;
	}
	
	// Update is called once per frame
	void Update () {
		var originPos = Origin.anchoredPosition;
		var endPos = End.anchoredPosition;

		var distance = Vector3.Distance(originPos, endPos);
		var beginningOff = BeginningOffset/distance;
		var endingOff = (distance-EndingOffset)/distance;

		var leftPos = Vector3.Lerp(originPos, endPos, beginningOff);
		var rightPos = Vector3.Lerp(originPos, endPos, endingOff);
		LeftEnd.anchoredPosition = leftPos;
		RightEnd.anchoredPosition = rightPos;

		var position = Vector3.Lerp(leftPos, rightPos, 0.5f);
		Line.anchoredPosition = position;
		distance = Vector3.Distance(leftPos, rightPos);
		Line.sizeDelta = new Vector2(distance, Line.sizeDelta.y);

		var angle = Mathf.Atan2(rightPos.y - leftPos.y,
								rightPos.x - leftPos.x);
		angle *= Mathf.Rad2Deg;
		
		var rotation = Quaternion.Euler(0f, 0f, angle);
		LeftEnd.localRotation = LeftRotation * rotation;
		RightEnd.localRotation = RightRotation * rotation;
		Line.localRotation = LineRotation * rotation;	
	}

	void SetScale(float scale) {
		LeftEnd.localScale = Vector3.one * scale;
		RightEnd.localScale = Vector3.one * scale;

		var lineScale = Line.localScale;
		lineScale.y = scale;
		Line.localScale = lineScale;	
	}
}