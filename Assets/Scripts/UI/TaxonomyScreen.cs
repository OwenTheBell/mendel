﻿using UnityEngine;

public class TaxonomyScreen : MonoBehaviour, ICancelReciever {

    public GameObject View;
    public UIText TitleText;

    private TaxonomyView _TaxView;
    private PlantView _PlantView;
    private FamilyTreeView _TreeView;
    private bool _Active = false;
    private bool _GameStarted = false;

	void Start () {
        _TaxView = GetComponentInChildren<TaxonomyView>();
        _PlantView = GetComponentInChildren<PlantView>();
        _TreeView = GetComponentInChildren<FamilyTreeView>();
        _TaxView.Hide();
        _PlantView.Hide();
        TitleText.Hide();
        _TreeView.gameObject.SetActive(false);
	}
	
	void Update () {
        if (Input.GetKeyDown(KeyCode.Tab)) {
            if (!_Active || _GameStarted) {
                Open();
            }
            else {
                Services.CancelStack.RemoveAll();
            }
        }
	}

    public void OnCancel() {
        _TaxView.Hide();
        TitleText.Hide();
        _PlantView.Hide();
        _TreeView.gameObject.SetActive(false);
        _Active = false;
        Events.instance.Raise(new UnHideAlertEvent());
    }

    private void Open() {
        Services.CancelStack.RemoveAll();
        Services.CancelStack.Add(this);
        _Active = true;
        _TaxView.SetValues(Services.SpeciesCatalog.Names);
        TitleText.Show();
        _TaxView.Show();
        _PlantView.Show();
        _TreeView.gameObject.SetActive(false);
        Events.instance.Raise(new HideAlertEvent());
    }

    private void OnEnable() {
        SetListeners();
    }

    private void OnDisable() {
        RemoveListeners(); 
    }

    private void OnDestroy() {
        RemoveListeners(); 
    }

    void SetListeners() {
        Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStartedEvent);
        Events.instance.AddListener<ShowSpeciesEntryEvent>(OnShowSpeciesEntryEvent);
    }

    void RemoveListeners() {
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
        Events.instance.RemoveListener<ShowSpeciesEntryEvent>(OnShowSpeciesEntryEvent);
    }

    void OnPlayerStartedEvent(PlayerStartedEvent e) {
        _GameStarted = true;
    }

    void OnShowSpeciesEntryEvent(ShowSpeciesEntryEvent e) {
        Open();
        _TaxView.SelectSpecies(e.name, e.identity);
    }
}