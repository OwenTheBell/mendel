﻿using UnityEngine;
using UnityStandardAssets.ImageEffects;
using System.Collections;
using System.Collections.Generic;

public class CameraFader : MonoBehaviour {

    public UIImageFader ImageFader;
    public List<Blur> Blurs;

    public float Duration;

    private float _PercentFaded;
    private int _Iterations;

    void Start() {
        _Iterations = Blurs[0].iterations;
        Blurs.Act(b => b.enabled = false);
        ImageFader.HideTime = Duration;
    }

    public void Show() {
        StopAllCoroutines();
        StartCoroutine(FadeRoutine());
    }

    public void Hide() {
        StopAllCoroutines();
        StartCoroutine(RefocusRoutine());
    }

    IEnumerator FadeRoutine() {
        ImageFader.Show();
        var time = _PercentFaded * Duration;
        Blurs.Act(b => b.enabled = true);
        while (time < Duration) {
            time += Time.deltaTime;
            Blurs.Act(b => b.iterations = Mathf.FloorToInt(time/Duration * _Iterations));
            _PercentFaded = Mathf.Clamp01(time / Duration);
            yield return 0;
        }
    }

    IEnumerator RefocusRoutine() {
        ImageFader.Hide();
        var time = _PercentFaded * Duration;
        while (time > 0f) {
            time -= Time.deltaTime;
            Blurs.Act(b => b.iterations = Mathf.FloorToInt(time/Duration * _Iterations));
            _PercentFaded = Mathf.Clamp01(time / Duration);
            yield return 0;
        }
        Blurs.Act(b => b.enabled = false);
    }
    
}
