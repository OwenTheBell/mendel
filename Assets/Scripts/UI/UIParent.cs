﻿using UnityEngine;

public class UIParent : MonoBehaviour, IUIElement {

    [SerializeField]
    private IUIElement _Element;

    public bool Available {
        get { return _Element.Available;  }
        set { _Element.Available = value; }
    }
    public bool Visible { get { return _Element.Visible; } }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }

    void Awake() {
        foreach (var e in GetComponentsInChildren<IUIElement>()) {
            if (e.Transform != transform) {
                _Element = e;
                break;
            }
        }
    }

    public void Hide() {
        _Element.Hide();
    }

    public void Show() {
        _Element.Show();
    }
}