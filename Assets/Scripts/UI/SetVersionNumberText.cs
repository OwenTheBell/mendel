﻿using UnityEngine;

[RequireComponent(typeof(UIText))]
public class SetVersionNumberText : MonoBehaviour {

    private void Awake() {
        var uiText = GetComponent<UIText>();
        uiText.Text += " v" + PROPERTIES.VersionNumber;
    }
}