﻿using UnityEngine;
using System.Collections;

public class NameUI : MonoBehaviour {

	private InputStringProcessor mInputProcessor;
	private NamingCallback mNameCallback;
	public KeyCode ConfirmKey;

	// Use this for initialization
	void Start () {
		mInputProcessor = GetComponentInChildren<InputStringProcessor>();
	}
	
	// Update is called once per frame
	void Update () {
		if (Input.GetKeyDown(ConfirmKey)) {
			mNameCallback(mInputProcessor.Message);
			gameObject.SetActive(false);
		}
	}

	public void Active(bool active) {
		gameObject.SetActive(active);
	}

	public void SetString(string name) {
		if (mInputProcessor == null) {
			mInputProcessor = GetComponentInChildren<InputStringProcessor>();
		}
		mInputProcessor.Message = name;
	}

	public void SetCallback(NamingCallback callback) {
		mNameCallback = callback;
	}
}
