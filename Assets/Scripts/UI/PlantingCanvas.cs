﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class PlantingCanvas : MonoBehaviour {

	private int _Count;
	private int _MaxCount;
	public int Count {
		get {
			return _Count;
		}
		set {
			_Count = value;
			var count = _CountString.Replace("x", _Count + "");
			count = count.Replace("y", _MaxCount + "");
			_CountText.text = count;
		}
	}

	public int MaxCount {
		get {
			return _MaxCount;
		}
		set {
			_MaxCount = value;
			var count = _CountString.Replace("x", _Count + "");
			count = count.Replace("y", _MaxCount + "");
			_CountText.text = count;
		}
	}

	private string __CountString;
	private string _CountString {
		get {
			if (__CountString == null) {
				_CountString = _CountText.text;
			}
			return __CountString;
		}
		set {
			__CountString = value;
		}
	}

    public string PromptString {
        get {
            return _PromptText.text;
        }
        set {
            _PromptText.text = value;
        }
    }

    public string ParentsString {
        get {
            return _CountText.text;
        }
        set {
            _CountText.text = value;
        }
    }

	public string PrimaryString {
		get {
			return _PrimaryMessage.Message;
		}
		set {
			_PrimaryMessage.Message = value;
			if (value == string.Empty || value == "") {
				_PrimaryMessage.Visible = false;
				_MovementMessage.Visible = false;
			}
			else {
				_PrimaryMessage.Visible = true;
				_MovementMessage.Visible = true;
			}
		}
	}

	public string SecondaryString {
		get {
			return _SecondaryMessage.Message;
		}
		set {
			_SecondaryMessage.Message = value;
			if (value == string.Empty || value == "") {
				_SecondaryMessage.Visible = false;
			}
			else {
				_SecondaryMessage.Visible = true;
			}
		}
	}

	public bool Active {
		get {
			return gameObject.activeSelf;
		}
		set {
			var oldstate = gameObject.activeSelf;
			gameObject.SetActive(value);
			if (!oldstate && value) {
				Appear();
			}
		}
	}

	[SerializeField]
	private TMP_Text _PromptText;
	[SerializeField]
	private TMP_Text _CountText;
	[SerializeField]
	private ControlMessage _MovementMessage;
	[SerializeField]
	private ControlMessage _PrimaryMessage;
	[SerializeField]
	private ControlMessage _SecondaryMessage;

	void Start () {
		// _CountString = _CountText.text;	
	}
	
	void Update () {
	
	}

	private void Appear() {
		// foreach (var controlMessage in GetComponentsInChildren<ControlMessage>()) {
		// 	// controlMessage.Visible = true;
		// 	controlMessage.gameObject.SetActive(true);
		// }
		_MovementMessage.gameObject.SetActive(true);
		_PrimaryMessage.gameObject.SetActive(true);
		_SecondaryMessage.gameObject.SetActive(true);
		_CountText.gameObject.SetActive(true);
		_PromptText.gameObject.SetActive(true);
	}

	public void ShowOnlyControls() {
		_PromptText.gameObject.SetActive(false);
		_CountText.gameObject.SetActive(false);
		// _MovementMessage.Visible = false;
		_MovementMessage.gameObject.SetActive(false);
	}

	public void ShowAll() {
		Appear();		
	}
}
