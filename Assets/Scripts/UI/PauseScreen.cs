﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

public class PauseScreen : MonoBehaviour, ICancelReciever {

    [SerializeField]
    private float _SelectionChangeRequirement;

    private List<IUIElement> _UIElements;
    private List<Button> _Buttons;
    private float _TotalMouseMovement;
    private int _SelectedIndex;
    private AudioSource _Source;
    private bool _JustSelected;

    private void Awake() {
        _Source = GetComponent<AudioSource>();
        _UIElements = new List<IUIElement>(GetComponentsInChildren<IUIElement>());
        _UIElements = _UIElements.Filter(g => { return g.Transform.parent == transform; });
        _Buttons = new List<Button>(GetComponentsInChildren<Button>());
    }

    void Start() {
        _UIElements.Act(g => StartCoroutine(HideGroup(g)));
    }

    public void Resume() {
        Services.CancelStack.Remove(this);
    }

    public void OnPause() {
        Services.CancelStack.Add(this);
        Events.instance.Raise(new HideAlertEvent());
        _UIElements.Act(g => {
            g.GameObject.SetActive(true);
            g.Show();
        });

        _SelectedIndex = 0;
        _TotalMouseMovement = 0f;
        _SelectedIndex = 0;
    }

    public void SelectionChange() {
        if (!_JustSelected) {
            _Source.Play();
        }
        _JustSelected = false;
    }

    IEnumerator HideGroup(IUIElement e) {
        e.Hide();
        while (e.Visible) yield return null;
        e.GameObject.SetActive(false);
    }

    public void OnCancel() {
        EventSystem.current.SetSelectedGameObject(null);
        _UIElements.Act(g => StartCoroutine(HideGroup(g)));
        Events.instance.Raise(new UnHideAlertEvent());
    }
}