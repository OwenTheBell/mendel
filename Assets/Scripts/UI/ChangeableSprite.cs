﻿using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class ChangeableSprite : MonoBehaviour {

	private Image uiImage;
	private Sprite defaultSprite;

	/* this list functions as a rough stack for all sprites coming in.
	* Index 0 is the sprite currently being displayed. */
	private List<Sprite> mSpriteBuffer = new List<Sprite>();

	void Awake() {
		uiImage = GetComponent<Image>();
	}

	// Use this for initialization
	void Start () {
		defaultSprite = uiImage.sprite;
	}

	// Update is called once per frame
	void Update () {

	}


	void OnEnable() {
		Events.instance.AddListener<SetCrosshairEvent>(SetSprite);
		Events.instance.AddListener<UnsetCrosshairEvent>(UnsetSprite);
		Events.instance.AddListener<ResetCrosshairEvent>(ResetSprite);
	}

	void OnDisable() {
		Events.instance.RemoveListener<SetCrosshairEvent>(SetSprite);
		Events.instance.RemoveListener<UnsetCrosshairEvent>(UnsetSprite);
		Events.instance.RemoveListener<ResetCrosshairEvent>(ResetSprite);
	}

	void OnDestroy() {
		Events.instance.RemoveListener<SetCrosshairEvent>(SetSprite);
		Events.instance.RemoveListener<UnsetCrosshairEvent>(UnsetSprite);
		Events.instance.RemoveListener<ResetCrosshairEvent>(ResetSprite);
	}

	/* Take a new sprite and put it into the buffer is such a sprite isn't
	* already there. Treat the new sprite as the new active sprite to display */
	void SetSprite(SetCrosshairEvent e) {
		if (e.sprite == null) {
			Debug.Log("I recieved a null sprite. Cannot set immage.");
		}
		if (!mSpriteBuffer.Contains(e.sprite)) {
			mSpriteBuffer.Insert(0, e.sprite);
			uiImage.sprite = mSpriteBuffer[0];
		}
	}


	void UnsetSprite(UnsetCrosshairEvent e) {
		if (e.sprite == null) {
			Debug.Log("I recieved a null sprite. Cannot unset immage.");
		}
		mSpriteBuffer.Remove(e.sprite);
		if (mSpriteBuffer.Count > 0) {
			uiImage.sprite = mSpriteBuffer[0];
		} else {
			uiImage.sprite = defaultSprite;
		}
	}

	void ResetSprite(ResetCrosshairEvent e) {
		uiImage.sprite = defaultSprite;
		mSpriteBuffer.Clear();
	}
}
