﻿using UnityEngine;
using System.Collections;

public class PromptController : MonoBehaviour {

    private UIText _Text;

    private string _BufferedText;
    private float _BufferDuration;
    private float _BufferedTime;
    private Coroutine _TextCoroutine;

    private void Awake() {
        _Text = GetComponent<UIText>();
    }

    private void OnEnable() {
        Events.instance.AddListener<PromptTextEvent>(OnPromptTextEvent);
        Events.instance.AddListener<HidePromptTextEvent>(OnHidePromptTextEvent);
        Events.instance.AddListener<HideAlertEvent>(OnHideAlertEvent);
        Events.instance.AddListener<UnHideAlertEvent>(OnUnHideAlertEvent);
    }

    private void OnDisable() {
        Events.instance.RemoveListener<PromptTextEvent>(OnPromptTextEvent);
        Events.instance.RemoveListener<HidePromptTextEvent>(OnHidePromptTextEvent);
        Events.instance.RemoveListener<HideAlertEvent>(OnHideAlertEvent);
        Events.instance.RemoveListener<UnHideAlertEvent>(OnUnHideAlertEvent);
    }

    void OnPromptTextEvent(PromptTextEvent e) {
        StopTextDisplay();
        _TextCoroutine = StartCoroutine(ShowText(e.text, e.duration));
    }

    void OnHidePromptTextEvent(HidePromptTextEvent e) {
        if (e.text != string.Empty && e.text != _BufferedText) {
            return;
        }
        StopTextDisplay();
        _BufferedText = string.Empty;
        _Text.Hide();
    }

    void OnHideAlertEvent(HideAlertEvent e) {
        StopTextDisplay();
        _Text.Hide();
    }

    void OnUnHideAlertEvent(UnHideAlertEvent e) {
        if (_BufferedText != string.Empty && _BufferedText != null) {
            _TextCoroutine = StartCoroutine(ShowText(_BufferedText, _BufferDuration, _BufferedTime));
        }
    }

    void StopTextDisplay() {
        if (_TextCoroutine != null) {
            StopCoroutine(_TextCoroutine);
            _TextCoroutine = null;
        }
    }

    IEnumerator ShowText(string text, float duration, float seedTime = 0f) {
        _BufferedText = text;
        _BufferDuration = duration;

        // wait until the cancel stack is empty to prevent overlaying
        var time = seedTime;
        while (Services.CancelStack.Occupied) {
            yield return null;
        }
        if (_Text.Visible) {
            _Text.Hide();
            while (_Text.Visible) {
                yield return null;
            }
        }
        _Text.Text = text;
        _Text.Show();

        //if (_TextCoroutine != null) {
        //    StopCoroutine(_TextCoroutine);
        //}
        while (time < duration) {
            time += Time.deltaTime;
            _BufferedTime = time;
            yield return null;
        }

        _BufferedText = string.Empty;
        _Text.Hide();
        _TextCoroutine = null;
    }
}