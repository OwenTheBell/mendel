﻿using UnityEngine;

public class GroundInteractionState : InteractionState {

    [SerializeField]
    private GameObject TargetIndicatorPrefab;
    private GameObject _TargetIndicator;

    [SerializeField]
    private LayerMask _GroundLayer;

    private Vector3 _TargetPosition;
    private UprootInteractionState _Uproot;
    private Vector3 _GroundUp;

    protected override void Awake() {
        base.Awake();

        Mode = InteractionStateMode.Ground;
        _Uproot = GetComponent<UprootInteractionState>();
    }

    protected override void Start() {
        base.Start();

        _TargetIndicator = Instantiate(TargetIndicatorPrefab) as GameObject;
        DontDestroyOnLoad(_TargetIndicator);
        _TargetIndicator.GetComponent<PlantingIndicator>().Hide();
    }

    protected override void Update() {
        base.Update();
    }

    public override bool CheckTarget(Collider collider, Vector3 point) {
        if (!base.CheckTarget(collider, point)) {
            return false;
        }

        if (_Uproot.HeldPlant != null) return false;

        // this raycast both gets the normal for placing the planting indicator
        // as well as determine whether the ground is what is directly in front of the
        // camera since the interaction probe can be a bit fuzzy
        RaycastHit hit;
        var camera = Camera.main;
        var ray = new Ray(camera.transform.position, point - camera.transform.position);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _GroundLayer)) {
            if (hit.transform.tag == "Ground") {
                _GroundUp = hit.normal;
            }
            else {
                return false;
            }
        }

        // if none of the behaviour are ready to do anything with the target
        // then don't alter that the ground state is ready to do anything
        var ready = false;
        foreach (var behaviour in _Behaviours) {
            if (behaviour.SetTarget(collider.gameObject, point)) {
                ready = true;
            }
        }

        return ready;
    }

    public override void Activate(Collider collider, Vector3 point) {
        base.Activate(collider, point);

        _TargetPosition = point;
        InteractionCanvas.Transform.position = _TargetPosition;
        _TargetIndicator.transform.position = _TargetPosition;
        _TargetIndicator.transform.up = _GroundUp;
    }

    protected override void DeferredActivate() {
        base.DeferredActivate();

        UpdateCanvas();
        InteractionCanvas.Show();
        _TargetIndicator.GetComponent<PlantingIndicator>().Show();
    }

    protected override void DeferredDeactivate() {
        base.DeferredDeactivate();

        InteractionCanvas.Hide();
        _TargetIndicator.GetComponent<PlantingIndicator>().Hide();
    }

    protected override void BehaviourActivated(InteractionBehaviour behaviour) {
        base.BehaviourActivated(behaviour);
        _TargetIndicator.GetComponent<PlantingIndicator>().Hide();
    }

    protected override void BehaviourCompleted(InteractionBehaviour behaviour) {
        base.BehaviourCompleted(behaviour);
        DeferredDeactivate();
    }
}