﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ResolutionControl : MonoBehaviour, IUIElement {

    public bool Available { get; set; }
    public bool Visible {
        get {
            return _DescriptorText.Visible && _ResolutionText.Visible;
        }
    }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }

    [SerializeField]
    private UIMenuEntry _ConfirmButton;
    [SerializeField]
    private UIText _DescriptorText;
    [SerializeField]
    private UIText _ResolutionText;
    [SerializeField]
    private UIText _WarningText;
    [SerializeField]
    private List<Animator> _VisibilityAnimators;
    private Coroutine _ShowRoutine;

    private Resolution _CurrentResolution;
    private List<Resolution> _Resolutions;
    private int _NewResolutionIndex;

    private void Awake() {
        Available = true;
        _Resolutions = new List<Resolution>(Screen.resolutions);
        _Resolutions = _Resolutions.Filter(r => r.width >= 800);
    }

    #region IUIElement functions

    public void Show() {
        // need to manually create _CurrentResolution since Screen.currentResolution
        // returns resolution of desktop if the game is in windowed mode
        _CurrentResolution = new Resolution();
        _CurrentResolution.width = Screen.width;
        _CurrentResolution.height = Screen.height;
        for (var i = 0; i < _Resolutions.Count; i++) {
            if (_CurrentResolution.width == _Resolutions[i].width
                && _CurrentResolution.height == _Resolutions[i].height) {
                _NewResolutionIndex = i;
                break;
            }
        }
        DisplayResolution(_CurrentResolution);
        _ShowRoutine = StartCoroutine(ShowRoutine());
    }

    public void Hide() {
        if (_ShowRoutine != null) {
            StopCoroutine(_ShowRoutine);
        }
        foreach (var element in GetComponentsInChildren<IUIElement>()) {
            if (element == this) {
                continue;
            }
            element.Hide();
        }
        _VisibilityAnimators.Act(a => a.SetBool("Show", false));
    }

    #endregion // IUIElement functions


    #region ResolutionControl UI Events

    public void ResolutionDown() {
        _NewResolutionIndex--;
        if (_NewResolutionIndex < 0) {
            _NewResolutionIndex = _Resolutions.Count - 1;
        }
        DisplayResolution(_Resolutions[_NewResolutionIndex]);
    }

    public void ResolutionUp() {
        _NewResolutionIndex++;
        if (_NewResolutionIndex >= _Resolutions.Count) {
            _NewResolutionIndex = 0;
        }
        DisplayResolution(_Resolutions[_NewResolutionIndex]);
    }

    public void SetResolution() {
        _CurrentResolution = _Resolutions[_NewResolutionIndex];
        Screen.SetResolution(_CurrentResolution.width, _CurrentResolution.height, Screen.fullScreen);
        //Cursor.lockState = CursorLockMode.Confined;
        _ConfirmButton.Hide();
        _WarningText.Show();
        StartCoroutine(DelayCursorReset());
    }

    #endregion // ResolutionControl UI Events


    #region Private functions

    IEnumerator ShowRoutine() {
        _DescriptorText.Show();
        _ResolutionText.Show();
        while (!Visible) {
            yield return null;
        }
        _VisibilityAnimators.Act(a => a.SetBool("Show", true));
        _ShowRoutine = null;
    }

    void DisplayResolution(Resolution resolution) {
        _ResolutionText.Text  = resolution.width + " x " + resolution.height;
        if (resolution.width != _CurrentResolution.width
            || resolution.height != _CurrentResolution.height) {
            if (!_ConfirmButton.Visible) {
                _ConfirmButton.Show();
                _WarningText.Hide();
            }
        }
        else {
            _ConfirmButton.Hide();
        }
    }

    IEnumerator DelayCursorReset() {
        Cursor.lockState = CursorLockMode.None;
        yield return new WaitForSeconds(0.1f);
        Cursor.lockState = CursorLockMode.Confined;
    }

    #endregion // Private functions
}
