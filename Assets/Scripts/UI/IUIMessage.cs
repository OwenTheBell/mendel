public interface IUIMessage {
	bool Visible { get; set; }
	void Clear();
}