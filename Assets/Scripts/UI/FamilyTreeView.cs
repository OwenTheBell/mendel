﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class FamilyTreeView : MonoBehaviour, ICancelReciever {

    public GameObject PlantPrefab;
    public GameObject PlantButton;
    public TaxonomyView TaxView;
    public float RowHeight;
    public float ColumnSpacing;
    public PlantView PlantView;
    public UIMenuEntry _ExitText;
    public GameObject LinePrefab;
    public float DepthStep;
    public AudioSource ActionAudio;

    private int _DisplayedIdentity;
    private ScrollRect _ScrollRect;
    private List<FamilyTreeButton> _ButtonPool;
    private List<List<FamilyTreeButton>> _PlantButtons;
    private List<List<PlantInfoStruct>> _PlantInfos;
    private List<GameObject> _LinePool;
    private List<GameObject> _Lines;
    private Vector2 _ScrollDirection = Vector2.zero;
    private Vector2 _ScrollDefaultSize;
    private float _DoneDrawingIn;

    private void Awake() {
        _ScrollRect = GetComponentInChildren<ScrollRect>();
        _PlantButtons = new List<List<FamilyTreeButton>>();
        _PlantInfos = new List<List<PlantInfoStruct>>();
        _ButtonPool = new List<FamilyTreeButton>();
        _LinePool = new List<GameObject>();
        _Lines = new List<GameObject>();

        _ScrollDefaultSize = new Vector2(_ScrollRect.content.rect.width
                                        , _ScrollRect.content.rect.height);
    }

    public void FixedUpdate() {
        _DoneDrawingIn -= Time.deltaTime;
        if (_ScrollDirection != Vector2.zero && _DoneDrawingIn <= 0f) {
            var x = (_ScrollDirection.x != 0f) ? _ScrollDirection.x / Mathf.Abs(_ScrollDirection.x) : 0f;
            var y = (_ScrollDirection.y != 0f) ? _ScrollDirection.y / Mathf.Abs(_ScrollDirection.y) : 0f;
            var verticalBar = _ScrollRect.verticalScrollbar;
            var horizontalBar = _ScrollRect.horizontalScrollbar;
            horizontalBar.value += x * _ScrollRect.scrollSensitivity * Time.fixedDeltaTime * horizontalBar.size;
            verticalBar.value += y * _ScrollRect.scrollSensitivity * Time.fixedDeltaTime * verticalBar.size;
        }
    }

    public void ShowPlant(int identity) {
        Services.CancelStack.Add(this);
        _ExitText.Show();
        gameObject.SetActive(true);
        ClearTree();
        _DisplayedIdentity = identity;
        var myInfo = Services.PlantInfoCatalog.Infos[identity];
        _PlantInfos.Add(new List<PlantInfoStruct>{ myInfo });
        var generation = new List<PlantInfoStruct>();
        var layer = 0;

        // get parents of each previous generation and add to end of _PlantInfos
        do {
            generation.Clear();
            foreach (var info in _PlantInfos[layer]) {
                if (info.Parents.First >= 0) {
                    var first = Services.PlantInfoCatalog.Infos[info.Parents.First];
                    if (!generation.Contains(first)) generation.Add(first);
                }
                if (info.Parents.Second >= 0) {
                    var second = Services.PlantInfoCatalog.Infos[info.Parents.Second];
                    if (!generation.Contains(second)) generation.Add(second);
                }
            }
            if (generation.Count > 0) {
                _PlantInfos.Add(new List<PlantInfoStruct>(generation));
            }
            layer++;
        } while (generation.Count > 0);

        // get children of each previous generation and add to begining of _PlantInfos
        var originLayer = 0;
        do {
            generation.Clear();
            foreach (var info in _PlantInfos[0]) {
                foreach (var childID in info.Children) {
                    var child = Services.PlantInfoCatalog.Infos[childID];
                    if (!generation.Contains(child)) generation.Add(child);
                }
            }
            if (generation.Count > 0) {
                _PlantInfos.Insert(0, new List<PlantInfoStruct>(generation));
                originLayer++;
            }
        } while (generation.Count > 0);

        foreach (var generationLayer in _PlantInfos) {
            _PlantButtons.Add(new List<FamilyTreeButton>());
            foreach (var info in generationLayer) {
                FamilyTreeButton plantButton;
                if (_ButtonPool.Count > 0) {
                    plantButton = _ButtonPool[0];
                    plantButton.gameObject.SetActive(true);
                    _ButtonPool.RemoveAt(0);
                }
                else {
                    plantButton = Instantiate(PlantButton).GetComponent<FamilyTreeButton>();
                    plantButton.transform.parent = _ScrollRect.content;
                    plantButton.Rect = _ScrollRect;
                    var plant = Instantiate(PlantPrefab);
                    plantButton.Plant = plant;
                    plant.transform.parent = plantButton.transform;
                    var plantRect = plant.GetComponent<RectTransform>();
                    plantRect.anchorMin = new Vector2(0.5f, 0f);
                    plantRect.anchorMax = new Vector2(0.5f, 0f);
                    plant.transform.localScale = Vector3.one * 60f;
                    plant.GetComponent<RectTransform>().anchoredPosition3D = Vector3.zero;
                }

                plantButton.Identity = info.Identity;
                plantButton.GetComponent<Button>().onClick.AddListener(() => {
                    _DisplayedIdentity = info.Identity;
                    Exit();
                });

                plantButton.Hide();
                _PlantButtons[_PlantButtons.Count - 1].Add(plantButton);
            }
        }

        var contentHeight = 0f;
        contentHeight = (_PlantButtons.Count + 2) * RowHeight;
        _ScrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Vertical, contentHeight);

        // place all the plants in their appropriate positions
        var rowIndex = 0;
        var widestRow = 0;
        var y = RowHeight;
        foreach (var row in _PlantButtons) {
            var plantsInRow = row.Count;
            // add a stager in every other row, makes the tree read a little clearn
            var x = -((plantsInRow - 1) * ColumnSpacing) / 2f;
            if (plantsInRow == 1) {
                if (rowIndex % 2 != 0) {
                    x += ColumnSpacing / 2f;
                }
                else {
                    x -= ColumnSpacing / 2f;
                }
            }
            if (rowIndex == originLayer) {
                x = 0f;
            }
            var columnIndex = 0;
            var delay = Mathf.Abs(originLayer - rowIndex) * 0.5f;
            foreach (var button in row) {
                button.GetComponent<RectTransform>().anchoredPosition3D = new Vector3(x, y, 10f);
                var plant = button.GetComponent<FamilyTreeButton>().Plant;

                var anchoredPosition = plant.GetComponent<RectTransform>().anchoredPosition3D;
                anchoredPosition.z = rowIndex * DepthStep;
                plant.GetComponent<RectTransform>().anchoredPosition3D = anchoredPosition;

                x += ColumnSpacing;
                var info = _PlantInfos[rowIndex][columnIndex];
                StartCoroutine(DisplayPlant(plant, info.Identity, delay));
                columnIndex++;
            }
            if (columnIndex > widestRow) widestRow = columnIndex;
            rowIndex++;
            y += RowHeight;
        }

        // draw all the connecting lines
        for (rowIndex = originLayer; rowIndex < _PlantButtons.Count; rowIndex++) {
            var delay = Mathf.Abs(rowIndex - originLayer) * 0.5f;
            var columnIndex = 0;
            foreach (var button in _PlantButtons[rowIndex]) {
                var info = _PlantInfos[rowIndex][columnIndex];

                if (info.Parents.First >= 0) {
                    var index = _PlantInfos[rowIndex + 1].FindIndex(pInfo => {
                        return pInfo.Identity == info.Parents.First;
                    });
                    var parent = _PlantButtons[rowIndex + 1][index];
                    StartCoroutine(DrawLineFromTo(button.transform, parent.transform, 0.5f, delay));
                }
                if (info.Parents.First != info.Parents.Second && info.Parents.Second > 0) {
                    var index = _PlantInfos[rowIndex + 1].FindIndex(pInfo => {
                        return pInfo.Identity == info.Parents.Second;
                    });
                    var parent = _PlantButtons[rowIndex + 1][index];
                    StartCoroutine(DrawLineFromTo(button.transform, parent.transform, 0.5f, delay));
                }

                //// if this is the source of the tree, draw lines to children too
                if (rowIndex == 0 && info.Children.Count > 0) {
                }
                columnIndex++;
            }
        }
        // now do all the children
        if (originLayer != 0) {
            for (rowIndex = originLayer; rowIndex > 0; rowIndex--) {
                var delay = Mathf.Abs(rowIndex - originLayer) * 0.5f;
                var columnIndex = 0;
                foreach (var button in _PlantButtons[rowIndex]) {
                    var info = _PlantInfos[rowIndex][columnIndex];
                    foreach (var id in info.Children) {
                        var index = _PlantInfos[rowIndex - 1].FindIndex(pInfo => {
                            return pInfo.Identity == id;
                        });
                        var child = _PlantButtons[rowIndex - 1][index];
                        StartCoroutine(DrawLineFromTo(button.transform, child.transform, 0.5f, delay));
                    }
                    columnIndex++;
                }
            }
        }

        var contentWidth = (widestRow + 1) * ColumnSpacing;
        contentWidth = Mathf.Clamp(contentWidth, _ScrollDefaultSize.x, Mathf.Infinity);
        _ScrollRect.content.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, contentWidth);

        var verticalScroll = ((originLayer + 1) * RowHeight) / contentHeight;
        if (contentHeight < _ScrollDefaultSize.y) {
            verticalScroll = 0f;
        }
        _ScrollRect.verticalScrollbar.value = verticalScroll;
        _ScrollRect.horizontalScrollbar.value = 0.5f;
    }

    public void Exit() {
        Services.CancelStack.Remove(this);
    }

    public void OnCancel() {
        gameObject.SetActive(false);
        var speciesName = Services.SpeciesCatalog.GetNameForIdentity(_DisplayedIdentity);
        Events.instance.Raise(new ShowSpeciesEntryEvent(speciesName, _DisplayedIdentity));
    }

    private void ClearTree() {
        _PlantButtons.Act(list => list.Act(b => {
            b.GetComponentInChildren<MeshFilter>().sharedMesh = null;
            b.GetComponentInChildren<FlowerCreator>().ClearNodes();
            b.GetComponent<Button>().onClick.RemoveAllListeners();
            b.gameObject.SetActive(false);
            //make sure to clear the button
            b.GetComponent<FamilyTreeButton>().Identity = -1;
            _ButtonPool.Add(b);
            b.Hide();
            ActionAudio.Play();
        }));
        _PlantButtons.Clear();
        _PlantInfos.Clear();

        _Lines.Act(l => l.SetActive(false));
        _LinePool.AddRange(_Lines);
        _Lines.Clear();
    }

    IEnumerator DisplayPlant(GameObject slot, int identity, float delay) {
        yield return new WaitForSeconds(delay);
        var info = Services.MeshVault.GetPlant(identity);
        slot.GetComponent<PlantInfo>().Identity = identity;
        slot.GetComponent<StemBuilder>().GrowMesh(info.Stem, 0.5f);
        var creator = slot.GetComponent<FlowerCreator>();
        yield return new WaitForSeconds(0.5f);
        for (var i = 0; i < info.Flowers.Count; i++) {
            creator.AddFlower(info.Flowers[i], info.Positions[i]);
        }
    }

    IEnumerator DrawLineFromTo(Transform plantButton, Transform relative, float duration, float delay) {
        if (duration + delay > _DoneDrawingIn) {
            _DoneDrawingIn = duration + delay;
        }
        yield return new WaitForSeconds(delay);

        var end = relative.position;

        LineRenderer line;
        if (_LinePool.Count > 0) {
            line = _LinePool[0].GetComponent<LineRenderer>();
            line.gameObject.SetActive(true);
            _LinePool.RemoveAt(0);
        }
        else {
            line = Instantiate(LinePrefab).GetComponent<LineRenderer>();
            line.transform.parent = _ScrollRect.content;
            line.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0f);
            line.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0f);
        }
        _Lines.Add(line.gameObject);

        var plant = plantButton.GetComponent<FamilyTreeButton>().Plant;
        var plantRelative = relative.GetComponent<FamilyTreeButton>().Plant;
        line.transform.parent = plantButton;
        line.transform.position = plant.transform.position;

        // add a lot of padding so they show up behind the plants
        line.transform.localPosition = new Vector3(0f, 0f, 20f);
        var target = line.transform.InverseTransformPoint(plantRelative.transform.position);
        target.z += 20f;
        var distance = Vector3.Distance(Vector3.zero, target);
        var percent = 15f / distance;
        var begin = Vector3.Lerp(Vector3.zero, target, percent);
        target = Vector3.Lerp(Vector3.zero, target, 1f - percent);
        line.SetPosition(0, begin);
        var time = 0f;
        while (time < duration) {
            time += Time.deltaTime;
            var easeInOut = Easing.EaseInOut(time / duration, 2);
            line.SetPosition(1, Vector3.Lerp(begin, target, easeInOut));
            yield return null;
        }
    }

    public void ScrollUp(bool enter) {
        if (enter) {
            _ScrollDirection.y += 1;
        }
        else {
            _ScrollDirection.y -= 1;
        }
    }

    public void ScrollDown(bool enter) {
        if (enter) {
            _ScrollDirection.y -= 1;
        }
        else {
            _ScrollDirection.y += 1;
        }
    }

    public void ScrollLeft(bool enter) {
        if (enter) {
            _ScrollDirection.x -= 1;
        }
        else {
            _ScrollDirection.x += 1;
        }
    }

    public void ScrollRight(bool enter) {
        if (enter) {
            _ScrollDirection.x += 1;
        }
        else {
            _ScrollDirection.x -= 1;
        }
    }

    void OnDisable() {
        ClearTree();
    }
}