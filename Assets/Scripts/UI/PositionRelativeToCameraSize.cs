﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class PositionRelativeToCameraSize : MonoBehaviour {

    public Camera ParentCamera;
    public Vector2 Position;

    void Start() {

    }

    void Update() {
        var position = ParentCamera.ViewportToWorldPoint(Position);
        position.z = transform.position.z;
        transform.position = position;
    }
}
