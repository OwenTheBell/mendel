﻿using UnityEngine;
using System.Collections.Generic;

public class InteractionPrompt : MonoBehaviour, IUIElement {

    public UIText LabelText;
    public string Label {
        get {
            return LabelText.Text;
        }
        set {
            LabelText.Text = value;
        }
    }
    public ControlPrompt Prompt;

    public virtual GameObject GameObject { get { return gameObject; } }
    public virtual Transform Transform { get { return transform; } }
    public virtual bool Available { get; set; }
    public virtual bool Visible { get { return LabelText.Visible; } }

    void Awake() {
    }

    public virtual void Show() {
        LabelText.Show();
        Prompt.Show();
    }

    public virtual void Hide() {
        LabelText.Hide();
        Prompt.Hide();
    }
}