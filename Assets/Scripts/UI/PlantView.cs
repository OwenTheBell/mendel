﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlantView : MonoBehaviour, ICancelReciever {

    public GameObject PlantPoint;
    public TaxonomyView TaxView;
    public FamilyTreeView TreeView;
    public Material FlowerMaterial;
    public UICollection DisplayGroup;
    public UICollection RenameGroup;
    public UIText SpeciesNameField;
    public UIText SampleNumber;
    public float CycleTime;

    private int _DisplayedIdentity;
    private int _SeededIdentity;
    private bool _SeededUsed = true;
    public int SeededIdentity {
        get {
            return _SeededIdentity;
        }
        set {
            _SeededIdentity = value;
            _SeededUsed = false;
        }
    }

    private StemBuilder _StemBuilder;
    private FlowerCreator _FlowerCreator;
    private int _SpeciesIndex = -1;
    private int _DisplayIndex;              //index in array of all identities in species
    private string _SpeciesName;
    private Coroutine _DisplayRoutine;
    [SerializeField]
    private List<Button> _Buttons;

    private List<GameObject> FlowerHolders = new List<GameObject>();
    private enum DisplayMode {
        Species,
        Rename,
        FamilyTree
    }
    private DisplayMode _Mode;

    private void Awake() {
        PlantPoint.GetComponent<MeshFilter>().sharedMesh = null;
        _StemBuilder = PlantPoint.GetComponent<StemBuilder>();
        _FlowerCreator = PlantPoint.GetComponent<FlowerCreator>();
    }

    private void Start() {
    }

    public void DisplayPlant(int identity, int speciesIndex = -1) {
        var speciesName = string.Empty;
        if (speciesIndex >= 0 && speciesIndex < Services.SpeciesCatalog.Count) {
            speciesName = Services.SpeciesCatalog.Names[speciesIndex];
        }
        else {
            speciesName = Services.SpeciesCatalog.GetNameForIdentity(identity);
            speciesIndex = Services.SpeciesCatalog.Names.IndexOf(speciesName);
        }

        // if this is an unnamed species, go straight to naming mode
        _SpeciesIndex = speciesIndex;
        if (speciesName == string.Empty) {
            speciesName = PROPERTIES.NoNameMessage;
            _Mode = DisplayMode.Rename;
        }
        // otherwise display the species
        else {
            DisplayCycleButtons(identity);
        }

        _DisplayedIdentity = identity;
        _Mode = DisplayMode.Species;
        enabled = true;
        StopAllCoroutines();
        SpeciesNameField.Text = speciesName;
        DisplayGroup.Show();
        RenameGroup.Hide();
        SpeciesNameField.GetComponent<InputStringProcessor>().enabled = false;
        SpeciesNameField.Show();
        StartCoroutine(DisplayPlant(Services.MeshVault.GetPlant(_DisplayedIdentity)));
    }

    void DisplayCycleButtons(int identity) {
        if (Services.SpeciesCatalog.Identities[_SpeciesIndex].Count == 1) {
            _Buttons.Act(b => b.gameObject.SetActive(false));
            _DisplayIndex = 0;
        }
        else {
            _Buttons.Act(b => b.gameObject.SetActive(true));
            _DisplayIndex = Services.SpeciesCatalog.Identities[_SpeciesIndex].IndexOf(identity);
            SampleNumber.Text = "#" + (_DisplayIndex + 1);
            SampleNumber.Show();
        }
    }

    public void Show() {
        _Mode = DisplayMode.Species;
        if (SpeciesNameField.Text != string.Empty) {
            DisplayGroup.Show();
            enabled = true;
            SpeciesNameField.GetComponent<InputStringProcessor>().enabled = false;
            SpeciesNameField.Show();
        }
    }

    public void Hide() {
        enabled = false;
        DisplayGroup.Hide();
        RenameGroup.Hide();
        SpeciesNameField.HideAndClear();
        SampleNumber.HideAndClear();
        PlantPoint.GetComponent<MeshFilter>().sharedMesh = null;
        _SpeciesIndex = -1;
        _FlowerCreator.ClearNodes();
        StopAllCoroutines();
        _Buttons.Act(b => b.gameObject.SetActive(false));
    }

    private void Update() {
        if (_Mode == DisplayMode.Rename) {
            if (Input.GetButtonDown("Submit")) {
                SaveName();
            }
        }
    }

    public void Rename() {
        DisplayGroup.Hide();
        RenameGroup.Show();
        var input = SpeciesNameField.GetComponent<InputStringProcessor>();
        input.enabled = true;
        if (SpeciesNameField.Text == PROPERTIES.NoNameMessage) {
            SpeciesNameField.Text = string.Empty;
            input.Message = string.Empty;
            _SpeciesName = PROPERTIES.NoNameMessage;
        }
        else {
            _SpeciesName = SpeciesNameField.Text;
            input.Message = _SpeciesName;
        }
        Services.CancelStack.Add(this);
        SpeciesNameField.GetComponent<InputStringProcessor>().enabled = true;
        _Mode = DisplayMode.Rename;
    }

    public void ViewFamilyTree() {
        TreeView.ShowPlant(_DisplayedIdentity);
        TaxView.Hide();
        Hide();
    }

    public void SaveName() {
        var input = SpeciesNameField.GetComponent<InputStringProcessor>();
        if (input.Message == string.Empty || input.Message == PROPERTIES.NoNameMessage) {
            CancelNaming();
        }
        else {
            if (_SpeciesIndex >= 0) {
                Services.SpeciesCatalog.SetNameForIndex(_SpeciesIndex, input.Message);
                TaxView.ChangeSpeciesName(_SpeciesIndex, input.Message);
            }
            else {
                _SpeciesIndex = Services.SpeciesCatalog.SetNameForIdentity(_DisplayedIdentity, input.Message);
                TaxView.AddSpeciesName(_SpeciesIndex, input.Message);
            }
            SpeciesNameField.Text = input.Message;
            _SpeciesName = input.Message;
            input.enabled = false;
            Services.CancelStack.Remove(this);
        }
    }

    public void CancelNaming() {
        Services.CancelStack.Remove(this);
    }

    IEnumerator DisplayPlant(PlantMeshInfo info) {
        _StemBuilder.GrowMesh(info.Stem, 0.5f);
        _FlowerCreator.ClearNodes();
        yield return new WaitForSeconds(0.5f);
        for (var i = 0; i < info.Flowers.Count; i++) {
            _FlowerCreator.AddFlower(info.Flowers[i], info.Positions[i]);
        }
    }

    public void OnCancel() {
        DisplayGroup.Show();
        RenameGroup.Hide();
        SpeciesNameField.GetComponent<InputStringProcessor>().enabled = false;
        SpeciesNameField.Text = _SpeciesName;
        _Mode = DisplayMode.Species;
    }

    public void CyclePlant(int direction) {
        if (Services.SpeciesCatalog.Identities[_SpeciesIndex].Count > 1) {
            var indexBefore = _DisplayIndex;
            if (direction > 0) {
                _DisplayIndex++;
                if (_DisplayIndex >= Services.SpeciesCatalog.Identities[_SpeciesIndex].Count) {
                    _DisplayIndex = 0;
                }
            }
            else if (direction < 0) {
                _DisplayIndex--;
                if (_DisplayIndex < 0) {
                    _DisplayIndex = Services.SpeciesCatalog.Identities[_SpeciesIndex].Count - 1;
                }
            }
            if (indexBefore != _DisplayIndex) {
                var identity = Services.SpeciesCatalog.Identities[_SpeciesIndex][_DisplayIndex];
                var info = Services.MeshVault.GetPlant(identity);
                if (_DisplayRoutine != null) StopCoroutine(_DisplayRoutine);
                _DisplayedIdentity = identity;
               _DisplayRoutine = StartCoroutine(DisplayPlant(info));
                SampleNumber.Text = "#" + (_DisplayIndex + 1);
            }
        }
    }
}