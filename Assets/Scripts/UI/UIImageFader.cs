﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIImageFader : MonoBehaviour, IUIElement {

    public bool Available { get; set; }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }
    public bool Visible { get; private set; }

    public float HideTime;
    public bool StartHidden;

    private Image _Image;
    private float _Alpha;

    void Awake() {
        _Image = GetComponent<Image>();
        _Alpha = _Image.color.a;
    }

    void Start() {
        if (StartHidden) {
            SetImageAlpha(0f);
            Visible = false;
        }
    }

    public void Show() {
        StopAllCoroutines();
        StartCoroutine(ShowRoutine());
    }

    IEnumerator ShowRoutine() {
        var time = (_Image.color.a / _Alpha) * HideTime;
        while (time < HideTime) {
            time += Time.deltaTime;
            var easeInOut = Easing.EaseInOut(time / HideTime, 2);
            SetImageAlpha(_Alpha * easeInOut);
            yield return null;
        }
        Visible = true;
        yield return null;
    }

    public void Hide() {
        StopAllCoroutines();
        StartCoroutine(HideRoutine());
    }

    IEnumerator HideRoutine() {
        var time = (1f - _Image.color.a / _Alpha) * HideTime;
        while (time < HideTime) {
            time += Time.deltaTime;
            var easeInOut = 1f - Easing.EaseInOut(time / HideTime, 2);
            SetImageAlpha(_Alpha * easeInOut);
            yield return null;
        }
        Visible = false;
        yield return null;
    }
    
    void SetImageAlpha(float alpha) {
        var color = _Image.color;
        color.a = alpha;
        _Image.color = color;
    }
}
