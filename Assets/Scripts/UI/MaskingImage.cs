using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class MaskingImage : MonoBehaviour, IShowHideUIPiece {

	public float Duration;

	public bool Hidden { get; private set; }

	[SerializeField]
	private Image mUiImage;
	private string mMessage;

	private bool mHasInitialized = false;


	private Vector2 mSavedPosition;

	private void Awake() {
		Hidden = false;
	}

	private void Start() {
		if (!mHasInitialized) Startup();
	}

	private void Startup() {
		mHasInitialized = true;
		mSavedPosition = mUiImage.rectTransform.anchoredPosition;
	}

	void Update() {
	}

	public void Hide() {
		if (!mHasInitialized) Startup();

		Hidden = true;
		PositionImageToHide();
	}

	public void Show() {
		Hidden = false;

		PositionImageToHide();
		StartCoroutine(RevealImage());
	}

	private void PositionImageToHide() {
		var position = mUiImage.rectTransform.anchoredPosition;
		position.x += mUiImage.rectTransform.rect.width;
		mUiImage.rectTransform.anchoredPosition = position;
	}

	IEnumerator RevealImage() {
		var time = 0f;
		var start = mUiImage.rectTransform.anchoredPosition;
		while (time < Duration) {
			time += Time.deltaTime;
			var easeOut = Easing.EaseOut(time/Duration, 2);
			var position = Vector2.Lerp(start, mSavedPosition, easeOut);
			mUiImage.rectTransform.anchoredPosition = position;
			yield return 0;
		}

		yield return null;
	}
	
}