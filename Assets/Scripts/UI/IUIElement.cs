﻿using UnityEngine;

public interface IUIElement {

    bool Available { get; set; }
    bool Visible { get; }
    GameObject GameObject { get; }
    Transform Transform { get; }

    void Hide();
    void Show();
}
