﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ImageFader : MonoBehaviour, IHideableUIElement {

    public float Duration;

    public bool Visible { get; private set; }
    public bool FullVisible { get; private set; }
    public GameObject GameObject { get { return gameObject; } }
    public bool HasContent { get { return _Image != null; } }

    private Image __Image;
    private Image _Image {
        get {
            if (__Image == null) {
                __Image = GetComponent<Image>();
            }
            return __Image;
        }
        set {
            __Image = value;
        }
    }

	void Start () {
	}
	
	void Update () {
		
	}

    public void Show() {
        InstantHide();
        StartCoroutine(FadeIn(Duration));
    }

    public void InstantShow() {
        var color = _Image.color;
        color.a = 1f;
        _Image.color = color;
        Visible = true;
        FullVisible = true;
    }

    public void Hide() {
        InstantShow();
        StartCoroutine(FadeOut(Duration));
    }

    public void InstantHide() {
        var color = _Image.color;
        color.a = 0f;
        _Image.color = color;
        Visible = false;
        FullVisible = false;
    }

    IEnumerator FadeIn(float duration) {
        var time = 0f;
        Visible = true;
        while (time < duration) {
            time += Time.deltaTime;
            var color = _Image.color;
            color.a = Easing.EaseInOut(time / duration, 2);
            _Image.color = color;
            yield return 0;
        }
        FullVisible = true;
        yield return null;
    }

    IEnumerator FadeOut(float duration) {
        var time = 0f;
        FullVisible = false;
        while (time < duration) {
            time += Time.deltaTime;
            var color = _Image.color;
            color.a = 1f - Easing.EaseInOut(time / duration, 2);
            _Image.color = color;
            yield return 0;
        }
        Visible = false;
        yield return null;
    }
}
