﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

[RequireComponent(typeof(LayoutGroup))]
public class UIGroup : MonoBehaviour, IUIElement {

    public float ShowGap;
    public GameObject[] Members;
    public bool IncludeChildren;

    public bool Available { get; set; }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }
    public bool Visible {
        get {
            foreach (var e in _Elements) {
                if (e.Visible) return true;
            }
            return false;
        }
    }

    private LayoutGroup _Group;
    private List<IUIElement> _Elements;

    public void Awake() {
        _Group = GetComponent<LayoutGroup>();
        _Elements = new List<IUIElement>();
        foreach (var m in Members) {
            var element = m.GetComponent<IUIElement>();
            if (element != null) _Elements.Add(element);
        }
        if (IncludeChildren) {
            foreach (var element in GetComponentsInChildren<IUIElement>()) {
                if (element.Transform.parent == transform) {
                    _Elements.Add(element);
                }
            }
        }
    }

    public void Hide() {
        StopAllCoroutines();
        _Elements.Act(e => e.Hide());
    }

    public void Show() {
        StartCoroutine(ShowGroup());
    }

    public void HideAllExcept(IUIElement element) {
        _Elements.Act<IUIElement>( e => {
            if (e != element)
                e.Hide();
        });
    }

    IEnumerator ShowGroup() {
        _Group.enabled = false;
        _Elements.Act(e => e.Hide());
        foreach (var element in _Elements) {
            if (element.Available) {
                element.GameObject.SetActive(true);
            }
            else {
                element.GameObject.SetActive(false);
            }
        }
        yield return null;
        _Group.enabled = true;
        foreach (var element in _Elements) {
            if (element.Available) {
                element.Show();
                yield return new WaitForSeconds(ShowGap);
            }
        }
    }
}