﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(UIText))]
public class LoadingText : MonoBehaviour, IUIElement {

    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform;  } }
    public bool Visible { get { return _Text.Visible; } }
    public bool Available {
        get {
            return _Text.Available;
        }
        set {
            _Text.Available = value;
        }
    }

    public float DotTime;

    private UIText _Text;
    private int _DotsDisplayed;

    void Awake() {
        _Text = GetComponent<UIText>();
    }

    public void Show() {
        _DotsDisplayed = 0;
        StartCoroutine(ShowDots());
    }

    public void Hide() {
        StopAllCoroutines();
    }

    IEnumerator ShowDots() {
        while(!_Text.Visible) {
            yield return null;
        }

        var message = _Text.Text;

        while (true) {
            yield return new WaitForSecondsRealtime(DotTime);
            var dots = "";
            for (var i = 0; i < _DotsDisplayed; i++) {
                dots += ".";
            }
            _Text.Text = message + dots;
            if (++_DotsDisplayed > 3) {
                _DotsDisplayed = 0;
            }
        }
    }
}
