﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class QuoteDisplay : MonoBehaviour, IUIElement {

    public bool Available { get; set; }
    public bool Visible {
        get {
            return _Quote.Visible && _Author.Visible;
        }
    }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }

    public List<Quotes> QuoteBlocks;

    private UIText _Quote;
    private UIText _Author;

    void Awake() {
        _Quote = transform.Find("Quote").GetComponent<UIText>();
        _Author = transform.Find("Author").GetComponent<UIText>();
    }

    public void Hide() {
        _Quote.Hide();
        _Author.Hide();
        StopAllCoroutines();
    }

    public void Show() {
        StopAllCoroutines();
        string quote, author;
        QuoteBlocks[Random.Range(0, QuoteBlocks.Count)].GetQuote(out quote, out author);

        _Quote.Text = quote;
        if (author != string.Empty) {
            author = "- " + author;
        }
        _Author.Text = author;

        StartCoroutine(Display());
    }

    IEnumerator Display() {
        _Quote.Show();
        while (!_Quote.Visible) {
            yield return null;
        }
        _Author.Show();
    }
}
