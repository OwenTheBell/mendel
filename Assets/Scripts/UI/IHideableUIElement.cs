﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

public interface IHideableUIElement {
    bool Visible { get; }
    bool FullVisible { get; }
    bool HasContent { get; }
    UnityEngine.GameObject GameObject { get; }

    void Show();
    void Hide();

    void InstantShow();
    void InstantHide();
}
