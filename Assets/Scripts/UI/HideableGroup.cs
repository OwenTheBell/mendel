﻿using System;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HideableGroup : MonoBehaviour, IHideableUIElement {

    public bool Visible { get; protected set; }
    public bool FullVisible { get; protected set; }
    public bool HasContent { get { return _ChildrenWithContent.Count > 0; } }
    public GameObject GameObject { get { return gameObject; } }

    /*
    * get all IHideableUIElements on direct children
    * only get direct children so that then can be handled only by the parent
    */
    private List<IHideableUIElement> _HideableDirectChildren;

    private List<IHideableUIElement> _ChildrenWithContent {
        get {
            Predicate<IHideableUIElement> p = (IHideableUIElement e) => {
                return e.HasContent;
            };
            return _HideableDirectChildren.Filter<IHideableUIElement>(p);
        }
    }

    private Vector3[] _positions;
    private Quaternion[] _rotations;
    
    void Awake() {
        _HideableDirectChildren = new List<IHideableUIElement>();
        foreach (var child in GetComponentsInChildren<IHideableUIElement>()) {
            if (child.GameObject.transform.parent == transform) {
                _HideableDirectChildren.Add(child);
            }
        }
    }

	void Start () {
        _positions = new Vector3[_HideableDirectChildren.Count];
        _rotations = new Quaternion[_HideableDirectChildren.Count];
        for (var i = 0; i < _HideableDirectChildren.Count; i++) {
            _positions[i] = _HideableDirectChildren[i].GameObject.transform.localPosition;
            _rotations[i] = _HideableDirectChildren[i].GameObject.transform.localRotation;
        }
	}

    public virtual void Show() {
        StartCoroutine(ShowCoroutine());
    }

    public virtual void Hide() {
        _HideableDirectChildren.Act<IHideableUIElement>( e => e.Hide() );
        Visible = false;
        FullVisible = false;
    }

    public virtual void InstantShow() {
        _HideableDirectChildren.Act<IHideableUIElement>( e => e.InstantShow() );
        Visible = true;
        FullVisible = true;
    }

    public virtual void InstantHide() {
        _HideableDirectChildren.Act<IHideableUIElement>( e => e.InstantHide() );
        Visible = false;
        FullVisible = false;
    }

    protected virtual IEnumerator ShowCoroutine() {
        InstantHide();
        Visible = true;

        var group = GetComponent<VerticalLayoutGroup>();
        if (group != null) {
            group.enabled = false;
            yield return null;
        }

        foreach (var child in _HideableDirectChildren) {
            if (child.HasContent) {
                child.GameObject.SetActive(true);
                child.Show();
            }
            else {
                child.GameObject.SetActive(false);
            }
        }

        if (group != null) group.enabled = true;

        FullVisible = true;
    }
}
