﻿using UnityEngine;

public class HideUIOnHideAlter : MonoBehaviour {

    private void OnEnable() {
        Events.instance.AddListener<HideAlertEvent>(OnHideAlertEvent);
    }

    private void OnDisable() {
        Events.instance.RemoveListener<HideAlertEvent>(OnHideAlertEvent);
    }

    void OnHideAlertEvent(HideAlertEvent e) {
        foreach (var element in GetComponents<IUIElement>()) {
            element.Hide();
        }
    }
}