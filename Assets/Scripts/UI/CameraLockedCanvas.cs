﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Assertions;

[RequireComponent(typeof(Canvas))]
public class CameraLockedCanvas : MonoBehaviour {

    public Camera LockedCamera;

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
        var distance = Vector3.Distance(transform.localPosition, Vector3.zero);
        var frustumHeight = (2.0f * distance * Mathf.Tan(LockedCamera.fieldOfView * 0.5f * Mathf.Deg2Rad));
        var frustrumWidth = (frustumHeight * LockedCamera.aspect);
        GetComponent<RectTransform>().sizeDelta = new Vector2(
            frustrumWidth / transform.localScale.x,
            frustumHeight / transform.localScale.y
        );
    }
}