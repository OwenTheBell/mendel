﻿using UnityEngine;
using UnityEngine.Audio;

[RequireComponent(typeof(ToggleText))]
public class UpdateToggleForMixerSnapshot : MonoBehaviour {

    public AudioMixerController MixerController;
    public AudioBanks bank;

    private ToggleText _ToggleText;

    private void Awake() {
        _ToggleText = GetComponent<ToggleText>();
    }

    private void OnEnable() {
        var isActive = MixerController.GetBank(bank);
        if ((isActive && !_ToggleText.IsOn) || (!isActive && _ToggleText.IsOn)) {
            _ToggleText.ToggleState();
        }
    }

    public void ToggleBank() {
        MixerController.ToggleBank(bank);
    }
}
