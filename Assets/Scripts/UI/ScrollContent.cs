﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;

[RequireComponent(typeof(ScrollRect))]
public class ScrollContent : MonoBehaviour {

    private ScrollRect _Scroll;

    private void Awake() {
        _Scroll = GetComponent<ScrollRect>();
    }

    public void FixedUpdate() {
        var scrollValue = Input.GetAxis("Mouse ScrollWheel");
        if (scrollValue != 0) {
            _Scroll.verticalScrollbar.value += scrollValue * _Scroll.scrollSensitivity * Time.fixedDeltaTime * _Scroll.verticalScrollbar.size;
        }
    }
}