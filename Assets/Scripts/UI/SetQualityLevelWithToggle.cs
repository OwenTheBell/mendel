﻿using UnityEngine;

[RequireComponent(typeof(ToggleText))]
public class SetQualityLevelWithToggle : MonoBehaviour {

    private ToggleText _ToggleText;
    private int _QualityLevel;

    private void Awake() {
        _ToggleText = GetComponent<ToggleText>();
    }

    private void OnEnable() {
        _QualityLevel = QualitySettings.GetQualityLevel();
        if (_QualityLevel == 0 && _ToggleText.IsOn) {
            _ToggleText.ToggleState();
        }
        else if (_QualityLevel == 1 && !_ToggleText.IsOn) {
            _ToggleText.ToggleState();
        }
    }

    public void ToggleQuality() {
        if (_QualityLevel == 0) {
            _QualityLevel++;
        }
        else {
            _QualityLevel--;
        }
        QualitySettings.SetQualityLevel(_QualityLevel);
    }
}