public interface IUIPrompt {
	UnityEngine.Sprite Sprite { get; set; }
	string Message { get; set; }
	bool Visible { get; }
	bool HasMessage { get; }

	void Appear();
	void Hide();
	void Clear();
}