﻿using UnityEngine;

public class NamingCanvas : ControlCanvas {

    public string Name {
        get {
            return _InputProcessor.Message;
        }
        set {
            _InputProcessor.Message = value;
        }
    }
    public UIText SavePrompt;
    public UIText ExitPrompt;
    
    private InputStringProcessor _InputProcessor;

    protected override void Awake() {
        base.Awake();
        _InputProcessor = GetComponentInChildren<InputStringProcessor>();
    }
}