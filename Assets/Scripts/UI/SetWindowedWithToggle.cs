﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(ToggleText))]
public class SetWindowedWithToggle : MonoBehaviour {

    private ToggleText _ToggleText;

    private void Awake() {
        _ToggleText = GetComponent<ToggleText>();
    }

    private void OnEnable() {
        if (Screen.fullScreen && _ToggleText.IsOn) {
            _ToggleText.ToggleState();
        }
        else if (!Screen.fullScreen && !_ToggleText.IsOn) {
            _ToggleText.ToggleState();
        }
    }

    public void ToggleWindowed() {
        Screen.fullScreen = !Screen.fullScreen;
        StartCoroutine(DelayCursorReset());
    }

    IEnumerator DelayCursorReset() {
        Cursor.lockState = CursorLockMode.None;
        yield return new WaitForSeconds(0.1f);
        Cursor.lockState = CursorLockMode.Confined;
    }
}
