﻿using UnityEngine;
using System.Collections;
using TMPro;

[RequireComponent(typeof(TMP_Text))]
public class UIText : MonoBehaviour, IUIElement {

    public float Speed;
    public float HideTime;
    public bool StartHidden;
    public bool ClearStartingText;

    public bool Available { get; set; }
    public bool Visible { get; private set; }
    public bool Typing { get; private set; }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }

    private string _Text;
    public string Text {
        get {
            return _Text;
        }
        set {
            _Text = value;
            if (Visible) {
                _TMP_Text.text = value;
            }
        }
    }

    private TMP_Text _TMP_Text;
    private bool _ClearAfterHide;

    void Awake() {
        _TMP_Text = GetComponent<TMP_Text>();
        Available = true;
        Visible = false;

        if (ClearStartingText) {
            _Text = string.Empty;
        }
        else {
            _Text = _TMP_Text.text;
        }
        if (StartHidden) {
            _TMP_Text.text = string.Empty;
        }
    }

    public void HideAndClear() {
        _ClearAfterHide = true;
        Hide();
    }

    public void Hide() {
        StopAllCoroutines();
        if (gameObject.activeInHierarchy)
            StartCoroutine(HideText());
    }

    public void Show() {
        StopAllCoroutines();
        if (gameObject.activeInHierarchy)
            StartCoroutine(TypeText());
    }

    IEnumerator TypeText() {
        if (Text != string.Empty) {
            Visible = false;
            Typing = true;
            var time = 0f;
            var duration = _Text.Length / Speed;
            while (time < duration) {
                time += Time.deltaTime;
                var percent = Mathf.Clamp01(time / duration);
                _TMP_Text.text = _Text.Substring(0, (int)(_Text.Length * percent));
                yield return null;
            }
        }
        Visible = true;
        Typing = false;
        _TMP_Text.text = _Text;
    }

    IEnumerator HideText() {
        if (Text != string.Empty) {
            Typing = true;
            var time = _TMP_Text.text.Length / _Text.Length * HideTime;
            while (time > 0f) {
                time -= Time.deltaTime;
                var percent = Mathf.Clamp01(time / HideTime);
                _TMP_Text.text = _Text.Substring(0, (int)(_Text.Length * percent));
                yield return null;
            }
        }
        Visible = false;
        Typing = false;
        _TMP_Text.text = string.Empty;
        if (_ClearAfterHide) {
            Text = string.Empty;
            _ClearAfterHide = false;
        }
    }
}
