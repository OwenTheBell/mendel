using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TypingTextDisplay : MonoBehaviour, IShowHideUIPiece {

	public float speed = 0.1f;

	public bool Hidden { get; private set; }

	private Text mUiText;
	private string mMessage;

	private bool mHasInitialized = false;

	private void Awake() {
		Hidden = false;
	}

	private void Start() {
		if (!mHasInitialized) Startup();
	}

	private void Startup() {
		mHasInitialized = true;
		mUiText = GetComponent<Text>();
		mMessage = mUiText.text;
	}

	public void Hide() {
		if (!mHasInitialized) Startup();

		Hidden = true;
		GetComponent<CanvasRenderer>().SetAlpha(0f);
	}

	public void Show() {
		Hidden = false;
		GetComponent<CanvasRenderer>().SetAlpha(1f);
		mMessage = mUiText.text;
		StartCoroutine(WriteText(mMessage));
	}
	
	IEnumerator WriteText(string text) {
		mUiText.text = "";
		var time = 0f;
		var duration = text.Length * speed;
		while (time < duration) {
			time += Time.deltaTime;
			var percent = Mathf.Clamp01(time/duration);
			mUiText.text = text.Substring(0, (int)(text.Length * percent));
			yield return 0;
		}
		mUiText.text = text;
		yield return null;
	}
}