﻿using UnityEngine;
using System.Collections.Generic;

public class ControlCanvas : MonoBehaviour, IUIElement {

    public virtual GameObject GameObject { get { return gameObject; } }
    public virtual Transform Transform { get { return transform; } }
    public virtual bool Available { get; set; }
    public virtual bool Visible { get { return _Group.Visible; } }

    private List<ControlPrompt> _Prompts;
    private UIGroup _Group;
    private List<IUIElement> _ImmediateChildren;
    private float _Scale;

    protected virtual void Awake() {
        _Prompts = new List<ControlPrompt>(GetComponentsInChildren<ControlPrompt>());
        _Group = GetComponentInChildren<UIGroup>();
        _ImmediateChildren = new List<IUIElement>();
        foreach (var child in GetComponentsInChildren<IUIElement>()) {
            if (child.Transform.parent == transform) {
                _ImmediateChildren.Add(child);
            }
        }
        _Scale = transform.localScale.x;
    }

    protected virtual void Start() {
        Hide();
    }

    protected virtual void Update() {
		if (Visible) {
			SetScale();
		}
    }

    public virtual void Show() {
        SetScale();
        _Group.Show();
    }
    public virtual void Hide() {
        _Group.Hide();
    }

    public virtual void HideAllExecpt(IUIElement element) {
        _Group.HideAllExcept(element);
    }

    public void ClearPrompts() {
        _Prompts.Act(p => p.Available = false);
    }

    public ControlPrompt GetPrompt(ActionType type) {
        foreach (var prompt in _Prompts) {
            if (prompt.ActionType == type) {
                return prompt;
            }
        }
        return null;
    }

    void SetScale() {
        var distance = Vector3.Distance(transform.position, Camera.main.transform.position);
        var percent = Mathf.Clamp((distance - 0.25f) / 1.5f, 0.33f, 1f);
        transform.localScale = Vector3.one * _Scale * percent;
    }
}