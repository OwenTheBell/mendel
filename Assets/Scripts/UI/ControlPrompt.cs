﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ControlPrompt : MonoBehaviour, IUIElement {

    public ActionType ActionType;
    public Image NegativeImage;
    public bool AvailableAtStart = false;

    private UIText _Text;
    public string Text {
        get { return _Text.Text; }
        set { _Text.Text = value; }
    }

    public bool Negative {
        get {
            return NegativeImage.gameObject.activeSelf;
        }
        set {
            NegativeImage.gameObject.SetActive(value);
        }
    }

    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }
    public bool Available { get; set; }
    public bool Visible { get { return _Text.Visible; } }

    private Animator _Animator;

    void Awake() {
        Available = AvailableAtStart;
        _Text = GetComponentInChildren<UIText>();
        _Animator = GetComponent<Animator>();
        NegativeImage.gameObject.SetActive(false);
    }

    public void Show() {
        _Animator.SetBool("Show", true);
        _Text.Show();
    }

    public void Hide() {
        _Animator.SetBool("Show", false);
        _Text.Hide();
    }
}

public enum ActionType {
    Action1,
    Action2,
    Action3,
    Action4
}