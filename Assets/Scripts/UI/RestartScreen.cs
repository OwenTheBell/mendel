﻿using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;

public class RestartScreen : MonoBehaviour, ICancelReciever {

    public PauseScreen Pause;
    public string NewGameScene;
    private List<IUIElement> _UIElements;

    void Awake() {
        _UIElements = new List<IUIElement>(GetComponentsInChildren<IUIElement>());
        _UIElements = _UIElements.Filter(g => { return g.Transform.parent == transform; });
    }

    void Start() {
        _UIElements.Act(g => StartCoroutine(HideGroup(g)));
    }

    public void Show() {
        Services.CancelStack.RemoveAll();
        Services.CancelStack.Add(this);
        _UIElements.Act(e => {
            e.GameObject.SetActive(true);
            e.Show();
        });
        Events.instance.Raise(new HideAlertEvent());
    }

    public void Exit() {
        Services.CancelStack.Remove(this); 
    }

    public void Restart() {
        Services.SaveLoad.ClearSave();
        Services.SpeciesCatalog.Clear();
        Services.MeshVault.Clear();
        Services.PlantInfoCatalog.Clear();
        SceneManager.LoadScene(NewGameScene);
    }

    public void OnCancel() {
        EventSystem.current.SetSelectedGameObject(null);
        Events.instance.Raise(new UnHideAlertEvent());
        _UIElements.Act(g => StartCoroutine(HideGroup(g)));
        Pause.OnPause();
    }

    IEnumerator HideGroup(IUIElement e) {
        e.Hide();
        while (e.Visible) yield return null;
        e.GameObject.SetActive(false);
    }
}
