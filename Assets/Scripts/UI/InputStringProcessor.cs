using UnityEngine;

public class InputStringProcessor : MonoBehaviour {

    public float BlinkSpeed;

    private string _Message;
    public string Message {
        get { return _Message; }
        set {
            _Message = value;
            _Text.Text = value;
        }
    }

    private UIText _Text;
	public int MaxCharacters;

    private float _BlinkTime;
    private bool _BlinkToggle;
    private AudioSource[] _Sources;
    private int _LastSource;

	public void Awake() {
        _Text = GetComponent<UIText>();
        _BlinkTime = 0f;
        _BlinkToggle = false;
        _Message = string.Empty;
        _LastSource = -1;
        _Sources = GetComponents<AudioSource>();
	}

	public void Update() {
        if (!_Text.Visible || _Text.Typing) return;

        if (Input.inputString.Length > 0) {
            foreach (char c in Input.inputString) {
                /* handle backspace */
                if (c == "\b"[0]) {
                    if (Message.Length > 0) {
                        Message = Message.Substring(0, Message.Length - 1);
                    }
                }
                /* catch enter key presses and ignore them */
                else if (c == "\n"[0] || c == "\r"[0]) {
                }
                else if (Message.Length < MaxCharacters) {
                    Message += c;
                }
            }
            if (_Sources.Length > 0) {
                var index = Random.Range(0, _Sources.Length  - 1);
                if (index >= _LastSource) index++;
                _Sources[index].Play();
                _LastSource = index;
            }
        }

        _BlinkTime += Time.deltaTime;
        if (_BlinkTime > BlinkSpeed) {
            _BlinkToggle = !_BlinkToggle;
            _BlinkTime = 0f;
        }

        if (_BlinkToggle) {
            _Text.Text = Message + "_";
        }
        else {
            _Text.Text = Message;
        }
	}
}
