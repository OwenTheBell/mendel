﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HowToPlayCanvas : MonoBehaviour {

    public float FadeDelay;

    private bool _Visible;

    private void Awake() {
        _Visible = false;
    }

    void Start () {
	}
	
	void Update () {
        if (!_Visible) {
            return;
        }
        if (
                Input.GetKey(KeyCode.W) ||
                Input.GetKey(KeyCode.S) ||
                Input.GetKey(KeyCode.A) ||
                Input.GetKey(KeyCode.D)
        ) {
            _Visible = false;
            StartCoroutine(Hide(FadeDelay));
        }
	}

    private void OnEnable() {
        Events.instance.AddListener<NewGameEvent>(OnNewGameEvent);
        Events.instance.AddListener<HideAlertEvent>(OnHideAlertEvent);
    }

    private void OnDisable() {
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
        Events.instance.RemoveListener<NewGameEvent>(OnNewGameEvent);
        Events.instance.RemoveListener<HideAlertEvent>(OnHideAlertEvent);
    }
    
    private void OnDestroy() {
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
        Events.instance.RemoveListener<NewGameEvent>(OnNewGameEvent);
        Events.instance.RemoveListener<HideAlertEvent>(OnHideAlertEvent);
    }

    void OnNewGameEvent(NewGameEvent e) {
        Events.instance.RemoveListener<NewGameEvent>(OnNewGameEvent);
        Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStartedEvent);
    }

    void OnPlayerStartedEvent(PlayerStartedEvent e) {
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
        GetComponent<Animator>().SetBool("Show", true);
        foreach (var text in GetComponentsInChildren<UIText>()) {
            text.Show();
        }
        _Visible = true;
    }

    void OnHideAlertEvent(HideAlertEvent e) {
        StopAllCoroutines();
        StartCoroutine(Hide(0f));
    }

    IEnumerator Hide(float delay) {
        yield return new WaitForSeconds(delay);
        GetComponent<Animator>().SetBool("Show", false);
        foreach (var text in GetComponentsInChildren<UIText>()) {
            text.Hide();
        }
    }
}