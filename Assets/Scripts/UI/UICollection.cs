﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class UICollection : MonoBehaviour, IUIElement {

    public float ShowGap;

    public bool Available { get; set; }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }
    public bool Visible {
        get {
            foreach (var e in Elements) {
                if (e.Visible) return true;
            }
            return false;
        }
    }

    [HideInInspector]
    public List<IUIElement> Elements; 

    void Awake() {
        Elements = new List<IUIElement>();
        foreach (var e in GetComponentsInChildren<IUIElement>()) {
            if (e.Transform.parent == transform) {
                Elements.Add(e);
            }
        }
    }

    public void Hide() {
        StopAllCoroutines();
        //_Elements.Act(e => e.Hide());
        if (gameObject.activeSelf) StartCoroutine(HideRoutine());
    }

    public void Show() {
        StopAllCoroutines();
        Elements.Act(e => {
            e.GameObject.SetActive(true);
            e.Show();
        });
    }

    IEnumerator ShowRoutine() {
        foreach (var e in Elements) {
            if (e.Available) {
                e.GameObject.SetActive(true);
                e.Show();
                yield return new WaitForSeconds(ShowGap);
            }
        }
    }

    IEnumerator HideRoutine() {
        Elements.Act(e => e.Hide());
        while (Visible) yield return null;
        Elements.Act(e => e.GameObject.SetActive(false));
    }
}