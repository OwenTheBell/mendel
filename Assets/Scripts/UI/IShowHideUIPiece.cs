public interface IShowHideUIPiece {
	bool Hidden { get; }

	void Hide();
	void Show();
}