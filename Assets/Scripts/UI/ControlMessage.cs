﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using TMPro;

public class ControlMessage : MonoBehaviour, IUIMessage {

	private bool _Visible;
	public bool Visible {
		get {
			return _Visible;
		}
		set {
			if (value && !_Visible) {
				Appear();
			}
			else if (!value && _Visible) {
				Hide();
			}
			_Visible = value;
		}
	}

	public string Message {
		get {
			return Text.text;
		}
		set {
			Text.text = value;
		}
	}

	private Image __Image;
	public Image Image {
		get {
			if (__Image == null) {
				__Image = GetComponentInChildren<Image>();
			}
			return __Image;
		}
		private set {
			__Image = value;
		}
	}
	private TMP_Text __Text;
	public TMP_Text Text {
		get {
			if (__Text == null) {
				__Text = GetComponentInChildren<TMP_Text>();
			}
			return __Text;
		}
		private set {
			__Text = value;
		}
	}
	// public ITextDisplay TextDisplayer { get; private set; }

	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Appear() {
		Image.gameObject.SetActive(true);
		Text.gameObject.SetActive(true);
		// TextDisplayer.DisplayString();
	}

	void Hide() {
		Image.gameObject.SetActive(false);
		Text.gameObject.SetActive(false);
		// TextDisplayer.Hide();
	}

	public void Clear() {
		Visible = false;
		Text.text = "";
	}
}
