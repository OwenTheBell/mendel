﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class UIMenuEntry : MonoBehaviour, IUIElement {

    public bool Available {
        get { return _Text.Available;  }
        set {
            Button.interactable = value;
            _Text.Available = value;
        }
    }
    public bool Visible { get { return _Text.Visible; } }
    public GameObject GameObject { get { return gameObject; } }
    public Transform Transform { get { return transform; } }

    public Button Button;

    private UIText _Text;
    public string Text {
        get {
            return _Text.Text;
        }
        set {
            _Text.Text = value;
        }
    }

    void Awake() {
        _Text = GetComponentInChildren<UIText>();
        Button = GetComponent<Button>();
    }

    public void Hide() {
        Button.enabled = false;
        _Text.Hide();
    }
    
    public void Show() {
        Button.enabled = true;
        _Text.Show();
    }
}