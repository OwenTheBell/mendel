﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using System.Collections;

public class TaxonomyView : MonoBehaviour {

    public string EmptyDisplay;
    [SerializeField]
    private PlantView _PlantView;
    public GameObject MenuSlotPrefab;

    private UIMenuEntry _SelectedSlot;
    private int _CurrentSlot;
    private int _TopIndex;
    private List<string> _Values;
    private bool _NothingToDisplay;
    private UICollection _Collection;
    private ScrollRect _Scroll;

    public int Count { get { return _Collection.Elements.Count;  } }

    private void Awake() {
        _Collection = GetComponentInChildren<UICollection>();
        _Scroll = GetComponentInChildren<ScrollRect>();
    }

    public void Show() {
        _Collection.gameObject.SetActive(true);
        _Collection.Show();
        _SelectedSlot = null;
        enabled = true;
    }

    public void Hide() {
        if (gameObject.activeSelf) StartCoroutine(HideRoutine());
        EventSystem.current.SetSelectedGameObject(null);
        enabled = false;
    }

    IEnumerator HideRoutine() {
        _Collection.Hide();
        while (_Collection.Visible) yield return null;
        _Collection.gameObject.SetActive(false);
    }

    public void SetValues(List<string> values) {
        _Values = new List<string>(values);
        _Values.Sort((string x, string y) => {
            return string.Compare(x, y);
        });
        var slotIndex = 0;
        for (slotIndex = 0; slotIndex < _Values.Count; slotIndex++) {
            UIMenuEntry entry;
            if (slotIndex < _Collection.Elements.Count) {
                entry = _Collection.Elements[slotIndex] as UIMenuEntry;
            }
            else {
                entry = CreateSlot();
            }
            entry.Text = _Values[slotIndex];
            entry.GetComponent<Button>().onClick.AddListener(() =>{
                SelectSlot(entry);
            });
            entry.Available = true;
        }
        if (_Values.Count == 0) {
            UIMenuEntry entry;
            if (_Collection.Elements.Count > 0) {
                entry = _Collection.Elements[slotIndex] as UIMenuEntry;
            }
            else {
                entry = CreateSlot();
            }
            entry.Text = EmptyDisplay;
            entry.Available = true;
            entry.Button.interactable = false;
            _NothingToDisplay = true;
            slotIndex++;
        }
        else {
            _NothingToDisplay = false;
        }
        for (var i = slotIndex; i < _Collection.Elements.Count; i++) {
            var entry = _Collection.Elements[i];
            entry.Available = false;
        }
        var height = _Collection.Elements[0].GameObject.GetComponent<RectTransform>().rect.height;
        var contentHeight = _Scroll.content.rect.height;
        var sizeDelta = _Scroll.content.sizeDelta;
        _Scroll.content.sizeDelta = new Vector2(sizeDelta.x, sizeDelta.y * (height * slotIndex + 10f) / contentHeight);
        _TopIndex = 0;
    }

    public UIMenuEntry CreateSlot() {
        var slot = Instantiate(MenuSlotPrefab).GetComponent<UIMenuEntry>();
        slot.transform.parent = _Scroll.content;
        slot.transform.localScale = Vector3.one;
        var rectTransform = slot.GetComponent<RectTransform>();
        var height = rectTransform.rect.height;
        rectTransform.anchoredPosition3D = new Vector3(0, -height * _Collection.Elements.Count, 0f);
        _Collection.Elements.Add(slot);
        return slot;
    }

    public void SelectSlot(UIMenuEntry slot) {
        if (slot == _SelectedSlot) return;

        var index = _Collection.Elements.IndexOf(slot) + _TopIndex;
        var speciesIndex = Services.SpeciesCatalog.GetSpeciesIndexForName(_Values[index]);
        _PlantView.DisplayPlant(Services.SpeciesCatalog.Identities[speciesIndex][0], speciesIndex);
        HighlightSlot(slot);
    }

    public void ChangeSpeciesName(int index, string name) {
        SetValues(Services.SpeciesCatalog.Names);
        _Collection.Show();
        SelectSpecies(name);
    }
    
    public void AddSpeciesName(int index, string name) {
        _Values.Insert(index, name);
        SetValues(_Values);
        _Collection.Show();
        SelectSpecies(name);
    }

    public void SelectSpecies(string name, int identity = -1) {
        var index = -1;
        var speciesIndex = -1;
        for (var i = 0; i < _Values.Count; i++) {
            if (_Values[i] == name) {
                index = i;
                if (identity == -1) {
                    speciesIndex = Services.SpeciesCatalog.GetSpeciesIndexForName(name);
                    identity = Services.SpeciesCatalog.Identities[speciesIndex][0];
                }
                break;
            }
        }
        if (index < 0) {
            if (identity >= 0) {
                _PlantView.DisplayPlant(identity);
            }
            return;
        }

        var entry = _Collection.Elements[index];
        var entryRectT = entry.GameObject.GetComponent<RectTransform>();
        var entryY = Mathf.Abs(entryRectT.anchoredPosition.y);
        var content = _Scroll.content;

        var scrollHeight = _Scroll.GetComponent<RectTransform>().rect.height;
        var contentHeight = content.rect.height;
        var heightDiff = contentHeight - scrollHeight;
        // entry is too low to be set the top of the scoll
        if (heightDiff - entryY < 0f) {
            StartCoroutine(DeferedValue(0f));
        }
        // entry is too high to tbe set to the top of the scroll
        else if (scrollHeight - entryY <= 0f) {
            StartCoroutine(DeferedValue(1f));
        }
        else {
            StartCoroutine(DeferedValue(1f - entryY / heightDiff));
        }

        _PlantView.DisplayPlant(identity, speciesIndex);
        HighlightSlot(entry as UIMenuEntry);
    }

    void HighlightSlot(UIMenuEntry slot) {
        if (_SelectedSlot != null) {
            _SelectedSlot.GetComponent<Animator>().SetTrigger("Deselect");
        }
        _SelectedSlot = slot;
        _SelectedSlot.GetComponent<Animator>().SetTrigger("Pressed");
    }

    IEnumerator DeferedValue(float value) {
        yield return null;
        _Scroll.verticalScrollbar.value = value;
    }
}