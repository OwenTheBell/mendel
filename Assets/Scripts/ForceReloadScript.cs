﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class ForceReloadScript : MonoBehaviour {

    public string SceneName;
    public KeyCode TriggerKey;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
	    if (Input.GetKeyDown(TriggerKey)) {
            SceneManager.LoadScene(SceneName);
        }
	}
}
