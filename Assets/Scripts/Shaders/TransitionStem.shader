﻿Shader "Unlit/TransitionStem"
{
	Properties
	{
		_WindNoise("Noise", 2D) = "white" {}
		_WindSpeed("Wind Speed", float) = 1
		_WindForce("Wind Force", float) = 1
		_DistanceAdjust("Distance Adjust", float) = 1
		_Offset("Offset", float) = 0
		_WindDirection("Wind Direction", Range(0, 360)) = 0
		_Percent("Percent", Range(0, 1)) = 0
	}
		SubShader
	{
		Tags{ "RenderType" = "Opaque" }
		LOD 100

		Pass
	{
		CGPROGRAM
#pragma vertex vert
#pragma fragment frag
		// make fog work
#pragma multi_compile_fog

#include "UnityCG.cginc"

		struct appdata
	{
		float4 vertex : POSITION;
		float2 uv : TEXCOORD0;
		float4 normal : NORMAL;
		float4 tangent : TANGENT;
		float4 color : COLOR;
	};

	struct v2f
	{
		float4 vertex : SV_POSITION;
		float4 color : COLOR;
	};

	float4 _WindNoise;
	float _WindSpeed;
	float _WindForce;
	float _DistanceAdjust;
	float _Offset;
	float _WindDirection;
	float _Percent;

	float4 rotateAroundLine(float4 vertex, float4 center, float4 direction, float theta) {
		// get the three values for the position of the line we're rotating around
		float a = center.x;
		float b = center.y;
		float c = center.z;
		// get the three values of the direction for line we're rotating around
		float u = direction.x;
		float v = direction.y;
		float w = direction.z;

		float4x4 rotationMatrix;
		// apply a matrix transform to rotate the vertex around center
		rotationMatrix[0][0] = u*u + (v*v + w*w) * cos(theta);
		rotationMatrix[0][1] = u*v * (1 - cos(theta)) + w * sin(theta);
		rotationMatrix[0][2] = u*w * (1 - cos(theta)) - v * sin(theta);
		rotationMatrix[0][3] = 0;
		rotationMatrix[1][0] = u*v * (1 - cos(theta)) - w * sin(theta);
		rotationMatrix[1][1] = v*v + (u*u + w*w) * cos(theta);
		rotationMatrix[1][2] = v*w * (1 - cos(theta)) + u * sin(theta);
		rotationMatrix[1][3] = 0;
		rotationMatrix[2][0] = u*w * (1 - cos(theta)) + v * sin(theta);
		rotationMatrix[2][1] = v*w * (1 - cos(theta)) - u * sin(theta);
		rotationMatrix[2][2] = w*w + (u*u + v*v) * cos(theta);
		rotationMatrix[2][3] = 0;
		rotationMatrix[3][0] = (a * (v*v + w*w) - u * (b*v + c*w)) *
			(1 - cos(theta)) + (b*w - c*v) * sin(theta);
		rotationMatrix[3][1] = (b * (u*u + w*w) - v * (a*u + c*w)) *
			(1 - cos(theta)) + (c*u - a*w) * sin(theta);
		rotationMatrix[3][2] = (c * (u*u + v*v) - w * (a*u + b*v)) *
			(1 - cos(theta)) + (a*v - b*u) * sin(theta);
		rotationMatrix[3][3] = 1;
		return mul(vertex, rotationMatrix);
	}

	v2f vert(appdata v) {
		v2f o;

		float4 meshCenter = v.normal;
		float4 worldVertex = mul(unity_ObjectToWorld, v.vertex);
		float height = worldVertex.y - meshCenter.y;
		float offset = _Offset * height / _DistanceAdjust;
		float theta = sin(_Time[0] * _WindSpeed + offset) * _WindForce * height / _DistanceAdjust * _Percent;
		float windAngle = _WindDirection * (3.14159 / 180);
		float4 direction = float4(cos(windAngle), 0, sin(windAngle), 0);
		worldVertex = rotateAroundLine(worldVertex, meshCenter, direction, theta);
		worldVertex = mul(unity_WorldToObject, worldVertex);

		o.vertex = UnityObjectToClipPos(worldVertex);
		o.color = v.color;
		return o;
	}

	fixed4 frag(v2f i) : SV_Target{
		return i.color;
	}
		ENDCG
	}
	}
}
