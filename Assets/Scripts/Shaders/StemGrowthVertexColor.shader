// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/StemGrowthVertexColor"
{
	Properties
	{
        _StartTime("Start Time", float) = 0
        _TotalTime("Total Time", float) = 0
        _HeightTime("Height Growth Time", Range(0, 1)) = 0.5
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "DisableBatching" = "True" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;   // target vertex position
				float4 normal : NORMAL;     // ungrown vertex
				float4 tangent : TANGENT;   // halfgrown vertex
				float4 color : COLOR;
				float2 uv : TEXCOORD2;      // x: start time, y: end time
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

            float _StartTime;
            float _TotalTime;
			
			v2f vert (appdata v) {

				float start = v.uv.x * _TotalTime + _StartTime;
				float end = v.uv.y * _TotalTime + _StartTime;
				float time = end - start;

				float delta = (_Time.y - start) / time;
				delta = clamp(delta, 0.0, 1.0);

				float heightDelta = clamp(delta, 0.0, 0.5) * 2;
				float widthDelta = (clamp(delta, 0.5, 1) - 0.5) * 2;
				
				float3 halfway = lerp(v.tangent, v.normal, heightDelta);
				v.vertex.xyz = lerp(halfway.xyz, v.vertex.xyz, widthDelta);

				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return i.color;
			}
			ENDCG
		}
	}
}
