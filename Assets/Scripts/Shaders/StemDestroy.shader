﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/StemGrowthVertexColor" {
	Properties {
        _Percent("Percent", Range(0, 1)) = 0
        _HeightShare("Height Growth Percent", Range(0, 1)) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "DisableBatching" = "True" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
				float4 tangent : TANGENT;
				float4 color : COLOR;
				float2 uv : TEXCOORD2;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

            float _Percent;
            float _HeightShare;
			
			v2f vert (appdata v) {

                float delta = (_Percent - v.uv.x) / (v.uv.y - v.uv.x);
				delta = clamp(delta, 0.0, 1.0);

                float heightShare = 0.5;
				float heightDelta = clamp(delta/heightShare, 0.0, 1.0);
				float widthDelta = clamp((delta - heightShare)/(1 - heightShare), 0.0, 1.0);
				
				float3 halfway = lerp(v.tangent, v.normal, heightDelta);
				v.vertex.xyz = lerp(halfway.xyz, v.vertex.xyz, widthDelta);

				v2f o;
				o.vertex = UnityObjectToClipPos(v.vertex);
				o.color = v.color;
				UNITY_TRANSFER_FOG(o,o.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return i.color;
			}
			ENDCG
		}
	}
}
