Shader "Custom/Colored Vertex Simple Lambert" {
    Properties {
        _MainTex("Texture", 2D) = "white" {}
        _MainTint("Global Color Tint", Color) = (1, 1, 1, 1)
    }
    SubShader {
        Tags {"RenderType" = "Opaque"}
        CGPROGRAM

        #pragma surface surf Lambert vertex:vert

        struct Input {
            float2 uv_MainTex;
            float4 vertColor;
        };

        sampler2D _MainTex;
        float4 _MainTint;

        void surf(Input IN, inout SurfaceOutput o) {
            //o.Albedo = tex2D(_MainTex, IN.uv_MainTex).rgb;
            o.Albedo = IN.vertColor.rgb * _MainTint.rgb;
        }

        half4 LightingSimpleLambert (SurfaceOutput s, half3 lightDir, half atten) {
            half NdotL = dot(s.Normal, lightDir);
            half4 c;
            c.rgb = s.Albedo * _LightColor0.rgb * (NdotL * atten * 2);
            c.a = s.Alpha;
            return c;
        }

        void vert(inout appdata_full v, out Input o) {
            UNITY_INITIALIZE_OUTPUT(Input, o);
            o.vertColor = v.color;
        }

        ENDCG
    }
    Fallback "Diffuse"
}
