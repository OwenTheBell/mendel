﻿Shader "Custom/Foam Test" {
	Properties {
        _Color("Color", Color) = (1, 1, 1, 1)
        _EdgeColor("Edge Color", Color) = (1, 1, 1, 1)
        _DepthFactor("Depth Factor", float) = 1.0
	}
	SubShader { Pass {
		Tags { "RenderType"="Opaque" }
		LOD 200

		CGPROGRAM

        #include "UnityCG.cginc"

        #pragma vertex vert
        #pragma fragment frag

        sampler2D _CameraDepthTexture;
        float4 _Color;
        float4 _EdgeColor;
        float _DepthFactor;

        struct vertexInput {
            float4 vertex : POSITION;
        };

        struct vertexOutput {
            float4 pos : SV_POSITION;
            float4 screenPos : TEXCOORD1;
        };

        vertexOutput vert(vertexInput input) {
            vertexOutput output;
            output.pos = UnityObjectToClipPos(input.vertex);
            output.screenPos = ComputeScreenPos(output.pos);
            return output;
        }

        float4 frag(vertexOutput input) : COLOR {
            float4 depthSample = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, input.screenPos);
            float depth = LinearEyeDepth(depthSample).r;
            float foamLine = 1 - saturate(_DepthFactor * (depth - input.screenPos.w));
            if (foamLine > 0.5f) return _EdgeColor;
            return _Color;
            float4 color = _Color + foamLine * _EdgeColor;
            return color;
        }

		ENDCG
	}}
}
