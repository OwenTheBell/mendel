﻿Shader "Unlit/Destroy Flower"
{
	Properties
	{
        _StartTime ("Start Time", float) = 0
        _TotalTime ("Total Time", float) = 0
        _PerPetalPercent ("Per Petal Percent", float) = 0
        _MaxPetalGap ("Max Petal Gap", float) = 0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" "DisableBatching" = "True" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv3 : TEXCOORD2;
                float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
                float4 color : COLOR;
			};

            float _StartTime;
            float _PerPetalPercent;
            float _TotalTime;
            float _MaxPetalGap;
			
			v2f vert (appdata v)
			{
				float petalDestroyTime = _TotalTime * _PerPetalPercent;
				float delay = v.uv3.x / v.uv3.y * petalDestroyTime;
				float maxDelay = v.uv3.x * _MaxPetalGap;
				delay = clamp(delay, 0.0, maxDelay);

                float delta = (_Time.y - delay - _StartTime) / petalDestroyTime;
                delta = clamp(delta, 0.0, 1.0);
                float easeIn = clamp(delta, 0.0, 1.0);
                float4 vertex = lerp(v.vertex, float4(0, 0, 0, 0), easeIn);

				v2f o;
				o.vertex = UnityObjectToClipPos(vertex);
                o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return i.color;
			}
			ENDCG
		}
	}
}