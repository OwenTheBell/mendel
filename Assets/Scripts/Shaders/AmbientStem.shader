﻿Shader "Unlit/AmbientStem"
{
	Properties
	{
		//_WindNoise("Noise", 2D) = "white" {}
		//_WindSpeed("Wind Speed", float) = 1
		//_WindForce("Wind Force", float) = 1
		_DistanceAdjust("Distance Adjust", float) = 1
		_Offset("Offset", float) = 0
		//_WindDirection("Wind Direction", Range(0, 360)) = 0
		// this shader gets used for both the transitional and ambient materials
		// _Percent should never be adjusted by script in the ambient materials
		_Percent("Percent", Range(0, 1)) = 1
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;   // vertex position
				float2 uv : TEXCOORD0;      // x & y or origin
				float2 uv2 : TEXCOORD1;     // x: z of origin, y: rigidity of vertex
				float4 color : COLOR;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			float _WindSpeed;
			float _WindStretch;
			float _WindForce;
			float _HeightDelay;
			float _WindDirection;
			float _BendBack;
			float _Percent;
			sampler2D _WindNoise;
			float _WindNoiseScale;
			float _WindNoiseSpeed;
            float _TestTime;
			
			float4 rotateAroundLine(float4 vertex, float4 center, float4 direction, float theta) {
				// get the three values for the position of the line we're rotating around
				float a = center.x;
				float b = center.y;
				float c = center.z;
				// get the three values of the direction for line we're rotating around
				float u = direction.x;
				float v = direction.y;
				float w = direction.z;

				float4x4 rotationMatrix;
				// calculate these in advance to reduce redundant operations
				float cosTheta = cos(theta);
				float sinTheta = sin(theta);
				// apply a matrix transform to rotate the vertex around center
				rotationMatrix[0][0] = u*u + (v*v + w*w) * cosTheta;
				rotationMatrix[0][1] = u*v * (1 - cosTheta) + w * sinTheta;
				rotationMatrix[0][2] = u*w * (1 - cosTheta) - v * sinTheta;
				rotationMatrix[0][3] = 0;
				rotationMatrix[1][0] = u*v * (1 - cosTheta) - w * sinTheta;
				rotationMatrix[1][1] = v*v + (u*u + w*w) * cosTheta;
				rotationMatrix[1][2] = v*w * (1 - cosTheta) + u * sinTheta;
				rotationMatrix[1][3] = 0;
				rotationMatrix[2][0] = u*w * (1 - cosTheta) + v * sinTheta;
				rotationMatrix[2][1] = v*w * (1 - cosTheta) - u * sinTheta;
				rotationMatrix[2][2] = w*w + (u*u + v*v) * cosTheta;
				rotationMatrix[2][3] = 0;
				rotationMatrix[3][0] = (a * (v*v + w*w) - u * (b*v + c*w)) *
											(1 - cosTheta) + (b*w - c*v) * sinTheta;
				rotationMatrix[3][1] = (b * (u*u + w*w) - v * (a*u + c*w)) *
											(1 - cosTheta) + (c*u - a*w) * sinTheta;
				rotationMatrix[3][2] = (c * (u*u + v*v) - w * (a*u + b*v)) *
											(1 - cosTheta) + (a*v - b*u) * sinTheta;
				rotationMatrix[3][3] = 1;
				return mul(vertex, rotationMatrix);
			}

			v2f vert (appdata v) {
				v2f o;

				float4 vertex = mul(unity_ObjectToWorld, v.vertex);
				float4 pivot = float4(v.uv.xy, v.uv2.x, 0);
				float rigidity = 1 - v.uv2.y;
				float windAngle = _WindDirection * (3.14159 / 180);
				float heightOffset = _HeightDelay * rigidity;
				float distanceOffset = (sin(windAngle) * pivot.x + cos(windAngle) * pivot.z) / _WindStretch;
				float noiseAdjust = _WindNoiseSpeed * _TestTime;
				float4 noiseuv = float4(pivot.x / _WindNoiseScale * noiseAdjust, pivot.z / _WindNoiseScale, 0, 0);
				float4 noise = tex2Dlod(_WindNoise, noiseuv);
				float theta = sin(_TestTime * _WindSpeed + distanceOffset - heightOffset) * noise.x * _WindForce * rigidity * _Percent;
				float max = clamp(noise.x * _WindForce * rigidity * _Percent, 0.01, 1);
				theta = lerp(max * _BendBack, max, theta + max / (max * 2));
				float4 direction = float4(cos(windAngle), 0, sin(windAngle), 0);
				vertex = rotateAroundLine(vertex, pivot, direction, theta);

				// unity_ObjectToWorld has already put the vertex in model space, now
				// it just needs to be modified by view * projection matrix
				o.vertex = mul(UNITY_MATRIX_VP, vertex);
				o.color = v.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target {
				return i.color;
			}
			ENDCG
		}
	}
}
