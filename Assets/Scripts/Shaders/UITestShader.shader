﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "UI/UITestShader"
{  
	Properties
	{
		[PerRendererData] _MainTex ("Sprite Texture", 2D) = "white" {}
		_Color ("Tint", Color) = (1,1,1,1)
		
		// required for UI.Mask
		_StencilComp ("Stencil Comparison", Float) = 8
		_Stencil ("Stencil ID", Float) = 0
		_StencilOp ("Stencil Operation", Float) = 0
		_StencilWriteMask ("Stencil Write Mask", Float) = 255
		_StencilReadMask ("Stencil Read Mask", Float) = 255
		_ColorMask ("Color Mask", Float) = 15
	}
	SubShader
	{
		Tags 
		{ 
			// ...
		}
		
		// required for UI.Mask
		Stencil
		{
			Ref [_Stencil]
			Comp [_StencilComp]
			Pass [_StencilOp] 
			ReadMask [_StencilReadMask]
			WriteMask [_StencilWriteMask]
		}
		ColorMask [_ColorMask]

		
		Pass
		{
		CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			struct appdata_c {
				float4 vertex : POSITION;
				float4 color : COLOR;
				float2 texcoord : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : SV_POSITION;
				fixed4 color : COLOR;
				half2 texcoord : TEXCOORD0;
			};

			sampler2D _MainTex;
			float4 _Color;

			v2f vert(appdata_c data) {
				v2f output;
				output.vertex = UnityObjectToClipPos(data.vertex);
				float rgb = lerp(0, 1, data.texcoord.y);
				output.color = fixed4(rgb, rgb, rgb, 1);
				if (rgb < 0.5) {
					output.color.a = 0;
				}
				output.color.a = 0;
				// output.color.a = 0;
				output.texcoord = data.texcoord;
				// float sine = sin(3.14159 * data.texcoord.y);
				// if (output.texcoord.x > sine) {
				// 	output.color.a = 0;
				// }
				return output;
			}

			fixed4 frag(v2f v) : SV_TARGET {
				// half4 color = tex2D(_MainTex, v.texcoord) * v.color;
				half4 color = v.color;
				return v.color;
			}
		ENDCG
		}
	}
}