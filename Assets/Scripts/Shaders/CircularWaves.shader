﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

Shader "Unlit/CircularWaves"
{
	Properties
	{
		_Color("Color", Color) = (0, 0, 1, 1)
		_PeakColor("Peak Color", Color) = (1, 1, 1, 1)
		_WaveHeight("Wave height", float) = 0.5
		_WaveScale("Wave Scale", float) = 1
		_Speed("Speed", float) = 1.0
		_PeakPercent("Peak Percent", Range(0, 1)) = 0.8
		_RandomAdjust("Random Range" , Range(0, 1)) = 0.0
		_Reach("Reach", Range(0, 1)) = 1.0
        _FoamDepthMin("Foam Depth Min", float) = 1.0
        _FoamDepthMax("Foam Depth Max", float) = 5.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			float rand(float3 co)
			{
				return frac(sin(dot(co.xyz ,float3(12.9898,78.233,45.5432))) * 43758.5453);
			}

			float rand2(float3 co)
			{
				return frac(sin(dot(co.xyz ,float3(19.9128,75.2,34.5122))) * 12765.5213);
			}

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float2 uv : TEXCOORD0;
                float4 screenPos : TEXCOORD1;
			};

			fixed4 _Color;
			fixed4 _PeakColor;

			float _WaveHeight;
			float _Speed;
			float _PeakPercent;
			float _RandomAdjust;
			float _WaveScale;
            float _FoamDepthMin;
            float _FoamDepthMax;

            sampler2D _CameraDepthTexture;
			
			float when_gt(float x, float y) {
				return max(sign(x - y), 0.0);
			}

			float when_ls(float x, float y) {
				return max(sign(y - x), 0.0);
			}

			v2f vert (appdata v)
			{
				v2f o;

				float height = _WaveHeight * (1 - _RandomAdjust);
				float randomHeight = _WaveHeight * _RandomAdjust;

				float phase0 = sin((_Time[1] * _Speed) + (v.uv.x * _WaveScale * 2) + rand2(v.vertex.xxz));
				float phase1 = sin((_Time[1] * _Speed) + (v.uv.x * _WaveScale * 3) + rand(v.vertex.zyx));

				float4 vertex = v.vertex;
				float percent = 1.0 - clamp(v.uv.x, 0.0, 1.0);
				float adjust = clamp(phase0 + phase1, 0.0, 1.0) * percent;
				vertex.y += adjust * _WaveHeight;
				o.vertex = UnityObjectToClipPos(vertex);
				o.uv = float2(adjust, 0);
                o.screenPos = ComputeScreenPos(o.vertex);
				return o;
			}
			
			fixed4 frag(v2f i) : SV_Target
			{
				float t = when_gt(i.uv.x, _PeakPercent);
                float4 depthSample = SAMPLE_DEPTH_TEXTURE_PROJ(_CameraDepthTexture, i.screenPos);
                float depth = LinearEyeDepth(depthSample).r;
                float depthFactor = lerp(_FoamDepthMin, _FoamDepthMax, t);
                float foamLine = 1 - saturate(depthFactor * (depth - i.screenPos.w));
                if (foamLine > 0.5f) return _PeakColor;

				float4 color = lerp(_Color, _PeakColor, t);
				return color;
			}
			ENDCG
		}

	}
}
