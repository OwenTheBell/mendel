﻿Shader "Unlit/PlanetRing"
{
	Properties
	{
		_PrimaryColor("Primary Color", Color) = (1, 0, 0, 1)
		_SecondaryColor("Secondary Color", Color) = (0, 0, 1, 1)
		_Speed("Speed", float) = 1.0
		_Radius("Radius", float) = 1.0
		_DistanceWarp("Distance Warp", Range(0, 1)) = 0.0
		_AtmosphereColor("Atmosphere Color", Color) = (0, 1, 0, 1)
		_DistanceFade("Distance Fade", Range(0, 1)) = 0.0
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
				float2 uv : TEXCOORD0;
				float4 color : COLOR;
				float4 tangent : TANGENT;
			};

			struct v2f
			{
				float4 color : COLOR;
				float4 vertex : SV_POSITION;
			};

			float4 _PrimaryColor;
			float4 _SecondaryColor;
			float _Speed;
			float _Radius;
			float _DistanceWarp;
			float4 _AtmosphereColor;
			float _DistanceFade;

			float4 rotateAroundLine(float4 vertex, float4 center, float4 direction, float theta) {
				// get the three values for the position of the line we're rotating around
				float a = center.x;
				float b = center.y;
				float c = center.z;
				// get the three values of the direction for line we're rotating around
				float u = direction.x;
				float v = direction.y;
				float w = direction.z;

				float4x4 rotationMatrix;
				// apply a matrix transform to rotate the vertex around center
				rotationMatrix[0][0] = u*u + (v*v + w*w) * cos(theta);
				rotationMatrix[0][1] = u*v * (1 - cos(theta)) + w * sin(theta);
				rotationMatrix[0][2] = u*w * (1 - cos(theta)) - v * sin(theta);
				rotationMatrix[0][3] = 0;
				rotationMatrix[1][0] = u*v * (1 - cos(theta)) - w * sin(theta);
				rotationMatrix[1][1] = v*v + (u*u + w*w) * cos(theta);
				rotationMatrix[1][2] = v*w * (1 - cos(theta)) + u * sin(theta);
				rotationMatrix[1][3] = 0;
				rotationMatrix[2][0] = u*w * (1 - cos(theta)) + v * sin(theta);
				rotationMatrix[2][1] = v*w * (1 - cos(theta)) - u * sin(theta);
				rotationMatrix[2][2] = w*w + (u*u + v*v) * cos(theta);
				rotationMatrix[2][3] = 0;
				rotationMatrix[3][0] = (a * (v*v + w*w) - u * (b*v + c*w)) *
											(1 - cos(theta)) + (b*w - c*v) * sin(theta);
				rotationMatrix[3][1] = (b * (u*u + w*w) - v * (a*u + c*w)) *
											(1 - cos(theta)) + (c*u - a*w) * sin(theta);
				rotationMatrix[3][2] = (c * (u*u + v*v) - w * (a*u + b*v)) *
											(1 - cos(theta)) + (a*v - b*u) * sin(theta);
				rotationMatrix[3][3] = 1;
				return mul(vertex, rotationMatrix);
			}
			
			v2f vert (appdata input)
			{

				float4x4 rotationMatrix;

				float theta = 3.14159 * _Time[1] * _Speed;
				float4 center = input.normal;

				float4 vertex = rotateAroundLine(input.vertex, center, input.tangent, theta);

				// calculate how close center is to the horizon, and shrink the vertex
				// appropriately
				float4 worldCenter = mul(unity_ObjectToWorld, center);
				float4 meshZero = mul(unity_ObjectToWorld, float4(0, 0, 0, 0));
				float distance = worldCenter.y;
				float warp = (1 - distance / _Radius) * _DistanceWarp;
				vertex = lerp(vertex, center, warp);
				float fade = lerp(0, _DistanceFade, warp);

				float4 color = float4(input.color.xyz, fade);

				v2f o;
				o.vertex = UnityObjectToClipPos(vertex);
				o.color = color;
				//o.color = input.color;
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				//float4 color = lerp(_PrimaryColor, _SecondaryColor, i.color.x);
				float4 color = lerp(_PrimaryColor, _SecondaryColor, 0);
				color = lerp(color, _AtmosphereColor, i.color.w);
				return color;
			}
			ENDCG
		}
	}
}
