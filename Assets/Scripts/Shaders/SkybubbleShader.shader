﻿// Upgrade NOTE: replaced 'mul(UNITY_MATRIX_MVP,*)' with 'UnityObjectToClipPos(*)'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

// Upgrade NOTE: replaced '_Object2World' with 'unity_ObjectToWorld'

Shader "Custom/SkybubbleShader"
{
	Properties
	{
		_PeakColor("Peak Color", Color) = (0, 0, 1, 1)
		_HorizonColor("Horizon Color", Color) = (1, 0, 0, 1)
		_PeakRadius("Peak Radius", Range(0, 1)) = 0
		_SinMagnitude("Sin Magnitude", Range(0, 1)) = 0
		_BandWidth("Band Width", Range(0.01, 1)) = 0.01
		_Frequency("Frequency", int) = 2
		_Frequency2("Frequency 2", int) = 3
		_Speed("Speed", float) = 1
		_Rings("Rings", int) = 1
		_Peaks("Peaks", int) = 2
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			//#pragma float when_gt

			#include "UnityCG.cginc"

			struct v2v {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f {
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			float _PeakRadius;
			float4 _PeakColor;
			float4 _HorizonColor;
			float _SinMagnitude;
			float _BandWidth;
			int _Frequency;
			int _Frequency2;
			float _Speed;
			int _Rings;
			int _Peaks;
			
			float when_gt(float x, float y) {
				return max(sign(x - y), 0.0);
			}

			float when_ls(float x, float y) {
				return max(sign(y - x), 0.0);
			}

			v2f vert(v2v v) {
				v2f o;
				o.uv = v.uv;
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target {
				float2 uv = i.uv;

				float pi2 = 3.14159 * 2;
				float division = pi2 / _Peaks;
				float angle = atan2(uv.y - 0.5f, uv.x - 0.5f) + 3.14159;

				float lastPeak = (int)(angle / division) * division;
				float nextPeak = lastPeak + division;

				float diff = (angle - lastPeak) / division;

				float lowRadius = sin(lastPeak * _Frequency + _Time[0] * _Speed) * _SinMagnitude;
				lowRadius += sin(lastPeak * _Frequency2 + _Time[0] * -_Speed) * _SinMagnitude;
				lowRadius /= 2;
				float highRadius = sin(nextPeak * _Frequency + _Time[0] * _Speed) * _SinMagnitude;
				highRadius += sin(nextPeak * _Frequency2 + _Time[0] * -_Speed) * _SinMagnitude;
				highRadius /= 2;

				float radius = _PeakRadius + lerp(lowRadius, highRadius, diff);

				float distance = sqrt(pow(uv.x - 0.5f, 2) + pow(uv.y - 0.5f, 2)) * 2;
				float adjust = distance - radius + _BandWidth / 2;
				adjust = clamp(adjust, 0.0, 1.0);
				division = _BandWidth / (_Rings + 1);
				adjust = (int)(adjust / division) * division;
				float percent = clamp(adjust / _BandWidth, 0.0, 1.0);

				fixed4 color = lerp(_PeakColor, _HorizonColor, percent);
				return color;
			}

			ENDCG
		}
	}
}
