﻿Shader "Unlit/Multipass Blooming" {

Properties {
    _VertexGrowTime("Vertex Grow Time", float) = 0
    _BouneForce("Bounce Force", float) = 1.70158
    _StartTime("Start Time", float) = 0
    _BloomTime("Bloom Time", float) = 0
    _PetalSpawnWindow("Petal Spawn Window", Range(0, 1)) = 0.5
    _MaxPetalGap("Maximum Gap Between Petal Spawns", float) = 0
    _GrowPercent("Grow Percent", Range(0, 1)) = 0.5
    _ExpandPercent("Expand Percent", Range(0, 1)) = 0.5
    _WindNoise("Noise", 2D) = "white" {}
    _WindSpeed("Wind Speed", float) = 1
    _WindForce("Wind Force", float) = 1
    _DistanceAdjust("Distance Adjust", float) = 1
    _Offset("Offset", float) = 0
    _WindDirection("Wind Direction", Range(0, 360)) = 0
    _Percent("Percent", Range(0, 1)) = 1
}

SubShader {
    Tags{ "RenderType" = "Opaque" "Batching" = "False" }
    LOD 100

    Pass {
        CGPROGRAM
        #pragma vertex vert
        #pragma fragment frag
        // make fog work
        #pragma multi_compile_fog

        #include "UnityCG.cginc"

        struct v2v {
            float4 vertex : POSITION;
            //contains the halfway expanded state
            float4 tangent : TANGENT;
            float4 color : COLOR;
            // xy values of the point to pivot around for the ambient motion
            float2 uv : TEXCOORD0;
            // z value for the ambient motion
            float2 uv2 : TEXCOORD1;
            // time information for when to start and end growth
            float3 uv3 : TEXCOORD2;
        };

        struct v2f {
            float4 vertex : SV_POSITION;
            float4 color : COLOR;
        };

        float _VertexGrowTime;
        float _BouneForce;

        float _StartTime;
        float _BloomTime;
        float _GrowPercent;
        float _ExpandPercent;
        float _PetalSpawnWindow;
        float _MaxPetalGap;

        float _WindSpeed;
        float _WindStretch;
        float _WindForce;
        float _HeightDelay;
        float _WindDirection;
        float _BendBack;
        float _Percent;
        sampler2D _WindNoise;
        float _WindNoiseScale;
        float _WindNoiseSpeed;

        float4 rotateAroundLine(float4 vertex, float4 center, float4 direction, float theta) {
            // get the three values for the position of the line we're rotating around
            float a = center.x;
            float b = center.y;
            float c = center.z;
            // get the three values of the direction for line we're rotating around
            float u = direction.x;
            float v = direction.y;
            float w = direction.z;

            float4x4 rotationMatrix;
            // calculate these in advance to reduce redundant operations
            float cosTheta = cos(theta);
            float sinTheta = sin(theta);
            // apply a matrix transform to rotate the vertex around center
            rotationMatrix[0][0] = u*u + (v*v + w*w) * cosTheta;
            rotationMatrix[0][1] = u*v * (1 - cosTheta) + w * sinTheta;
            rotationMatrix[0][2] = u*w * (1 - cosTheta) - v * sinTheta;
            rotationMatrix[0][3] = 0;
            rotationMatrix[1][0] = u*v * (1 - cosTheta) - w * sinTheta;
            rotationMatrix[1][1] = v*v + (u*u + w*w) * cosTheta;
            rotationMatrix[1][2] = v*w * (1 - cosTheta) + u * sinTheta;
            rotationMatrix[1][3] = 0;
            rotationMatrix[2][0] = u*w * (1 - cosTheta) + v * sinTheta;
            rotationMatrix[2][1] = v*w * (1 - cosTheta) - u * sinTheta;
            rotationMatrix[2][2] = w*w + (u*u + v*v) * cosTheta;
            rotationMatrix[2][3] = 0;
            rotationMatrix[3][0] = (a * (v*v + w*w) - u * (b*v + c*w)) *
                                        (1 - cosTheta) + (b*w - c*v) * sinTheta;
            rotationMatrix[3][1] = (b * (u*u + w*w) - v * (a*u + c*w)) *
                                        (1 - cosTheta) + (c*u - a*w) * sinTheta;
            rotationMatrix[3][2] = (c * (u*u + v*v) - w * (a*u + b*v)) *
                                        (1 - cosTheta) + (a*v - b*u) * sinTheta;
            rotationMatrix[3][3] = 1;
            return mul(vertex, rotationMatrix);
        }

        v2f vert(v2v v) {
            float petalBloomTime = _BloomTime * _PetalSpawnWindow;
            float spawnDelay = v.uv3.x / v.uv3.y * petalBloomTime;
            float maxDelay = v.uv3.x * _MaxPetalGap;
            spawnDelay = clamp(spawnDelay, 0.0, maxDelay);

            float adjustedPetalTime = _BloomTime - spawnDelay;

            float growTime = adjustedPetalTime * _GrowPercent;
            float expandTime = adjustedPetalTime * _ExpandPercent;
            float expandStart = adjustedPetalTime - expandTime;

            float growDelta = (_Time.y - spawnDelay - _StartTime) / growTime;
            growDelta = clamp(growDelta, 0.0, 1.0);
            float expandDelta = (_Time.y - spawnDelay - _StartTime - expandStart) / expandTime;
            expandDelta = clamp(expandDelta, 0.0, 1.0);

            /* apply an ease out formula to both deltas */
            growDelta = -1 * ((growDelta - 1) * (growDelta - 1) - 1);
            expandDelta = -1 * ((expandDelta - 1) * (expandDelta - 1) - 1);

            float4 expandedVertices = lerp(v.tangent, v.vertex, expandDelta);
            float4 zero = float4(0, 0, 0, 0);
            float4 vertex = lerp(zero, expandedVertices, growDelta);
            //vertex = v.tangent;

            // convert vertex into world space
            //vertex = v.vertex;
            //vertex = mul(unity_ObjectToWorld, vertex);
            //float4 pivot = float4(v.uv.xy, v.uv2.x, 0);
            ////pivot = mul(unity_WorldToObject, pivot);
            //float height = v.uv2.y;
            //float angle = _WindDirection * (3.14159 / 180);
            //float sinA = sin(angle);
            //float cosA = cos(angle);
            //float hOffset = _HeightDelay * height;
            //float dOffset = (sinA * pivot.x + cosA * pivot.z) / _WindStretch;
            //float noiseAdjust = _WindNoiseSpeed * _Time[0];
            //float4 noiseuv = float4(pivot.x / _WindNoiseScale * noiseAdjust, pivot.z / _WindNoiseScale, 0, 0);
            //float4 noise = tex2Dlod(_WindNoise, noiseuv);
            //float theta = sin(_Time[0] * _WindSpeed + dOffset - hOffset) * noise * _WindForce * height * _Percent;
            //float max = clamp(noise.x * _WindForce * height * _Percent, 0.01, 1);
            //theta = lerp(max * _BendBack, max, theta + max / (max * 2));
            //float4 direction = float4(cosA, 0, sinA, 0);
            //vertex = rotateAroundLine(vertex, pivot, direction, theta);
            //vertex = mul(unity_WorldToObject, vertex);

            v2f o;
            o.vertex = UnityObjectToClipPos(vertex);
            //o.vertex = mul(UNITY_MATRIX_VP, vertex);
            //o.vertex = vertex;
            o.color = v.color;
            return o;
        }

        fixed4 frag(v2f i) : SV_Target{
            return i.color;
        }
        ENDCG
    }
}
}