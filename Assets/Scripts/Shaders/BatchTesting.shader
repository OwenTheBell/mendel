﻿Shader "Custom/BatchTesting"
{
	Properties
	{
		_Distance("Distance", float) = 5
		_Color1("Color 1", Color) = (1, 0, 0, 1)
		_Color2("Color 2", Color) = (0, 1, 0, 1)
	}
	SubShader
	{
		Tags { "RenderType"="Opaque" }
		LOD 100

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			// make fog work
			#pragma multi_compile_fog
			
			#include "UnityCG.cginc"

			struct appdata
			{
				float4 vertex : POSITION;
				float4 normal : NORMAL;
				float4 tangent : TANGENT;
				float2 uv : TEXCOORD0;
				float2 uv2 : TEXCOORD1;
			};

			struct v2f
			{
				float4 vertex : SV_POSITION;
				float4 color : COLOR;
			};

			float _Distance;
			float4 _Color1;
			float4 _Color2;

			float when_gt(float x, float y) {
				return max(sign(x - y), 0.0);
			}

			float when_ls(float x, float y) {
				return max(sign(y - x), 0.0);
			}

			v2f vert (appdata v)
			{
				v2f o;
				//float4 vertex = v.vertex;
				float4 vertex = mul(unity_ObjectToWorld, v.vertex);
				//float4 pivot = v.tangent;
				//float4 pivot = mul(unity_WorldToObject, v.tangent);
				float4 pivot = float4(v.uv.xy, v.uv2.xy);
				float distance = sqrt(
										pow(vertex.x - pivot.x, 2) + 
										pow(vertex.y - pivot.y, 2) + 
										pow(vertex.z - pivot.z, 2)
									);

				//distance = when_gt(distance, 0);

				//float distance = v.tangent.x + v.tangent.y + v.tangent.x;

				o.color = lerp(_Color1, _Color2, clamp(distance / _Distance, 0, 1));
				o.vertex = UnityObjectToClipPos(v.vertex);
				return o;
			}
			
			fixed4 frag (v2f i) : SV_Target
			{
				return i.color;
			}
			ENDCG
		}
	}
}
