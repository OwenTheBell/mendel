﻿using UnityEngine;

public class TaxonomyBehaviour : InteractionBehaviour {

    public string Message;

    public override void SetControlPrompt(ControlPrompt prompt) {
        var info = TargetObject.GetComponent<PlantInfo>();
        var name = Services.SpeciesCatalog.GetNameForIdentity(info.Identity);
        if (name == string.Empty) {
            prompt.Available = false;
            Accessible = false;
        }
        else {
            prompt.Available = true;
            prompt.Text = Message;
            Accessible = true;
        }
    }

    public override void Activate() {
        base.Activate();

        var info = TargetObject.GetComponent<PlantInfo>();
        var name = Services.SpeciesCatalog.GetNameForIdentity(info.Identity);
        Events.instance.Raise(new ShowSpeciesEntryEvent(name, info.Identity));
        WorkingOver();
    }
}