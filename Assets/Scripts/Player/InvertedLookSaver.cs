﻿using UnityEngine;

[RequireComponent(typeof(MendelMouseLook))]
public class InvertedLookSaver : MonoBehaviour {

	const string KEY = "Mouse Look";
	private MendelMouseLook _Look;

	private void Awake() {
		_Look = GetComponent<MendelMouseLook>();
	}

	private void Start() {
		if (PlayerPrefs.HasKey(KEY)) {
			_Look.InvertY = PlayerPrefs.GetInt(KEY) == 1;
		}
	}

	private void OnDisable() {
		PlayerPrefs.SetInt(KEY, _Look.InvertY ? 1 : 0);
	}
}