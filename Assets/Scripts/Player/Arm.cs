﻿using UnityEngine;
using System.Collections;

public class Arm : MonoBehaviour {

	public Transform UpperArm;
	public Transform Shoulder;
	public Transform Hand;
	public Transform Elbow;

	protected Quaternion _StartShoulder;
    protected Quaternion _StartElbow;

	protected virtual void Awake() {
        _StartShoulder = Shoulder.localRotation;
        _StartElbow = Elbow.localRotation;
	}

	protected virtual void Start () {
		_StartShoulder = Shoulder.localRotation;
	}
	
	protected virtual void Update () {

    }

	public void PointShoulderAt(Vector3 target) {
		Shoulder.LookAt(target);
	}

	public void PointElbowAt(Vector3 target) {
		Elbow.LookAt(target);
	}

	public void RotateElbowTo(Quaternion rotation) {
		Elbow.localRotation = rotation;
	}

	public void RotateShoulderTo(Quaternion rotation) {
		Shoulder.localRotation = rotation;
	}

    public void PointElbowAtOver(Vector3 target, float duration) {
        StopAllCoroutines();
        var rotation = Elbow.localRotation;
        Elbow.LookAt(target);
        var targetRotation = Elbow.localRotation;
        Elbow.localRotation = rotation;
        StartCoroutine(RotateElbowToOver(targetRotation, duration));
    }

    public void ReturnElbowToDefault(float duration) {
        StopAllCoroutines();
        StartCoroutine(RotateElbowToOver(_StartElbow, duration));
    }

    IEnumerator RotateElbowToOver(Quaternion rotation, float duration) {
        var time = 0f;
        var startRotation = Elbow.localRotation;
        while (time < duration) {
            time += Time.deltaTime;
            var easeInOut = Easing.EaseInOut(time / duration, 2);
            Elbow.localRotation = Quaternion.Lerp(startRotation, rotation, easeInOut);
            yield return null;
        }
    }
}
