using System.Collections.Generic;

public class SpeciesCatalog {

	public List<string> Names { get; private set; }
	public List<List<int>> Identities { get; private set; }

    public int Count { get { return Names.Count; } }

	public SpeciesCatalog() {
		Names = new List<string>();
		Identities = new List<List<int>>();
	}

    public int GetSpeciesIndexForName(string name) {
        return Names.IndexOf(name);
    }

	public string GetNameForIdentity(int identity, bool includeNumber = false) {
		var closestSimilarity = 0f;
		var closestMatch = -1;
		var seed = Services.SeedVault.Get(identity);

		for (var i = 0; i < Names.Count; i++) {
			var comparisonSeed = Services.SeedVault.Get(Identities[i][0]);
			var similiarity = Seed.CompareSeeds(seed, comparisonSeed);

            var indexOf = Identities[i].IndexOf(identity);
			if (indexOf >= 0) {
                if (includeNumber) {
                    return Names[i] + " #" + (indexOf + 1);
                }
                else {
                    return Names[i];
                }
			}
			if (similiarity > closestSimilarity && similiarity > PROPERTIES.SpeciesCutoff) {
				closestSimilarity = similiarity;
				closestMatch = i;
			}
		}
		if (closestMatch >= 0) {
			Identities[closestMatch].Add(identity);
            if (includeNumber) {
                return Names[closestMatch] + " #" + Identities[closestMatch].Count;
            }
            else {
                return Names[closestMatch];
            }
		}
		return string.Empty;
	}

    public void SetNameForIndex(int index, string name) {
        if (index >= 0 && index < Names.Count)
            Names[index] = name;
    }

	public int SetNameForIdentity(int identity, string name) {
		for (var i = 0; i < Identities.Count; i++) {
			if (Identities[i].Contains(identity)) {
				Names[i] = name;
				return i;
			}
		}
        // insert the name alphabetically
        var index = 0;
        while (index < Names.Count) {
            if (string.Compare(name.ToLower(), Names[index].ToLower()) == -1) {
                break;
            }
            index++;
        }
        Names.Insert(index, name);
        Identities.Insert(index, new List<int>(new int[]{identity}));
        return index;
	}

	public Tuple<string, float> GetClosestRelative(int identity) {
		var seed = Services.SeedVault.Get(identity);
		var species = string.Empty;
		var similiarity = 0f;

		for (var i = 0; i < Identities.Count; i++) {
            if (!Identities[i].Contains(identity)) {
                var comparisonSeed = Services.SeedVault.Get(Identities[i][0]);
                var percent = Seed.CompareSeeds(seed, comparisonSeed);
                if (percent > similiarity) {
                    similiarity = percent;
                    species = Names[i];
                }
            }
		}
		return Tuple.New<string, float>(species, similiarity);
	}

	public void SetNameForIdentities(string name, int[] identities) {
		var index = Names.IndexOf(name, 0);
		if (index > -1) {
			foreach (var identity in identities) {
				if (!Identities[index].Contains(identity)) {
					Identities[index].Add(identity);
				}
			}
		}
		else {
			Names.Add(name);
			Identities.Add(new List<int>(identities));
		}
	}

    public void Clear() {
        Names.Clear();
        Identities.Clear();
    }
}