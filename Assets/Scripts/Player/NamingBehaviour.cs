﻿using UnityEngine;
using System.Collections;

public class NamingBehaviour : InteractionBehaviour, ICancelReciever {

    public NamingCanvas NameCanvas;
    public GameObject NameCanvasPrefab;
    public string NameMessage;
    public string RenameMessage;
    public string TaxonomyMessage;
    public AudioSource ActionAudio;

    private int _TargetIdentity;
    private bool _Submitted;

    protected override void Awake() {
        base.Awake();
        _Submitted = false;
    }

    protected override void Start() {
        base.Start();

        NameCanvas.Hide();
        NameCanvas.GetComponent<PointAtTransform>().PointAt = GameObject.Find("UI Camera").transform;
    }

	protected override void Update() {
		base.Update();

        if (!Working) {
            return;
        }

        if (Input.GetButtonDown("Submit")) {
            Services.SpeciesCatalog.SetNameForIdentity(_TargetIdentity, NameCanvas.Name);
            _Submitted = true;
            Events.instance.Raise(new PromptTextEvent(TaxonomyMessage, 5f));
            ActionAudio.Play();
            Services.CancelStack.Remove(this);
        }
	}

    public override void Activate() {
		base.Activate();

        Services.CancelStack.Add(this, false);

		var name = Services.SpeciesCatalog.GetNameForIdentity(_TargetIdentity);
		NameCanvas.Show();
        NameCanvas.Name = name;
	}

    public override bool SetTarget(GameObject target, Vector3 position) {
        if (!base.SetTarget(target, position)) {
            return false;
        }
        if (target.GetComponent<FlowerCreator>().AvailableFlowerCount == 0) {
            return false;
        }

        _TargetIdentity = target.GetComponent<PlantInfo>().Identity;

		TargetPosition = position;
        NameCanvas.transform.position = position;

        return true;
    }

    public override void SetControlPrompt(ControlPrompt prompt) {
        prompt.Available = true;
        var name = Services.SpeciesCatalog.GetNameForIdentity(_TargetIdentity);
		if (name == string.Empty) {
            prompt.Text = NameMessage;
		}
		else {
            prompt.Text = RenameMessage;
		}
    }

    public void OnCancel() {
        StartCoroutine(HideCanvas());
        ActionAudio.Play();
    }

    IEnumerator HideCanvas() {
        if (_Submitted)
            NameCanvas.HideAllExecpt(NameCanvas.SavePrompt);
        else {
            NameCanvas.HideAllExecpt(NameCanvas.ExitPrompt);
        }
        yield return new WaitForSeconds(0.35f);
        NameCanvas.Hide();
        while (NameCanvas.Visible) yield return null;
        _Submitted = false;
        WorkingOver();
    }
}