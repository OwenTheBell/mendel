﻿using UnityEngine;

public enum InteractionType {
	Primary,
	Secondary,
    Tertiary
}

public abstract class InteractionBehaviour : MonoBehaviour {

	protected Arm LeftArm;
	protected Arm RightArm;
	protected Animator PlayerAnimator;

    public ActionType ActionType;
    public InteractionStateMode Mode;

	public GameObject TargetObject { get; protected set; }
	public virtual Vector3 TargetPosition { get; protected set; }
	public bool Working { get; protected set; }
    public bool Accessible { get; protected set; }

	protected delegate void BehaviourUpdate();
	protected BehaviourUpdate _Update;
	protected BehaviourUpdate _LateUpdate;

	protected virtual void Awake() {
		_Update = IdleUpdate;
		_LateUpdate = LateIdleUpdate;
        Accessible = true;
	}

	protected virtual void Start () {
		LeftArm = transform.Find("Main Camera/Arms/Left Arm").GetComponent<Arm>();
		RightArm = transform.Find("Main Camera/Arms/Right Arm").GetComponent<Arm>();
		PlayerAnimator = GetComponent<Animator>();
	}
	
	protected virtual void Update () {
		if (_Update != null &&	_Update.GetInvocationList().Length > 0) {
			_Update.DynamicInvoke();
		}
	}

	protected virtual void LateUpdate () {
		if (_LateUpdate != null &&	_LateUpdate.GetInvocationList().Length > 0) {
			_LateUpdate.DynamicInvoke();
		}
	}

	protected virtual void IdleUpdate() {}
	protected virtual void LateIdleUpdate() {}
	protected virtual void WorkingUpdate() {}
	protected virtual void LateWorkingUpdate() {}

	public virtual void Activate() {
		Working = true;	
		_Update = WorkingUpdate;
		_LateUpdate = LateWorkingUpdate;
	}

	protected virtual void WorkingOver() {
		if (!Working) {
			return;
		}
		Working = false;
		_Update = IdleUpdate;
		_LateUpdate = LateIdleUpdate;
	}

    public virtual bool SetTarget(GameObject target) {
        return SetTarget(target, target.transform.position);
    }

    public virtual bool SetTarget(GameObject target, Vector3 position) {
        TargetObject = target;
        TargetPosition = position;
        return true;
    }

	public virtual void RemoveTarget() {
		TargetObject = null;
	}

	protected virtual void ArmAnimationOver() {}

	protected void RecursiveSetLayer(Transform transform, int layer) {
		transform.gameObject.layer = layer;
		foreach (Transform child in transform) {
			RecursiveSetLayer(child, layer);
		}
	}

    public abstract void SetControlPrompt(ControlPrompt prompt);
}