using UnityEngine;

public class UprootInteractionState : InteractionState {

    [SerializeField]
    private GameObject _TargetIndicatorPrefab;
    private GameObject _TargetIndicator;
    private ReplantBehaviour _ReplantBehaviour;

    [HideInInspector]
    public GameObject HeldPlant;
    [HideInInspector]
    public int SavedLayer;
    [HideInInspector]
    public Vector3 StartHandPosition;
    [HideInInspector]
    public Transform HandParent;
    [SerializeField]
    public LayerMask _GroundLayer;

    private Vector3 _GroundUp;

    protected override void Awake() {
        base.Awake();

        Mode = InteractionStateMode.Uproot;
    }

    protected override void Start() {
        base.Start();

        _TargetIndicator = Instantiate(_TargetIndicatorPrefab) as GameObject;
        DontDestroyOnLoad(_TargetIndicator);
        _TargetIndicator.GetComponent<PlantingIndicator>().Hide();
    }

    public override bool CheckTarget(Collider collider, Vector3 point) {
        var targetCheck = base.CheckTarget(collider, point);
        if (HeldPlant == null) {
            Block = false;
            return false;
        }
        else {
            Block = true;
        }

        if (!targetCheck) {
            return false;
        }

        // this raycast both gets the normal for placing the planting indicator
        // as well as determine whether the ground is what is directly in front of the
        // camera since the interaction probe can be a bit fuzzy
        RaycastHit hit;
        var camera = Camera.main;
        var ray = new Ray(camera.transform.position, point - camera.transform.position);
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, _GroundLayer)) {
            if (hit.transform.tag == "Ground") {
                _GroundUp = hit.normal;
            }
            else {
                return false;
            }
        }

        var ready = false;
        foreach (var b in _Behaviours) {
            if (b.SetTarget(collider.gameObject, point)) {
                ready = true;
            }
        }
        return ready;
    }

    public override void Activate(Collider collider, Vector3 point) {
        base.Activate(collider, point);

        InteractionCanvas.Transform.position = point;
        _TargetIndicator.transform.position = point;
        _TargetIndicator.transform.up = _GroundUp;
    }

    protected override void DeferredActivate() {
        base.DeferredActivate();

        UpdateCanvas();
        InteractionCanvas.Show();
        _TargetIndicator.GetComponent<PlantingIndicator>().Show();
    }

    protected override void DeferredDeactivate() {
        base.DeferredDeactivate();

        InteractionCanvas.Hide();
        _TargetIndicator.GetComponent<PlantingIndicator>().Hide();
    }

    protected override void BehaviourActivated(InteractionBehaviour behaviour) {
        base.BehaviourActivated(behaviour);
        InteractionCanvas.Hide();
        _TargetIndicator.GetComponent<PlantingIndicator>().Hide();
    }

    protected override void BehaviourCompleted(InteractionBehaviour behaviour) {
        base.BehaviourCompleted(behaviour);
        DeferredDeactivate();
    }
}