using UnityEngine;

public class UprootBehaviour : InteractionBehaviour {

    public string UprootMessage;
    public string PlantMessage;
    public float UprootTime;
    public Vector3 HoldingOffset;
    public Vector3 HandOffset;
    public float LookAtTime;
    public float StopDistance;
    public float ReachOutSpeed;
    public float MaxTime;
    public string HoldMessage;

    private Transform _Hand;
    private LeftHandController _HandController;
    private StateValues _UpdateValues;

    [HideInInspector]
    public Vector3 SavedHandPosition;

    protected override void Start() {
        base.Start();

        _Hand = LeftArm.Hand;
        GetComponent<UprootInteractionState>().StartHandPosition = _Hand.localPosition;
        GetComponent<UprootInteractionState>().HandParent = _Hand.parent;
        _HandController = _Hand.GetComponent<LeftHandController>();
    }

    public override bool SetTarget(GameObject target, Vector3 position) {
        if (!base.SetTarget(target, position)) {
            return false;
        }
        return true;
    }

    public override void SetControlPrompt(ControlPrompt prompt) {
        prompt.Available = true;
        prompt.Text = UprootMessage;
    }

    public override void Activate() {
        base.Activate();

        SwitchToLookingAtTarget();
    }
    
    private void SwitchToLookingAtTarget() {
        var values = new LookAtValues();
        values.StartRotation = Camera.main.transform.rotation;
        Camera.main.transform.LookAt(TargetObject.transform.position);
        values.TargetRotation = Camera.main.transform.rotation;
        Camera.main.transform.rotation = values.StartRotation;
        values.Duration = LookAtTime;

        _UpdateValues = values;
        _LateUpdate = LookingAtUpdate;
    }

    private void LookingAtUpdate() {
        base.LateWorkingUpdate();

        var values = (LookAtValues)_UpdateValues;
        values.Time += Time.deltaTime;
        var p = Easing.EaseInOut(values.Time / values.Duration, 2);
        var rotation = Quaternion.Lerp(values.StartRotation, values.TargetRotation, p);
        Camera.main.transform.rotation = rotation;

        if (values.Time > values.Duration) {
            // snap the rest of the player to the rotation to prevent offsetting
            var playerTransform = Camera.main.transform.parent;
            var playerEulers = playerTransform.rotation.eulerAngles;
            playerEulers.y = rotation.eulerAngles.y;
            playerTransform.rotation = Quaternion.Euler(playerEulers);
            Camera.main.transform.LookAt(TargetObject.transform.position);

            SwitchToReachingOut();
        }
    }

    private void SwitchToReachingOut() {
        _Hand.parent = null;
        var values = new ReachingOutValues();
        values.StartPos = _Hand.position;
        values.StartRot = _Hand.rotation;
        _Hand.GetComponent<AudioFader>().Play();

        var distance = Vector3.Distance(_Hand.position, TargetObject.transform.position);
        values.Duration = distance / ReachOutSpeed;
        if (values.Duration > MaxTime) values.Duration = MaxTime;
        var radius = TargetObject.GetComponent<StemBuilder>().Radius;
        var percent = Mathf.Clamp01((distance - StopDistance - radius)  / distance);
        values.TargetPos = Vector3.Lerp(values.StartPos, TargetObject.transform.position, percent);
        _Hand.GetComponent<Animator>().SetBool("Pickup", true);
        _HandController.MovingParticles.Play();
        RecursiveSetLayer(_Hand, 0);

        _UpdateValues = values;
        _LateUpdate = ReachingOutUpdate;
    }

    private void ReachingOutUpdate() {
		base.LateWorkingUpdate();

		var values = (ReachingOutValues)_UpdateValues;
        values.Time += Time.deltaTime;
        var time = values.Time - values.Delay;
        _Hand.LookAt(TargetObject.transform);
        var easeInOut = Easing.EaseInOut(Mathf.Clamp01(time/values.Duration), 2);
        _Hand.position = Vector3.Lerp(values.StartPos, values.TargetPos, easeInOut);
        _Hand.rotation = Quaternion.Lerp(values.StartRot, _Hand.rotation, easeInOut);

        if (values.Duration - values.Time < 1.5f && !_HandController.GrabbingParticles.isPlaying) {
            _HandController.GrabbingParticles.Play();
        }

        if (values.Time - values.Delay > values.Duration) {
            SwitchToUproot();
        }
    }

    private void SwitchToUproot() {
        var position = TargetObject.transform.position;
        var rotation = TargetObject.transform.rotation;
        _UpdateValues = new UprootValues(UprootTime, position, rotation, _Hand.position);
        _LateUpdate = UprootUpdate;
    }

    private void UprootUpdate() {
        var values = _UpdateValues as UprootValues;
        var time = values.Time + Time.deltaTime;
        var percent = time/values.Duration;
        var camTransform = Camera.main.transform;
        var targetPos = camTransform.TransformPoint(HoldingOffset);
        var easeInOut = Easing.EaseInOut(percent, 2);
        var upTarget = TargetPosition;
        upTarget.y += 0.3f;
        var adjustedTarget = Vector3.Lerp(upTarget, targetPos, Easing.EaseIn(percent, 2));
        var position = Vector3.Lerp(values.StartPosition, adjustedTarget, easeInOut);
        TargetObject.transform.position = position;

        var handTarget = camTransform.TransformPoint(HoldingOffset + HandOffset);

        var savedParent = TargetObject.transform.parent;
        TargetObject.transform.parent = camTransform;
        var targetRotEuler = TargetObject.transform.localRotation.eulerAngles;
        targetRotEuler.x = 0f;
        targetRotEuler.z = 0f;
        TargetObject.transform.localRotation = Quaternion.Euler(targetRotEuler);
        var targetRot = TargetObject.transform.rotation;
        TargetObject.transform.parent = savedParent;
        var rotation = Quaternion.Lerp(values.StartRotation, targetRot, easeInOut);
        TargetObject.transform.rotation = rotation;
        TargetObject.GetComponent<ShaderController>().SetIntensity(1f - easeInOut);

        _Hand.position = Vector3.Lerp(values.HandStart, handTarget, easeInOut);
        _Hand.LookAt(TargetObject.transform);

        if (percent >= 1f) {
            TargetObject.transform.SetParent(camTransform, true);
            HoldPlant(TargetObject
                , TargetObject.transform.localPosition
                , TargetObject.transform.localRotation);
            WorkingOver();
        }

        values.Time = time;
    }

    /// <summary>
    /// This function is used both by this behaviour as well as external functions
    /// to add plants as held for the player
    /// </summary>
    /// <param name="plant"></param>
    /// <param name="position"></param>
    /// <param name="rotation"></param>
    public void HoldPlant(GameObject plant, Vector3 position, Quaternion rotation) {
        var camTransform = GetComponentInChildren<Camera>().transform;
        plant.transform.parent = camTransform;
        plant.transform.localPosition = position;
        plant.transform.localRotation = rotation;
        plant.GetComponent<ShaderController>().SetIntensity(0f);
        _Hand.parent = camTransform;
        _Hand.position = camTransform.TransformPoint(HoldingOffset + HandOffset);
        _Hand.LookAt(plant.transform);
        // set these particle systems and animators again incase an external class
        // called this function
        _Hand.GetComponent<Animator>().SetBool("Pickup", true);
        _HandController.MovingParticles.Play();
        _HandController.GrabbingParticles.Play();
        GetComponent<UprootInteractionState>().HeldPlant = plant;
        GetComponent<UprootInteractionState>().SavedLayer = plant.layer;

        plant.SetLayers(LeftArm.gameObject.layer);
        _Hand.gameObject.SetLayers(LeftArm.gameObject.layer);
        Events.instance.Raise(new PromptTextEvent(HoldMessage));
    }
}

class UprootValues : StateValues {
    public Vector3 StartPosition;
    public Quaternion StartRotation;
    public Vector3 HandStart;

    public UprootValues(
        float duration,
        Vector3 position,
        Quaternion rotation,
        Vector3 hand
    ) : base() {
        this.Duration = duration;
        StartPosition = position;
        StartRotation = rotation;
        HandStart = hand;
    }
}
