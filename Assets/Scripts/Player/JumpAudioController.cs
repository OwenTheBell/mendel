﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpAudioController : MonoBehaviour {

    public AudioSource RocketSource;
    public AudioSource LandingSource;

    private CharacterMotor _CharacterMotor;
    private float _DefaultRocketVolume;
    private bool _GroundedLastFrame;

	void Start () {
        _CharacterMotor = GetComponent<CharacterMotor>();
        _DefaultRocketVolume = RocketSource.volume;
	}

	void Update () {
		if (
                _CharacterMotor.jumping.holdingJumpButton &&
                !_CharacterMotor.jumping.boostOver &&
                !RocketSource.isPlaying
        ) {
            RocketSource.volume = _DefaultRocketVolume;
            RocketSource.Play();
            StopAllCoroutines();
        }
        else if (_CharacterMotor.jumping.boostOver && RocketSource.isPlaying) {
            StartCoroutine(RollOffRockets());
        }
        else if (!_GroundedLastFrame && _CharacterMotor.grounded) {
            LandingSource.Play();
        }
        _GroundedLastFrame = _CharacterMotor.grounded;
	}

    IEnumerator RollOffRockets() {
        var time = 0f;
        var duration = 0.5f;
        while (time < duration) {
            time += Time.deltaTime;
            RocketSource.volume = Mathf.Lerp(_DefaultRocketVolume, 0, time / duration);
            yield return 0;
        }
        RocketSource.Stop();
        yield return null;
    }
}
