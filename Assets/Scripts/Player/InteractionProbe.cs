﻿using System.Collections.Generic;
using UnityEngine;

public class InteractionProbe : MonoBehaviour {

    public LayerMask PrePassLayer;

    private List<Collider> _IntersectedColliders;
    private CapsuleCollider _CapsuleCollider;

    [Range(0, 1)]
    public float ScreenCoverage = 1.0f;

    private void Awake() {
        _IntersectedColliders = new List<Collider>();
    }

    private void Start() {
        _CapsuleCollider = GetComponent<CapsuleCollider>();
    }

    private void OnTriggerEnter(Collider other) {
        ProcessColliderEnter(other);
    }

    private void OnTriggerExit(Collider other) {
        ProcessColliderExit(other);
    }

    private void OnCollisionEnter(Collision collision) {
        ProcessColliderEnter(collision.collider);
    }

    private void OnCollisionExit(Collision collision) {
        ProcessColliderExit(collision.collider);
    }

    void ProcessColliderEnter(Collider other) {
        if (!_IntersectedColliders.Contains(other)) {
            _IntersectedColliders.Add(other);
        }
    }

    void ProcessColliderExit(Collider other) {
        if (_IntersectedColliders.Contains(other)) {
            _IntersectedColliders.Remove(other);
        }
    }

    public bool GetIntersectedCollider(out Collider collider, out Vector3 point) {
        collider = null;
        point = Vector3.zero;

        var shortestDistance = 100f;
        var maxDistance = _CapsuleCollider.height;

        RaycastHit hit;
        var ray = new Ray(transform.position, transform.forward);
        if (Physics.Raycast(ray, out hit, maxDistance, PrePassLayer)) {
            collider = hit.collider;
            point = hit.point;
            return true;
        }

        if (_IntersectedColliders.Count == 0) {
            return false;
        }

        var hasCollider = false;

        for (var i = _IntersectedColliders.Count - 1; i >= 0; i--) {
            var testCollider = _IntersectedColliders[i];
            // the collider could potentially have been deleted without exiting
            // the probe, so check for null before testing the collider
            if (testCollider == null) {
                _IntersectedColliders.RemoveAt(i);
                continue;
            }
            var capsuleCollider = testCollider as CapsuleCollider;
            if (capsuleCollider != null) {
                var center = testCollider.transform.TransformPoint(capsuleCollider.center);
                var distance = Vector3.Distance(center, transform.position);
                
                if ((distance > shortestDistance) || (distance > maxDistance)) {
                    continue;
                }

                var viewPort = Camera.main.WorldToViewportPoint(center);
                var min = (1f - ScreenCoverage) / 2f;
                var max = 1f - min;
                if (
                        !(viewPort.x > min && viewPort.x < max) ||
                        !(viewPort.y > min && viewPort.y < max) ||
                        viewPort.z <= 0f
                    ) {
                    continue;
                }

                shortestDistance = distance;
                collider = testCollider;
                point = center;
                hasCollider = true;
            }
            else {
                if (testCollider.Raycast(ray, out hit, maxDistance)) {
                    if (hit.distance < shortestDistance) {
                        shortestDistance = hit.distance;
                        collider = testCollider;
                        point = hit.point;
                        hasCollider = true;
                    }
                }
            }
        }

        return hasCollider;
    }
}