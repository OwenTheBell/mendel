using UnityEngine;
using System;
using System.Collections;

public class PickFlowerBehaviour : InteractionBehaviour {

    public string PickMessage;
    public string FullInventoryMessage;
    public string NoFlowersMessage;
	public FlowerInventory PlayerInventory;
    public Material TransitionMaterial;
    public Material StaticMaterial;
    public Vector3 GrabOffset;
    public Vector2 SelectionRange;
    public float LookAtTime;
    public float StopDistance;
	public float ReachOutSpeed;
    public float GrabDuration;
	public float PullbackSpeed;
	public float FinishUpSpeed;
    public float MaxTime;

    public GameObject HoveredObject { get; set; }

	private StateValues _UpdateValues;
	private Transform _HandTransform;
    private GameObject _TargetFlower;
	private Transform _SavedHandParent;
	private Vector3 _SavedHandPosition;
	private Quaternion _SavedHandRotation;
	private int _CurrentIdentityViewing;
    private Vector3 _LockedPlayerPosition;
    private LeftHandController _HandController;
    private FlowerNode _SelectedFlowerNode;

    public override bool SetTarget(GameObject target, Vector3 position) {
        if (!base.SetTarget(target, position)) {
            return false;
        }

		if (PlayerInventory.IsFull) {
            Accessible = false;
			return false;
		}

        if (target.GetComponent<FlowerCreator>().AvailableFlowerCount == 0) {
            Accessible = false;
            return false;
        }

        Accessible = true;
		TargetPosition = TargetObject.transform.position;

		return true;
    }

    public override void SetControlPrompt(ControlPrompt prompt) {
        prompt.Available = true;
		if (PlayerInventory.IsFull) {
            prompt.Negative = true;
            prompt.Text = FullInventoryMessage;
		}
        else if (TargetObject.GetComponent<FlowerCreator>().AvailableFlowerCount == 0) {
            prompt.Negative = true;
            prompt.Text = NoFlowersMessage;
        }
        else {
            prompt.Negative = false;
            prompt.Text = PickMessage;
        }
    }

    protected override void Start() {
		base.Start();

		_HandTransform = LeftArm.Hand;
        _HandController = _HandTransform.GetComponent<LeftHandController>();
	}

	protected override void Update() {
		base.Update();
	}

	public override void Activate() {
		base.Activate();

        _LockedPlayerPosition = transform.position;
        try {
            var testingObject = HoveredObject;
            while (_SelectedFlowerNode == null) {
                if (testingObject.HasComponent<FlowerCreator>()) {
                    _SelectedFlowerNode = GetFlower(testingObject, true, SelectionRange.x, SelectionRange.y);
                    if (_SelectedFlowerNode == null) {
                        _SelectedFlowerNode = GetFlower(testingObject, true, 0f, SelectionRange.y);
                    }
                    if (_SelectedFlowerNode == null) {
                        _SelectedFlowerNode = GetFlower(testingObject, false, 0f, SelectionRange.y);
                    }
                    if (_SelectedFlowerNode == null) {
                        _SelectedFlowerNode = GetFlower(testingObject, false, 0f, Mathf.Infinity);
                    }
                }
                else if (testingObject.HasComponent<FlowerNode>()) {
                    _SelectedFlowerNode = testingObject.GetComponent<FlowerNode>();
                }
                if (testingObject.transform.parent == null) {
                    break;
                }
                testingObject = testingObject.transform.parent.gameObject;
            }
            if (_SelectedFlowerNode != null) {
                TargetPosition = CalculateTargetPosition();
                SwitchToLookingAtTarget();
            }
        }
        catch (NullReferenceException e) {
            WorkingOver();
        }
    }

    private FlowerNode GetFlower(GameObject gO, bool visible, float minRange, float maxRange) {
        var selectedIndex = -1;
        var closestDistance = Mathf.Infinity;
        var flowerNodes = gO.GetComponent<FlowerCreator>().AvailableFlowerNodes;
        var screenCenter = new Vector2(Screen.width/2, Screen.height/2);
        for (var i = 0; i < flowerNodes.Length; i++) {
            var position = flowerNodes[i].transform.position;
            if (visible) {
                var viewPosition = Camera.main.WorldToViewportPoint(position);
                if (viewPosition.x < 0
                    || viewPosition.x > 1
                    || viewPosition.y < 0
                    || viewPosition.y > 1) {
                    continue;
                }
            }
            var distance = Vector2.Distance(transform.position, position);
            if (distance < closestDistance && distance >= minRange && distance <= maxRange) {
                closestDistance = distance;
                selectedIndex = i;
            }
        }
        return (selectedIndex == -1) ? null : flowerNodes[selectedIndex];
    }

    private void SwitchToLookingAtTarget() {
        var values = new LookAtValues();
        values.StartRotation = Camera.main.transform.rotation;
        Camera.main.transform.LookAt(TargetPosition);
        values.TargetRotation = Camera.main.transform.rotation;
        Camera.main.transform.rotation = values.StartRotation;
        values.Duration = LookAtTime;

        _UpdateValues = values;
        _LateUpdate = LookingAtUpdate;
    }

    private void LookingAtUpdate() {
        base.LateWorkingUpdate();

        var values = (LookAtValues)_UpdateValues;
        values.Time += Time.deltaTime;
        var p = Easing.EaseInOut(values.Time / values.Duration, 2);
        var rotation = Quaternion.Lerp(values.StartRotation, values.TargetRotation, p);
        Camera.main.transform.rotation = rotation;

        if (values.Time > values.Duration) {
            // snap the rest of the player to the rotation to prevent offsetting
            var playerTransform = Camera.main.transform.parent;
            var playerEulers = playerTransform.rotation.eulerAngles;
            playerEulers.y = rotation.eulerAngles.y;
            playerTransform.rotation = Quaternion.Euler(playerEulers);
            Camera.main.transform.LookAt(TargetPosition);

            SwitchToReachingOut();
        }
    }

    private void SwitchToReachingOut() {
		_HandTransform = LeftArm.Hand;
		_SavedHandParent = _HandTransform.parent;
		_SavedHandPosition = _HandTransform.localPosition;
		_SavedHandRotation = _HandTransform.localRotation;
        var position = _HandTransform.position;
        var rotation = _HandTransform.rotation;
        _HandTransform.parent = null;
        _HandTransform.position = position;
        _HandTransform.rotation = rotation;
        RecursiveSetLayer(_HandTransform, 0);
        _HandTransform.GetComponent<AudioFader>().Play();

		var values = new ReachingOutValues();
		values.StartPos = _HandTransform.position;
		values.StartRot = _HandTransform.rotation;
		var distance = Vector3.Distance(values.StartPos, TargetPosition);
		values.Duration = distance/ReachOutSpeed;
        if (values.Duration > MaxTime) values.Duration = MaxTime;
        var percent = Mathf.Clamp01((distance - StopDistance)  / distance);
        values.TargetPos = Vector3.Lerp(values.StartPos, TargetPosition, percent);
        _HandTransform.LookAt(TargetPosition);
        values.TargetRot = _HandTransform.rotation * Quaternion.AngleAxis(-20, _HandTransform.up);
        _HandTransform.rotation = values.StartRot;
        _HandTransform.GetComponent<Animator>().SetBool("Pickup", true);
        _HandController.MovingParticles.Play();

		_UpdateValues = values;
		_LateUpdate = ReachingOutUpdate;
    }

	private void ReachingOutUpdate() {
		base.LateWorkingUpdate();
        transform.position = _LockedPlayerPosition;
		var values = (ReachingOutValues)_UpdateValues;

        var target = CalculateTargetPosition();
		var distance = Vector3.Distance(values.StartPos, target);
        var percent = Mathf.Clamp01((distance - StopDistance)  / distance);
        var adjustedTarget = Vector3.Lerp(values.StartPos, target, percent);

        values.Time += Time.deltaTime;
        var time = values.Time;
        _HandTransform.LookAt(target);
        var easeInOut = Easing.EaseInOut(Mathf.Clamp01(time/values.Duration), 2);
        _HandTransform.position = Vector3.Lerp(values.StartPos, adjustedTarget, easeInOut);
        _HandTransform.rotation = Quaternion.Lerp(values.StartRot, _HandTransform.rotation, easeInOut);

        if (values.Duration - values.Time < 1.5f && !_HandController.GrabbingParticles.isPlaying) {
            _HandController.GrabbingParticles.Play();
        }

        if (values.Time > values.Duration) {
            SwitchToRetracting();
        }
	}

    private void SwitchToRetracting() {
        _TargetFlower = TargetObject.GetComponent<FlowerCreator>().PickFlowerFromNode(_SelectedFlowerNode);

        var nextValues = new RetractingValues();
        nextValues.StartPos = _TargetFlower.transform.position;
        nextValues.StartRot = _TargetFlower.transform.rotation;
        nextValues.StartHandPos = _HandTransform.position;
        nextValues.StartHandRot = _HandTransform.rotation;
        nextValues.StartDelay = 0f;

        var size = _TargetFlower.GetComponentInChildren<MeshFilter>().sharedMesh.bounds.size;
        var max = 0f;
        for (var i = 0; i < 3; i++) {
            if (size[i] > max) max = size[i];
        }
        nextValues.TargetScale = Vector3.one * (PlayerInventory.ReducedScale / max);
        var openSlot = PlayerInventory.FirstOpenSlot;
        openSlot.y += 0.1f;
        var distance = Vector3.Distance(nextValues.StartPos, openSlot);
        nextValues.Duration = distance / PullbackSpeed;
        if (nextValues.Duration > MaxTime) nextValues.Duration = MaxTime;
        var renderer = _TargetFlower.GetComponent<MeshRenderer>();
        _LateUpdate = RetractingUpdate;
        _UpdateValues = nextValues;

        PlayerAnimator.SetTrigger("Show inventory");
    }

    private void RetractingUpdate() {
		base.LateWorkingUpdate();
        transform.position = _LockedPlayerPosition;

        var values = (RetractingValues)_UpdateValues;
        values.Time += Time.deltaTime;
        var targetPos = PlayerInventory.FirstOpenSlot;
        targetPos.y += 0.1f;

        var handTarget = Camera.main.transform.InverseTransformPoint(targetPos);
        handTarget += GrabOffset;
        handTarget = Camera.main.transform.TransformPoint(handTarget);

        var savedRotation = _TargetFlower.transform.rotation;
        _TargetFlower.transform.parent = PlayerInventory.transform;
        _TargetFlower.transform.localRotation = Quaternion.identity;
        var targetRot = _TargetFlower.transform.rotation;
        _TargetFlower.transform.parent = null;
        _TargetFlower.transform.rotation = savedRotation;

        var time = values.Time - values.StartDelay;
        var easeInOut = Easing.EaseInOut(Mathf.Clamp01(time / values.Duration), 2);

        var flowerPos = Vector3.Lerp(values.StartPos, targetPos, easeInOut);
        _TargetFlower.transform.position = flowerPos;
        _TargetFlower.transform.rotation = Quaternion.Lerp(values.StartRot, targetRot, easeInOut);
        _HandTransform.transform.position = Vector3.Lerp(values.StartHandPos, handTarget, easeInOut);
        _HandTransform.forward = (flowerPos - _HandTransform.transform.position).normalized;
        _TargetFlower.transform.localScale = Vector3.Lerp(Vector3.one, values.TargetScale, easeInOut);

        if (values.Duration - values.Time - values.StartDelay < 1f && _HandController.GrabbingParticles.isPlaying) {
            _HandController.GrabbingParticles.Stop();
        }

        if (values.Time - values.StartDelay >= values.Duration) {
            SwitchToReturnHand();
        }
    }

    private void SwitchToReturnHand() {
        PlayerAnimator.SetTrigger("Open grasper");

        var nextValues = new ReturningValues();

        _HandTransform.parent = _SavedHandParent;

        _TargetFlower.transform.parent = null;
        _TargetFlower.GetComponent<MeshRenderer>().material = StaticMaterial;
        PlayerInventory.Add(_TargetFlower.transform);
        _SelectedFlowerNode = null;

        nextValues.StartPos = _HandTransform.localPosition;
        nextValues.StartRot = _HandTransform.localRotation;
        nextValues.StartDelay = 0.25f;
        var distance = Vector3.Distance(_HandTransform.localPosition, _SavedHandPosition);
        nextValues.Duration = distance/FinishUpSpeed;

        _HandTransform.GetComponent<Animator>().SetBool("Pickup", false);
        _HandController.GrabbingParticles.Stop();

        _UpdateValues = nextValues;
        _LateUpdate = ReturnHandUpdate;
    }

	private void ReturnHandUpdate() {
		base.LateWorkingUpdate();
        transform.position = _LockedPlayerPosition;

		var values = (ReturningValues)_UpdateValues;
		values.Time += Time.deltaTime;
        var time = values.Time - values.StartDelay;
        var easeInOut = Easing.EaseInOut(Mathf.Clamp01(time/values.Duration), 2);
        var easeIn = Easing.EaseIn(Mathf.Clamp01(time / values.Duration), 2);
        var easeOut = Easing.EaseOut(Mathf.Clamp01(time / values.Duration), 2);

        var x = Mathf.Lerp(values.StartPos.x, _SavedHandPosition.x, easeOut);
        var y = Mathf.Lerp(values.StartPos.y, _SavedHandPosition.y, easeIn);
        var z = Mathf.Lerp(values.StartPos.z, _SavedHandPosition.z, easeOut);

        _HandTransform.localPosition = new Vector3(x, y, z);
        _HandTransform.localRotation = Quaternion.Lerp(values.StartRot, _SavedHandRotation, easeInOut);

		if (values.Time - values.StartDelay >= values.Duration) {
            RecursiveSetLayer(_HandTransform, LeftArm.gameObject.layer);
            _HandTransform.GetComponent<AudioFader>().Stop();
            _HandController.MovingParticles.Stop();
            SwitchToLateUpdate();
		}
	}

    private void SwitchToLateUpdate() {
        PlayerAnimator.SetTrigger("Hide inventory");
        _LateUpdate = LateWorkingUpdate;
    }

	protected override void ArmAnimationOver() {
		if (!Working) {
			return;
		}

		base.ArmAnimationOver();
		WorkingOver();
	}

	protected override void WorkingOver() {
		base.WorkingOver();
	}

    Vector3 CalculateTargetPosition() {
        var collider = _SelectedFlowerNode.GetComponentInChildren<Collider>();
        var position = Vector3.zero;
        if (collider != null) {
            position = collider.transform.position;
        }
        else {
            position = _SelectedFlowerNode.transform.position;
        }
        return position;
    }

}

class StateValues {
	public float Time;
	public float Duration;
	public StateValues() {
		Time = 0f;
	}
}

class LookAtValues : StateValues {
    public Quaternion StartRotation;
    public Quaternion TargetRotation;

    public LookAtValues() : base() { }
}

class ReachingOutValues : StateValues {
	public Vector3 StartPos;
    public Vector3 TargetPos;
	public Quaternion StartRot;
    public Quaternion TargetRot;
	public float Delay = 0.0f;

	public ReachingOutValues() : base() {}
}

class GrabbingValues : StateValues {
    public Quaternion StartRot;
    public Vector3 StartPos;
    public float StartingPercent;
    public Vector3 FreezePosition;
    public Quaternion FreezeRotation;

    public GrabbingValues() : base() {}
}

class RetractingValues : StateValues {
	public Vector3 StartPos;
	public Quaternion StartRot;
    public Vector3 StartHandPos;
    public Quaternion StartHandRot;
    public Vector3 TargetScale;
	public float StartDelay;
	public bool HandOpened = false;

	public RetractingValues() : base() {}	
}

class ReturningValues : StateValues {
	public Vector3 StartPos;
	public Quaternion StartRot;
	public float StartDelay;

	public ReturningValues() : base() {}	
}
