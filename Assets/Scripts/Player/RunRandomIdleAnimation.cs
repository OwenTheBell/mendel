﻿using UnityEngine;

public class RunRandomIdleAnimation : MonoBehaviour {

    public Animator LeftAnimator;
    public Animator RightAnimator;
    public float WaitTime;

    private float _TimeSinceLastInput = 0f;

    // Use this for initialization
    void Start() {

    }

    // Update is called once per frame
    void Update() {
        if (Input.anyKey ||
            Input.GetButton("Action 1") ||
            Input.GetButton("Action 2") ||
            Input.GetAxis("Mouse X") != 0f ||
            Input.GetAxis("Mouse Y") != 0f
        ) {
            _TimeSinceLastInput = 0f;
        }
        _TimeSinceLastInput += Time.deltaTime;
        if (_TimeSinceLastInput >= WaitTime) {
            if (Random.value < 0.5f) {
                LeftAnimator.SetTrigger("Idle");
            }
            else {
                RightAnimator.SetTrigger("Idle");
            }
            _TimeSinceLastInput = 0f;
        }
    }
}
