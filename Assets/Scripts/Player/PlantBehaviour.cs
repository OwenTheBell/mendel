using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PlantBehaviour : InteractionBehaviour, ICancelReciever {

    public FlowerInventory PlayerInventory;
    public GameObject PlantPrefab;
    public string ExitMessage;
    public GameObject PlantingParticlesPrefab;
    public GameObject PlantBlocker;
    public Material DestroyFlowerMaterial;
    public InteractionPrompt SelectParentPrompt;
    public GameObject PlantingAudio;
    public AudioSource ActionAudio;
    public Transform Hand;
    public Transform[] RingTransforms;
    public Vector3 SelectionCanvasOffset;
    public LayerMask GroundLayer;
    public float GroundOffset;
    public int PlantsBeforeMutation;

    public string ReadyToPlantMessage;
    public string MoreSamplesMessage;
    public string ScreenshotMessage;

    public float RingToFlowerTime;
    public Vector3 ParentOffset;
    public float LerpToParentTime;
    public float ReturnRingsTime;

    [SerializeField]
    private float _SlotChangeRequirement;
    public float PlantTime = 0.5f;

    private float _CurrentMousePosition;
    private float _MouseRange;
    private Vector3 _FirstSlotPosition;
    private Vector3 _LastSlotPosition;
    private int _PlantCount;

    public Vector3 CanvasOffset;

    private int _CurrentIndex = 0;

    private float _TotalMouseMovement = 0f;
    private float _LastMouseAxis = 0f;

    private Vector3[] _SlotPositions;
    private bool[] _SlotsOccupied;
    private Transform _LeftParent;
    private Transform _RightParent;
    private bool _Planting;

    private SavedTransformInfo[] _SavedInfo;
    struct SavedTransformInfo {
        public Transform parent;
    }
    private int _AvailableRings;
    private int _ParentsInPosition;
    private Coroutine _MoveCanvasRoutine;
    private Quaternion _DefaultRightArmRotation;

    private Coroutine _PositioningRightArm;

    private int ParentCount {
        get {
            var value = 0;
            foreach (var slot in _SlotsOccupied) {
                if (slot) value++;
            }
            return value;
        }
    }

    private bool _Quit = false;

    protected override void Awake() {
        base.Awake();
    }

    protected override void Start() {
        base.Start();

        SelectParentPrompt.transform.SetParent(PlayerInventory.transform);
        SelectParentPrompt.transform.localRotation = Quaternion.Euler(Vector3.up * -90f);

        SelectParentPrompt.Hide();
    }

    public override bool SetTarget(GameObject target, Vector3 position) {
        if (!base.SetTarget(target, position)) {
            return false;
        }

        if (PlayerInventory.Count < 2) {
            Accessible = false;
        }
        else {
            Accessible = true;
        }

        TargetPosition = position;

        return true;
    }

    public override void SetControlPrompt(ControlPrompt prompt) {
        prompt.Available = true;
        if (PlayerInventory.Count < 2) {
            prompt.Text = MoreSamplesMessage;
            prompt.Negative = true;
        }
        else {
            prompt.Text = ReadyToPlantMessage;
            prompt.Negative = false;
        }
    }

    public override void Activate() {
        base.Activate();
        PlayerAnimator.SetTrigger("Show inventory");
        Hand.GetComponent<Animator>().SetBool("Freeze", true);
        Events.instance.Raise(new PromptTextEvent(ExitMessage));
        Services.CancelStack.Add(this, false);
        _Quit = false;
        _ParentsInPosition = 0;
        _AvailableRings = 2;
        _Planting = true;

        _SlotPositions = new Vector3[2] {
            new Vector3(-ParentOffset.x, ParentOffset.y, ParentOffset.z),
            new Vector3(ParentOffset.x, ParentOffset.y, ParentOffset.z),
        };
        var localPosition = Camera.main.transform.InverseTransformPoint(TargetPosition);
        for (var i = 0; i < 2; i++) {
            _SlotPositions[i] = _SlotPositions[i] + localPosition;
            _SlotPositions[i] = Camera.main.transform.TransformPoint(_SlotPositions[i]);
        }
        _SlotsOccupied = new bool[2];

        PlantingAudio.transform.position = TargetPosition;
    }

    private void InventoryVisible() {
        if (!Working) {
            return;
        }

        _SavedInfo = new SavedTransformInfo[RingTransforms.Length];
        for (var i = 0; i < RingTransforms.Length; i++) {
            _SavedInfo[i].parent = RingTransforms[i].parent;
            RingTransforms[i].parent = Hand;
        }
        Hand.GetComponent<Animator>().enabled = false;

        _MouseRange = _SlotChangeRequirement * PlayerInventory.Slots;
        _CurrentMousePosition = 0f;
        _FirstSlotPosition = PlayerInventory.AllSlots[0];
        _LastSlotPosition = PlayerInventory.AllSlots[PlayerInventory.Slots - 1];

        _PositioningRightArm = StartCoroutine(PositionRightArm());
    }

    IEnumerator PositionRightArm() {
        RightArm.PointElbowAtOver(_FirstSlotPosition, 0.3f);
        yield return new WaitForSeconds(0.3f);

        _LateUpdate = SelectingUpdate;
        _LastMouseAxis = Input.GetAxis("Mouse X");
        _CurrentIndex = 0;
        UpdateSlot(_CurrentIndex);
        _PositioningRightArm = null;
    }

    private void SelectingUpdate() {
        _TotalMouseMovement += Input.GetAxis("Mouse X");
        _CurrentMousePosition += Input.GetAxis("Mouse X") * -1;
        _CurrentMousePosition = Mathf.Clamp(_CurrentMousePosition, 0f, _MouseRange);

        var percent = _CurrentMousePosition / _MouseRange;

        var pointAt = Vector3.Lerp(_FirstSlotPosition, _LastSlotPosition, percent);
        RightArm.PointElbowAt(pointAt);

        var slots = PlayerInventory.Slots;
        var testIndex = Mathf.Clamp((int)Mathf.Floor(percent * slots), 0, slots - 1);
        if (testIndex != _CurrentIndex && ParentCount < 2) {
            UpdateSlot(testIndex);
        }

        if (Input.GetButtonDown("Action 1")) {
            if (_AvailableRings > 0 && PlayerInventory.SlotOccupied(_CurrentIndex)) {
                var name = PlayerInventory.NameAtSlot(_CurrentIndex);
                if (name == string.Empty) name = PROPERTIES.NoNameMessage;
                var flowerTransform = PlayerInventory.RetrieveAt(_CurrentIndex);
                flowerTransform.transform.SetParent(null);
                var slot = (_SlotsOccupied[0]) ? 1 : 0;
                ActionAudio.Play();
                StartCoroutine(MoveRingToSelectedFlower(flowerTransform, slot, name));
                UpdateSlot(_CurrentIndex);
            }
        }
        if (Input.GetButtonDown("Cancel")) {
            ActionAudio.Play();
        }
    }

    public void OnCancel() {
        QuitEarly();
    }

    void QuitEarly() {
        _Quit = true;

        if (_Planting) {
            if (_SlotsOccupied[0] && _LeftParent != null) {
                _LeftParent.localScale = Vector3.one;
                PlayerInventory.Add(_LeftParent);
            }
            if (_SlotsOccupied[1] && _RightParent != null) {
                _RightParent.localScale = Vector3.one;
                PlayerInventory.Add(_RightParent);
            }

            HideAllCanvases();
            PlayerAnimator.SetTrigger("Hide inventory");
            PlayerInventory.DeHover(_CurrentIndex);
            _LateUpdate = null;
            _Planting = false;
        }

        if (_PositioningRightArm != null) {
            StopCoroutine(_PositioningRightArm);
        }
        WorkingOver();
        RightArm.ReturnElbowToDefault(0.3f);
        StartCoroutine(ReturnRings());
        return;
    }

    void HideAllCanvases() {
        Events.instance.Raise(new HidePromptTextEvent());
        SelectParentPrompt.Hide();
    }

    private void UpdateSlot(int slot) {
        if (PlayerInventory.SlotOccupied(_CurrentIndex)) {
            PlayerInventory.DeHover(_CurrentIndex);
        }

        _CurrentIndex = slot;

        if (ParentCount < 2 && _AvailableRings > 0) {
            var transform = PlayerInventory.Peak(_CurrentIndex);
            if (transform != null) {
                PlayerInventory.Hover(_CurrentIndex);
                var position = PlayerInventory.AllLocalSlots[_CurrentIndex];
                position.y += 0.4f;
                var name = PlayerInventory.NameAtSlot(_CurrentIndex);
                name = (name == string.Empty) ? PROPERTIES.NoNameMessage : name;
                if (_MoveCanvasRoutine != null) {
                    StopCoroutine(_MoveCanvasRoutine);
                }
                _MoveCanvasRoutine = StartCoroutine(ShowParentCanvas(position, name));
            }
            else {
                SelectParentPrompt.Hide();
                _CurrentIndex = -1;
            }
        }
        else {
            SelectParentPrompt.Hide();
            _CurrentIndex = -1;
        }
    }

    IEnumerator ShowParentCanvas(Vector3 position, string name) {
        SelectParentPrompt.Hide();
        while (SelectParentPrompt.Visible) yield return null;
        SelectParentPrompt.Transform.localPosition = position;
        SelectParentPrompt.Label = name;
        SelectParentPrompt.Show();
    }

    IEnumerator MoveRingToSelectedFlower(Transform flower, int slot, string name) {
        _SlotsOccupied[slot] = true;
        var ring = RingTransforms[0];
        if (slot == 1) {
            ring = RingTransforms[1];
        }
        ring.SetParent(null);
        flower.SetParent(null);
        _AvailableRings--;

        ring.GetComponent<AudioFader>().Play();

        if (slot == 0) {
            _LeftParent = flower;
        }
        else {
            _RightParent = flower;
        }

        var ringStart = ring.position;
        var leftTarget = ring.right * -0.4f + ring.up * -0.1f + ring.position;
        var ringRotation = ring.rotation;
        var targetEuler = ring.rotation.eulerAngles;
        targetEuler.z = 90f;
        var targetRotation = Quaternion.Euler(targetEuler);
        var time = 0f;

        while (time < RingToFlowerTime) {
            if (_Quit) {
                yield break;
            }
            time += Time.deltaTime;
            var p = Mathf.Clamp01(time / RingToFlowerTime);
            var target = Vector3.Lerp(leftTarget, flower.position, p);
            var easeInOut = Easing.EaseInOut(p, 2);
            ring.position = Vector3.Lerp(ringStart, target, easeInOut);
            ring.rotation = Quaternion.Lerp(ringRotation, targetRotation, easeInOut);
            yield return 0;
        }

        StartCoroutine(MoveFlowerToParentSlot(flower, ring, slot, name));
        yield return null;
    }

    IEnumerator MoveFlowerToParentSlot(Transform flower, Transform ring, int slot, string name) {
        var startPos = flower.position;
        var startScale = flower.localScale;
        var startRingRight = ring.right;
        var startRingRot = ring.rotation;
        ring.right = Vector3.up;
        var targetRingRot = ring.rotation;
        var startFlowerRot = flower.rotation;
        flower.up = Vector3.up;
        var targetFlowerRot = flower.rotation;
        var targetPosition = _SlotPositions[slot];

        RaycastHit hit;
        if (Physics.Linecast(startPos, targetPosition, out hit, GroundLayer)) {
            var adjustedPoint = hit.point - startPos;
            adjustedPoint *= (adjustedPoint.magnitude - GroundOffset) / adjustedPoint.magnitude;
            targetPosition = adjustedPoint + startPos;
        }

        var time = 0f;
        while (time < LerpToParentTime) {
            if (_Quit) {
                yield break;
            }
            time += Time.deltaTime;
            var easeInOut = Easing.EaseInOut(time / LerpToParentTime, 2);
            ring.position = Vector3.Lerp(startPos, targetPosition, easeInOut);
            ring.right = Vector3.Lerp(startRingRight, Vector3.up, easeInOut);
            ring.rotation = Quaternion.Lerp(startRingRot, targetRingRot, easeInOut);
            var flowerPosition = targetPosition;
            flowerPosition.y += 0.05f;
            flower.position = Vector3.Lerp(startPos, flowerPosition, easeInOut);
            flower.localScale = Vector3.Lerp(startScale, Vector3.one, easeInOut);
            flower.rotation = Quaternion.Lerp(startFlowerRot, targetFlowerRot, easeInOut);
            yield return 0;
        }

        _ParentsInPosition++;
        if (_ParentsInPosition == 2) {
            HideAllCanvases();
            PlayerAnimator.SetTrigger("Hide inventory");

            var flowers = new Transform[] { _LeftParent, _RightParent };
            StartCoroutine(PlantFlowers(TargetPosition, flowers));
            _Planting = false;

            _LateUpdate = null;
        }

        yield return null;
    }

    IEnumerator ReturnToInventory(int slot) {

        var ring = RingTransforms[0];
        if (slot == 1) {
            ring = RingTransforms[1];
        }

        var startPos = ring.position;
        var startRot = ring.rotation;
        var fader = ring.GetComponent<AudioFader>();

        var time = 0f;
        while (time < LerpToParentTime) {
            if (_Quit) {
                yield break;
            }
            time += Time.deltaTime;
            var rightTarget = Hand.right * 0.4f + Hand.position;
            var targetRot = Hand.rotation;

            var p = Mathf.Clamp01(time / LerpToParentTime);
            var rotationInOut = Easing.EaseInOut(Mathf.Clamp01((p - 0.2f) / 0.4f), 2);
            var target = Vector3.Lerp(rightTarget, Hand.position, Easing.EaseIn(p, 4));
            ring.position = Vector3.Lerp(startPos, target, Easing.EaseInOut(p, 2));
            ring.rotation = Quaternion.Lerp(startRot, Hand.rotation, rotationInOut);

            if (LerpToParentTime - time < fader.Duration && fader.Playing) {
                fader.Stop();
            }
            yield return 0;
        }

        ring.parent = Hand;
        ring.GetComponent<AudioFader>().Stop();
        _AvailableRings++;

        yield return null;
    }

    IEnumerator PlantFlowers(Vector3 target, Transform[] flowers) {
        var leftParticles = (Instantiate(PlantingParticlesPrefab) as GameObject).GetComponent<ParticleSystem>();
        leftParticles.transform.position = flowers[0].position;
        leftParticles.GetComponent<BendPlantingParticles>().Target = TargetPosition;
        var rightParticles = (Instantiate(PlantingParticlesPrefab) as GameObject).GetComponent<ParticleSystem>();
        rightParticles.transform.position = flowers[1].position;
        rightParticles.GetComponent<BendPlantingParticles>().Target = TargetPosition;
        leftParticles.Play();
        SetStartColorsForSystem(leftParticles, flowers[0].GetComponent<PlantInfo>());
        rightParticles.Play();
        SetStartColorsForSystem(rightParticles, flowers[1].GetComponent<PlantInfo>());
        var audio = Instantiate(PlantingAudio) as GameObject;

        var blocker = Instantiate(PlantBlocker) as GameObject;
        blocker.transform.position = TargetPosition;

        DestroyFlowerMaterial.SetFloat("_StartTime", Time.time);
        DestroyFlowerMaterial.SetFloat("_TotalTime", PlantTime);
        _LeftParent.GetComponent<MeshRenderer>().material = DestroyFlowerMaterial;
        _RightParent.GetComponent<MeshRenderer>().material = DestroyFlowerMaterial;
        audio.GetComponent<AudioFader>().Play();

        yield return new WaitForSeconds(0.5f);
        Services.CancelStack.Remove(this);
        yield return new WaitForSeconds(PlantTime - 1);

        // planting over, handle generating the child
        var ids = new List<int>(flowers.Length);
        for (int i = 0; i < flowers.Length; i++) {
            ids.Add(flowers[i].GetComponent<PlantInfo>().Identity);
        }
        bool mutate = (_PlantCount > PlantsBeforeMutation);
        var plant = PlantFunctions.GeneratePlant(PlantPrefab, ids[0], ids[1], target, mutate);
        _PlantCount++;
        GeneManager geneManager = plant.GetComponent<GeneManager>();
        var seed = Services.SeedVault.Get(plant.GetComponent<PlantInfo>().Identity);
        foreach (var flower in flowers) {
            Destroy(flower.gameObject);
        }
        leftParticles.Stop();
        rightParticles.Stop();
        audio.GetComponent<AudioFader>().Stop();
        yield return new WaitForSeconds(1.25f);
        geneManager.Grow();
        Events.instance.Raise(new ScreenshotPromptEvent());
        Destroy(leftParticles.gameObject);
        Destroy(rightParticles.gameObject);
        Destroy(audio);
        Destroy(blocker);
    }

    IEnumerator ReturnRings() {
        var startPositions = new Vector3[RingTransforms.Length];
        var startRotations = new Quaternion[RingTransforms.Length];
        for (var i = 0; i < RingTransforms.Length; i++) {
            startPositions[i] = RingTransforms[i].position;
            startRotations[i] = RingTransforms[i].rotation;
            RingTransforms[i].GetComponent<AudioFader>().Stop();
        }

        var time = 0f;
        while (time < ReturnRingsTime) {
            time += Time.deltaTime;
            var easeInOut = Easing.EaseInOut(time / ReturnRingsTime, 2);
            for (var i = 0; i < RingTransforms.Length; i++) {
                RingTransforms[i].position = Vector3.Lerp(RingTransforms[i].position, Hand.position, easeInOut);
                RingTransforms[i].rotation = Quaternion.Lerp(RingTransforms[i].rotation, Hand.rotation, easeInOut);
            }
            yield return 0;
        }

        // it is possible for this to be null if the player exists planting before the inventory
        // is in place
        if (_SavedInfo != null) {
            for (var i = 0; i < RingTransforms.Length; i++) {
                RingTransforms[i].parent = _SavedInfo[i].parent;
                RingTransforms[i].localPosition = Vector3.zero;
                RingTransforms[i].localRotation = Quaternion.identity;
            }
        }

        Hand.GetComponent<Animator>().enabled = true;
        Hand.GetComponent<Animator>().SetBool("Freeze", false);
        yield return null;
    }

    void SetStartColorsForSystem(ParticleSystem system, PlantInfo info) {
        var seed = Services.SeedVault.Get(info.Identity);

        var color1 = (seed.Genes[typeof(BaseFlowerColorGene)] as BaseFlowerColorGene).BaseRGBColor;
        var color2 = (seed.Genes[typeof(BaseFlowerColorGene)] as BaseFlowerColorGene).TipRGBColor;
        var main = system.main;
        main.startColor = new ParticleSystem.MinMaxGradient(color1, color2);
    }
}