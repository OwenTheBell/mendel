﻿using UnityEngine;

public class PlantColliderDetector : MonoBehaviour {

	private void OnTriggerEnter(Collider other) {
		if (other.gameObject.HasComponent<GeneManager>()) {
            foreach (var node in other.gameObject.GetComponentsInChildren<PetalFlowerNode>()) {
                node.ActiveFlowerColliders(true);
            }
		}
	}

	private void OnTriggerExit(Collider other) {
		if (other.gameObject.HasComponent<GeneManager>()) {
            foreach (var node in other.gameObject.GetComponentsInChildren<PetalFlowerNode>()) {
                node.ActiveFlowerColliders(false);
            }
		}
	}
}