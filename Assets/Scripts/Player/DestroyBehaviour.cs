﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DestroyBehaviour : InteractionBehaviour, ITertiaryBehaviour {

    public string DestroyMessage;
    public Material FlowerDestroy;
    public Material StemDestroy;
    public float StopDistance;
    public float ExtendSpeed;
    public float MaxTravelTime;
    public float DestroyTime;

    private Transform _Hand;
    private Transform _Plant;

    private float _Time;
    private float _Duration;
    private Vector3 _StartPosition;
    private Quaternion _StartRotation;

    public float TertiaryCompletion { get; set; }

    private UprootInteractionState _UprootState;

    protected override void Start() {
        base.Start();

        _Hand = LeftArm.Hand;
        _UprootState = GetComponent<UprootInteractionState>();
    }

    public override bool SetTarget(GameObject target, Vector3 position) {
        if (!base.SetTarget(target, position)) {
            Accessible = false;
            return false;
        }

        Accessible = true;
        return true;
    }

    public override void Activate() {
        base.Activate();

        _Plant = _UprootState.HeldPlant.transform;
        _Plant.parent = null;
        _Hand.parent = _Plant;
        _StartPosition = _Plant.position;
        _StartRotation = _Plant.rotation;

        TargetPosition = Camera.main.transform.forward * StopDistance;
        TargetPosition += Camera.main.transform.position;

        var distance = Vector3.Distance(_StartPosition, TargetPosition);
        _Time = 0f;
        _Duration = distance / ExtendSpeed;
        if (_Duration > MaxTravelTime) _Duration = MaxTravelTime;

        Events.instance.Raise(new HidePromptTextEvent());
        _UprootState.HeldPlant = null;
        _LateUpdate = ExtendingUpdate;
    }

    public override void SetControlPrompt(ControlPrompt prompt) {
        prompt.Available = true;
        prompt.Text = DestroyMessage;
    }

    void ExtendingUpdate() {
        _Time += Time.deltaTime;
        var p = _Time / _Duration;
        var easeInOut = Easing.EaseInOut(p, 2);

        var targetRotEuler = _StartRotation.eulerAngles;
        targetRotEuler.x = 0f;
        targetRotEuler.z = 0f;
        var targetRot = Quaternion.Euler(targetRotEuler);
        _Plant.position = Vector3.Lerp(_StartPosition, TargetPosition, easeInOut);
        _Plant.rotation = Quaternion.Lerp(_StartRotation, targetRot, easeInOut);

        if (_Time >= _Duration) {
            StartCoroutine(DestroyPlant());
            _Hand.parent = _UprootState.HandParent;
            _StartPosition = _Hand.localPosition;
            _StartRotation = _Hand.localRotation;
            _Hand.GetComponent<Animator>().SetBool("Pickup", false);
            _Hand.GetComponentInChildren<ParticleSystem>().Stop();

            var distance = Vector3.Distance(_StartPosition, _UprootState.StartHandPosition);
            _Duration = distance / ExtendSpeed;
            _Time = 0f;
            _LateUpdate = RetractingUpdate;
        }
    }

    void RetractingUpdate() {
        _Time += Time.deltaTime;

        var easeInOut = Easing.EaseInOut(_Time / _Duration, 2);
        _Hand.localPosition = Vector3.Lerp(_StartPosition, _UprootState.StartHandPosition, easeInOut);
        _Hand.localRotation = Quaternion.Lerp(_StartRotation, Quaternion.identity, easeInOut);

        if (_Time >= _Duration) {
            RecursiveSetLayer(_Hand, _Hand.parent.gameObject.layer);
            _Hand.GetComponent<AudioFader>().Stop();
            _Hand.GetComponent<LeftHandController>().MovingParticles.Stop();
            WorkingOver();
            _LateUpdate = null;
        }
    }

    IEnumerator DestroyPlant() {
        Services.PlantInfoCatalog.Destroy(_Plant.GetComponent<PlantInfo>().Identity);
        var flowerDestroyTime = 1f;

        var flowers = _Plant.GetComponentsInChildren<FlowerNode>();
        for (var i = 0; i < flowers.Length; i++) {
            var renderer = flowers[i].GetComponentInChildren<MeshRenderer>();
            renderer.material = FlowerDestroy;
            renderer.material.SetFloat("_StartTime", Time.timeSinceLevelLoad);
            renderer.material.SetFloat("_TotalTime", flowerDestroyTime);
        }
        yield return new WaitForSeconds(flowerDestroyTime);

        var time = 0f;
        var plantRenderer = _Plant.GetComponent<MeshRenderer>();
        plantRenderer.material = StemDestroy;
        while (time < DestroyTime) {
            time += Time.deltaTime;
            var p = 1f - time / DestroyTime;
            plantRenderer.material.SetFloat("_Percent", Easing.EaseInOut(p, 2));
            yield return 0;
        }

        Destroy(_Plant.gameObject);
        //WorkingOver();

        yield return null;
    }
}
