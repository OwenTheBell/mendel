﻿using UnityEngine;
using System.Collections;

public class Hand : MonoBehaviour {

	private bool mOpen;

	public Transform LeftClaw;
	public Transform RightClaw;

	public float Speed;

	private float mElapsedTime;
	private float mSplitAngle;

	// Use this for initialization
	void Start () {
		mSplitAngle = RightClaw.localRotation.eulerAngles.y;	
		mOpen = true;
		mElapsedTime = Speed;
	}
	
	// Update is called once per frame
	void Update () {

		if (mOpen && mElapsedTime < Speed) {
			mElapsedTime += Time.deltaTime;
		}
		else if (!mOpen && mElapsedTime > 0) {
			mElapsedTime -= Time.deltaTime;
		}

		mElapsedTime = Mathf.Clamp(mElapsedTime, 0f, Speed);

		var easeInOut = Easing.EaseInOut(mElapsedTime/Speed, 2);

		var openLeft = Quaternion.Euler(0f, -mSplitAngle, 0f);
		var openRight = Quaternion.Euler(0f, mSplitAngle, 0f);
		var closed = Quaternion.Euler(Vector3.zero);

		LeftClaw.localRotation = Quaternion.Lerp(closed, openLeft, easeInOut);
		RightClaw.localRotation = Quaternion.Lerp(closed, openRight, easeInOut);
	}

	public void Open() {
		if (mOpen) {
			return;
		}

		mOpen = true;
	}

	public void Close() {
		if (!mOpen) {
			return;
		}

		mOpen = false;
	}
}
