﻿using UnityEngine;
using System.Collections;

public class FlowerInventory : MonoBehaviour {

    public GameObject FlowerObject;
    public GameObject SlotParticle;
	public float Width;
	public int Slots;
    public float FlowerOffset;
    public float HoverHeight;
    public float ScaleOnHover;
    public float RaiseSpeed;
    public float LowerSpeed;
    public float ReducedScale;

	public int Count {
		get {
			var count = 0;
			foreach (var slot in _InventorySlots) {
				if (!slot.Empty) {
					count++;
				}
			}
			return count;
		}
	}

	private InventorySlot[] _InventorySlots;

	public Vector3[] AllSlots {
		get {
			var slots = new Vector3[Slots];
			for (var i = 0; i < Slots; i++) {
				slots[i] = transform.TransformPoint(AllLocalSlots[i]);
			}
			return slots;
		}
	}

	public Vector3[] AllLocalSlots {
		get {
			var slots = new Vector3[Slots];
			var start = transform.localPosition;
			start.z += Width/2;
			var chunk = Width/2/Slots;
			for (var i = 0; i < Slots; i++) {
				var position = start;
				position.z -= Width/(Slots-1) * i;
				slots[i] = position;
			}
			return slots;
		}
	}

	public Vector3[] UsedSlots {
		get {
			var slots = new Vector3[Count];
			var index = 0;
			for (var i = 0; i < _InventorySlots.Length; i++) {
				if (!_InventorySlots[i].Empty) {
					slots[index++] = _InventorySlots[i].Position;
				}
			}
			return slots;
		}
	}

	public int[] UsedIndices {
		get {
			var indices = new int[Count];
			var index = 0;
			for (var i = 0; i < _InventorySlots.Length; i++) {
				if (!_InventorySlots[i].Empty) {
					indices[index++] = i;
				}
			}
			return indices;
		}
	}

	public int FirstUsedIndex {
		get {
			if (Count <= 0) {
				return -1;
			}
			var index = 0;
			for (var i = 0; i < _InventorySlots.Length; i++) {
				if (!_InventorySlots[i].Empty) {
					index = i;
					break;
				}
			}
			return index;
		}
	}

	public Vector3 FirstUsedSlot {
		get {
			var index = FirstUsedIndex;
			if (index < 0) {
				return Vector3.zero;
			}
			else {
				return AllSlots[FirstUsedIndex];
			}
		}
	}

	public Vector3 FirstOpenSlot {
		get {
			if (Count == Slots) {
				return Vector3.zero;
			}
			var index = 0;
			for (var i = 0; i < _InventorySlots.Length; i++) {
				if (_InventorySlots[i].Empty) {
					index = i;
					break;
				}
			}
			return AllSlots[index];
		}
	}

	public bool IsFull {
		get {
			return Count == _InventorySlots.Length;
		}
	}

	void Awake() {
		_InventorySlots = new InventorySlot[Slots];
		var layer = gameObject.layer;
		for (var i = 0; i < AllSlots.Length; i++) {
			var position = transform.InverseTransformPoint(AllSlots[i]);
            var particle = Instantiate(SlotParticle);
            particle.transform.SetParent(transform, false);
            particle.transform.localPosition = position;
            particle.gameObject.SetLayers(gameObject.layer);
            position.y += FlowerOffset;
            var system = particle.GetComponent<ParticleSystem>();
			_InventorySlots[i] = new InventorySlot(system, transform, position, layer, ReducedScale);
		}
	}

	void Start () {
	}
	
	void Update () {
		foreach (var slot in _InventorySlots) {
			slot.Update();
		}	
	}

	public void Add(Transform t) {
		var index = 0;
		while (index < _InventorySlots.Length) {
			if (_InventorySlots[index].Empty) {
				break;
			}
			index++;
		}
		if (index >= _InventorySlots.Length) {
			return;
		}
		Add(t, index);
	}

	public void AddToBack(Transform t) {
		var index = _InventorySlots.Length - 1;
		do {
			if (_InventorySlots[index].Empty) {
				break;
			}
		} while (--index >= 0);

		if (index < 0) {
			return;
		}
		Add(t, index);
	}

	public void Add(Transform t, int slot) {
		if (IsFull || slot >= Slots || slot < 0) {
			return;
		}

		if (_InventorySlots[slot].Empty) {
			_InventorySlots[slot].Add(t);
		}
	}

	public Transform Retrieve() {
		return RetrieveAt(FirstUsedIndex);
	}

	public Transform RetrieveLast() {
		return RetrieveAt(UsedIndices[Count-1]);
	}

	public Transform Peak(int slot) {
		if (slot < 0 || slot >= Slots) {
			return null;
		}
		if (_InventorySlots[slot].Empty || _InventorySlots[slot].Retrieving) {
			return null;
		}
		var t = _InventorySlots[slot].transform;
		if (t == null) {
			Debug.Log("the flower at slot " + slot + " was null");
		}
		return t;
	}

	public Transform RetrieveAt(int slot) {
		if (slot < 0 || slot >= Slots) {
			return null;
		}
		if (_InventorySlots[slot].Empty) {
			return null;
		}
		DeHover(slot);
		var t = _InventorySlots[slot].Remove();
		if (t == null) {
			Debug.Log("the flower at slot " + slot + " was null");
		}
		return t;
	}

	public void Clean() {
		for (var i = 0; i < Slots; i++) {
			if (_InventorySlots[i].Empty) {
				for (var j = i + 1; j < Slots; j++) {
					if (!_InventorySlots[j].Empty) {
						_InventorySlots[i].Add(_InventorySlots[j].Remove());
					}
				}
			}
		}
	}

	public int NextOpenSlot(int slot) {
		var index = slot+1;
		while (index < Slots) {
			if (!_InventorySlots[index].Empty) {
				break;
			}
			index++;
		}
		if (index < Slots) {
			return index;
		}
		return slot;
	}

	public int PreviousOpenSlot(int slot) {
		var index = slot-1;
		while (index >= 0) {
			if (!_InventorySlots[index].Empty) {
				break;
			}
			index--;
		}
		if (index >= 0) {
			return index;
		}
		return slot;
	}

    public bool SlotOccupied(int index) {
        if (index < 0 || index >= _InventorySlots.Length) {
            return false;
        }
        return !_InventorySlots[index].Empty && !_InventorySlots[index].Retrieving;
    }

	public void Hover(int slot) {
		if (_InventorySlots[slot].Empty || _InventorySlots[slot].Hover) {
			return;
		}
        _InventorySlots[slot].Hover = true;
        StartCoroutine(Raise(_InventorySlots[slot]));
	}

	public void DeHover(int slot) {
        if (slot < 0 || slot >= _InventorySlots.Length) { return; }
		if (_InventorySlots[slot].Empty || !_InventorySlots[slot].Hover) {
			return;
		}

		_InventorySlots[slot].Hover = false;
        StartCoroutine(Lower(_InventorySlots[slot]));
	}

    IEnumerator Raise(InventorySlot slot) {
        while (slot.Retrieving) {
            yield return 0;
        }

        var start = slot.transform.localPosition;
        var target = slot.LocalPosition;
        var startScale = slot.transform.localScale;
        var targetScale = slot.transform.localScale * ScaleOnHover;
        target.y = HoverHeight + FlowerOffset;
        var difference = target.y - start.y;
        var duration = difference / HoverHeight;
        var time = (1f - duration) * RaiseSpeed;
        while (time < RaiseSpeed) {
            if (!slot.Hover || slot.Empty) {
                yield break;
            }
            time += Time.deltaTime;
            var p = Easing.EaseInOut(time / RaiseSpeed, 2);
            slot.transform.localPosition = Vector3.Lerp(start, target, p);
            slot.transform.localScale = Vector3.Lerp(startScale, targetScale, p);
            yield return 0;
        }

        yield return null;
    }

    IEnumerator Lower(InventorySlot slot) {
        var start = slot.transform.localPosition;
        var target = slot.LocalPosition;
        var startScale = slot.transform.localScale;
        var targetScale = slot.LocalScale;
        var difference = start.y - target.y;
        var duration = difference / HoverHeight;
        var time = (1f - duration) * LowerSpeed;
        while (time < LowerSpeed) {
            if (slot.Hover || slot.Empty) {
                yield break;
            }
            time += Time.deltaTime;
            var p = Easing.EaseInOut(time / LowerSpeed, 2);
            slot.transform.localPosition = Vector3.Lerp(start, target, p);
            slot.transform.localScale = Vector3.Lerp(startScale, targetScale, p);
            yield return 0;
        }

        yield return null;
    }

    public string NameAtSlot(int slot) {
		var t = Peak(slot);
        if (t == null) {
            return string.Empty;
        }
        var name = Services.SpeciesCatalog.GetNameForIdentity(t.GetComponent<PlantInfo>().Identity);
        return name;
    }

	void OnDrawGizmos() {
		Gizmos.color = Color.blue;
		foreach (var slot in AllSlots) {
			Gizmos.DrawSphere(slot, 0.05f);
		}
	}

    public void IDsInSlots(out int[] ids) {
        ids = new int[Slots];
        for (var i = 0; i < Slots; i++) {
            if (_InventorySlots[i].Empty) {
                ids[i] = -1;
            }
            else {
                ids[i] = _InventorySlots[i].transform.GetComponent<PlantInfo>().Identity;
            }
        }
    }

    public void AddFlowersFromIDs(int[] ids) {
        for (var i = 0; i < Slots && i < ids.Length; i++) {
            if (_InventorySlots[i].Empty && ids[i] >= 0) {
                var flower = Instantiate(FlowerObject);
                flower.GetComponent<PlantInfo>().Identity = ids[i];
                flower.GetComponentInChildren<MeshFilter>().sharedMesh = Services.MeshVault.GetFlower(ids[i]);
                Add(flower.transform, i);
            }
        }
    }
}

class InventorySlot {

	private float RETRIEVE_DURATION = 1f;

	public Transform transform { get; private set; }

	public Vector3 LocalPosition { get; private set; }
    public Vector3 LocalScale { get; private set; }
	public Vector3 Position {
		get {
			return _InventoryTransform.TransformPoint(LocalPosition);
		}
	}
	public int SavedLayer { get; private set; }
    public bool Hover;
	public bool Empty { get; private set; }
    public bool Retrieving { get; private set; }

    private ParticleSystem _ParticleSystem;
	private Transform _InventoryTransform;
	private int _InventoryLayer;
	private Vector3 _StartPosition;
	private Quaternion _StartRotation;
	private float _Time;
	private delegate void SlotUpdate();
	private SlotUpdate _UpdateDelegate;
    private float _ScaleAdjust;
    private Vector3 _StartScale;

    private float _HoverHeight;
    private float _HoverSpeed;

	public InventorySlot(ParticleSystem particles, Transform transform, Vector3 position, int layer, float scale) {
        _ParticleSystem = particles;
		_InventoryTransform = transform;
		_UpdateDelegate = IdleUpdate;
		LocalPosition = position;
		_InventoryLayer = layer;
        _ScaleAdjust = scale;
		Hover = false;
		Retrieving = false;
		Empty = true;
	}

	public void Update() {
        _UpdateDelegate();
	}

	private void IdleUpdate() {

	}

	public void Add(Transform t) {
		transform = t;
		SavedLayer = t.gameObject.layer;
		t.gameObject.SetLayers(_InventoryLayer);
		_StartPosition = t.position;
		_StartRotation = t.rotation;
        _StartScale = t.localScale;
        var size = t.GetComponentInChildren<MeshFilter>().sharedMesh.bounds.size;
        var max = 0f;
        for (var i = 0; i < 3; i++) {
            if (size[i] > max) max = size[i];
        }
        LocalScale = Vector3.one * (_ScaleAdjust / max);
		_Time = 0f;
		_UpdateDelegate = RetrievingUpdate;
		Retrieving = true;
		Empty = false;
        _ParticleSystem.Play();
	}

	public Transform Remove() {
		Empty = true;
		Hover = false;
        _UpdateDelegate = IdleUpdate;
        _ParticleSystem.Stop();
		return transform;
	}

	private void RetrievingUpdate() {
		if (_Time < RETRIEVE_DURATION) {
			_Time += Time.deltaTime;
			var easeInOut = Easing.EaseInOut(_Time/RETRIEVE_DURATION, 2);
            var targetPosition = _InventoryTransform.TransformPoint(LocalPosition);
            var savedRotation = transform.rotation;
            transform.parent = _InventoryTransform;
            transform.localRotation = Quaternion.identity;
            var targetRotation = transform.rotation;
            transform.parent = null;
            transform.rotation = savedRotation;
			var position = Vector3.Lerp(_StartPosition, targetPosition, easeInOut);
			var identity = Quaternion.identity;
            var rotation = Quaternion.Lerp(_StartRotation, targetRotation, easeInOut);
            transform.localScale = Vector3.Lerp(_StartScale, LocalScale, easeInOut);
            transform.position = position;
            transform.rotation = rotation;
		}
		else {
            transform.localScale = LocalScale;
            transform.parent = _InventoryTransform;
			transform.localPosition = LocalPosition;
            transform.localRotation = Quaternion.identity;
            transform.GetComponentInChildren<TriggerRotation>().StartRotating();
            LocalScale = transform.localScale;
            _UpdateDelegate = IdleUpdate;
			Retrieving = false;
		}
	}
}