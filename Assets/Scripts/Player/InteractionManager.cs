using UnityEngine;

public class InteractionManager : MonoBehaviour {

	public float RaycastRange = 1.5f;

    private InteractionState[] _InteractionStates;
    private InteractionProbe _InteractionProbe;
    private int _InteractionIndex = -1;
    private bool _PlayerStarted = false;

    private void Awake() {
        Services.PlayerTransform = transform;
    }

    void Start() {
        _InteractionStates = GetComponents<InteractionState>();
        _InteractionProbe = GetComponentInChildren<InteractionProbe>();
	}

	void Update() {
        if (!_PlayerStarted) {
            return;
        }

        // if a state is active but the cancel stack is occupied, make sure to disable it
        // This prevents things like picking flowers while a menu screen is open
        if (Services.CancelStack.Occupied && _InteractionIndex >= 0) {
            _InteractionStates[_InteractionIndex].Deactivate();
            return;
        }

        if (_InteractionIndex >= 0 && _InteractionStates[_InteractionIndex].Working) {
            return;
        }

        Collider intersectedCollider;
        Vector3 point;
        var hasCollider = _InteractionProbe.GetIntersectedCollider(out intersectedCollider, out point);

        _InteractionIndex = -1;
        int idleIndex = -1;
        var blockIndex = -1;

        // check all priority interaction states first;
        if (hasCollider) {
            for (var i = 0; i < _InteractionStates.Length; i++) {
                if (_InteractionStates[i].Block) {
                    blockIndex = i;
                }
                if (_InteractionStates[i].Mode == InteractionStateMode.Idle) {
                    idleIndex = i;
                }
                if (_InteractionStates[i].CheckTarget(intersectedCollider, point)) {
                    if (_InteractionStates[i].Priority || _InteractionIndex == -1)
                        _InteractionIndex = i;
                }
            }
        }

        // if a state is blocking and not the active state, prevent other states from appearing
        if (blockIndex >= 0 && _InteractionIndex != blockIndex && _InteractionIndex >= 0) {
            _InteractionIndex = -1;
        }
        // if no state was selected, default to the idle state
        if (_InteractionIndex < 0) {
            _InteractionIndex = idleIndex;
        }
        for (var i = 0; i < _InteractionStates.Length; i++) {
            if (i == _InteractionIndex) {
                _InteractionStates[i].Activate(intersectedCollider, point);
            }
            else {
                _InteractionStates[i].Deactivate();
            }
        }

        return;
	}

    void OnEnable() {
        Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStarted);
    }

    void OnDisable() {
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStarted);
    }

    void OnDestroy() {
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStarted);
    }

    void OnPlayerStarted(PlayerStartedEvent e) {
        _PlayerStarted = true;
    }
}