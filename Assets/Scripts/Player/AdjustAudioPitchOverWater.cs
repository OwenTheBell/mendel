﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class AdjustAudioPitchOverWater : MonoBehaviour {

    public float Adjust;
    public float Speed;
    public LayerMask Layers;

    private List<AudioSource> _Sources;
    private List<float> _StartingPitches;
    private float _Percent;
    private bool _AboveWater;

    private void Awake() {
        _Sources = new List<AudioSource>();
        GetComponents(_Sources);
        _StartingPitches = new List<float>(_Sources.Count);
        for (var i = 0; i < _Sources.Count; i++) {
            _StartingPitches.Add(_Sources[i].pitch);
        }
    }

    private void Start() {

    }

    void Update() {
        RaycastHit hit;
        if (Physics.Raycast(transform.position, Vector3.down, out hit, 2f, Layers)) {
            var other = hit.collider.gameObject;
            if (other.layer == LayerMask.NameToLayer("Ground") && _AboveWater) {
                _AboveWater = false;
                StopAllCoroutines();
                StartCoroutine(AdjustPitch(-1));
            }
            else if (other.layer == LayerMask.NameToLayer("Water") && !_AboveWater) {
                _AboveWater = true;
                StopAllCoroutines();
                StartCoroutine(AdjustPitch(1));
            }
        }
    }

    IEnumerator AdjustPitch(int direction) {
        var time = Speed * _Percent;
        while ((time < Speed && direction > 0) || (time > 0f && direction < 0)) {
            time += Time.deltaTime * direction;
            _Percent = time / Speed;
            var easeInOut = Easing.EaseInOut(_Percent, 2);
            for (var i = 0; i < _Sources.Count; i++) {
                var pitch = Mathf.Lerp(_StartingPitches[i], _StartingPitches[i] + Adjust, easeInOut);
                _Sources[i].pitch = pitch;
            }
            yield return null;
        }
    }
}
