﻿using UnityEngine;
using System.Collections;

public class ArmsAppearAtStart : MonoBehaviour {

    public float Duration;

    [SerializeField]
    private float _SetBack;
    private Vector3 _StartingPosition;

    private void Awake() {
        _StartingPosition = transform.localPosition;
        var position = transform.localPosition;
        position.z -= _SetBack;
        transform.localPosition = position;
        Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStarted);
    }

    void OnPlayerStarted(PlayerStartedEvent e) {
        StartCoroutine(MoveToPosition());
        Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStarted);
    }

    IEnumerator MoveToPosition() {
        var time = 0f;
        var starting = transform.localPosition;
        while (time < Duration) {
            time += Time.deltaTime;
            var easeInOut = Easing.EaseInOut(time / Duration, 2);
            transform.localPosition = Vector3.Lerp(starting, _StartingPosition, easeInOut);
            yield return null;
        }
    }

}