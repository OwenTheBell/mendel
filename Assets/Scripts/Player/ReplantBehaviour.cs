﻿using UnityEngine;

public class ReplantBehaviour : InteractionBehaviour {

    public string ReplantMessage;
    public float ReplantTime;
    public Vector3 HandOffset;
    public float StopDistance;
    public float HeightOffset;
    public float HandReturnSpeed;

    private UprootInteractionState _UprootState;
    private Transform _HeldPlant;
    private Vector3 _StartPosition;
    private Quaternion _StartRotation;
    private Vector3 _TargetHandPosition;
    private float _Elapsed;
    private float _ReturnDuration;

    private Transform _Hand;

    private Vector3 _HandReturnPosition;
    private Quaternion _HandReturnRotation;

    protected override void Start() {
        base.Start();

        _Hand = LeftArm.Hand;
        _UprootState = GetComponent<UprootInteractionState>();
    }

    public override bool SetTarget(GameObject target, Vector3 position) {
        if (!base.SetTarget(target, position)) {
            return false;
        }
        return true;
    }

    public override void SetControlPrompt(ControlPrompt prompt) {
        prompt.Available = true;
        prompt.Negative = false;
        prompt.Text = ReplantMessage;
    }

    public override void Activate() {
        base.Activate();

        _Hand.parent = null;
        _HeldPlant = _UprootState.HeldPlant.transform;
        _HeldPlant.parent = null;
        _StartPosition = _HeldPlant.position;
        _StartRotation = _HeldPlant.rotation;
        _HeldPlant.GetComponent<StemBuilder>().RelocateMesh(TargetPosition);
        _HeldPlant.GetComponent<FlowerCreator>().RelocateFlowerMeshes(TargetPosition);
        _HeldPlant.gameObject.SetLayers(_UprootState.SavedLayer);
        _Hand.gameObject.SetLayers(_UprootState.SavedLayer);

        var distance = Vector3.Distance(_Hand.position, TargetPosition);
        var radius = _HeldPlant.GetComponent<StemBuilder>().Radius;
        var percent = Mathf.Clamp01((distance - StopDistance - radius)  / distance);
        _TargetHandPosition = Vector3.Lerp(_Hand.position, TargetPosition, percent);
        _TargetHandPosition.y += HeightOffset;
        _HandReturnPosition = _Hand.position;

        Events.instance.Raise(new HidePromptTextEvent());
        _LateUpdate = ReplantingUpdate;
        _Elapsed = 0f;
    }

    private void ReplantingUpdate() {
        _Elapsed += Time.deltaTime;
        var percent = _Elapsed / ReplantTime;
        var easeInOut = Easing.EaseInOut(percent, 2);
        var easeIn = Easing.EaseIn(percent, 2);

        var upTarget = TargetPosition;
        upTarget.y += 0.3f;
        var adjustedTarget = Vector3.Lerp(upTarget, TargetPosition, easeIn);
        var position = Vector3.Lerp(_StartPosition, adjustedTarget, easeInOut);
        _HeldPlant.position = position;

        var targetRotEuler = _StartRotation.eulerAngles;
        targetRotEuler.x = 0f;
        targetRotEuler.z = 0f;
        var targetRot = Quaternion.Euler(targetRotEuler);
        var rotation = Quaternion.Lerp(_StartRotation, targetRot, easeInOut);
        _HeldPlant.rotation = rotation;

        _Hand.position = Vector3.Lerp(_HandReturnPosition, _TargetHandPosition, easeInOut);
        _Hand.LookAt(_HeldPlant);

        if (percent >= 1f) {
            var info = _UprootState.HeldPlant.GetComponent<PlantInfo>();
            Services.PlantInfoCatalog.UpdatePositionAndRotation(info.Identity, position, rotation.eulerAngles);

            _UprootState.HeldPlant = null;

            _Elapsed = 0f;
            _Hand.parent = _UprootState.HandParent;
            _HandReturnPosition = _Hand.localPosition;
            _HandReturnRotation = _Hand.localRotation;

            var distance = Vector3.Distance(_UprootState.StartHandPosition, _Hand.localPosition);
            _ReturnDuration = distance / HandReturnSpeed;

            _Hand.GetComponent<Animator>().SetBool("Pickup", false);
            _Hand.GetComponent<LeftHandController>().GrabbingParticles.Stop();

            _LateUpdate = ReturnHandUpdate;
        }
    }

    private void ReturnHandUpdate() {
        base.LateWorkingUpdate();

        _Elapsed += Time.deltaTime;
        var percent = _Elapsed / _ReturnDuration;
        var easeInOut = Easing.EaseInOut(percent, 2);
        _HeldPlant.GetComponent<ShaderController>().SetIntensity(easeInOut);

        _Hand.localPosition = Vector3.Lerp(_HandReturnPosition, _UprootState.StartHandPosition, easeInOut);
        _Hand.localRotation = Quaternion.Lerp(_HandReturnRotation, Quaternion.identity, easeInOut);

        if (percent > 1f) {
            RecursiveSetLayer(_Hand, _Hand.parent.gameObject.layer);
            _HeldPlant.GetComponent<ShaderController>().SetIntensity(1f);
            _HeldPlant = null;
            _Hand.GetComponent<AudioFader>().Stop();
            _Hand.GetComponent<LeftHandController>().MovingParticles.Stop();
            WorkingOver();
        }
    }
}

class ReplantValues : StateValues {
    public Vector3 StartPosition;
    public Quaternion StartRotation;
    
    public ReplantValues (
        float duration,
        Vector3 position,
        Quaternion rotation
    ) : base() {
        Duration = duration;
        StartPosition = position;
        StartRotation = rotation;
    }
}