﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlantingIndicator : MonoBehaviour {

    public float ScaleTime;

    private void Awake() {
        transform.localScale = Vector3.zero;
    }

    public void Show() {
        StopAllCoroutines();
        StartCoroutine(Scale(1));
    }

    public void Hide() {
        StopAllCoroutines();
        StartCoroutine(Scale(-1));
    }

    IEnumerator Scale(int direction) {
        GetComponentInChildren<Renderer>().enabled = true;
        var time = transform.localScale.x * ScaleTime;
        while ((direction == 1 && time < ScaleTime) || (direction == -1 && time >= 0f)) {
            time += Time.deltaTime * direction;
            var easeInOut = Mathf.Clamp01(Easing.EaseInOut(time / ScaleTime, 2));
            transform.localScale = Vector3.Lerp(Vector3.zero, Vector3.one, easeInOut);
            yield return null;
        }
        if (time < 0f) {
            GetComponentInChildren<Renderer>().enabled = false;
        }
    }
}