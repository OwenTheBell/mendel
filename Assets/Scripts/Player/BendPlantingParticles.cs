﻿using UnityEngine;
using System.Collections;

public class BendPlantingParticles : MonoBehaviour {

    public Vector3 Target;

    private ParticleSystem _System;
    private ParticleSystem.Particle[] _Particles;

    void Start() {
        _System = GetComponent<ParticleSystem>();
        _Particles = new ParticleSystem.Particle[_System.main.maxParticles];
    }

    void Update() {
        if (!_System.isPlaying) return;
        var count = _System.GetParticles(_Particles);
        var distance = transform.position.y - Target.y;
        for (var i = 0; i < count; i++) {
            var y = _Particles[i].position.y;
            var p = _Particles[i].position.y - Target.y;
            p = 1f - p / distance;
            var easeInOut = Easing.EaseInOut(p, 3);

            var position = Vector3.Lerp(transform.position, Target, easeInOut);
            position.y = y;
            _Particles[i].position = position;
        }
        _System.SetParticles(_Particles, count);
    }
}