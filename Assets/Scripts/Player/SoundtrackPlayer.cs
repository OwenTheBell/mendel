using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;

public class SoundtrackPlayer : MonoBehaviour {

	public float GapBetweenTracks;

	public bool ForceTrackAdvance;
	private float _ForceNextTrackTime;

	private List<AudioClip> mClips;
	private bool mLoadingClips;

	private AudioSource mMusicSource;

	private float mNextTrackTime = -1f;
	private int mCurrentIndex = -1;

	private bool mActive = false;

	void Awake() {
		mClips = new List<AudioClip>();
		ReloadClips();
	}

	void Start() {
		// StartCoroutine(WaitForSoundtrackLoading());
		mMusicSource = GetComponent<AudioSource>();
		_ForceNextTrackTime = 2f;
	}

	void Update() {
		if (ForceTrackAdvance) {
			_ForceNextTrackTime -= Time.deltaTime;
			if (_ForceNextTrackTime <= 0f) {
				PlayNextTrack();
				_ForceNextTrackTime = 2f;
			}
		}
		if (!mActive || mMusicSource.isPlaying) {
			return;
		}
		// if (!mLoadingClips && !mMusicSource.isPlaying) {
		// 	PlayNextTrack();
		// }
		if (mNextTrackTime < 0 && !mMusicSource.isPlaying) {
			mNextTrackTime = Time.timeSinceLevelLoad + GapBetweenTracks;
		}
		else if (mNextTrackTime <= Time.timeSinceLevelLoad) {
			PlayNextTrack();
			mNextTrackTime = -1f;
		}
	}

	void OnEnable() {
		Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnDisable() {
		Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnDestroy() {
		Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnPlayerStartedEvent(PlayerStartedEvent e) {
		mActive = true;
		mNextTrackTime = Time.timeSinceLevelLoad + GapBetweenTracks;
	}

	void ReloadClips() {

		mLoadingClips = true;

		var absolutePath = "./";
		if (Application.isEditor) {
			absolutePath = "Assets/";
		}

		mClips.Clear();
		var blankFiles = new FileInfo[0];
		StartCoroutine(LoadAudioClipsInBackground(blankFiles));
	}

	IEnumerator LoadAudioClipsInBackground(FileInfo[] files) {

		var clips = Resources.LoadAll<AudioClip>("Audio/Music");
		foreach (var clip in clips) {
			mClips.Add(clip);
		}

		RandomizeList(mClips);

		mLoadingClips = false;

		yield return null;
	}

	IEnumerator WaitForSoundtrackLoading() {
		while (mLoadingClips) {
			yield return 0;
		}

		PlayNextTrack();
	}

	void RandomizeList<T>(List<T> list) {
		for (var i = 0; i < list.Count; i++) {
			var j = Random.Range(0, list.Count - 1);
			var obj1 = list[i];
			var obj2 = list[j];
			list.RemoveAt(j);
			list.Insert(j, obj1);
			list.RemoveAt(i);
			list.Insert(i, obj2);
		}
	}

	void PlayNextTrack() {
		/* if the soundtrack isn't done laoding, wait until it is.
		* This coroutine will call this function again once the soundtrack is ready */
		if (mLoadingClips) {
			StartCoroutine(WaitForSoundtrackLoading());
		}
		else {
			mCurrentIndex++;
			if (mCurrentIndex >= mClips.Count) {
				mCurrentIndex = 0;
				var count = mClips.Count;
				var lastClip = mClips[count - 1];
				mClips.Remove(lastClip);
				RandomizeList(mClips);
				var random = Random.Range(count/2, count);
				mClips.Insert(random, lastClip);
			}
			mMusicSource.clip = mClips[mCurrentIndex];
			Debug.Log("playing track: " + mMusicSource.clip.name);
			mMusicSource.Play();
		}
	}
}