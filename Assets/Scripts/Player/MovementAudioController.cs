﻿using UnityEngine;

public class MovementAudioController : MonoBehaviour {

    public AudioFader Fader;

    private CharacterMotor _Motor;

    private void Awake() {
        _Motor = GetComponent<CharacterMotor>();
    }

    void Update() {
        if (_Motor.grounded) {
            if (_Motor.inputMoveDirection.sqrMagnitude > 0 && !Fader.Playing && _Motor.canControl) {
                Fader.Play();
            }
            else if (_Motor.inputMoveDirection.sqrMagnitude == 0f && Fader.Playing) {
                Fader.Stop();
            }
        } else if (Fader.Playing) {
            Fader.Stop();
        }
    }
}