﻿using UnityEngine;

public class AdjustOceanVolume : MonoBehaviour {

    public LayerMask BoundsLayer;
    public GameObject OceanAudio;

    void Update() {
        var ray = new Ray(transform.position, transform.position.normalized);
        RaycastHit hit;
        if (Physics.Raycast(ray, out hit, Mathf.Infinity, BoundsLayer)) {
            OceanAudio.transform.position = hit.point;
        }
    }
}