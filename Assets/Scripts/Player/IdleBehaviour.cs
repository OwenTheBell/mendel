using UnityEngine;

public class IdleBehaviour : InteractionBehaviour {

	public float Distance;

	public override Vector3 TargetPosition {
		get {
    		return transform.TransformPoint(Vector3.forward * Distance);
		}
	}

	protected override void Awake() {
		base.Awake();
	}

	public override void Activate() {
		base.Activate();
	}

	protected override void ArmAnimationOver() {
		if (!Working) {
			return;
		}
		base.ArmAnimationOver();

		WorkingOver();
	}

    protected override void IdleUpdate() {
        base.IdleUpdate();
    }

    protected override void WorkingUpdate() {
		base.WorkingUpdate();
	}

    public override bool SetTarget(GameObject target, Vector3 position) {
        return false;
    }

    public override void SetControlPrompt(ControlPrompt prompt) { }
}