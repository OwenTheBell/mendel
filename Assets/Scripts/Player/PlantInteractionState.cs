﻿using System.Collections;
using UnityEngine;

public class PlantInteractionState : InteractionState {

    private GameObject _TargetPlant;
    private GameObject _TargetObject;
    private Vector3 _TargetPosition;

    public string NewSpeciesMessage;
    public string ComparisonMessage;
    public string ParentsMessage;

    private PlantInteractionCanvas _PlantingCanvas;
    private Coroutine _CanvasRetargetting;

    protected override void Awake() {
        base.Awake();
        Mode = InteractionStateMode.Plant;

        _PlantingCanvas = InteractionCanvas as PlantInteractionCanvas;
    }

    protected override void Start() {
        base.Start();
    }

    protected override void Update() {
        base.Update();

        if (!Active || Working) {
            return;
        }
        if (_TargetObject != null && _CanvasRetargetting == null) {
            CalculateTargetPosition();
            InteractionCanvas.transform.position = _TargetPosition;
        }
    }

    public override bool CheckTarget(Collider collider, Vector3 point) {
        if (!base.CheckTarget(collider, point)) {
            return false;
        }

        if (collider.gameObject.HasComponent<PlantInfo>()) {
            var flowerCreator = collider.GetComponent<FlowerCreator>();
            var parent = collider.transform.parent;
            while (flowerCreator == null && parent != null) {
                flowerCreator = parent.GetComponent<FlowerCreator>();
                parent = parent.parent;
            }
            if (flowerCreator == null || flowerCreator.AvailableFlowerCount == 0) {
                return false;
            }
        }
        return true;
    }

    public override void Activate(Collider collider, Vector3 point) {
        base.Activate(collider, point);

        if (_TargetObject != collider.gameObject) {
            var plant = collider.gameObject;
            while (!plant.HasComponent<FlowerCreator>()) {
                plant = plant.transform.parent.gameObject;
            }
            _TargetObject = collider.gameObject;
            CalculateTargetPosition();
            StopAllCoroutines();
            _CanvasRetargetting = StartCoroutine(RetargetCanvas(plant));
        }
    }

    IEnumerator RetargetCanvas(GameObject target) {
        if (target != _TargetPlant) {
            InteractionCanvas.Hide();
            _TargetPlant = target;

            while (InteractionCanvas.Visible) {
                yield return null;
            }

            InteractionCanvas.transform.position = _TargetPosition;
            InteractionCanvas.Show();
        }
        else {
            var time = 0f;
            var startPosition = InteractionCanvas.transform.position;
            while (time < 0.2f) {
                time += Time.deltaTime;
                var easeInOut = Easing.EaseInOut(time / 0.2f, 2);
                InteractionCanvas.transform.position = Vector3.Lerp(startPosition, _TargetPosition, easeInOut);
                yield return null;
            }
        }
        UpdateTarget(target);
        _CanvasRetargetting = null;
    }

    protected override void DeferredActivate() {
        base.DeferredActivate();
        InteractionCanvas.Show();
    }

    protected override void DeferredDeactivate() {
        base.DeferredDeactivate();
        _TargetPlant = null;
        _TargetObject = null;
        InteractionCanvas.Hide();
    }

    protected override void BehaviourActivated(InteractionBehaviour behaviour) {
        base.BehaviourActivated(behaviour);
        _PlantingCanvas.NameText.Hide();
        InteractionCanvas.Hide();
    }

    protected override void BehaviourCompleted(InteractionBehaviour behaviour) {
        base.BehaviourCompleted(behaviour);
        DeferredDeactivate();
    }

    private void UpdateTarget(GameObject newTarget) {
        _TargetPlant = newTarget;
        foreach (var behaviour in _Behaviours) {
            behaviour.SetTarget(newTarget, _TargetPosition);
            if (behaviour.GetType() == typeof(PickFlowerBehaviour)) {
                (behaviour as PickFlowerBehaviour).HoveredObject = _TargetObject;
            }
        }

        var targetIdentity = _TargetPlant.GetComponent<PlantInfo>().Identity;
        var name = Services.SpeciesCatalog.GetNameForIdentity(targetIdentity, true);
        var flavor = string.Empty;

        if (name == string.Empty) {
            name = NewSpeciesMessage;
        }
        var speciesPercentTuple = Services.SpeciesCatalog.GetClosestRelative(targetIdentity);
        var species = speciesPercentTuple.First;
        var percentSimilarity = speciesPercentTuple.Second;
		if (species != string.Empty) {
            var comparison = (int)(percentSimilarity * 100) + "";
            flavor = ComparisonMessage.Replace("xx", comparison).Replace("yy", species);
		}

        var parentsString = string.Empty;
        var parentsTuple = _TargetPlant.GetComponent<PlantInfo>().Info.Parents;
        if (parentsTuple.First != -1) {
            var parent1 = Services.SpeciesCatalog.GetNameForIdentity(parentsTuple.First);
            if (parent1 == string.Empty) {
                parent1 = PROPERTIES.NoNameMessage;
            }
            var parent2 = Services.SpeciesCatalog.GetNameForIdentity(parentsTuple.Second);
            if (parent2 == string.Empty) {
                parent2 = PROPERTIES.NoNameMessage;
            }
            parentsString = ParentsMessage.Replace("xx", parent1).Replace("yy", parent2);
        }

        _PlantingCanvas.NameText.Text = name;
        if (parentsString != string.Empty) {
            _PlantingCanvas.ParentsText.Available = true;
            _PlantingCanvas.ParentsText.Text = parentsString;
        }
        else {
            _PlantingCanvas.ParentsText.Available = false;
        }
        if (flavor != string.Empty) {
            _PlantingCanvas.FlavorText.Available = true;
            _PlantingCanvas.FlavorText.Text = flavor;
        }
        else {
            _PlantingCanvas.FlavorText.Available = false;
        }

        foreach (var b in _Behaviours) {
            b.SetTarget(_TargetPlant, _TargetPosition);
        }
        UpdateCanvas();
    }
    
    private void CalculateTargetPosition() {
        var collider = _TargetObject.GetComponent<Collider>();
        var center = Vector3.zero;
        if (collider.GetType() == typeof(SphereCollider)) {
            center = (collider as SphereCollider).center;
        }
        else if (collider.GetType() == typeof(CapsuleCollider)) {
            center = (collider as CapsuleCollider).center;
        }
        _TargetPosition = collider.transform.TransformPoint(center);
    }
}