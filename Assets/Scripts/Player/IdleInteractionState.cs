﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IdleInteractionState : InteractionState {

    protected override void Awake() {
        base.Awake();
        Mode = InteractionStateMode.Idle;
    }

    protected override void Start() {
        base.Start();
    }

    public override bool CheckTarget(Collider collider, Vector3 point) {
        return false;
    }

    protected override void DeferredActivate() {
        base.DeferredActivate();
    }

    protected override void DeferredDeactivate() {
        base.DeferredDeactivate();
    }

    //protected override void HandleQuitKey() {
    //    Events.instance.Raise(new PauseEvent());
    //}
}
