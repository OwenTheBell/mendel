﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum InteractionStateMode { Ground, Plant, Idle, Uproot }
public abstract class InteractionState : MonoBehaviour {

    public LayerMask TargetLayer;
    public bool Priority;
    public ControlCanvas InteractionCanvas;
    public AudioSource ActionAudio;

    public bool Block { get; protected set; }

    protected List<InteractionBehaviour> _Behaviours;
    protected InteractionBehaviour _WorkingBehaviour { get; private set; }

    private bool _Active;
    private bool _nextActive;
    public bool Active {
        get { return _Active; }
        private set {
            _nextActive = value;
        }
    }
    private bool _SelectingBehaviour;
    private Coroutine _ShowSelectedRoutine;

    public InteractionStateMode Mode { get; protected set; }

    public bool Working {
        get {
            return _WorkingBehaviour != null;
        }
    }

    private float _TertiaryTime;

    protected virtual void Awake() {
        _Active = false;
        _Behaviours = new List<InteractionBehaviour>();
        _TertiaryTime = 0f;
        Block = false;
    }

	protected virtual void Start () {
        if (InteractionCanvas != null) {
            InteractionCanvas.Hide();
        }
        foreach (var behaviour in GetComponents<InteractionBehaviour>()) {
            if (behaviour.Mode == Mode) {
                _Behaviours.Add(behaviour);
            }
        }
	}

    protected virtual void Update() {
        // if _nextActive is different from _Active, then there is a defered
        // state change that needs to be handled
        if (_nextActive != _Active) {
            if (_nextActive) {
                DeferredActivate();
            }
            else {
                DeferredDeactivate();
            }
        }

        if ((_WorkingBehaviour != null && _WorkingBehaviour.Working) || _SelectingBehaviour) {
            return;
        }
        else if (_WorkingBehaviour != null) {
            BehaviourCompleted(_WorkingBehaviour);
        }

        if (!Active) {
            return;
        }

        var action1 = Input.GetButtonUp("Action 1");
        var action2 = Input.GetButtonUp("Action 2");
        var action3 = Input.GetButtonUp("Action 3");
        var action4 = Input.GetButtonUp("Action 4");

        foreach (var behaviour in _Behaviours) {
            if (!behaviour.Accessible) {
                continue;
            }
            if (
                (behaviour.ActionType == ActionType.Action1 && action1) ||
                (behaviour.ActionType == ActionType.Action2 && action2) ||
                (behaviour.ActionType == ActionType.Action3 && action3) ||
                (behaviour.ActionType == ActionType.Action4 && action4)
            ) {
                _ShowSelectedRoutine = StartCoroutine(ShowSelectedBehaviour(behaviour));
                ActionAudio.Play();
                break;
            }
        }
	}

    IEnumerator ShowSelectedBehaviour(InteractionBehaviour behaviour) {
        _SelectingBehaviour = true;
        if (_Behaviours.Count > 1) {
            InteractionCanvas.HideAllExecpt(InteractionCanvas.GetPrompt(behaviour.ActionType));
            yield return new WaitForSeconds(0.2f);
        }
        InteractionCanvas.Hide();
        while (InteractionCanvas.Visible && _Active) {
            yield return null;
        }
        if (!_Active) {
            _SelectingBehaviour = false;
            yield break;
        }
        BehaviourActivated(behaviour);
        _SelectingBehaviour = false;
    }

    protected virtual void BehaviourActivated(InteractionBehaviour behaviour) {
        behaviour.Activate();
        _WorkingBehaviour = behaviour;
        Events.instance.Raise(new SetPlayerActiveEvent(false));
    }

    protected virtual void BehaviourCompleted(InteractionBehaviour behaviour) {
        _WorkingBehaviour = null;
        Events.instance.Raise(new SetPlayerActiveEvent(true));
    }

    public virtual bool CheckTarget(Collider collider, Vector3 point) {
        if (((1<<collider.gameObject.layer) & TargetLayer) == 0) {
            return false;
        }
        return true;
    }

    public virtual void Activate(Collider collider, Vector3 point) {
        _nextActive = true;
    }

    public virtual void Deactivate() {
        _nextActive = false;
    }

    protected virtual void DeferredActivate() {
        _Active = true;
        _nextActive = true;
        _SelectingBehaviour = false;
    }

    protected virtual void DeferredDeactivate() {
        _Active = false;
        _nextActive = false;
        if (_ShowSelectedRoutine != null) {
            StopCoroutine(_ShowSelectedRoutine);
            _ShowSelectedRoutine = null;
        }
    }

    protected void UpdateCanvas() {
        if (InteractionCanvas == null) return;

        InteractionCanvas.ClearPrompts();
        foreach (var behaviour in _Behaviours) {
            var prompt = InteractionCanvas.GetPrompt(behaviour.ActionType);
            if (prompt != null) {
                behaviour.SetControlPrompt(prompt);
            }
        }
    }
}