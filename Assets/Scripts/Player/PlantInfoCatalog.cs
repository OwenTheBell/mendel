﻿using System.Collections.Generic;
using UnityEngine;

public class PlantInfoCatalog {

    public Dictionary<int, PlantInfoStruct> Infos;

    public PlantInfoCatalog() {
        Infos = new Dictionary<int, PlantInfoStruct>();
    }

    public void Add(int identity, PlantInfoStruct info) {
        if (!Infos.ContainsKey(identity)) {
            Infos.Add(identity, info);
        }
    }

    // since we want to retain information about plants for purposes of the family tree
    // never actually remove a plant from Infos. Instead the flag lets PopulationManager
    // know to not actually spawn this plant.
    public void Destroy(int identity)
    {
        if (Infos.ContainsKey(identity))
        {
            var info = Infos[identity];
            info.Destroyed = true;
            Infos[identity] = info;
        }
    }

    public void UpdatePositionAndRotation(int id, Vector3 position, Vector3 rotation) {
        if (Infos.ContainsKey(id)) {
            var info = Infos[id];
            info.Position = position;
            info.Rotation = rotation;
            Infos[id] = info;
        }
    }

    public void Clear() {
        Infos.Clear();
    }
}