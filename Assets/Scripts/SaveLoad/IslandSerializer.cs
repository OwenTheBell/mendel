﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Xml;
using System.Xml.Serialization;
using System.Threading;
using System.Text.RegularExpressions;

[XmlRoot("Island")]
public class IslandSerializer {

    [XmlElement("Version")]
    public string VersionNumber;

	[XmlElement("Save Info")]
	public SaveInfoStruct saveInfo;
	[XmlElement("Player")]
	public PlayerXmlStruct player;

	[XmlArray("Names"), XmlArrayItem("Name")]
	public SpeciesXmlStruct[] species;

	[XmlArray("Seeds"), XmlArrayItem("Seed")]
	public SeedXmlStruct[] seeds;

    [XmlArray("PlantInfos"), XmlArrayItem("Info")]
    public PlantInfoStruct[] infos;

    [XmlArray("HeldPlants"), XmlArrayItem("ID")]
    public int[] HeldFlowers;

    [XmlElement("Held Plant")]
    public HoldingStruct heldPlant;

    private const string SAVE_NAME = "IslandSave";
    private const int FILES_TO_KEEP = 3;


	public void Save(string path, out Thread thread) {
        ThreadStart threadStart = delegate { SaveThread(path); };
        VersionNumber = PROPERTIES.VersionNumber;
        thread = new Thread(threadStart);
        thread.Start();
	}

    void SaveThread(string path) {
		try {
            lock (this) {
                var directoryInfo = new DirectoryInfo(path);
                var files = new List<FileInfo>(directoryInfo.GetFiles());
                files = files.Filter( f => f.Name.Contains(SAVE_NAME) && !f.Name.Contains("meta"));
                while (files.Count > FILES_TO_KEEP - 1) {
                    var oldestFile = files[0];
                    var oldestDate = -1L;
                    foreach (var file in files) {
                        var match = Regex.Match(file.Name, @"\d+");
                        var date = 0L;
                        if (match.Success && long.TryParse(match.Value, out date)) {
                            if (oldestDate < 0 || date < oldestDate) {
                                oldestDate = date;
                                oldestFile = file;
                            }
                        }
                    }
                    if (oldestDate > 0) {
                        files.Remove(oldestFile);
                        oldestFile.Delete();
                    }
                    // throw in this else just in case something is parsing wrong to prevent
                    // an infinite loop
                    else {
                        break;
                    }
                }
                // get tick time and devide by 10 million to get seconds
                var time = DateTime.UtcNow.Ticks / 10000000L;
                var fileName = SAVE_NAME + "_" + time + ".xml";
                var savePath = Path.Combine(path, fileName);
                var serializer = new XmlSerializer(typeof(IslandSerializer));
                // wrap the FileStream in a StreamWriter so that encoding can be set
                using (var stream = new FileStream(savePath, FileMode.Create)) {
                    using (var writer = new StreamWriter(stream, System.Text.Encoding.UTF8)) {
                        serializer.Serialize(writer, this);
                        // infinite loop while we wait for the stream to be over
                    }
                    var stillWriting = true;
                    var file = new FileInfo(savePath);
                    while (stillWriting) {
                        FileStream testStream = null;
                        try {
                            testStream = file.Open(FileMode.Open, FileAccess.ReadWrite, FileShare.None);
                        }
                        catch (IOException e) {
                            stillWriting = true;
                            continue;
                        }
                        finally {
                            if (testStream != null) {
                                testStream.Close();
                            }
                        }
                        stillWriting = false;
                    }
                }
            }
        }
		catch (Exception e) {
			var message = "----Outer Exception Data----\n";
			message += "Message: " + e.Message + "\n";
			message += "Exception Type: " + e.GetType().FullName + "\n";
			message += "Source: " + e.Source + "\n";
			message += "StackTrace: " + e.StackTrace + "\n";
			message += "TargetSite: " + e.TargetSite + "\n";

			if (e.InnerException != null) {
				e = e.InnerException;
				message += "----Inner Exception Data----\n";
				message += "Message: " + e.Message + "\n";
				message += "Exception Type: " + e.GetType().FullName + "\n";
				message += "Source: " + e.Source + "\n";
				message += "StackTrace: " + e.StackTrace + "\n";
				message += "TargetSite: " + e.TargetSite + "\n";
			}
		}
    }

	public static bool Load(string path, out IslandSerializer island) {
        var directoryInfo = new DirectoryInfo(path);
        var files = new List<FileInfo>(directoryInfo.GetFiles());
        files = files.Filter( f => f.Name.Contains(SAVE_NAME) && !f.Name.Contains("meta"));
        while (files.Count > 0) {
            var newestFile = files[0];
            var newestDate = 0L;
            foreach (var file in files) {
                var match = Regex.Match(file.Name, @"\d+");
                var date = 0L;
                if (match.Success && long.TryParse(match.Value, out date)) {
                    if (date > newestDate) {
                        newestDate = date;
                        newestFile = file;
                    }
                }
            }
            try {
                var serializer = new XmlSerializer(typeof(IslandSerializer));
                using (var stream = newestFile.Open(FileMode.Open)) {
                    island = serializer.Deserialize(stream) as IslandSerializer;
                    if (island.VersionNumber == default(string)) {
                        Debug.Log("version number isn't in save file");
                        return false;
                    }
                    // Check all the genes for integrity. Missing genes will cause crashes
                    // This is primarily for beta purposes as the gene list changes rapidly
                    for (var i = 0; i < island.seeds.Length; i++) {
                        for (var j = 0; j < island.seeds[i].genes.Length; j++) {
                            if (!Gene.VerifyIntegrity(island.seeds[i].genes[j])) {
                                island = null;
                                files.Remove(newestFile);
                                continue;
                            }
                        }
                    }
                }
            }
            catch (Exception e) {
                Debug.Log("Cannot read save file. Trying next.");
                island = null;
                files.Remove(newestFile);
            }
            if (island != null) {
                return true;
            }
        }
        island = null;
        return false;
    }
}

[XmlRoot("SaveInfo")]
public struct SaveInfoStruct {
	public DateTime Date;
}

[XmlRoot("Species")]
public struct SpeciesXmlStruct {
	[XmlAttribute("name")]
	public string name;
	public int[] identities;
}

[XmlRoot("Plant")]
public struct PlantXmlStruct {
    //[XmlAttribute("position")]
	public Vector3 position;
    //[XmlAttribute("rotation")]
	public Vector3 rotation;
	[XmlAttribute("identity")]
	public int identity;
}

[XmlRoot("Seed")]
public struct SeedXmlStruct {
	[XmlArray("Genes"),
	XmlArrayItem("PetalFlowerGene", Type = typeof(PetalFlowerGene)),
    XmlArrayItem("StemColorHybridGene", Type = typeof(StemColorHybridGene)),
    XmlArrayItem("HybridStemGene", Type = typeof(HybridStemGene)),
    XmlArrayItem("FlowerColorHybridGene", Type = typeof(FlowerColorHybridGene)),
    ]
	public IGene[] genes;
}

[XmlRoot("Player")]
public struct PlayerXmlStruct {
	public Vector3 position;
	public Vector3 rotation;
}

[XmlRoot("Held Plant")]
public struct HoldingStruct {
    public int id;
    public Vector3 position;
    public Vector3 rotation;

    public HoldingStruct(int id, Vector3 position, Vector3 rotation) {
        this.id = id;
        this.position = position;
        this.rotation = rotation;
    }
}