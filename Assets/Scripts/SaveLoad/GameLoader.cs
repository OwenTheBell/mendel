﻿using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameLoader : MonoBehaviour {

    public string StartingSceneName;
    public string GameSceneName;

    public GameObject LoadingCanvasObject;
    public GameObject CameraObject;
    public LoadingDisplay Loading;

    public float HoursPerNewPlant;
	public int MaximumNewPlants;

    public bool ForceNewGameInEditor;
    public bool AlwaysNewGame;

    private Scene _ActiveScene;

    private void Start() {
        Cursor.visible = false;
        _ActiveScene = SceneManager.GetActiveScene();
        StartCoroutine(CheckForSaveOverride());
    }

    void LoadGameCallback(IslandSerializer serializer) {
        StartCoroutine(LoadOldGame(serializer));
    }

    void StartNewGame() {
        SceneManager.LoadScene(StartingSceneName, LoadSceneMode.Single);
    }

    IEnumerator LoadOldGame(IslandSerializer serializer) {
        if (serializer == null) {
            StartNewGame();
            yield break;
        }
        Loading.Show();
        while (!Loading.Visible) {
            yield return null;
        }
        var elapsedTime = 0f;
        var operation = SceneManager.LoadSceneAsync(GameSceneName, LoadSceneMode.Additive);
        while (!operation.isDone) {
            elapsedTime += Time.deltaTime;
            yield return 0;
        }

        // if the plant count is this low, there was almost a certainly an issue with the build
        if (serializer.infos.Length < 800) {
            Debug.Log("ERROR: number of plants is very small, perhaps force a new build");
        }

        // gather all the seeds and add them into the vault
        var popManger = GameObject.Find("Populator").GetComponent<PopulationManager>();
		foreach (var seedStruct in serializer.seeds) {
			var seed = new Seed(seedStruct.genes);
			Services.SeedVault.Add(seed);
		}

        // Gather all the different species and place them appropriately
		foreach (var speciesStruct in serializer.species) {
			Services.SpeciesCatalog.SetNameForIdentities(
				speciesStruct.name,
				speciesStruct.identities
			);
		}

        foreach (var info in serializer.infos) {
            Services.PlantInfoCatalog.Infos.Add(info.Identity, info);
        }
        List<GameObject> spawnedPlants;
        // place the plants
        popManger.Populate(serializer.infos, out spawnedPlants);

        // now check how many plants should be spawned based on time since last play
        var lastPlayed = serializer.saveInfo.Date;
        var newCount = (int)DateTime.Now.Subtract(lastPlayed).TotalHours / HoursPerNewPlant;
        // if lastPlayed is equal to a new DateTime, then it was missing from the save file
        if (lastPlayed.CompareTo(new DateTime()) == 0 || serializer.species.Length <= 1) {
            newCount = 0;
        }
		newCount = Mathf.Clamp(newCount, 0, MaximumNewPlants);

        for (var i = 0; i < newCount; i++) {
            var index1 = UnityEngine.Random.Range(0, serializer.species.Length);
            var index2 = UnityEngine.Random.Range(0, serializer.species.Length - 1);
            if (index2 >= index1) index2++;

            var id1Index = UnityEngine.Random.Range(0, serializer.species[index1].identities.Length);
            var id2Index = UnityEngine.Random.Range(0, serializer.species[index2].identities.Length);
            var id1 = serializer.species[index1].identities[id1Index];
            var id2 = serializer.species[index2].identities[id2Index];

            Vector3 position1 = Vector3.zero, position2 = Vector3.zero;
            for (var j = 0; j < serializer.infos.Length; j++) {
                if (serializer.infos[j].Identity == id1) {
                    position1 = serializer.infos[j].Position;
                }
                else if (serializer.infos[j].Identity == id2) {
                    position2 = serializer.infos[j].Position;
                }
            }

            Events.instance.Raise(new HybridizePlantsEvent(
                id1,
                id2,
                position1,
                position2,
                true
            ));
        }

        while (!popManger.AllDoneGrowing) {
            yield return null;
        }
        Loading.Hide();
        while (Loading.Visible) {
            yield return null;
        }

        var playerTransform = Transform.FindObjectOfType<CharacterController>().transform;
        playerTransform.position = serializer.player.position;
        playerTransform.rotation = Quaternion.Euler(serializer.player.rotation);

        if (serializer.HeldFlowers != null) {
            var inventory = playerTransform.GetComponentInChildren<FlowerInventory>();
            inventory.AddFlowersFromIDs(serializer.HeldFlowers);
        }

        if (!serializer.heldPlant.Equals(default(HoldingStruct))) {
            var uproot = playerTransform.GetComponentInChildren<UprootBehaviour>();
            foreach (var plant in spawnedPlants) {
                if (plant.GetComponent<PlantInfo>().Identity == serializer.heldPlant.id) {
                    var rotation = Quaternion.Euler(serializer.heldPlant.rotation);
                    uproot.HoldPlant(plant, serializer.heldPlant.position, rotation);
                }
            }
        }

        CameraObject.SetActive(false);
        var hideables = LoadingCanvasObject.GetComponentsInChildren<IHideableUIElement>();
        foreach (var hideable in hideables) {
            hideable.Hide();
        }
        var hidden = false;
        while (!hidden) {
            hidden = true;
            foreach (var hideable in hideables) {
                if (hideable.Visible) {
                    hidden = false;
                }
            }
            yield return 0;
        }
        Events.instance.Raise(new PlayerStartedEvent());
        SceneManager.UnloadSceneAsync(_ActiveScene);

        yield return null;
    }

    IEnumerator CheckForSaveOverride() {
        var time = 0f;
        while (time < 1f) {
            time += Time.deltaTime;
            if (Input.GetKey(KeyCode.Escape)) {
                StartNewGame();
                yield break;
            }
            yield return null;
        }

#if UNITY_EDITOR
        if (ForceNewGameInEditor) {
            StartNewGame();
			yield break;
        }
#endif
		if (AlwaysNewGame) {
			StartNewGame();
			yield break;
		}
		IslandSerializer serializer;
		if (Services.SaveLoad.LoadGame(out serializer)){
			LoadGameCallback(serializer);
			yield break;
		}
        else {
            StartNewGame();
			yield break;
        }
		Debug.Log("did I end this routine?");
    }
}