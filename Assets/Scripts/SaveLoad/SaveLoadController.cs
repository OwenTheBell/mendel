﻿using UnityEngine;
using System;
using System.IO;
using System.Threading;

public class SaveLoadController {

	public GameObject PlantPrefab;
	public PopulationManager Populator;
	public bool LoadFromFileInInspector = false;

	public PlantXmlStruct[] Plants { get; private set; }
	public SpeciesXmlStruct[] Species { get; private set; }
	public SeedXmlStruct[] Seeds { get; private set; }

	public string SavePath {
		get {
#if UNITY_EDITOR
            return Application.dataPath + "//SaveLoadTest";
#else
            return Application.persistentDataPath;
#endif
		}
	}
	
	public SaveLoadController() { }

    public bool LoadGame(out IslandSerializer serializer) {
		if (IslandSerializer.Load(SavePath, out serializer)) {
			return true;
		}
        return false;
	}

	void Save(out Thread thread) {
		Save(SavePath, out thread);
	}

	void Save(string path, out Thread thread) {
		var time = Time.realtimeSinceStartup;
		var serializer = new IslandSerializer();

		//create all the saving info
		var saveInfo = new SaveInfoStruct();
		saveInfo.Date = System.DateTime.Now;
		serializer.saveInfo = saveInfo;

		// player save info
		var player = new PlayerXmlStruct();
		var playerTransform = Transform.FindObjectOfType<CharacterController>().transform;
		player.position = playerTransform.position;
		player.rotation = playerTransform.rotation.eulerAngles;
		serializer.player = player;

		// save all the seed info
		var seeds = Services.SeedVault.AllSeeds();
		var seedSaves = new SeedXmlStruct[seeds.Length];
		for (var i = 0; i < seeds.Length; i++) {
			var xmlStruct = new SeedXmlStruct();
			var genes = new IGene[seeds[i].Genes.Count];
			seeds[i].Genes.Values.CopyTo(genes, 0);
			xmlStruct.genes = genes;
			seedSaves[i] = xmlStruct;
		}
		serializer.seeds = seedSaves;

        playerTransform.GetComponentInChildren<FlowerInventory>().IDsInSlots(out serializer.HeldFlowers);
        var uproot = playerTransform.GetComponentInChildren<UprootInteractionState>();
        if (uproot.HeldPlant != null) {
            var id = uproot.HeldPlant.GetComponent<PlantInfo>().Identity;
            serializer.heldPlant = new HoldingStruct(id
                , uproot.HeldPlant.transform.localPosition
                , uproot.HeldPlant.transform.localRotation.eulerAngles);
        }

        serializer.infos = new PlantInfoStruct[Services.PlantInfoCatalog.Infos.Count];
        Services.PlantInfoCatalog.Infos.Values.CopyTo(serializer.infos, 0);

		// save all the species
		var identities = Services.SpeciesCatalog.Identities;
		var names = Services.SpeciesCatalog.Names;
		var species = new SpeciesXmlStruct[names.Count];
		for (var i = 0; i < names.Count; i++) {
			species[i].name = names[i];
			species[i].identities = identities[i].ToArray();
		}
		serializer.species = species;

		Debug.Log("saving to " + path);
		serializer.Save(path, out thread);
	}

	public void SaveGame(out Thread thread) {
		Save(out thread);
	}

    public void ClearSave() {
        var d = new DirectoryInfo(SavePath);
        var files = d.GetFiles("*.xml");
        foreach (var file in files) {
            if (file.Name.Contains("IslandSave")) {
                File.Delete(SavePath + "//" + file.Name);
            }
        }
    }
}