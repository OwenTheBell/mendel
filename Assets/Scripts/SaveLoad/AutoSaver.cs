﻿using UnityEngine;
using System.Collections;
using System.Threading;

public class AutoSaver : MonoBehaviour {

    public float Interval;
    public string Message;
    public float MiniumMessageTime;
    public bool DebugOutOfEditor;
    public KeyCode Key;

    private float _RemainingUntilSave;

    // Use this for initialization
    void Start() {
        _RemainingUntilSave = Interval;
    }

    // Update is called once per frame
    void Update() {
        _RemainingUntilSave -= Time.deltaTime;
        if (Input.GetKeyDown(Key) && (Application.isEditor || DebugOutOfEditor)) {
            Save();
        }
        else if (_RemainingUntilSave <= 0f) {
            Save();
            _RemainingUntilSave = Interval;
        }
    }

    void Save() {
        Events.instance.Raise(new PromptTextEvent(Message));
        Thread thread;
        Services.SaveLoad.SaveGame(out thread);
        StartCoroutine(WaitForThread(thread));
    }

    IEnumerator WaitForThread(Thread thread) {
        var elapsedTime = 0f;
        while (thread.IsAlive || elapsedTime < MiniumMessageTime) {
            elapsedTime += Time.deltaTime;
            yield return null;
        }
        Debug.Log("auto save complete");
        Events.instance.Raise(new HidePromptTextEvent(Message));
    }
}
