﻿using System.Collections;
using UnityEngine;
using System.Threading;

public class SavingScreen : MonoBehaviour, ICancelReciever {

    public float MinimumTime;

    public void Quit() {
        Services.CancelStack.RemoveAll();
        Services.CancelStack.Add(this);
        Services.CancelStack.Locked = true;
        foreach (var e in GetComponentsInChildren<IUIElement>()) {
            e.GameObject.SetActive(true);
            e.Show();
        }
        Events.instance.Raise(new HideAlertEvent());
        Thread thread;
        Services.SaveLoad.SaveGame(out thread);
        StartCoroutine(WaitForThread(thread));
    }

    public void OnCancel() {
        //this will never be used for anything
    }

    IEnumerator WaitForThread(Thread thread) {
        var time = 0f;
        while (time < MinimumTime || thread.IsAlive) {
            time += Time.deltaTime;
            yield return null;
        }
        Application.Quit();
    }
}
