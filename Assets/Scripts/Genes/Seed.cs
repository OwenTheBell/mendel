using System;
using System.Collections.Generic;
public class Seed {

	public readonly Dictionary<Type, IGene> Genes;	

	public Seed(IGene[] genes) {
		Genes = new Dictionary<Type, IGene>();
		foreach (var gene in genes) {
			Genes.Add(gene.SortingType, gene);
		}
	}

	public Seed(Seed parent1, Seed parent2, bool mutate) {
		Genes = new Dictionary<Type, IGene>();
		foreach(System.Type key in parent1.Genes.Keys) {
			IGene babyGene;
			parent1.Genes[key].MixWith(parent2.Genes[key], out babyGene, mutate);
			Genes.Add(key, babyGene);
		}
	}

	public static bool VerifyIntegrity(Seed seed) {
		var requiredTypes = new Type[4] {
			typeof(BaseStemGene),
			typeof(BaseStemColorGene),
			typeof(BaseFlowerColorGene),
			typeof(BaseFlowerGene)
		};
		foreach (var type in requiredTypes) {
			if (!seed.Genes.ContainsKey(type)) {
				return false;
			}
		}
		return true;
	}

	public static float CompareSeeds(Seed seed1, Seed seed2) {
		var weight = 0f;
		var similarity = 0f;
		foreach (var key in seed1.Genes.Keys) {
			var gene1 = seed1.Genes[key];
			var gene2 = seed2.Genes[key];
			var comparison = Gene.WeightedCompare(gene1, gene2);
			similarity += comparison.Similarity;
			weight += comparison.Weight;
		}
		return similarity/weight;
	}
}