using UnityEngine;

public abstract class BaseFlowerGene : Gene {

	public override System.Type SortingType {
		get {
			return typeof(BaseFlowerGene);
		}
	}
}