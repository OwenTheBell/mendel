using System;

public class AlleleArray {

	public int Size { get { return Collection.Length; }}

	public int Dominance {
		get {
			var output = 0;
			foreach (var allele in Collection) {
				if (allele.Dominance == 1) {
					output++;
				}
			}
			return output;
		}
	}	

	public int IncompleteDominance {
		get {
			var output = 0;
			foreach (var allele in Collection) {
				output += allele.IncompleteDominance;
			}
			return output;
		}
	}

	public Allele[] Collection { get; private set; }

	private AlleleArray() {}

	public AlleleArray(int size) {
		Collection = new Allele[Size];
		for (var i = 0; i < size; i++) {
			Collection[i] = new Allele(-1);
		}
	}

	public AlleleArray(AlleleArray array1, AlleleArray array2) {
		Collection = new Allele[array1.Size];
		if (array2.Size == Size) {
			Collection = new Allele[array2.Size];
		}

		var index = 0;
		while (index < array1.Size && index < array2.Size) {
			Collection[index] = new Allele(array1.Get(index), array2.Get(index));
			index++;
		}

		if (index < array1.Size) {
			while (index < array1.Size) {
				Collection[index] = new Allele(array1.Get(index).IncompleteDominance);
				index++;
			}
		}
		else if (index < array2.Size) {
			while (index < array2.Size) {
				Collection[index] = new Allele(array2.Get(index).IncompleteDominance);
				index++;
			}
		}
	}

	public Allele Get(int index) {
		if (index >= Size || index < 0) {
			return null;
		}
		return Collection[index];
	}
}