using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using System;

public abstract class Gene : IGene {

	public virtual System.Type SortingType {
		get {
			Debug.Assert(false, "SortingType has not been implemented in this gene");
			return typeof(Gene);
		}
	}

	public abstract GeneValues GeneValues { get; set; }
	public virtual bool Comparable{ get{ return true; }}

	public Gene() {
	}

	abstract public bool MixWith(IGene otherGene, out IGene babyGene, bool mutate);
	// abstract public string ToString();
	abstract public bool CompareWith(IGene gene);

	static public bool Compare(IGene gene1, IGene gene2) {
		var comparison = WeightedCompare(gene1, gene2);
		return (comparison.Percent >= PROPERTIES.SpeciesCutoff);
	}

    static public bool VerifyIntegrity(IGene gene) {
        var valueType = gene.GeneValues.GetType();
        var fields = valueType.GetFields();

        for (var i = 0; i < fields.Length; i++) {
            var field = fields[i];
            if (field.FieldType == typeof(Allele)) {
                if ((Allele)field.GetValue(gene.GeneValues) == null) {
                    Debug.Log(field.Name + " is null");
                    return false;
                }
            }
            else if (field.FieldType == typeof(HSVColor)) {
                if ((HSVColor)field.GetValue(gene.GeneValues) == null) {
                    Debug.Log(field.Name + " is null");
                    return false;
                }
            }
            else if (field.FieldType != typeof(System.Single)) {
                Assert.IsFalse(true, field.FieldType + " is not a valid type for genes");
            }
        }
        return true;
    }

	static public GeneComparisonStruct WeightedCompare(IGene gene1, IGene gene2) {
 		Assert.IsTrue(gene1.GetType() == gene2.GetType(),
 									"Gene types failed to match");
		
		var similarity = 0f;
		var weight = 0f;

		var values1 = gene1.GeneValues;
		var values2 = gene2.GeneValues;

		Assert.IsNotNull(values1, gene1.GetType() + " has a null value");

		var valueType = values1.GetType();

		for (var i = 0; i < valueType.GetFields().Length; i++) {
			var field1 = valueType.GetFields()[i];
			var field2 = values2.GetType().GetFields()[i];

			var sim = 0f;
			if (field1.FieldType == typeof(float)) {
				var float1 = (float)field1.GetValue(gene1.GeneValues);
				var float2 = (float)field2.GetValue(gene2.GeneValues);

				if (float1 < 0f) {
					float2 += Mathf.Abs(float1);
					float1 += Mathf.Abs(float1);
				}
				if (float2 < 0f) {
					float1 += Mathf.Abs(float2);
					float2 += Mathf.Abs(float2);
				}
				sim = (float1 > float2) ? float2/float1 : float1/float2;
				//check for NaN, indicating a divide by 0
				if (sim != sim) {
					sim = 1f;
				}
			}
			else if (field1.FieldType == typeof(Allele)) {
               var allele1 = (Allele)field1.GetValue(gene1.GeneValues);
               var allele2 = (Allele)field2.GetValue(gene2.GeneValues);

               sim = Allele.PercentDifference(allele1, allele2);
			}
			else if (field1.FieldType == typeof(HSVColor)) {
				var hsv1 = (HSVColor)field1.GetValue(gene1.GeneValues);
				var hsv2 = (HSVColor)field2.GetValue(gene2.GeneValues);

				sim = HSVColor.PercentDifference(hsv1, hsv2);
			}
            else {
                Assert.IsFalse(true, field1.FieldType + " is not a valid type for genes");
            }

			similarity += sim;
			weight += 1f;
		}

        return new GeneComparisonStruct(similarity, weight);
	}

	public virtual string ToString() {
		var type = GeneValues.GetType();
		var output = "<b>" + type + "</b>\n";
		foreach (var field in type.GetFields()) {
			if (field.FieldType == typeof(float)) {
				var floatValue = (float)field.GetValue(GeneValues);
				output += field.Name + " = " + floatValue;
			}
			else if (field.FieldType == typeof(int)) {
				var intValue = (int)field.GetValue(GeneValues);
				output += field.Name + " = " + intValue;
			}
			else if (field.FieldType == typeof(bool)) {
				var boolValue = (bool)field.GetValue(GeneValues);
				output += field.Name + " = " + boolValue;
			}
			else if (field.FieldType == typeof(Allele)) {
				var allele = (Allele)field.GetValue(GeneValues);
				output += field.Name + " = " + allele.ToString();
			}
			else if (field.FieldType == typeof(HSVColor)) {
				var color = (HSVColor)field.GetValue(GeneValues);
				output += field.Name + " = " + color;
			}
			output += "\n";
		}

		return output;
	}	

}

[System.Serializable]
public class GeneValues {

    struct MutationStruct {
        public float Min;
        public float Max;
        public float Step;

        public MutationStruct(float min, float max, float step) {
            Min = min;
            Max = max;
            Step = step;
        }
    }

    [NonSerialized]
    private static List<string> FloatGenes = new List<string>(new string[] {
        // stem genes
        "BranchAngle",
        "Depth",
        "BranchStunting",
        "FirstBranch",
        "Width",
        "Height",
        "Twist",
        "Trunk",
        
        // flower genes
        "FlowerHeight",
        "TipHeight",
        "TipWidth",
        "SpreadAngle",
        "TipAngle",
        "WrapAngle",
        "UpperAngle",
        "Count",
        "PetalAngle",
    });

    [NonSerialized]
    private static List<MutationStruct> MutationValues = new List<MutationStruct>(new MutationStruct[] {
        new MutationStruct(0f, Mathf.Infinity, 10f), // Branch Angle
        new MutationStruct(0f, Mathf.Infinity, 2f), // Depth
        new MutationStruct(0f, 1f, 0.05f), // Branch Stunting
        new MutationStruct(0f, 1f, 0.05f), // First Branch
        new MutationStruct(0f, Mathf.Infinity, 0.025f), // Width
        new MutationStruct(0f, Mathf.Infinity, 0.025f), // Height
        new MutationStruct(Mathf.NegativeInfinity, Mathf.Infinity, 10f), // Twist
        new MutationStruct(0f, 1f, 0.1f), // Trunk
        new MutationStruct(0f, Mathf.Infinity, 0.04f), // Flower Height
        new MutationStruct(0f, Mathf.Infinity, 0.04f), // Tip Height
        new MutationStruct(0f, Mathf.Infinity, 0.04f), // Tip Width
        new MutationStruct(1f, 360f, 10f), // Spread Angle
        new MutationStruct(Mathf.NegativeInfinity, Mathf.Infinity, 7f), // Tip Angle
        new MutationStruct(Mathf.NegativeInfinity, Mathf.Infinity, 7f), // Wrap Angle
        new MutationStruct(1f, Mathf.Infinity, 7f), // Upper Angle
        new MutationStruct(1f, Mathf.Infinity, 1f), // Count
        new MutationStruct(1f, Mathf.Infinity, 7f) // Petal Angle
    });

    [NonSerialized]
    private static List<string> AlleleGenes = new List<string>(new string[] {
        // stem genes
        "BranchSpacingAllele",
        "BudsAtBranchTipsAllele",
        "StraightAllele",
        "ClockwiseAllele",
        "CounterclockwiseAllele",
        "CrookedBase",

        // flower color genes
        "CoolBaseAllele",
        "WarmBaseAllele",
        "CoolTipAllele",
        "WarmTipAllele",

        // flower genes
        "FlatSpiraled"
    });

    [NonSerialized]
    private static List<string> HSVColorGenes = new List<string>(new string[] {
        // stem color genes
        "BaseColor",
        "TipColor",

        // flower color genes
        "CoolBaseColor",
        "WarmBaseColor",
        "CoolTipColor",
        "WarmTipColor"
    });

	static public void Mix<T>(T values1, T values2, ref T mixedValues, bool mutate) where T : GeneValues {
		var fields = typeof(T).GetFields();
		for (var i = 0; i < fields.Length; i++) {
			var field = fields[i];
			if (field.FieldType == typeof(float)) {
				var float1 = (float)field.GetValue(values1);
				var float2 = (float)field.GetValue(values2);
				var random = UnityEngine.Random.Range(float1, float2);
                if (UnityEngine.Random.value < PROPERTIES.MutationChance && mutate) {
                    Debug.Log(field.Name + " mutated");
                    var lookupIndex = FloatGenes.IndexOf(field.Name);
                    var direction = UnityEngine.Random.Range(0, 2);
                    if (direction == 0) direction = -1;
                    var mutStruct = MutationValues[lookupIndex];
                    random = Mathf.Clamp(random + direction * mutStruct.Step, mutStruct.Min, mutStruct.Max);
                }
				field.SetValue(mixedValues, random);
			}
			else if (field.FieldType == typeof(Allele)) {
                var lookupIndex = AlleleGenes.IndexOf(field.Name);
				var allele1 = (Allele)field.GetValue(values1);
				var allele2 = (Allele)field.GetValue(values2);
				field.SetValue(mixedValues, new Allele(allele1, allele2));
			}
			else if (field.FieldType == typeof(HSVColor)) {
                var lookupIndex = HSVColorGenes.IndexOf(field.Name);
				var hsv1 = (HSVColor)field.GetValue(values1);
				var hsv2 = (HSVColor)field.GetValue(values2);
				var t = UnityEngine.Random.Range(0f, 1f);
				field.SetValue(mixedValues, HSVColor.Lerp(hsv1, hsv2, t));}
		}
	}
}