using UnityEngine;

public abstract class BaseFlowerColorGene : Gene {

	public virtual HSVColor BaseHSVColor { get; private set; }
	public virtual HSVColor TipHSVColor { get; private set;  }
	public virtual UnityEngine.Color BaseRGBColor { get; private set;  }
	public virtual UnityEngine.Color TipRGBColor { get; private set;  }
    public virtual bool Gradient { get; private set; }

	public override System.Type SortingType {
		get {
			return typeof(BaseFlowerColorGene);
		}
	}
}