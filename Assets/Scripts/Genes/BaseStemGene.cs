using UnityEngine;

public abstract class BaseStemGene : Gene {
	
    // how far left/right each branch goes from its parent.
    virtual public int BranchAngle { get; private set; }
    // how much random variation is there in the angle for each branch?
    virtual public int AngleRandom { get; private set; }
    // number of stem chunks to generate, minimum 1
    virtual public int MaxDepth { get; private set; }
    virtual public int BranchSpacing { get; private set; }
    virtual public int BranchChange { get; private set; }
    virtual public int BudSpacing { get; private set; }
    virtual public bool Stunted { get; private set; }
    virtual public bool NoBranches { get; private set; }
    virtual public float BranchStunting { get; private set; }
    virtual public bool Straight { get; private set; }
    virtual public Vector3 BaseChunkSize { get; private set; }
    virtual public int Twist { get; private set; }
    virtual public bool Clockwise { get; private set; }
    virtual public bool Counterclockwise { get; private set; }
    virtual public float Trunk { get; private set; }
    virtual public float FirstBranch { get; private set; }
    virtual public bool CrookedBase { get; private set; }

	public override System.Type SortingType {
		get {
			return typeof(BaseStemGene);
		}
	}
}