using UnityEngine;
using UnityEngine.Assertions;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class PetalFlowerGeneValues : GeneValues {
	[XmlAttribute("Height")]
	public float FlowerHeight;
	[XmlAttribute("TipHeight")]
	public float TipHeight;
	[XmlAttribute("TipWidth")]
	public float TipWidth;
	[XmlAttribute("SpreadAngle")]
	public float SpreadAngle;
	[XmlAttribute("TipAngle")]
	public float TipAngle;
	[XmlAttribute("WrapAngle")]
	public float WrapAngle;

	public Allele FlatSpiraled;
    [XmlAttribute("Upper Angle")]
    public float UpperAngle;
    public Allele TightSpiral;
    public Allele Clockwise;
	[XmlAttribute("PetalCount")]
	public float Count;
	[XmlAttribute("PetalAngle")]
	public float PetalAngle;
}

[System.Serializable]
public class PetalFlowerGene : BaseFlowerGene {

	[XmlIgnoreAttribute]
	public override GeneValues GeneValues {
		get { 
			return FlowerValues;
		}
		set {
			if (value.GetType() == typeof(PetalFlowerGeneValues)) {
				FlowerValues = value as PetalFlowerGeneValues;
			}
		}
	}
	public PetalFlowerGeneValues FlowerValues { get; set; }

	public float Height { get { return FlowerValues.FlowerHeight; } }
	public float TipHeight { get { return FlowerValues.TipHeight; } }
	public float TipWidth { get { return FlowerValues.TipWidth; } }
	public float SpreadAngle { get { return FlowerValues.SpreadAngle; } }
	public float TipAngle { get { return FlowerValues.TipAngle; } }
	public float WrapAngle { get { return FlowerValues.WrapAngle; } }

	public bool Spiraled { get { return FlowerValues.FlatSpiraled.Dominance == -1; }}
    public float UpperAngle { get { return FlowerValues.UpperAngle; } }
    public bool TightSpiral { get { return FlowerValues.TightSpiral.Dominance == 1; } }
    public bool Clockwise { get { return FlowerValues.Clockwise.Dominance == 1; } }
	public int Count { get { return Mathf.RoundToInt(FlowerValues.Count); }}
	public float PetalAngle { get { return FlowerValues.PetalAngle; }}

	/* emptry constructor for serialization */
	public PetalFlowerGene() {}

	public PetalFlowerGene(PetalFlowerGeneValues values) {
		FlowerValues = values;
	}

    public PetalFlowerGene(PetalFlowerGene parent1, PetalFlowerGene parent2, bool mutate) {
        var values1 = parent1.FlowerValues;
        var values2 = parent2.FlowerValues;
        var flowerValues = new PetalFlowerGeneValues();
        GeneValues.Mix<PetalFlowerGeneValues>(values1, values2, ref flowerValues, mutate);
        FlowerValues = flowerValues;
	}

	public override bool MixWith(IGene otherGene, out IGene babyGene, bool mutate) {
		Assert.IsTrue(otherGene.GetType() == this.GetType(), "mismatched type in genes");
		babyGene = new PetalFlowerGene(this, otherGene as PetalFlowerGene, mutate);
		return true;
	}

	public override bool CompareWith(IGene gene) {
		Assert.IsTrue(gene.GetType() == this.GetType());
		var otherGene = gene as PetalFlowerGene;
		
		var otherType = otherGene.FlowerValues.GetType();	
		var valuesType = FlowerValues.GetType();

		var equivalent = true;

		for (var i = 0; i < valuesType.GetFields().Length; i++) {
			var field1 = valuesType.GetFields()[i];
			var field2 = otherType.GetFields()[i];

			if (field1.FieldType == typeof(float)) {
				var accuracy = 10000;

				var float1 = (float)field1.GetValue(FlowerValues);
				var float2 = (float)field2.GetValue(otherGene.FlowerValues);

				if ((int)(float1 * accuracy) != (int)(float2 * accuracy)) {
					equivalent = false;
					break;	
				}
			}
			else if (field1.FieldType == typeof(Allele)) {
               var allele1 = (Allele)field1.GetValue(FlowerValues);
               var allele2 = (Allele)field2.GetValue(otherGene.FlowerValues);

               if (allele1.Dominance != allele2.Dominance) {
               		equivalent = false;
               		break;
               }
			}
			else if (field1.FieldType == typeof(int)) {
				var int1 = (int)field1.GetValue(FlowerValues);
				var int2 = (int)field2.GetValue(otherGene.FlowerValues);

				if (int1 != int2) {
					equivalent = false;
					break;
				}
			}
		}

        return equivalent;
    }
}