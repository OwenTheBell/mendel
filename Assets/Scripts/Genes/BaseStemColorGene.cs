using UnityEngine;

public abstract class BaseStemColorGene : Gene {

	virtual public HSVColor BaseColor { get; protected set; }
	virtual public HSVColor TipColor { get; protected set; }

	public override System.Type SortingType {
		get {
			return typeof(BaseStemColorGene);
		}
	}
 }