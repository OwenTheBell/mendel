using UnityEngine;
using UnityEngine.Assertions;
using System.Xml.Serialization;

[System.Serializable]
public class StemColorGeneValues : GeneValues {
    public HSVColor BaseColor;
    public HSVColor TipColor;
}

[System.Serializable]
public class StemColorHybridGene : BaseStemColorGene { //IStemColorGene {

    /**********************
    * Interface Variables *
    **********************/

    [XmlIgnoreAttribute]
	public override HSVColor BaseColor {
        get {
            return StemValues.BaseColor;
        }
        protected set {
            StemValues.BaseColor = value;
        }
    }
    [XmlIgnoreAttribute]
	public override HSVColor TipColor {
        get {
            return StemValues.TipColor;
        }
        protected set {
            StemValues.TipColor = value;
        }
    }

    [XmlIgnoreAttribute]
    public override GeneValues GeneValues {
        get {
            return StemValues;
        }
        set {
            if (value.GetType() == typeof(StemColorGeneValues)) {
                StemValues = value as StemColorGeneValues;
            }
        }
    }
    public StemColorGeneValues StemValues { get; set; }

    /* parameterless contructor for serialization */
    private StemColorHybridGene() {}

    public StemColorHybridGene(StemColorGeneValues values) {
        StemValues = values;
    }

    public StemColorHybridGene(Color baseColor, Color tipColor) {
        StemValues = new StemColorGeneValues();
        BaseColor = new HSVColor(baseColor);
        TipColor = new HSVColor(tipColor);
    }

    public StemColorHybridGene(StemColorHybridGene parent1, StemColorHybridGene parent2, bool mutate) {
        var values1 = parent1.StemValues;
        var values2 = parent2.StemValues;
        var stemValues = new StemColorGeneValues();
        GeneValues.Mix<StemColorGeneValues>(values1, values2, ref stemValues, mutate);
        StemValues = stemValues;
    }

    public override bool MixWith(IGene otherGene, out IGene babyGene, bool mutate) {
        if (otherGene.GetType() != typeof(StemColorHybridGene)) {
            babyGene = null;
            return false;
        }
        StemColorHybridGene other = otherGene as StemColorHybridGene;
        babyGene = new StemColorHybridGene(this, other, mutate);
        return true;
    }

    public override bool CompareWith(IGene gene) {
        var hDivide = 0.05f;
        var sDivide = 0.1f;
        var vDivide = 0.1f;

        Assert.IsTrue(gene.GetType() == typeof(StemColorHybridGene));

        StemColorHybridGene otherGene = gene as StemColorHybridGene;

        HSVColor tipDiff = TipColor.Difference(otherGene.TipColor);
        HSVColor baseDiff = BaseColor.Difference(otherGene.BaseColor);
        var result = (tipDiff.h < hDivide && tipDiff.s < sDivide && tipDiff.v < vDivide) &&
                (baseDiff.h < hDivide && baseDiff.s < sDivide && baseDiff.v < vDivide);

        return result;
    }
}