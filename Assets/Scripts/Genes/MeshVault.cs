﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public class MeshVault {

    private Dictionary<int, PlantMeshInfo> _AllPlantMeshes = new Dictionary<int, PlantMeshInfo>();

    public int Count { get { return _AllPlantMeshes.Count;  } }

    public void AddStemInfo(int identity, Mesh stem) {
        var meshInfo = new PlantMeshInfo();
        if (_AllPlantMeshes.TryGetValue(identity, out meshInfo)) {
            meshInfo.Stem = stem;
        }
        else {
            _AllPlantMeshes.Add(identity, new PlantMeshInfo(stem));
        }
    }

    public void AddFlowerInfo(int identity, Mesh flower, FlowerPosition spawn) {
        var meshInfo = new PlantMeshInfo();
        if (_AllPlantMeshes.TryGetValue(identity, out meshInfo)) {
            meshInfo.Flowers.Add(flower);
            meshInfo.Positions.Add(spawn);
        }
        else {
            _AllPlantMeshes.Add(identity, new PlantMeshInfo(flower, spawn));
        }
    }

    public void Add(int identity, PlantMeshInfo info) {
        _AllPlantMeshes.Add(identity, info);
    }

    public Mesh GetStem(int identity) {
        var meshInfo = new PlantMeshInfo();
        if (_AllPlantMeshes.TryGetValue(identity, out meshInfo)) {
            return meshInfo.Stem;
        }
        return null;
    }

    public Mesh GetFlower(int identity) {
        var meshInfo = new PlantMeshInfo();
        if (_AllPlantMeshes.TryGetValue(identity, out meshInfo)) {
            return meshInfo.Flowers[0];
        }
        return null;
    }

    public List<Mesh> GetAllFlowers(int identity) {
        var meshInfo = new PlantMeshInfo();
        if (_AllPlantMeshes.TryGetValue(identity, out meshInfo)) {
            return meshInfo.Flowers;
        }
        return null;
    }

    public PlantMeshInfo GetPlant(int identity) {
        var meshInfo = new PlantMeshInfo();
        if (_AllPlantMeshes.TryGetValue(identity, out meshInfo)) {
            return meshInfo;
        }
        return new PlantMeshInfo();
    }

    public void Clear() {
        // go through all plants and destroy the meshes to make sure they are removed
        // from memory
        foreach (var key in _AllPlantMeshes.Keys) {
            var info = _AllPlantMeshes[key];
            Mesh.Destroy(info.Stem);
            foreach (var flowerMesh in _AllPlantMeshes[key].Flowers) {
                Mesh.Destroy(flowerMesh);
            }
            _AllPlantMeshes[key].Flowers.Clear();
        }
        _AllPlantMeshes.Clear();
    }
}

public struct PlantMeshInfo {
    public Mesh Stem;
    public List<Mesh> Flowers;
    public List<FlowerPosition> Positions;

    public PlantMeshInfo(Mesh stem) {
        Stem = stem;
        Flowers = new List<Mesh>();
        Positions = new List<FlowerPosition>();
    }

    public PlantMeshInfo(Mesh flower, FlowerPosition spawn) {
        Stem = new Mesh();
        Flowers = new List<Mesh>(new Mesh[] { flower });
        Positions = new List<FlowerPosition>(new FlowerPosition[] { spawn });
    }
    
    public PlantMeshInfo(Mesh[] flowers, FlowerPosition[] spawns) {
        Stem = new Mesh();
        Flowers = new List<Mesh>(flowers);
        Positions = new List<FlowerPosition>(spawns);
    }
}