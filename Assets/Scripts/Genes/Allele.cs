using UnityEngine;
using System.Xml.Serialization;

[System.Serializable]
public class Allele {

    /*******************
    * Member Variables *
    *******************/

    public int DominanceAdjust {
        get {
           return IncompleteDominance; 
        }
        set {
            if (value == 1) {
                Pair = new int[2]{1, 1};
            } else if (value == 0) {
                Pair = new int[2]{1, -1};
            } else if (value == -1) {
                Pair = new int[2]{-1, -1};
            }
        }
    }

    [HideInInspector]
    [XmlIgnoreAttribute]
    public int[] Pair {
        get {
            return new int[2]{allele1, allele2};
        }
        private set {
            UnityEngine.Assertions.Assert.IsTrue(value.Length >= 2);
            UnityEngine.Assertions.Assert.IsTrue(value[0] >= -1 && value[0] <= 1);
            UnityEngine.Assertions.Assert.IsTrue(value[1] >= -1 && value[1] <= 1);
            allele1 = value[0];
            allele2 = value[1];
        }
    }

    [XmlAttribute("allele1")]
    public int allele1 = 1;
    [XmlAttribute("allele2")]
    public int allele2 = 1;

    public int IncompleteDominance {
        get {
            return (Pair[0] + Pair[1]) / 2;
        }
    }

    public int Dominance {
        get {
            int output = (Pair[0] + Pair[1]) / 2;
            if (output == 0) output = 1;
            return output;
        }
    }

    /***************
    * Constructors *
    ***************/

    /* parametereless constructor for serialization */
    private Allele() { }

    public Allele (int dominance) {
        UnityEngine.Assertions.Assert.IsTrue(dominance >= -1 && dominance <= 1);
        if (dominance == 1) {
            Pair = new int[2]{1, 1};
        } else if (dominance == 0) {
            Pair = new int[2]{1, -1};
        } else if (dominance == -1) {
            Pair = new int[2]{-1, -1};
        }
    }

    public Allele (Allele parent1, Allele parent2) {
        int value1 = parent1.Pair[UnityEngine.Random.Range(0, 2)];
        int value2 = parent2.Pair[UnityEngine.Random.Range(0, 2)];
        Pair = new int[2]{value1, value2};
    }

    /******************* 
    * Member Functions *
    *******************/

    public override string ToString() {
        return "Allele: (" + Pair[0] + ", " + Pair[1] + ")";
    }

    static public float PercentDifference(Allele allele1, Allele allele2) {
        var incomplete1 = allele1.IncompleteDominance;
        var incomplete2 = allele2.IncompleteDominance;

        return Mathf.Abs(Mathf.Abs(incomplete1 - incomplete2)/2 - 1);
    }
}
