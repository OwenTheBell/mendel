using UnityEngine;
using UnityEngine.Assertions;
using System.Collections.Generic;
using System.Reflection;
using System.Xml;
using System.Xml.Serialization;

[System.Serializable]
public class StemGeneValues : GeneValues {
    [XmlAttribute("BranchAngle")]
    public float BranchAngle = 20f;
    [XmlAttribute("BranchDepth")]
    public float Depth = 10f;
    public Allele BranchSpacingAllele;
    public Allele BudsAtBranchTipsAllele;
    [Range(0f, 1f)]
    [XmlAttribute("BranchStunting")]
    public float BranchStunting;
    public Allele StraightAllele;
    [Range(0f, 1f)]
    public float FirstBranch;

    [Range(0.03f, 0.4f)]
    public float Width;
    [Range(0.1f, 0.75f)]
    public float Height;
    [Range(25, 90)]
    public float Twist;
    public Allele ClockwiseAllele;
    public Allele CounterclockwiseAllele;
    [Range(0f, 1f)]
    public float Trunk;
    public Allele CrookedBase;
}

[System.Serializable]
public class HybridStemGene : BaseStemGene {

    /*******************
    * Member Variables *
    *******************/

    [XmlIgnoreAttribute]
    public override GeneValues GeneValues {
        get {
            return StemValues;
        }
        set {
            if (value.GetType() == typeof(StemGeneValues)) {
                StemValues = value as StemGeneValues;
            }
        }
    }
    public StemGeneValues StemValues { get; set; }

    /**********************
    * Interface Variables *
    **********************/

    public override int BranchAngle {
        get {
            return (int)Mathf.Round(StemValues.BranchAngle);
        }
    }
    //how much random variation is there in the angle for each branch?
    /* HARDCODED UNTIL GENETICS DETERMINED */
    public override int AngleRandom {
        get {
            return 5;
        }
    }
    //what's the maximum "depth" of the recursive algorithm?
    public override int MaxDepth {
        get {
            var depth = Mathf.Clamp(StemValues.Depth, 1f, Mathf.Infinity);
            return (int)Mathf.Round(depth);
        }
    }
    public override int BranchSpacing {
        get {
            int center = 2;
            return center + StemValues.BranchSpacingAllele.IncompleteDominance;
        }
    }

    /* HARDCODED UNTIL GENETICS DETERMINED */
    public override int BranchChange {
        get {
            return 1;
        }
    }

    public override int BudSpacing {
        get {
            if (StemValues.BudsAtBranchTipsAllele.Dominance == 1) {
                return 10000;
            }
            return 2;
        }
    }

    public override float BranchStunting {
        get {
            if (StemValues.BranchStunting >= 0.7f) {
                return 1f;
            }
            return StemValues.BranchStunting;
        }
    }
    public override bool Straight {
        get {
            return StemValues.StraightAllele.Dominance == 1;
        }
    }

    public override Vector3 BaseChunkSize {
        get {
            return new Vector3(StemValues.Width, StemValues.Height, StemValues.Width);
        }
    }

    public override int Twist {
        get { return (int)Mathf.Round(StemValues.Twist); }
    }

    public override bool Clockwise {
        get { return StemValues.ClockwiseAllele.Dominance == 1;  }
    }

    public override bool Counterclockwise {
        get { return StemValues.CounterclockwiseAllele.Dominance == 1;  }
    }

    public override float Trunk {
        get { return 1f - StemValues.Trunk; }
    }

    public override float FirstBranch {
        get { return Easing.EaseIn(StemValues.FirstBranch, 2);  }
    }

    public override bool CrookedBase {
        get {
            return StemValues.CrookedBase.Dominance == 1;
        }
    }

    /***************
    * Constructors *
    ***************/

    /* parameterless constructor for serialization */
    private HybridStemGene() {}

    public HybridStemGene(StemGeneValues values) {
        StemValues = values;
    }

    public HybridStemGene(HybridStemGene parent1, HybridStemGene parent2, bool mutate) {
        var values1 = parent1.StemValues;
        var values2 = parent2.StemValues;
        var stemValues = new StemGeneValues();
        GeneValues.Mix<StemGeneValues>(values1, values2, ref stemValues, mutate);
        StemValues = stemValues;
    }

    /**********************
    * Interface Functions *
    **********************/

    public override bool MixWith(IGene otherGene, out IGene babyGene, bool mutate) {
        if (otherGene.GetType() != typeof(HybridStemGene)) {
            babyGene = null;
            return false;
        }
        HybridStemGene other = otherGene as HybridStemGene;

        babyGene = new HybridStemGene(this, other, mutate);
        return true;
    }

    public override bool CompareWith(IGene gene) {

        var branchAngleGap = 3f;
        var stuntingGap = 0.1f;
        Assert.IsTrue(gene.GetType() == this.GetType());

        HybridStemGene otherGene = gene as HybridStemGene;

        var angleDiff = Mathf.Abs(BranchAngle - otherGene.BranchAngle);
        var stuntingDiff = Mathf.Abs(BranchStunting - otherGene.BranchStunting);

        var result = angleDiff <= branchAngleGap &&
                MaxDepth == otherGene.MaxDepth &&
                BranchSpacing == otherGene.BranchSpacing &&
                BudSpacing == otherGene.BudSpacing &&
                stuntingDiff <= stuntingGap &&
                Straight == otherGene.Straight;

        return result;
    }
}