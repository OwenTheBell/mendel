using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class HybridStemGeneCreator : MonoBehaviour, IGeneCreator {

    /**********************
    * Interface Variables *
    **********************/

    public IGene Gene {
        get {
            return new HybridStemGene(StemValues);
        }
    }

    /*******************
    * Member Variables *
    *******************/

    public StemGeneValues StemValues;
}
