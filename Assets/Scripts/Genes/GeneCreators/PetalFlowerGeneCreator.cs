using UnityEngine; 

public class PetalFlowerGeneCreator : MonoBehaviour, IGeneCreator {

    /**********************
    * Interface Variables *
    **********************/

    public IGene Gene {
        get {
            return new PetalFlowerGene(FlowerValues);
        }
    }

    /*******************
    * Member Variables *
    *******************/

    public PetalFlowerGeneValues FlowerValues;
	
}