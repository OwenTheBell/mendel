﻿using UnityEngine;
using System.Collections;

public class FlowerColorHybridGeneCreator : MonoBehaviour, IGeneCreator {

	/*******************
	* Member Variables *
	*******************/

	// [Range(-1, 1)]
	// public int BaseWarmAllele;
	public Allele BaseWarmAllele;
	public Color BaseWarmColor = Color.red;

	// [Range(-1, 1)]
	// public int BaseCoolAllele;
	public Allele BaseCoolAllele;
	public Color BaseCoolColor = Color.blue;

	// [Range(-1, 1)]
	// public int TipWarmAllele;
	public Allele TipWarmAllele;
	public Color TipWarmColor = Color.red;

	// [Range(-1, 1)]
	// public int TipCoolAllele;
	public Allele TipCoolAllele;
	public Color TipCoolColor = Color.blue;

    public Allele Random;

	// public Color baseColor = Color.black;
	// public Color tipColor = Color.white;

	/**********************
	* Interface Variables *
	**********************/

	private FlowerColorHybridGene mFlowerColorHybridGene;
	public IGene Gene {
		get {
			if (mFlowerColorHybridGene == null) {
				// mFlowerColorHybridGene = new FlowerColorHybridGene(baseColor, tipColor);
				HSVColor[] colors = new HSVColor[4];
				colors[0] = new HSVColor(BaseWarmColor);
				colors[1] = new HSVColor(BaseCoolColor);
				colors[2] = new HSVColor(TipWarmColor);
				colors[3] = new HSVColor(TipCoolColor);
				Allele[] alleles = new Allele[4];
				alleles[0] = BaseWarmAllele;
				alleles[1] = BaseCoolAllele;
				alleles[2] = TipWarmAllele;
				alleles[3] = TipCoolAllele;
				mFlowerColorHybridGene = new FlowerColorHybridGene(colors, alleles, Random);
			}
			return mFlowerColorHybridGene;
		}
	}

	[ExecuteInEditMode]
	void OnValidate() {
		BaseWarmColor = ClampWarmColor(BaseWarmColor);
		TipWarmColor = ClampWarmColor(TipWarmColor);
		BaseCoolColor = ClampCoolColor(BaseCoolColor);
		TipCoolColor = ClampCoolColor(TipCoolColor);
	}

	Color ClampWarmColor(Color color) {
		HSVColor hsv = new HSVColor(color);
		if (hsv.h < 0.75f && hsv.h > 0.25f) {
			float diff = Mathf.Abs(0.75f - hsv.h);
			if (diff < 0.25f) {
				hsv.h = 0.75f;
			}
			else {
				hsv.h = 0.25f;
			}
		}

		// return (new HSVColor(h, s, v)).RGBColor;
		return hsv.RGBColor;
	}

	Color ClampCoolColor(Color color) {
		HSVColor hsv = new HSVColor(color);
		if (hsv.h > 0.75f || hsv.h < 0.25f) {
			float diff = hsv.h - 0.75f;
			if (diff > 0) {
				hsv.h = 0.75f;
			}
			else {
				hsv.h = 0.25f;
			}
		}

		return hsv.RGBColor;
	}

}
