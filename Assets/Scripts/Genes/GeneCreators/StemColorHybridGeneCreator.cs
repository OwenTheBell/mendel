using UnityEngine;

public class StemColorHybridGeneCreator : MonoBehaviour, IGeneCreator {
	
	public Color BaseColor;
	public Color TipColor;

	public IGene Gene {
		get {
			return new StemColorHybridGene(BaseColor, TipColor);
		}
	}

}