public interface IFlowerColorGene : IGene {
	HSVColor BaseHSVColor { get; }
	HSVColor TipHSVColor { get;  }
	UnityEngine.Color BaseRGBColor { get; }
	UnityEngine.Color TipRGBColor { get; }
}