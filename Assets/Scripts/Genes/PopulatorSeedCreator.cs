﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;

public class PopulatorSeedCreator : MonoBehaviour {

	public float Density;

	[SerializeField]
	private SeededStemValuesCreator StemValues;
	[SerializeField]
	private SeededPetalValuesCreator PetalValues;
	[SerializeField]
	private SeededStemColorValuesCreator StemColorValues;
	[SerializeField]
	private SeededFlowerColorValuesCreator FlowerColorValues;
 
	// Use this for initialization
	void Start () {
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void Reset() {
	}

	public Seed SeedForProceduralSeed(int seed) {
		var genes = new List<IGene>();
		var seededRandom = new SeededRandom(seed);
		genes.Add(StemValues.GetGeneForSeed(seededRandom));
		genes.Add(PetalValues.GetGeneForSeed(seededRandom));
		genes.Add(StemColorValues.GetGeneForSeed(seededRandom));
		genes.Add(FlowerColorValues.GetGeneForSeed(seededRandom));
		return new Seed(genes.ToArray());
	}
}

[System.Serializable]
public abstract class SeededValuesCreator<T> where T : GeneValues {

	public T LowerValues;
	public T UpperValues;

	public SeededValuesCreator() {}

	public abstract Gene GetGeneForSeed(SeededRandom seed);

	protected GeneValues MixTwoGeneValues(SeededRandom seeded) {
		var returnObject = Activator.CreateInstance(LowerValues.GetType());
		foreach (var field in LowerValues.GetType().GetFields()) {
			if (field.FieldType == typeof(float)) {
				var float1 = (float)field.GetValue(LowerValues);
				var float2 = (float)field.GetValue(UpperValues);
				var t = seeded.Range(0f, 1f);
				var random = Mathf.Lerp(float1, float2, t);
				field.SetValue(returnObject, random);
			}
			else if (field.FieldType == typeof(int)) {
				var int1 = (int)field.GetValue(LowerValues);
				var int2 = (int)field.GetValue(UpperValues);
				var t = seeded.Range(0f, 1f);
				var random = (int)Mathf.Round(Mathf.Lerp(int1, int2, t));
				field.SetValue(returnObject, random);
			}
			else if (field.FieldType == typeof(bool)) {
				var bool1 = (bool)field.GetValue(LowerValues);
				var bool2 = (bool)field.GetValue(UpperValues);
				var random = (seeded.Next(0, 2) == 0) ? bool1 : bool2;
				field.SetValue(returnObject, random);
			}
			else if (field.FieldType == typeof(Allele)) {
				var dominance1 = ((Allele)field.GetValue(LowerValues)).IncompleteDominance;
				var dominance2 = ((Allele)field.GetValue(UpperValues)).IncompleteDominance;
				var dominance = seeded.Next(dominance1, dominance2 + 1);
				field.SetValue(returnObject, new Allele(dominance));
			}
			else if (field.FieldType == typeof(HSVColor)) {
				var hsv1 = (HSVColor)field.GetValue(LowerValues);
				var hsv2 = (HSVColor)field.GetValue(UpperValues);
				var t = seeded.Range(0f, 1f);
				field.SetValue(returnObject, HSVColor.Lerp(hsv1, hsv2, t));
			}
		}
		return (GeneValues)returnObject;
	}
}

[System.Serializable]
class SeededStemValuesCreator : SeededValuesCreator<StemGeneValues> {

	public SeededStemValuesCreator() {
		LowerValues = new StemGeneValues();	
		UpperValues = new StemGeneValues();	
	}

	public override Gene GetGeneForSeed(SeededRandom seed) {
		var values = MixTwoGeneValues(seed);
		return new HybridStemGene((StemGeneValues)values);
	}
}

[System.Serializable]
class SeededPetalValuesCreator : SeededValuesCreator<PetalFlowerGeneValues> {

	public SeededPetalValuesCreator() {
		LowerValues = new PetalFlowerGeneValues();	
		UpperValues = new PetalFlowerGeneValues();	
	}

	public override Gene GetGeneForSeed(SeededRandom seed) {
		var values = MixTwoGeneValues(seed);
		return new PetalFlowerGene((PetalFlowerGeneValues)values);
	}
}

[System.Serializable]
class SeededStemColorValuesCreator : SeededValuesCreator<StemColorGeneValues> {

	public SeededStemColorValuesCreator() {
		LowerValues = new StemColorGeneValues();	
		UpperValues = new StemColorGeneValues();	
	}

	public override Gene GetGeneForSeed(SeededRandom seed) {
		var values = MixTwoGeneValues(seed);
		return new StemColorHybridGene((StemColorGeneValues)values);
	}
}

[System.Serializable]
class SeededFlowerColorValuesCreator : SeededValuesCreator<FlowerColorGeneValues> {

	public SeededFlowerColorValuesCreator() {
		LowerValues = new FlowerColorGeneValues();	
		UpperValues = new FlowerColorGeneValues();	
	}

	public override Gene GetGeneForSeed(SeededRandom seed) {
		var values = MixTwoGeneValues(seed);
		return new FlowerColorHybridGene((FlowerColorGeneValues)values);
	}
}
