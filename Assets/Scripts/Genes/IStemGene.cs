public interface IStemGene : IGene {

    //how far left/right each branch goes from its parent.
    int BranchAngle { get; }
    //how much random variation is there in the angle for each branch?
    int AngleRandom { get; }
    //what's the maximum "depth" of the recursive algorithm?
    int MaxDepth { get; }
    int BranchSpacing { get; }
    int BranchChange { get; }
    int BudSpacing { get; }
    bool Stunted { get; }
    bool NoBranches { get; }
    float BranchStunting { get; }
    bool Straight { get; }
}
