using UnityEngine;
using UnityEngine.Assertions;
using System.Reflection;
using System.Xml.Serialization;

[System.Serializable]
public class FlowerColorGeneValues : GeneValues {
	public Allele CoolBaseAllele;
	public HSVColor CoolBaseColor;
	public Allele WarmBaseAllele;
	public HSVColor WarmBaseColor;
	public Allele CoolTipAllele;
	public HSVColor CoolTipColor;
	public Allele WarmTipAllele;
	public HSVColor WarmTipColor;
    public Allele Random;
}

[System.Serializable]
public class FlowerColorHybridGene : BaseFlowerColorGene {

	/*******************
	* Member Variables *
	*******************/

	/**********************
	* Interface Variables *
	**********************/

	public override HSVColor BaseHSVColor {
		get {
			HSVColor returnColor = new HSVColor(Color.white);
			if (FlowerValues.WarmBaseAllele.Dominance == 1) {
				returnColor = FlowerValues.WarmBaseColor;
			}
			else if (FlowerValues.CoolBaseAllele.Dominance == 1) {
				returnColor = FlowerValues.CoolBaseColor;
			}
			return returnColor;
		}	
	}

	public override HSVColor TipHSVColor {
		get {
			HSVColor returnColor = new HSVColor(Color.black);
			if (FlowerValues.WarmTipAllele.Dominance == 1) {
				returnColor = FlowerValues.WarmTipColor;
			}
			else if (FlowerValues.CoolTipAllele.Dominance == 1) {
				returnColor = FlowerValues.CoolTipColor;
			}
			return returnColor;
		}	
	}

	public override Color BaseRGBColor {
		get {
			return BaseHSVColor.RGBColor;
		}
	}

	public override Color TipRGBColor {
		get {
			return TipHSVColor.RGBColor;
		}
	}

    public override bool Gradient {
        get {
            return FlowerValues.Random.Dominance == -1;
        }
    }

    // public HSVColor[] HSVColors { get; private set; }
    // public Allele[] Alleles { get; private set; }

    [XmlIgnoreAttribute]
	public override GeneValues GeneValues {
		get {
			return FlowerValues;
		}
		set {
			if (value.GetType() == typeof(FlowerColorGeneValues)) {
				FlowerValues = value as FlowerColorGeneValues;
			}
		}
	}
	public FlowerColorGeneValues FlowerValues { get; private set; }

	// public System.Type SortingType {
	// 	get {
	// 		return typeof(IFlowerColorGene);
	// 	}
	// }

	/***************
	* Constructors *
	***************/

	/* empty constructor for serialization */
	private FlowerColorHybridGene() {}

	public FlowerColorHybridGene(FlowerColorGeneValues values) {
		FlowerValues = values;
	}

    public FlowerColorHybridGene(HSVColor[] colors, Allele[] alleles, Allele random) {
		FlowerValues = new FlowerColorGeneValues();

		FlowerValues.WarmBaseColor = colors[0];
		FlowerValues.CoolBaseColor = colors[1];
		FlowerValues.WarmTipColor = colors[2];
		FlowerValues.CoolTipColor = colors[3];
		FlowerValues.WarmBaseAllele = alleles[0];
		FlowerValues.CoolBaseAllele = alleles[1];
		FlowerValues.WarmTipAllele = alleles[2];
		FlowerValues.CoolTipAllele = alleles[3];
        FlowerValues.Random = random;
	}

	public FlowerColorHybridGene(FlowerColorHybridGene parent1, FlowerColorHybridGene parent2, bool mutate) {
        var values1 = parent1.FlowerValues;
        var values2 = parent2.FlowerValues;
        var flowerValues = new FlowerColorGeneValues();
        GeneValues.Mix<FlowerColorGeneValues>(values1, values2, ref flowerValues, mutate);
        FlowerValues = flowerValues;
	}

	/**********************
	* Interface Functions *
	**********************/

	public override bool MixWith(IGene otherGene, out IGene babyGene, bool mutate) {
		if (otherGene.GetType() != typeof(FlowerColorHybridGene)) {
			babyGene = null;
			return false;
		}
		FlowerColorHybridGene colorGene = otherGene as FlowerColorHybridGene;
		babyGene = new FlowerColorHybridGene(this, colorGene, mutate);
		return true;
	}

    public override bool CompareWith(IGene gene) {
    	var hDivide = 0.05f;
    	var sDivide = 0.1f;
    	var vDivide = 0.1f;

		Assert.IsTrue(gene.GetType() == typeof(FlowerColorHybridGene));

		FlowerColorHybridGene otherGene = gene as FlowerColorHybridGene;

		HSVColor tipDiff = TipHSVColor.Difference(otherGene.TipHSVColor);
		HSVColor baseDiff = BaseHSVColor.Difference(otherGene.BaseHSVColor);
		var result = (tipDiff.h < hDivide && tipDiff.s < sDivide && tipDiff.v < vDivide) &&
				(baseDiff.h < hDivide && baseDiff.s < sDivide && baseDiff.v < vDivide);

		return result;
    }
}
