using System;
using System.Collections.Generic;

public class SeedVault {
	private List<Seed> _Seeds = new List<Seed>();

	public int Count { get { return _Seeds.Count; } }

	public int Add(Seed seed) {
		_Seeds.Add(seed);
		return _Seeds.Count - 1;
	}

	public Seed Get(int identity) {
		var values = _Seeds[identity].Genes.Values;
		var genes = new IGene[values.Count];
		values.CopyTo(genes, 0);
		return new Seed(genes);
	}

	public Seed[] AllSeeds() {
		var seeds = new Seed[_Seeds.Count];
		for (var i = 0; i < seeds.Length; i++) {
			var genes = new IGene[_Seeds[i].Genes.Count];
			_Seeds[i].Genes.Values.CopyTo(genes, 0);
			seeds[i] = new Seed(genes);
		}

		return seeds;
	}

    public void Clear() {
        _Seeds.Clear();
    }
}