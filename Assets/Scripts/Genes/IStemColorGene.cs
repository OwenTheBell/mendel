public interface IStemColorGene : IGene {
	HSVColor BaseColor { get; }
	HSVColor TipColor { get; }
}