﻿using System.Collections;
using UnityEngine;
using UnityEngine.SceneManagement;

public class IntroControl : MonoBehaviour {

    public LoadingDisplay Loading;
    public DisplayTextSequenceScript IntroText;
    public ControlPlayerDescentScript PlayerDescent;
    public string SceneName;
    public AudioSource MusicSource;

    private Animator _Animator;
    private bool _WaitingOnText = true;

    private void Awake() {
        _Animator = GetComponent<Animator>();	
    }

    private void Start() {
        StartCoroutine(ControlIntro());
    }

    IEnumerator ControlIntro() {
        Loading.Show();
        while (!Loading.Visible) {
            yield return null;
        }
		var operation = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);
        var time = 0f;
        while (!operation.isDone) {
            time += Time.deltaTime;
            yield return null;
        }
        var popManger = GameObject.Find("Populator").GetComponent<PopulationManager>();
        Events.instance.Raise(new NewGameEvent());
        while (!popManger.AllDoneGrowing) {
            yield return null;
        }
        Loading.Hide();
        while (Loading.Visible) {
            yield return null;
        }
        IntroText.Display();
        while (!IntroText.Done) {
            yield return null;
        }
        _Animator.SetTrigger("begin");
    }

    public void PlayMusic() {
        MusicSource.Play();
    }

    public void StartDescent() {
        PlayerDescent.StartDescent();
    }
}