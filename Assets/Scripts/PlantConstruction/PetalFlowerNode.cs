﻿using UnityEngine;
using System;
using System.Collections;

public class PetalFlowerNode : FlowerNode, ISwayController {

	public float BloomTime;
    public bool InstantFirstBloom;
    public float ColliderPadding;

    public Vector2 RebloomDelayRange;

    private bool _IsBlooming = false;
    private bool _IsTransitioning = false;
    private bool _IsAmbient = false;
    private float _MinimumColliderRadius;

    public Material BloomingMaterial;
    public Material TransitionMaterial;
    public Material AmbientMaterial;
    private float _Intensity;
    private AmbientSway _Sway;
    private SphereCollider _Collider;

    public float Intensity {
        get {
            return _Intensity;
        }
        set {
            _Intensity = Mathf.Clamp01(value);
            if (_Sway != null) _Sway.Percent = _Intensity;
            if (_IsBlooming) {
                return;
            }
            var flowerMesh = FlowerObj.GetComponent<MeshRenderer>();
            if (_Intensity < 1f) {
                if (flowerMesh.material != TransitionMaterial) {
                    flowerMesh.material = TransitionMaterial;
                }
                flowerMesh.material.SetFloat("_Percent", _Intensity);
            }
            else if (flowerMesh.material != AmbientMaterial) {
                flowerMesh.material = AmbientMaterial;
            }
        }
    }

	private Texture2D mTexture;
    private bool _DoneGrowing;

    protected override void Awake() {
        _Sway = GetComponent<AmbientSway>();
        _Collider = GetComponentInChildren<SphereCollider>();
		_Collider.gameObject.SetActive(false);
        _MinimumColliderRadius = _Collider.radius;
        if (_Sway != null) _Sway.enabled = false;
    }

	protected override void Start() {
	}

	protected override bool SpawnFlowerImmediate(Mesh flowerMesh) {
        return SpawnFlower(flowerMesh, 0f);
	}

	protected override bool SpawnFlower(Mesh flowerMesh) {
        var delay = UnityEngine.Random.Range(RebloomDelayRange.x, RebloomDelayRange.y);
        return SpawnFlower(flowerMesh, delay);
	}

    public bool DelayedSpawnFlower(Mesh flowerMesh, float delay) {
        return SpawnFlower(flowerMesh, delay);
    }

    protected override bool SpawnFlower(Mesh flowerMesh, float delay) {
        var startTime = Time.timeSinceLevelLoad + delay;

        var origin = new Vector3(flowerMesh.uv[0].x, flowerMesh.uv[1].y, flowerMesh.uv2[0].x);
		// toggle the collider on and off to ensure that AmbientSway gets setup
		_Collider.gameObject.SetActive(true);
        foreach (var sway in GetComponentsInChildren<AmbientSway>()) {
            sway.Setup(origin, flowerMesh.uv2[0].y);
        }
		_Collider.gameObject.SetActive(false);
        var max = 0f;
        for (var i = 0; i < 3; i++) {
            if (flowerMesh.bounds.extents[i] > max) max = flowerMesh.bounds.extents[i];
        }
        max = Mathf.Clamp(max + ColliderPadding, _MinimumColliderRadius, Mathf.Infinity);
        if (!_Collider.gameObject.HasComponent<PlantInfo>()) {
            var info = _Collider.gameObject.AddComponent<PlantInfo>();
            info.Identity = GetComponent<PlantInfo>().Identity;
        }
        _Collider.radius = max;

        Available = false;
        _IsBlooming = true;
        _IsTransitioning = false;
        _IsAmbient = false;
		mFlowerMesh = flowerMesh;
        if (FlowerObj == null) {
            FlowerObj = Instantiate(flowerPrefab);
        }
		FlowerObj.transform.parent = transform;
        FlowerObj.transform.localPosition = Vector3.zero;
		FlowerObj.transform.localRotation = Quaternion.identity;
        FlowerObj.transform.localScale = Vector3.one;
		FlowerObj.GetComponent<MeshFilter>().sharedMesh = mFlowerMesh;
		FlowerObj.GetComponent<MeshRenderer>().material = BloomingMaterial;
		FlowerObj.GetComponent<MeshRenderer>().material.SetFloat("_StartTime", startTime);
        FlowerObj.GetComponent<MeshRenderer>().material.SetFloat("_BloomTime", BloomTime);
        FlowerObj.GetComponent<PlantInfo>().Identity = GetComponent<PlantInfo>().Identity;
        _DoneGrowing = false;
        if (InstantFirstBloom) {
            SwitchToAmbientMaterial();
            InstantFirstBloom = false;
            _IsBlooming = false;
            Intensity = 1;
            _IsAmbient = true;
        }
        else {
            if (_Sway != null) {
                _Sway.enabled = true;
            }
            Invoke("SwitchToAmbientMaterial", delay + BloomTime);
            var fader = GetComponent<AudioFader>();
            if (fader != null) {
                fader.Play();
                fader.Invoke("Stop", delay + BloomTime - fader.Duration);
            }
        }

        return true;
    }

    IEnumerator SpawnFlowerDelay(Mesh flowerMesh, float delay) {
        yield return new WaitForSeconds(delay);
        SpawnFlowerImmediate(flowerMesh);
        yield return false;
    }

    public override void SetMaterial(Material material) {
        AmbientMaterial = material;
    } 

    public void ActiveFlowerColliders(bool enable = true) {
        _Collider.gameObject.SetActive(enable);
    }

    public override GameObject PickFlower() {
        var flower = base.PickFlower();
        flower.transform.position = _Collider.transform.position;
        flower.transform.rotation = _Collider.transform.rotation;
        var renderer = flower.GetComponent<MeshRenderer>();
        renderer.material = TransitionMaterial;
        renderer.material.SetFloat("_Percent", 0f);
        return flower;
    }

    protected void SetMaterialByIntensity(float intensity) {
        if (intensity < 1f) {
            if (!_IsTransitioning) {
                FlowerObj.GetComponent<MeshRenderer>().material = TransitionMaterial;
                _IsTransitioning = true;
                _IsAmbient = false;
                _IsBlooming = false;
            }
            FlowerObj.GetComponent<MeshRenderer>().material.SetFloat("_Percent", intensity);
        }
        else if (!_IsAmbient) {
            FlowerObj.GetComponent<MeshRenderer>().material = AmbientMaterial;
            _IsAmbient = true;
            _IsTransitioning = false;
            _IsBlooming = false;
        }
    }

	void SwitchToAmbientMaterial() {
        Available = true;
        if (_Sway != null) {
            _Sway.enabled = false;
        }
        _IsBlooming = false;
        SetMaterialByIntensity(_Intensity);
        _Collider.transform.localPosition = Vector3.zero;
        _Collider.transform.localRotation = Quaternion.Euler(Vector3.zero);
    }
}