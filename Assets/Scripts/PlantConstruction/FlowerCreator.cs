﻿using UnityEngine;
using System.Collections.Generic;

public struct FlowerPosition {
    public Vector3 position;
    public Quaternion rotation;
    public float height;

    public FlowerPosition (Vector3 pos, Quaternion rot, float height) {
        position = pos;
        rotation = rot;
        this.height = height;
    }
}

public class FlowerCreator : MonoBehaviour, IGeneUser {

	/**********************
	* Interface Variables *
	**********************/

	/*******************
	* Member Variables *
	*******************/

    public float BloomTime;
    public bool InstantFirstBloom;
	public GameObject flowerNodePrefab;

    private List<Mesh> _MeshQueue;
	private List<FlowerPosition> _NewFlowerQueue;
	private List<Transform> mFlowerNodeTransforms;
    private List<FlowerNode> _FlowerNodes;
    private List<AmbientSway> _Swayers;
    private Mesh _Mesh;
    private bool _InAmbientMode = false;
    private bool _StartedFlowering = false;
    public int _NextEmptyNode = 0;
    public bool FullyBloomed { get; private set; }

    public int FlowerCount {
        get {
            return _FlowerNodes.Count;
        }
    }

    public int AvailableFlowerCount {
        get {
            var count = 0;
            for (var i = 0; i < _FlowerNodes.Count; i++) {
                if (_FlowerNodes[i].Available) {
                    count++;
                }
            }
            return count;
        }
    }

    public FlowerNode[] AvailableFlowerNodes {
        get {
            var nodes = new List<FlowerNode>(_FlowerNodes);
            return nodes.Filter( n => { return n.Available; } ).ToArray();
        }
    }

    public Vector3[] FlowerLocalPositions {
        get {
            var positions = new Vector3[FlowerCount];
            for (var i = 0; i < FlowerCount; i++) {
                positions[i] = _FlowerNodes[i].transform.localPosition;
            }
            return positions;
        }
    }

    public Vector3[] FlowerPositions {
        get {
            var positions = new List<Vector3>();
            for (var i = 0; i < AvailableFlowerNodes.Length; i++) {
                positions.Add(AvailableFlowerNodes[i].transform.position);
            }
            return positions.ToArray();
        }
    }

	/*******************
	* Unity Functions *
	*******************/

	void Awake() {
        _MeshQueue = new List<Mesh>();
		_NewFlowerQueue = new List<FlowerPosition>();
		mFlowerNodeTransforms = new List<Transform>();
        _FlowerNodes = new List<FlowerNode>();
        _Swayers = new List<AmbientSway>();
        FullyBloomed = false;
	}

	void Start () {
	}

	void Update () {
	}

	void LateUpdate() {
        if (!FullyBloomed &&
            _NewFlowerQueue.Count == 0 &&
            _StartedFlowering &&
            FlowerCount == AvailableFlowerCount
        ) {
            GetComponent<PlantEventSystem>().Raise(new DoneBloomingEvent());
            FullyBloomed = true;
            enabled = false; // only need to update if there are flowers to spawn
        }

        for (var i = 0; i < _NewFlowerQueue.Count; i++) {
            PlaceFlowerNode(_MeshQueue[i], _NewFlowerQueue[i]);
		}
        _MeshQueue.Clear();
		_NewFlowerQueue.Clear();
    }

	void OnEnable() {
        var eventsystem = GetComponent<PlantEventSystem>();
        eventsystem.AddListener<SwitchToAmbientEvent>(OnSwitchToAmbientEvent);
        eventsystem.AddListener<ChangeFlowerMaterialEvent>(OnChangeFlowerMaterialEvent);
        eventsystem.AddListener<PlaceFlowerEvent>(OnPlaceFlowerEvent);
	}

	void OnDisable() {
        var eventsystem = GetComponent<PlantEventSystem>();
        eventsystem.RemoveListener<PlaceFlowerEvent>(OnPlaceFlowerEvent);
        eventsystem.RemoveListener<SwitchToAmbientEvent>(OnSwitchToAmbientEvent);
        eventsystem.RemoveListener<ChangeFlowerMaterialEvent>(OnChangeFlowerMaterialEvent);
	}

	/**********************
	* Interface Functions *
	**********************/

	public bool Build() {
        var identity = GetComponent<PlantInfo>().Identity;
        var seed = Services.SeedVault.Get(identity);
        _Mesh = MeshConstruction.BuildPetalFlowerMesh(seed, transform.position);
		return true;
	}

	/*******************
	* Member Functions *
	*******************/

    public void SetMesh(Mesh mesh) {
        _Mesh = mesh;
    }

	void OnPlaceFlowerEvent(PlaceFlowerEvent e) {
		FlowerPosition flowerPos = new FlowerPosition(e.position, e.rotation, e.height);
        var identity = GetComponent<PlantInfo>().Identity;
        var newMesh = MeshConstruction.CloneMesh(_Mesh);
        MeshConstruction.UpdateFlowerMeshForHeight(newMesh, flowerPos.height);
        Services.MeshVault.AddFlowerInfo(identity, newMesh, flowerPos);
        PlaceFlowerNode(newMesh, flowerPos, e.time);
        _StartedFlowering = true;
	}

    void OnSwitchToAmbientEvent(SwitchToAmbientEvent e) {
        _InAmbientMode = true;

        foreach (var flowerNode in _FlowerNodes) {
            (flowerNode as PetalFlowerNode).Intensity = 1f;
        }
    }

    void OnChangeFlowerMaterialEvent(ChangeFlowerMaterialEvent e) {
        _FlowerNodes.Act<FlowerNode>( n => n.SetMaterial(e.material) );
    }

	public void AddFlower(Mesh mesh, FlowerPosition position) {
        _MeshQueue.Add(mesh);
		_NewFlowerQueue.Add(position);
        enabled = true;
	}

    void PlaceFlowerNode(Mesh mesh, FlowerPosition flowerPosition, float time = 0f) {
        var shaderController = GetComponent<ShaderController>();
        GameObject flowerNodeObject = null;
        if (_NextEmptyNode < _FlowerNodes.Count) {
            flowerNodeObject = _FlowerNodes[_NextEmptyNode].gameObject;
            _NextEmptyNode++;
        }
        else {
            flowerNodeObject = (GameObject)Instantiate(flowerNodePrefab);
            _NextEmptyNode++;
            _FlowerNodes.Add(flowerNodeObject.GetComponent<PetalFlowerNode>());
            var swayers = flowerNodeObject.GetComponentsInChildren<AmbientSway>();
            _Swayers.AddRange(swayers);
        }
        flowerNodeObject.name = "Flower Node " + _FlowerNodes.Count;
        flowerNodeObject.transform.parent = transform;
        flowerNodeObject.transform.localPosition = flowerPosition.position;
        flowerNodeObject.transform.localRotation = flowerPosition.rotation;
        flowerNodeObject.transform.localScale = Vector3.one;
        var petalFlowerNode = flowerNodeObject.GetComponent<PetalFlowerNode>();
        petalFlowerNode.BloomTime = BloomTime;
        if (InstantFirstBloom) {
            petalFlowerNode.InstantFirstBloom = InstantFirstBloom;
            shaderController.TransitionTime = 0f;
        }
        else {
        }
        flowerNodeObject.GetComponent<PlantInfo>().Identity = GetComponent<PlantInfo>().Identity;

        var identity = GetComponent<PlantInfo>().Identity;
        petalFlowerNode.DelayedSpawnFlower(mesh, Mathf.Clamp(time - Time.timeSinceLevelLoad, 0f, Mathf.Infinity));
    }

    public GameObject PickFlowerFromNode(FlowerNode node) {
        var flowerObject = node.PickFlower();
        node.RegrowFlower();
        FullyBloomed = false;
        return flowerObject;
    }

    public void RelocateFlowerMeshes(Vector3 origin) {
        _FlowerNodes.Act(n => {
            var filter = n.GetComponentInChildren<MeshFilter>();
            MeshConstruction.ChangeMeshOrigin(filter.sharedMesh, origin);
        });
        _Swayers.Act(s => s.Pivot = origin);
        // due to references, the MeshVault will update automatically
    }

    public void ClearNodes() {
        foreach (var node in _FlowerNodes) {
            node.GetComponentInChildren<MeshFilter>().sharedMesh = null;
        }
        _NextEmptyNode = 0;
        _NewFlowerQueue.Clear();
    }
}