﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Text;

public class LSystem {

	private int mGrowthLevel;
	public Dictionary<string, string> ProceduresDict { get; private set; }

	private string mAxiom;
	public string Axiom {
		get {
			return mAxiom;
		}
	}

	private List<string> mExpandedSystems;
	public string[] ExpandedSystems {
		get {
			return mExpandedSystems.ToArray();
		}
	}

	public string[] Procedures {
		get {
			Dictionary<string, string>.KeyCollection keys = ProceduresDict.Keys;
			string[] procedures = new string[keys.Count];
			int index = 0;
			foreach(string key in ProceduresDict.Keys) {
				string procedure;
				ProceduresDict.TryGetValue(key, out procedure);
				procedures[index++] = key + ": " + procedure;
			}
			return procedures;
		}
	}

	/***************
	* Constructors *
	***************/

	public LSystem(string axiom, Vector2 procedure_range, Vector2 F_range) {
		ProceduresDict = RandomizedLSystem(procedure_range, F_range);

		SetDefaultValues(axiom, 5);
	}

	/*
	* Create a new L System using a single set of procedures
	*/
	public LSystem(string axiom, string[] procedures) {
		// ProceduresDict = new Dictionary<string, string>();
		ProceduresDict = CreateProcedureDict(procedures);

		SetDefaultValues(axiom, 5);
	}

	public LSystem(string axiom, Dictionary<string, string> procedures) {
		ProceduresDict = procedures;

		SetDefaultValues(axiom, 5);
	}

	/*
	* Create a new L System by combining two sets of procedures
	* The first argument is the dominant set of procedures and the second
	* the recessive set
	*/
	public LSystem(string axiom, string[] dominantProcedures, string[] recessiveProcedures) {
		ProceduresDict = CombineProcedures(dominantProcedures, recessiveProcedures);

		SetDefaultValues(axiom, 5);
		// foreach(string system in mExpandedSystems) {
		// 	Debug.Log(system);
		// }
	}

	private void SetDefaultValues(string axiom, int growth) {
		mGrowthLevel = growth;
		mAxiom = axiom;

		mExpandedSystems = new List<string>(mGrowthLevel);
		ExpandProcedures();
	}

	/*******************
	* Member Functions *
	*******************/

	/*
	* Split string array of procedures into the word and procedure.
	* The procedures are saved as List<char> since that is easier to parse.
	*/
	public Dictionary<string, string> CreateProcedureDict(string[] procedures) {
		Dictionary<string, string> proceduresDict = new Dictionary<string, string>();
		foreach (string tempProcedure in procedures) {
			string[] halves = tempProcedure.Split(new char[]{':'});
			proceduresDict.Add(halves[0].Trim(), halves[1].Trim());
		}
		return proceduresDict;
	}

	private void ExpandProcedures() {
		if (mAxiom.Equals(string.Empty)) {
			Debug.Log("this L system lacks an axiom");
			return;
		}
		StringBuilder stringOut = new StringBuilder(mAxiom);
		string procedure = "";
		for (int iGrowth = 0; iGrowth < mGrowthLevel; iGrowth++) {
			for (int iWord = stringOut.Length - 1; iWord >= 0; iWord--) {
				string word = stringOut[iWord] + "";
				if (ProceduresDict.TryGetValue(word, out procedure)) {
					stringOut.Remove(iWord, 1);
					stringOut.Insert(iWord, procedure);
				}
			}
			mExpandedSystems.Add(stringOut.ToString());
		}
	}

	private Dictionary<string, string> CombineProcedures(string[] mainProcedures, string[] altProcedures) {
		Dictionary<string, string> newProceduresDict = new Dictionary<string, string>();

		Dictionary<string, string> mainDict = CreateProcedureDict(mainProcedures);
		Dictionary<string, string> altDict = CreateProcedureDict(altProcedures);

		Dictionary<string, string>.KeyCollection keys = mainDict.Keys;

		string mainProcedure;
		string altProcedure;
		List<Vector2> mainRanges;
		List<Vector2> altRanges;
		StringBuilder newProcedure;
		foreach (string key in keys) {
			if (mainDict.TryGetValue(key, out mainProcedure) && altDict.TryGetValue(key, out altProcedure)) {
				newProcedure = new StringBuilder(mainProcedure);
				mainRanges = FindSubProcedureIndexes(mainProcedure);
				altRanges = FindSubProcedureIndexes(altProcedure);

				Vector2 removeRange = mainRanges[Random.Range(0, mainRanges.Count)];
				Vector2 addRange = altRanges[Random.Range(0, altRanges.Count)];

				string procedureToAdd = altProcedure.Substring((int)addRange.x, (int)addRange.y);

				newProcedure.Remove((int)removeRange.x, (int)removeRange.y);
				newProcedure.Insert((int)removeRange.x, procedureToAdd);

				newProceduresDict.Add(key, newProcedure.ToString());
			} else if (mainDict.TryGetValue(key, out mainProcedure)) {
				newProceduresDict.Add(key, mainProcedure);
			}
		}

		return newProceduresDict;
	}

	List<Vector2> FindSubProcedureIndexes(string procedure) {
		List<Vector2> substrings = new List<Vector2>();

		int counting = 0;
		int subIndex = 0, index = 0;

		foreach (char character in procedure) {
			if (character != '[' && character != ']') {
				if (counting <= 0) {
					subIndex = index;
				}
				counting++;
			} else if (counting > 0) {
				substrings.Add(new Vector2(subIndex, counting));
				counting = 0;
			}
			if (index == procedure.Length - 1) {
				substrings.Add(new Vector2(subIndex, counting));
			}
			index++;
		}
		return substrings;
	}

	private Dictionary<string, string> RandomizedLSystem(Vector2 procedure_range, Vector2 F_range) {

		int length = Random.Range((int)procedure_range.x, (int)procedure_range.y + 1);
		/*
		* first populate the procedure with Fs. This ensures that there are
		* at least a minimum amount in the procedure. The plants just don't look
		* good if there aren't enough Fs.
		*/
		StringBuilder new_procedure = new StringBuilder();
		int F_count = Random.Range((int)F_range.x, (int)F_range.y + 1);
		for (int i = 0; i < F_count; i++) {
			new_procedure.Append('F');
		}

		/*
		* Populate with [ ] pairs. This is done second to ensure that all pairs
		* include at least one F inside them.
		* The total number of brackets needs to be limited by the number of Fs in
		* the system or it is possible for an impossible number of brackets to be
		* fit into the system.
		*/
		int max_brackets = 4;
		if (new_procedure.Length <= 11) {
			max_brackets = Mathf.RoundToInt(new_procedure.Length / 3f);
		}
		// max_brackets = 2;
		// int[] bracket_positions = new int[Random.Range(1, 4)];
		int bracket_count = Random.Range(2, max_brackets);
		for (int i = 0; i < Random.Range(2, max_brackets); i++) {
			int index = Random.Range(0, new_procedure.Length - 2);
			int checkBehind = (index > 0) ? index - 1 : 0;
			int checkAhead = index + 1;
			while (new_procedure[checkBehind] == '[' || new_procedure[checkAhead] == '[') {
				index++;
				checkAhead++;
				checkBehind++;
				if (index == new_procedure.Length - 1) {
					index = 0;
					checkBehind = 0;
					checkAhead = 1;
				}
			}
			new_procedure.Insert(index, '[');
		}
		for (int i = 0; i < new_procedure.Length - 1; i++) {
			if (new_procedure[i] == '[') {
				int close_index = Random.Range(i + 2, new_procedure.Length);
				while(new_procedure[close_index - 1] == '[') {
					close_index++;
				}
				new_procedure.Insert(close_index, ']');
			}
		}

		/*
		* insert operations between the characters.
		* Do not put any at the very end as they will have no effect on the
		* interpretation.
		* Also ensure that all operations are not directly to the left of brackets
		* as then they will have no effect on the system.
		*/
		int operations_count = length - F_count - bracket_count;
		char[] operations = new char[]{'+', '-', '/', '\\', '&', '^'};
		for (int i = 0; i < operations_count; i++) {
			int operation_index = Random.Range(0, new_procedure.Length);
			while (new_procedure[operation_index] == ']' || new_procedure[operation_index] == '[') {
				operation_index++;
				if (operation_index >= new_procedure.Length - 1) {
					operation_index = 0;
				}
			}
			char operation = operations[Random.Range(0, operations.Length)];
			new_procedure.Insert(operation_index, operation);
		}

		Dictionary<string, string> newDict = new Dictionary<string, string>();
		newDict.Add("F", new_procedure.ToString());

		return newDict;
	}

}
