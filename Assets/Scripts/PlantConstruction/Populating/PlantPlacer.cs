﻿using UnityEngine;
using System.Collections.Generic;

public class PlantPlacer : MonoBehaviour {

	public GameObject SpawnPrefab;
	public LayerMask Layer;
	public int Seed;
	public float ChunkGrowthTime;

	struct PlantsToPlace {
		public readonly Vector3[] Positions;
		public readonly int SeedCreatorIndex;

		public PlantsToPlace(Vector3[] positions, int index) {
			Positions = positions;
			SeedCreatorIndex = index;
		}
	}

	public bool HasPlantsToPlace {
		get {
			return _ToPlace.Count > 0;
		}
	}

	private List<PlantsToPlace> _ToPlace;
	private PopulatorSeedCreator[] _Creators;
	private int _CreatorIndex;
	private DefinedZone _Zone;

	private float[,] _Noise;

	void Awake() {
		_ToPlace = new List<PlantsToPlace>();
        _Creators = GetComponents<PopulatorSeedCreator>();
		_Zone = GetComponent<DefinedZone>();
        _CreatorIndex = 0;
	}

	public void Populate() {
		_Noise = Noise.GenerateNoiseMap(512, 512, 20f, Seed);
		_Zone.RequestPoints(_Creators[_CreatorIndex].Density, _Noise, ReceivedSeedPositions);
	}

	public List<GeneManager> PlacePlants(int count = 30) {
        var managers = new List<GeneManager>();
		if (_ToPlace.Count > 0) {
            for (var i = 0; i < count && i < _ToPlace.Count; i++) {
                var toPlace = _ToPlace[i];
                var creator = _Creators[toPlace.SeedCreatorIndex];
                foreach (var pos in toPlace.Positions) {
                    var position = transform.TransformPoint(pos);

                    var rayOrigin = position;
                    rayOrigin.y += 50;

                    RaycastHit hit;
                    if (Physics.Raycast(rayOrigin, Vector3.down, out hit)) {
                        if (((1 << hit.transform.gameObject.layer) & Layer) != 0) {
                            position = hit.point;

                            var gameObject = (GameObject)Instantiate(SpawnPrefab);
                            gameObject.name = name;
                            gameObject.transform.parent = transform;
                            gameObject.transform.position = position;
                            var rotation = Quaternion.Euler(new Vector3(0f, Random.Range(0f, 360f), 0f));
                            gameObject.transform.rotation = rotation;
                            gameObject.GetComponent<StemBuilder>().ChunkGrowthTime = ChunkGrowthTime;
                            gameObject.GetComponent<FlowerCreator>().InstantFirstBloom = true;

                            var procSeed = PlantFunctions.GenerateProceduralSeed(position, rotation);
                            var seed = creator.SeedForProceduralSeed(procSeed);
                            var identity = Services.SeedVault.Add(seed);
                            var parents = Tuple.New<int, int>(-1, -1);
                            gameObject.GetComponent<PlantInfo>().Create(
                                identity,
                                procSeed,
                                parents,
                                position,
                                rotation.eulerAngles
                            );

                            var manager = gameObject.GetComponent<GeneManager>();
                            manager.Grow();
                            managers.Add(manager);
                        }
                    }
                }
            }
            _ToPlace.RemoveRange(0, (count < _ToPlace.Count) ? count : _ToPlace.Count );
		}
        return managers;
	}

	public void ReceivedSeedPositions(Vector3[] positions) {
		_ToPlace.Add(new PlantsToPlace(positions, _CreatorIndex));

		_CreatorIndex++;
        // if muliple SeedCreators were attached to this object, request points again
		if (_CreatorIndex < _Creators.Length) {
			_Zone.RequestPoints(_Creators[_CreatorIndex].Density, _Noise, ReceivedSeedPositions);
		}
	}
}
