﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopulationManager : MonoBehaviour {

	public GameObject PlantPrefab;
    public int LoadPerFrame;

    [HideInInspector]
    public PlantXmlStruct[] PlantStructs;

    public bool AllDoneGrowing { get; private set; }

    private List<PlantPlacer> _LeftToPlace;
    private List<GeneManager> _GrowingManagers;
	private bool _LoadingFromXML;

    private void Awake() {
        _LeftToPlace = new List<PlantPlacer>();
        _GrowingManagers = new List<GeneManager>();
        enabled = false;
        Events.instance.AddListener<NewGameEvent>(OnNewGameEvent);
    }

	public void Populate() {
        _LeftToPlace = new List<PlantPlacer>(GetComponentsInChildren<PlantPlacer>());
        _LeftToPlace.Act(p => p.Populate());
        enabled = true;
	}

	public void Populate(PlantInfoStruct[] plants) {
        List<GameObject> spawnedPlants;
        Populate(plants, out spawnedPlants);
	}

	public void Populate(PlantInfoStruct[] plants, out List<GameObject> spawnedPlants) {
        spawnedPlants = new List<GameObject>();
		enabled = true;
        StartCoroutine(LoadFromXML(plants, spawnedPlants));
	}

    IEnumerator LoadFromXML(PlantInfoStruct[] plants, List<GameObject> spawnedPlants) {
        var count = 0;
		_LoadingFromXML = true;
		foreach(PlantInfoStruct plantStruct in plants) {
			var position = plantStruct.Position;
			var rotation = Quaternion.Euler(plantStruct.Rotation);
			var plant = Instantiate(PlantPrefab, position, rotation);
			plant.GetComponent<StemBuilder>().ChunkGrowthTime = 0f;
			plant.GetComponent<FlowerCreator>().InstantFirstBloom = true;
            plant.GetComponent<PlantInfo>().Identity = plantStruct.Identity;
            plant.transform.parent = transform;
            spawnedPlants.Add(plant);
            var manager = plant.GetComponent<GeneManager>();
            manager.DestroyWhenDone = plantStruct.Destroyed;
			manager.Grow();
            _GrowingManagers.Add(manager);
            // wait a frame once the per-frame allocation has been reached
            count++;
            if (count >= LoadPerFrame) {
                count = 0;
                yield return null;
            }
		}
        StartCoroutine(WaitForGrowth(_GrowingManagers));
    }
	
	void Update () {
        if (!AllDoneGrowing && !_LoadingFromXML) {
            if (_LeftToPlace.Count >= 1 && _LeftToPlace[0].HasPlantsToPlace) {
                _GrowingManagers.AddRange(_LeftToPlace[0].PlacePlants(LoadPerFrame));
                if (!_LeftToPlace[0].HasPlantsToPlace) {
                    _LeftToPlace.RemoveAt(0);
                }
            }
            for (var i = _GrowingManagers.Count - 1; i >= 0; i--) {
                if (_GrowingManagers[i].GrowingOver) {
                    _GrowingManagers.RemoveAt(i);
                }
            }
            if (_GrowingManagers.Count == 0 && _LeftToPlace.Count == 0) {
                AllDoneGrowing = true;
            }
        }
	}

    private void OnDestroy() {
        Events.instance.RemoveListener<NewGameEvent>(OnNewGameEvent);
    }

    void OnNewGameEvent(NewGameEvent e) {
        Populate();
        Events.instance.RemoveListener<NewGameEvent>(OnNewGameEvent);
    }

    IEnumerator WaitForGrowth(List<GeneManager> managers) {
        var parsingList = new List<GeneManager>(managers);
        while (parsingList.Count > 0) {
            for (var i = parsingList.Count - 1; i >= 0; i--) {
                if (parsingList[i].GrowingOver) {
                    parsingList.RemoveAt(i);
                }
            }
            yield return null;
        }
        AllDoneGrowing = true;
    }
}