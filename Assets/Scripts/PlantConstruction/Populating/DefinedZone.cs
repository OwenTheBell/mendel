﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class DefinedZone : MonoBehaviour {

	[HideInInspector]
	public Vector3[] Points;

	public Color Color;

	private Rect _Rect;

	struct PointThreadInfo<T> {
		public readonly Action<T> callback;
		public readonly T parameters;

		public PointThreadInfo (Action<T> callback, T parameters) {
			this.callback = callback;
			this.parameters = parameters;
		}
	}

	private float _ThreadTime = 0;

	Queue<PointThreadInfo<Vector3[]>> _PointThreadQueue;

	void Awake() {
		_PointThreadQueue = new Queue<PointThreadInfo<Vector3[]>>();

		/* generate bounding box */
		var min = Vector2.zero;
		var max = Vector2.zero;

		foreach (var point in Points) {
			if (point.x < min.x) {
				min.x = point.x;
			}
			if (point.z < min.y) {
				min.y = point.z;
			}
			if (point.x > max.x) {
				max.x = point.x;
			}
			if (point.z > max.y) {
				max.y = point.z;
			}
		}

		var width = max.x - min.x;
		var height = max.y - min.y;
		_Rect = new Rect(min.x, min.y, width, height);
	}

	public void RequestPoints(float density, Action<Vector3[]> callback) {
		var noise = new float[,]{{0.5f}};
		RequestPointsThread(density, noise, callback);
	}

	public void RequestPoints(float density, float[,] noise, Action<Vector3[]> callback) {
		ThreadStart threadStart = delegate {
			RequestPointsThread(density, noise, callback);
		};
		new Thread(threadStart).Start();
	}

	void RequestPointsThread(float density, float[,] noise, Action<Vector3[]> callback) {
		var count = (int)(density * _Rect.width * _Rect.height);
		var positionCandidates = PointDistributer.Mitchells(_Rect, count, 2, noise);
		var info = new PointThreadInfo<Vector3[]>(callback, positionCandidates);

		var polygon = new Vector2[0];
		lock (Points) {
			polygon = GeometryHelper.ConvertVector3XZToVector2(Points);
		}

		var placedPoints = new List<Vector3>();
		foreach (var candidate in info.parameters) {
			var candidate2D = new Vector2(candidate.x, candidate.z);
			candidate2D += _Rect.position;
			if (GeometryHelper.DoesPolygonContainPoints(polygon, candidate2D)) {
				var newCandidate = candidate;
				newCandidate.x += _Rect.position.x;
				newCandidate.z += _Rect.position.y;
				placedPoints.Add(newCandidate);
			}
		}

		info = new PointThreadInfo<Vector3[]>(callback, placedPoints.ToArray());

		lock (_PointThreadQueue) {
			_PointThreadQueue.Enqueue(info);
		}
	}
	
	// Update is called once per frame
	void Update () {
		lock (_PointThreadQueue) {
			while (_PointThreadQueue.Count > 0) {
				var threadInfo = _PointThreadQueue.Dequeue();
				threadInfo.callback(threadInfo.parameters);
			}
		}
	}

	void Reset() {
		Points = new Vector3[4]{
									new Vector3(-1, 0, -1),
									new Vector3(1, 0, -1),
									new Vector3(1, 0, 1),
									new Vector3(-1, 0, 1)
								};
	}
}
