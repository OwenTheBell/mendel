﻿using UnityEngine;
using System.Collections.Generic;

public class GeneManager : MonoBehaviour {

	/*******************
	* Member Variables *
	*******************/

	public bool cleanupCreators = false;

    public bool GrowingOver { get { return _DoneBlooming && _DoneGrowing; } }
    private bool _DoneBlooming;
    private bool _DoneGrowing;

    // Some plants will be generated at game load that have previously been destroyed.
    // This is necessary to ensure that their mesh info ends up in the MeshVault.
    // This bool lets the GeneManager know to destroy the plant once generation is complete
    public bool DestroyWhenDone { get; set; }

	/*******************
	* Unity Functions *
	*******************/

	void Awake() {
		// if there are IGeneCreators attached to this object then build from them
		if (GetComponents<IGeneCreator>().Length > 0) {
			var genes = new List<IGene>();
			foreach (IGeneCreator geneCreator in GetComponents<IGeneCreator>()) {
				genes.Add(geneCreator.Gene);
			}
			/*
			* this is done after all the creators have been used to make the interface
			* in the inspector a bit cleaner during runtime
			*/
			if (cleanupCreators) {
				foreach(IGeneCreator geneCreator in GetComponents<IGeneCreator>()) {
					Destroy((geneCreator as MonoBehaviour));
				}
			}
			var seed = new Seed(genes.ToArray());
			var identity = Services.SeedVault.Add(seed);
			var proceduralSeed = PlantFunctions.GenerateProceduralSeed(
				transform.position,
				transform.rotation
			);
			var parents = Tuple.New<int, int>(-1, -1);
            var position = transform.position;
            var rotation = transform.eulerAngles;
            var info = GetComponent<PlantInfo>();
            info.Create(identity, proceduralSeed, parents, position, rotation, DestroyWhenDone);
		}
        var eventSystem = GetComponent<PlantEventSystem>();
        eventSystem.AddListener<DoneGrowingEvent>(OnDoneGrowing);
        eventSystem.AddListener<DoneBloomingEvent>(OnDoneBlooming);
	}

	/*******************
	* Member Functions *
	*******************/

	public void Grow() {
        var info = GetComponent<PlantInfo>();
        var name = Services.SpeciesCatalog.GetNameForIdentity(info.Identity);
        gameObject.name = name + " " + info.Identity;
        Events.instance.Raise(new NewPlantEvent(gameObject));
		(new List<IGeneUser>(GetComponents<IGeneUser>())).Act( g => g.Build() );
	}

    private void OnDoneGrowing(DoneGrowingEvent e) {
        _DoneGrowing = true;
        GetComponent<PlantEventSystem>().RemoveListener<DoneGrowingEvent>(OnDoneGrowing);
    }

    private void OnDoneBlooming(DoneBloomingEvent e) {
        _DoneBlooming = true;
        GetComponent<PlantEventSystem>().RemoveListener<DoneBloomingEvent>(OnDoneBlooming);

        if (DestroyWhenDone)
        {
            Destroy(gameObject);
        }
    }
}