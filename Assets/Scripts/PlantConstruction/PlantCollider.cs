﻿using System.Collections;
using UnityEngine;

[RequireComponent(typeof(FlowerCreator))]
public class PlantCollider : MonoBehaviour {

    public float HeightPadding;
    public float Scalar;
    private CapsuleCollider _Collider;
    private float _MinimumRadius;
    private float _MinimumHeight;


    private float _TimeSinceUpdate;
    private bool _Constructed;

    private void Awake() {
        _Collider = gameObject.GetComponent<CapsuleCollider>();
        _MinimumHeight = _Collider.height;
        _MinimumRadius = _Collider.radius;
    }

    public void ConstructCollider(float time) {
        StartCoroutine(GrowToSize(time));
    }

    IEnumerator GrowToSize(float duration) {
        var time = 0f;
        var center = _Collider.center;
        var height = _Collider.height;
        var radius = _Collider.radius;
        var targetCenter = center;
        var targetHeight = height;
        var targetRadius = radius;
        var creator = GetComponent<FlowerCreator>();
        var lastFlowerCount = creator.FlowerLocalPositions.Length;
        CalculateTargets(creator, out targetCenter, out targetHeight, out targetRadius);
        while (time < duration) {
            time += Time.deltaTime;

            if (lastFlowerCount != creator.FlowerLocalPositions.Length) {
                CalculateTargets(creator, out targetCenter, out targetHeight, out targetRadius);
            }

            var p = time / duration;
            _Collider.center = Vector3.Lerp(center, targetCenter, p);
            _Collider.height = Mathf.Lerp(height, targetHeight, p);
            _Collider.radius = Mathf.Lerp(radius, targetRadius, p);

            yield return null;
        }
        _Collider.center = targetCenter;
        _Collider.height = targetHeight;
        _Collider.radius = targetRadius;
    }

    void CalculateTargets(FlowerCreator creator, out Vector3 center, out float height, out float radius) {
        height = 0f;
        var minPosition = Vector3.zero;
        var maxPosition = Vector3.zero;

        foreach (var position in creator.FlowerLocalPositions) {
            if (position.x < minPosition.x) {
                minPosition.x = position.x;
            }
            else if (position.x > maxPosition.x) {
                maxPosition.x = position.x;
            }
            if (position.z < minPosition.z) {
                minPosition.z = position.z;
            }
            else if (position.z > maxPosition.z) {
                maxPosition.z = position.z;
            }

            var testHeight = Mathf.Abs(position.y);
            if (testHeight > height) {
                height = testHeight;
            }
        }

        radius = Vector2.Distance(minPosition, maxPosition) / 2;
        height = Mathf.Clamp((height + HeightPadding) * Scalar, _MinimumHeight, Mathf.Infinity);
        radius = Mathf.Clamp(radius * Scalar, _MinimumRadius, Mathf.Infinity);

        center = new Vector3(
                                (minPosition.x + maxPosition.x) / 2,
                                height / 2,
                                (minPosition.z + maxPosition.z) / 2
                            );
    }
}
