﻿using UnityEngine;
using System.Collections.Generic;

public class PlantRenderZones : MonoBehaviour {

    [Range(0, 1)]
    public float CullPercent;

    public Vector3 Min;
    public Vector3 Max;
    public int SubdivisionsPerSide;

    private List<CullGroup> _CullGroups = new List<CullGroup>();

    private void Awake() {
        _CullGroups = new List<CullGroup>(SubdivisionsPerSide * SubdivisionsPerSide);

        var xDimension = (Max.x - Min.x) / SubdivisionsPerSide;
        var zDimension = (Max.z - Min.z) / SubdivisionsPerSide;
        for (var x = Min.x; x < Max.x; x += xDimension) {
            for (var z = Min.z; z < Max.z; z += zDimension) {
                var min = new Vector3(x, 0f, z);
                var max = new Vector3(x + xDimension, z + zDimension);
                _CullGroups.Add(new CullGroup(min, max, CullPercent));
            }
        }
    }

    private void OnEnable() {
        Events.instance.AddListener<NewPlantEvent>(OnNewPlantEvent); 
        Events.instance.AddListener<PlantUprootedEvent>(OnPlantUprootedEvent); 
        Events.instance.AddListener<PlantReplantedEvent>(OnPlantReplantedEvent); 
    }

    private void OnDisable() {
        Events.instance.RemoveListener<NewPlantEvent>(OnNewPlantEvent); 
        Events.instance.RemoveListener<PlantUprootedEvent>(OnPlantUprootedEvent); 
        Events.instance.RemoveListener<PlantReplantedEvent>(OnPlantReplantedEvent); 
    }

    void OnNewPlantEvent(NewPlantEvent e) {
        var position = e.gameObject.transform.position;
        foreach (var c in _CullGroups) {
            if (c.min.x <= position.x && c.max.x > position.x &&
                c.min.z <= position.z && c.max.z > position.z
            ) {
                c.AddController(e.gameObject.GetComponent<DistanceRenderController>());
                break;
            }
        }
    }

    void OnPlantUprootedEvent(PlantUprootedEvent e) {

    }

    void OnPlantReplantedEvent(PlantReplantedEvent e) {

    }

#if UNITY_EDITOR
    private void OnValidate() {
        if (_CullGroups.Count == 0) return;
        foreach (var c in _CullGroups) {
            c.SetCullPercent(CullPercent);
        }
    }
#endif
}

class CullGroup {

    public List<DistanceRenderController> Controllers;
    public Vector3 min;
    public Vector3 max;
    public float CullPercent { get; private set; }

    private List<bool> _CullBag;

    public CullGroup(Vector3 min, Vector3 max, float percent) {
        this.min = min;
        this.max = max;
        this.CullPercent = percent;
        _CullBag = new List<bool>();
        Controllers = new List<DistanceRenderController>();

        MakeBag();
    }

    private void MakeBag() {
        for (var i = 0f; i < 1f; i += 0.1f) {
            if (i < CullPercent)
                _CullBag.Add(true);
            else
                _CullBag.Add(false);
        }
    }

    public void AddController(DistanceRenderController controller) {
        SetControllerFromBag(controller);
        Controllers.Add(controller);
    }

    public void RemoveController(DistanceRenderController controller) {
        Controllers.Remove(controller);
    }

    public void SetCullPercent(float percent) {
        CullPercent = percent;
        MakeBag();
        foreach (var controller in Controllers) {
            SetControllerFromBag(controller);
        }
    }

    void SetControllerFromBag(DistanceRenderController controller) {
        var index = Random.Range(0, _CullBag.Count);
        if (_CullBag[index]) {
            controller.enabled = false;
        }
        _CullBag.RemoveAt(index);
        if (_CullBag.Count == 0) {
            MakeBag();
        }
    }
}