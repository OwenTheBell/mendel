﻿using UnityEngine;
using System.Collections.Generic;

public class DistanceRenderController : MonoBehaviour {

    public float CullDistance;

    private FlowerCreator _FlowerCreator;
    private List<MeshRenderer> _Renderers;
    private bool _Hidden = false;

    private void Start() {
        _FlowerCreator = GetComponent<FlowerCreator>();
    }

    void Update() {
        var playerPosition = Camera.main.transform.position;
        if (!_Hidden && Vector3.Distance(playerPosition, transform.position) > CullDistance) {
            if (_FlowerCreator.FullyBloomed) {
                _Renderers.Act(r => r.enabled = false);
            }
            _Hidden = true;
        }
        else if (_Hidden && Vector3.Distance(playerPosition, transform.position) < CullDistance) {
            _Renderers.Act(r => r.enabled = true);
            _Hidden = false;
        }
    }

    private void OnEnable() {
        _Renderers = new List<MeshRenderer>(GetComponentsInChildren<MeshRenderer>());
        GetComponent<PlantEventSystem>().AddListener<DoneBloomingEvent>(OnDoneBloomingEvent);
    }

    private void OnDisable() {
        GetComponent<PlantEventSystem>().RemoveListener<DoneBloomingEvent>(OnDoneBloomingEvent);
    }

    void OnDoneBloomingEvent(DoneBloomingEvent e) {
        _Renderers = new List<MeshRenderer>(GetComponentsInChildren<MeshRenderer>());
    }
}