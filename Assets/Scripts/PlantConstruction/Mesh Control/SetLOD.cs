﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SetLOD : MonoBehaviour {

	// Use this for initialization
	void Start () {
        var LOD = new LOD(0.2f, new Renderer[] { GetComponent<Renderer>() });
        GetComponent<LODGroup>().SetLODs(new LOD[] { LOD });
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}