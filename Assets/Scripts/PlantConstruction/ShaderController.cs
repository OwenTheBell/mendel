﻿using System.Collections;
using UnityEngine;

public class ShaderController : MonoBehaviour {

    public float TransitionTime;

    private float _Intensity;

    void OnEnable() {
        GetComponent<PlantEventSystem>().AddListener<DoneGrowingEvent>(OnDoneGrowingEvent);
        GetComponent<PlantEventSystem>().AddListener<DoneBloomingEvent>(OnDoneBloomingEvent);
    }

    void OnDisable() {
        GetComponent<PlantEventSystem>().RemoveListener<DoneGrowingEvent>(OnDoneGrowingEvent);
        GetComponent<PlantEventSystem>().RemoveListener<DoneBloomingEvent>(OnDoneBloomingEvent);
    }

    void OnDoneBloomingEvent(DoneBloomingEvent e) {
        var eventSystem = GetComponent<PlantEventSystem>();
        eventSystem.RemoveListener<DoneBloomingEvent>(OnDoneBloomingEvent);
    }

    void OnDoneGrowingEvent(DoneGrowingEvent e) {
        GetComponent<PlantEventSystem>().RemoveListener<DoneGrowingEvent>(OnDoneGrowingEvent);
        var eventSystem = GetComponent<PlantEventSystem>();
        eventSystem.RemoveListener<DoneBloomingEvent>(OnDoneBloomingEvent);
        StartCoroutine(TransitionMaterials());
    }

    IEnumerator TransitionMaterials() {
        var time = 0f;

        while (time < TransitionTime) {
            time += Time.deltaTime;
            var percent = Mathf.Clamp01(time / TransitionTime);
            SetIntensity(percent);
            yield return 0;
        }

        SetIntensity(1f);

        yield return null;
    }

    public void SetIntensity(float intensity) {
        _Intensity = Mathf.Clamp01(intensity);
        foreach (var controller in GetComponentsInChildren<ISwayController>()) {
            controller.Intensity = _Intensity;
        }
    }
}
