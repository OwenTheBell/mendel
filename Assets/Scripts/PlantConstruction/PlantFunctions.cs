using System;
using UnityEngine;

public class PlantFunctions {

    private static int _Generations = 0;

	public static int GenerateProceduralSeed(Vector3 position, Quaternion rotation) {
		const int AMPLIFY = 1000;
		Func<Vector3, int> value = (Vector3 vector) => {
			var returnValue = 0;
			for (var i = 0; i < 3; i++) { returnValue += (int)vector[i] * AMPLIFY; }
			return returnValue;
		};
		return value(position) + value(rotation.eulerAngles);
	}

    public static GameObject GeneratePlant(GameObject prefab, int parent1, int parent2, Vector3 position, bool mutate) {
		var random = UnityEngine.Random.Range(0f, 360f);
		var rotation = Quaternion.AngleAxis(random, Vector3.up);
		var parentIdentities = Tuple.New<int, int>(parent1, parent2);
		var genes1 = Services.SeedVault.Get(parent1);
		var genes2 = Services.SeedVault.Get(parent2);
		var seed = new Seed(genes1, genes2, mutate);
		var identity = Services.SeedVault.Add(seed);
		var procSeed = PlantFunctions.GenerateProceduralSeed(position, rotation);
		var plant = (GameObject)UnityEngine.Object.Instantiate(prefab, position, rotation);
		plant.GetComponent<PlantInfo>().Create(identity, procSeed, parentIdentities, position, rotation.eulerAngles);
        return plant;
    }
}