﻿using UnityEngine;
using System.Collections.Generic;
using System.Xml.Serialization;

public class PlantInfo : MonoBehaviour {

    public bool ExcludeFromCatalog;

    private int _Identity;
	public int Identity {
        get { return _Identity; }
        set {
            _Identity = value;
            Info = Services.PlantInfoCatalog.Infos[_Identity];
        }
    }
    public PlantInfoStruct Info;

	public void Create(int identity, int seed, Tuple<int, int> parents, Vector3 position, Vector3 rotation, bool destroyed = false) {
        _Identity = identity;
        Info = new PlantInfoStruct {
            Identity = identity,
            Seed = seed,
            Parents = parents,
            Children = new List<int>(),
            Position = position,
            Rotation = rotation,
            Destroyed = destroyed
        };

        if (!ExcludeFromCatalog) {
            if (Services.PlantInfoCatalog.Infos.ContainsKey(parents.First)) {
                Services.PlantInfoCatalog.Infos[parents.First].AddChild(Identity);
            }
            if (Services.PlantInfoCatalog.Infos.ContainsKey(parents.Second)) {
                Services.PlantInfoCatalog.Infos[parents.Second].AddChild(Identity);
            }
            Services.PlantInfoCatalog.Add(identity, Info);
        }

        Identity = identity;
	}
}

[XmlRoot("PlantInfo")]
public struct PlantInfoStruct {
    public int Identity;
    public int Seed;
    public Tuple<int, int> Parents;
    public List<int> Children;
    public Vector3 Position;
    public Vector3 Rotation;
    public bool Destroyed;

    public bool AddChild(int id) {
        if (Children.Contains(id)) return false;
        Children.Add(id);
        return true;
    }
}