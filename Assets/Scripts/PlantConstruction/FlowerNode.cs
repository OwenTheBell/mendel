﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

/*
* Handles spawning a single flower and regrowing it if it gets plucked
*/

public struct FlowerLocation {
	public Vector3 position;
	public Quaternion rotation;

	public FlowerLocation(Vector3 pos, Quaternion rot) {
		position = pos;
		rotation = rot;
	}
}

public class FlowerNode : MonoBehaviour {

	/*******************
	* Member Variables *
	*******************/

	public GameObject flowerPrefab;

	public bool checkForCreator = false;
	public float RegrowTime = 5f;

	public float MinimumColliderRadius;

    public bool Available { get; protected set; }
    public bool Empty { get { return FlowerObj.GetComponent<MeshFilter>().sharedMesh == null;  } }

    [HideInInspector]
	public GameObject FlowerObj;
	protected Mesh mFlowerMesh;

	float growTime = 10f;

	/*******************
	* Unity Functions *
	*******************/

	protected virtual void Awake() {
        Available = false;
	}

	protected virtual void Start () {
	}

	/*******************
	* Member Functions *
	*******************/

	public virtual bool SetupNode(Dictionary<Type, IGene> genes, Mesh flowerMesh) {
		return SpawnFlower(flowerMesh);
	}

    protected virtual bool SpawnFlower(Mesh flowerMesh, float delay) {
		mFlowerMesh = flowerMesh;

        if (FlowerObj == null) {
            FlowerObj = (GameObject)Instantiate(flowerPrefab);
        }
		FlowerObj.transform.parent = transform;
        FlowerObj.transform.localPosition = Vector3.zero;
		FlowerObj.transform.localRotation = Quaternion.identity;
		FlowerObj.GetComponent<MeshFilter>().sharedMesh = flowerMesh;

		SphereCollider collider = FlowerObj.GetComponent<SphereCollider>();
		collider.center = new Vector3(0f, flowerMesh.bounds.extents.y, 0f);
		collider.radius = flowerMesh.bounds.extents.x;
		if (collider.radius < MinimumColliderRadius) {
			collider.radius = MinimumColliderRadius;
		}

        Available = true;

		return true;
    }

	protected virtual bool SpawnFlowerImmediate(Mesh flowerMesh) {
        return SpawnFlower(flowerMesh, 0f);
	}

	protected virtual bool SpawnFlower(Mesh flowerMesh) {
		SpawnFlowerImmediate(flowerMesh);

		StartCoroutine(GrowToSize(FlowerObj, RegrowTime));
		return true;
	}

	IEnumerator GrowToSize(GameObject flowerObject, float duration) {
		var time = 0f;
		var startScale = flowerObject.transform.localScale;
		var collider = flowerObject.GetComponent<Collider>();
		var flowerTransform = flowerObject.transform;

		while (time < duration) {
			time += Time.deltaTime;
			float percent = Easing.EaseInOut(time/duration, 2);
			flowerTransform.localScale = startScale * percent;
			yield return 0;
		}

        Available = true;

		yield return null;
	}

	public void RegrowFlower() {
        Available = false;
        FlowerObj = null;
		SpawnFlower(mFlowerMesh);
	}

    public virtual GameObject PickFlower() {
        FlowerObj.transform.parent = null;
        return FlowerObj;
    }

    public virtual void SetMaterial(Material material) {
        FlowerObj.GetComponent<MeshRenderer>().material = material;
    }
}