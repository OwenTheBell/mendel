﻿using UnityEngine;

/*
* This class gets instantiated by a BodyBuilderGeneUser and then passed down
* to all the ChildBodyBuilders. This allows for a precise controlling of plant
* growth time, as well as preventing the ChildBodyBuilders needing to have a
* reference backup to their parent.
*/

public class PlantGrowthTimer {

	/*******************
	* Member Variables *
	*******************/

	private float mTime;
	private float mDuration;

	private float mMultiplier = 4;
	private float mDivisor = 1;

	private int mPlantHeight;

	public float Time {
		get {
			return mTime;
		}
	}

	public float Percent {
		get {
			// return mTime/mDuration;
			return Easing.EaseInOut(mTime/mDuration, 3);
		}
	}

	public float SegmentChunk {
		get {
			return 1f / mPlantHeight;
		}
	}

	public float Duration {
		get {
			return mDuration;
		}
	}

	/***************
	* Constructors *
	***************/

	public PlantGrowthTimer(float duration, int plantHeight) {
		mDuration = duration;
		mTime = 0f;
		mPlantHeight = plantHeight;

		// Debug.Log("Log test " + (Mathf.Log(plantHeight / mDivisor + 1, 2)));
		mDuration = (Mathf.Log(plantHeight / mDivisor + 1, 2)) * mMultiplier;
	}

	/*******************
	* Member Functions *
	*******************/

	public void UpdateTime(float deltaTime) {
		mTime += deltaTime;
	}


}
