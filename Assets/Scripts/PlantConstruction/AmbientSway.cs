﻿using UnityEngine;

public class AmbientSway : MonoBehaviour {

    public float WindSpeed = 1;
    public float WindStretch = 1;
    public float WindForce = 1;
    [Range(0, 360)]
    public float WindDirection = 0;
    public float HeightDelay = 1;
    [Range(0, 1)]
    public float BendBack = 0.25f;
    public float WindNoiseScale = 1;
    public float WindNoiseSpeed = 1;
    public Texture2D Noise;
    public float Percent;
    public float SwayTime;

    public Vector3 Pivot;
    private float _Rigidity;
    private Vector3 _DefaultPosition;
    private Quaternion _DefaultRotation;

    private void Awake() {
        _DefaultPosition = transform.localPosition;
        _DefaultRotation = transform.localRotation;
    }
	
	void LateUpdate () {
        if (Noise == null) {
            return;
        }
        transform.localPosition = _DefaultPosition;
        transform.localRotation = _DefaultRotation;
        var windAngle = WindDirection * Mathf.Deg2Rad;
        var heightOffset = HeightDelay * _Rigidity;
        var distanceOffset = (Mathf.Sin(windAngle) * Pivot.x + Mathf.Cos(windAngle) * Pivot.z) / WindStretch;
        var noiseAdjust = WindNoiseSpeed * SwayTime;
        var pixelX = Pivot.x / WindNoiseScale * noiseAdjust;
        pixelX -= (int)pixelX;
        var pixelY = Pivot.z / WindNoiseScale;
        pixelY -= (int)pixelY;
        var noise = Noise.GetPixelBilinear(pixelX, pixelY);
        var theta = Mathf.Sin(SwayTime * WindSpeed + distanceOffset - heightOffset) * noise.r * WindForce * _Rigidity * Percent;
        var max = Mathf.Clamp(noise.r * WindForce * _Rigidity * Percent, 0.01f, 1f);
        var p = theta + max / (max * 2);
        theta = Mathf.Lerp(max * BendBack, max, p);
        var direction = new Vector3(Mathf.Cos(windAngle), 0f, Mathf.Sin(windAngle));
        transform.position = TranslateAroundLine(transform.position, Pivot, direction, theta);
	}

    private void OnEnable() {
        Events.instance.Raise(new RegisterAmbientSwayEvent(this));
        _DefaultPosition = transform.localPosition;
        _DefaultRotation = transform.localRotation;
    }

    private void OnDisable() {
        Events.instance.Raise(new UnregisterAmbientSwayEvent(this));
        transform.localPosition = _DefaultPosition;
        transform.localRotation = _DefaultRotation;
    }

    public void Setup(Vector3 origin, float rigidity) {
        Pivot = origin;
        _Rigidity = 1f - rigidity;
        _DefaultPosition = transform.localPosition;
        _DefaultRotation = transform.localRotation;
    }

    private Vector3 TranslateAroundLine(Vector3 position, Vector3 pivot, Vector3 direction, float theta) {
        // get the three values for the position of the line we're rotating around
        float a = pivot.x;
        float b = pivot.y;
        float c = pivot.z;
        // get the three values of the direction for line we're rotating around
        float u = direction.x;
        float v = direction.y;
        float w = direction.z;
        var rotationMatrix = Matrix4x4.identity;
        float cosTheta = Mathf.Cos(theta);
        float sinTheta = Mathf.Sin(theta);
        var column0 = new Vector4(
            u * u + (v * v + w * w) * cosTheta,
            u * v * (1 - cosTheta) + w * sinTheta,
            u * w * (1 - cosTheta) - v * sinTheta,
            0);
        var column1 = new Vector4(
            u * v * (1 - cosTheta) - w * sinTheta,
            v * v + (u * u + w * w) * cosTheta,
            v * w * (1 - cosTheta) + u * sinTheta,
            0);
        var column2 = new Vector4(
            u * w * (1 - cosTheta) + v * sinTheta,
            v * w * (1 - cosTheta) - u * sinTheta,
            w * w + (u * u + v * v) * cosTheta,
            0);
        var column3 = new Vector4(
            (a * (v * v + w * w) - u * (b * v + c * w)) * (1 - cosTheta) + (b * w - c * v) * sinTheta,
            (b * (u * u + w * w) - v * (a * u + c * w)) * (1 - cosTheta) + (c * u - a * w) * sinTheta,
            (c * (u * u + v * v) - w * (a * u + b * v)) * (1 - cosTheta) + (a * v - b * u) * sinTheta,
            1);
        rotationMatrix.SetColumn(0, column0);
        rotationMatrix.SetColumn(1, column1);
        rotationMatrix.SetColumn(2, column2);
        rotationMatrix.SetColumn(3, column3);
        //rotationMatrix.SetRow(3, new Vector4(0, 0, 0, 1));
        return rotationMatrix.MultiplyPoint(position);
    }
}
