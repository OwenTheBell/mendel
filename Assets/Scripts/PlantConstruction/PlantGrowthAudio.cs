﻿using UnityEngine;

public class PlantGrowthAudio : MonoBehaviour, IOnGrow, IOnDoneGrowing {

    public float SpatialBlendDistance;

    private CapsuleCollider _Collider;

    private void Awake() {
        _Collider = GetComponent<CapsuleCollider>();
        enabled = false;
    }

    void Update() {
        foreach (var blender in GetComponentsInChildren<SpatialBlender>()) {
            var distance = Vector3.Distance(transform.position, Services.PlayerTransform.position);
            var blend = Easing.EaseInOut((distance - _Collider.radius) / SpatialBlendDistance, 2);
            blender.SetSpatialBlend(blend, _Collider.radius);
        }
    }

    public void OnGrow() {
        enabled = true;
    }

    public void OnDoneGrowing() {
        enabled = false;
    }
}