using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class StemBuilder : MonoBehaviour, IGeneUser, ISwayController {

    private float _Intensity;
    public float Intensity {
        get {
            return _Intensity;
        }
        set {
            _Intensity = Mathf.Clamp01(value);
            if (!Growing) {
                if (_Intensity < 1f) {
                    if (_MeshRenderer.material != TransitionMaterial) {
                        _MeshRenderer.material = TransitionMaterial;
                    }
                    _MeshRenderer.material.SetFloat("_Percent", _Intensity);
                }
                else if (_MeshRenderer.material != AmbientMaterial) {
                    _MeshRenderer.material = AmbientMaterial;
                }
            }
        }
    }

    public bool Growing { get; private set; }
    public float GrowTime { get; private set; }
    public float Radius { get; private set; }

    private float MAX_STEM_BEND_WIDTH = 0.3f;
	public float ChunkGrowthTime = 3f;
	public Vector3 TrunkDimensions;
    public Material GrowthMaterial;
    public Material TransitionMaterial;
    public Material AmbientMaterial;
    private float _GrowthTimer;

    private MeshFilter _MeshFilter;
    private MeshRenderer _MeshRenderer;

	void Awake() {
        _GrowthTimer = -1;
        _MeshFilter = GetComponent<MeshFilter>();
        _MeshRenderer = GetComponent<MeshRenderer>();
	}

	public bool Build() {
        var identity = GetComponent<PlantInfo>().Identity;
        var eventSystem = GetComponent<PlantEventSystem>();
		var seed = Services.SeedVault.Get(identity);
		var stemGene = seed.Genes[typeof(BaseStemGene)] as BaseStemGene;
        Radius = (stemGene.BaseChunkSize.x + stemGene.BaseChunkSize.z) / 2f;
		var origin = transform.position;
		var procSeed = GetComponent<PlantInfo>().Info.Seed;
		var flowers = new FlowerSpawn[0];
		var time = ChunkGrowthTime * stemGene.MaxDepth;
        var height = 0;
		var mesh = MeshConstruction.BuildStemMesh(seed, origin, procSeed, out flowers, out height);
        Services.MeshVault.AddStemInfo(identity, mesh);
        for (var i = 0; i < flowers.Length; i++) {
            flowers[i].time = flowers[i].time * ChunkGrowthTime + Time.timeSinceLevelLoad;
            eventSystem.Raise(new PlaceFlowerEvent(
                                                flowers[i].vertex,
                                                flowers[i].rotation,
                                                flowers[i].height,
                                                flowers[i].time
                                            ));
        }
        if (flowers.Length == 0) {
            eventSystem.Raise(new DoneBloomingEvent());
        }
        GrowMesh(mesh, time);
		return true;
	}

    public void GrowMesh(Mesh mesh, float time) {
        GrowTime = time;
        StopAllCoroutines();
        Growing = true;
		var renderer = _MeshRenderer;
		_MeshFilter.sharedMesh = mesh;
		renderer.material = GrowthMaterial;
        renderer.material.SetFloat("_StartTime", Time.time);
        renderer.material.SetFloat("_TotalTime", GrowTime);
        var collider = GetComponent<PlantCollider>();
        if (collider != null) collider.ConstructCollider(GrowTime);
        foreach (var onGrow in GetComponentsInChildren<IOnGrow>()) {
            if (onGrow is RepeatingAudioBank && ChunkGrowthTime.Equals(0f)) {
                continue;
            }
            onGrow.OnGrow();
        }
        StartCoroutine(Grow());
    }

    IEnumerator Grow() {
        var time = 0f;
        var renderer = _MeshRenderer;
        while (time < GrowTime) {
            time += Time.deltaTime;
            renderer.material.SetFloat("_Percent", time / GrowTime);
            yield return null;
        }

        Growing = false;
        foreach (var doneGrowing in GetComponentsInChildren<IOnDoneGrowing>()) {
            doneGrowing.OnDoneGrowing();
        }
        GetComponent<PlantEventSystem>().Raise(new DoneGrowingEvent());
    }

    public void RelocateMesh(Vector3 origin) {
        var identity = GetComponent<PlantInfo>().Identity;
        var plantMesh = Services.MeshVault.GetStem(identity);
        MeshConstruction.ChangeMeshOrigin(plantMesh, origin);
        //Destroy(_MeshFilter.mesh);
        //_MeshFilter.mesh = plantMesh;
    }
}