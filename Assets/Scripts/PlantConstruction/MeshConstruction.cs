using System;
using System.Collections.Generic;
using UnityEngine;

public class MeshConstruction {

	const int VERTS_PER_PETAL = 7;
	const float STEM_BUD_SIZE_REDUCTION = 0.5f;
	const float MAX_STEM_BEND_WIDTH = 0.3f;

	public static Mesh BuildPetalFlowerMesh(Seed seed, Vector3 plantPivot, int proceduralSeed = 1) {
		var mesh = new Mesh();
		var petalGene = seed.Genes[typeof(BaseFlowerGene)] as PetalFlowerGene;
		var flowerColorGene = seed.Genes[typeof(BaseFlowerColorGene)] as BaseFlowerColorGene;
		var stemGene = seed.Genes[typeof(BaseStemGene)] as BaseStemGene;
		var seededRandom = new SeededRandom(proceduralSeed);

		var vertices = new Vector3[VERTS_PER_PETAL * petalGene.Count];
		var triangles = new int[(VERTS_PER_PETAL - 1) * 6 * petalGene.Count];
		var normals = new Vector3[vertices.Length];
		var tangents = new Vector4[vertices.Length];
		var colors = new Color[vertices.Length];
		var uvs = new Vector2[vertices.Length];
		var uv2s = new Vector2[vertices.Length];
		var uv3s = new Vector2[vertices.Length];

		var petalTips = VERTS_PER_PETAL - 2;
		var halfSized = (stemGene.BudSpacing < 10000) ? true : false;
		var baseColor = flowerColorGene.BaseHSVColor;
		var tipColor = flowerColorGene.TipHSVColor;

		/*
		* create the rotation and pitch for each individual petal
		 */
		var pitchPivotTuples = new Tuple<float, float>[petalGene.Count];
		if (petalGene.Spiraled && petalGene.Count > 5) {
            var startAngle = petalGene.UpperAngle;
            var endAngle = petalGene.PetalAngle;
            // use a larger thetaStep so the last few petals wrap under
            var thetaStep = Mathf.PI * 2.2f;
            thetaStep /= petalGene.Count;
			for(var i = 0; i < petalGene.Count; i++) {
                var percent = 0f;
                if (petalGene.TightSpiral) {
                    percent = Easing.EaseOut(i/(petalGene.Count - 1f), 2);
                }
                else {
                    percent = Easing.EaseIn(i / (petalGene.Count - 1f), 2);
                }
                var pivot = thetaStep * i;
                var pitchPercent = (petalGene.Clockwise) ? 1f - percent : percent;
                var pitch = Mathf.Lerp(startAngle, endAngle, pitchPercent);
				pitchPivotTuples[i] = Tuple.New<float, float>(pitch, pivot * Mathf.Rad2Deg);
			}
		}
		else {
			var thetaStep = Mathf.PI * 2f / petalGene.Count;
			for (var i = 0; i < petalGene.Count; i++) {
				var pivot = thetaStep * i;
				var pitch = petalGene.PetalAngle;
				pitchPivotTuples[i] = Tuple.New<float, float>(pitch, pivot * Mathf.Rad2Deg);
			}
		}
		/*
		* build the mesh
		*/
		for (var petal = 0; petal < petalGene.Count; petal++) {
			var tips = new Vector3[VERTS_PER_PETAL];
			var tipNormals = new Vector3[VERTS_PER_PETAL];
			tips[0] = Vector3.zero;
			tipNormals[0] = Vector3.down;

			var stepAngle = petalGene.SpreadAngle * Mathf.Deg2Rad / 4;
			var minAngle = -petalGene.SpreadAngle/2 * Mathf.Deg2Rad;
			var maxAngle = minAngle + stepAngle * (petalTips - 1);

			var tipWidth = petalGene.TipWidth;
			var tipHeight = petalGene.TipHeight;
			var height = petalGene.Height;
			if (halfSized) {
				tipWidth *= STEM_BUD_SIZE_REDUCTION;
				tipHeight *= STEM_BUD_SIZE_REDUCTION;
				height *= STEM_BUD_SIZE_REDUCTION;
			}

			for (var i = 0; i < petalTips; i++) {
				/* place the tips around the center of the petal. The center of 
				* the petal is located at (0, 0, petalGene.Height) */
				var theta = stepAngle * i + minAngle;
				var newTip = new Vector3(
										tipWidth * Mathf.Sin(theta),
										tipHeight * Mathf.Cos(theta),
										0f);

				/* twist the tip around right and forward to give the petal 
				* some more  shape */
				var percent = (theta - minAngle) / (maxAngle - minAngle);
				percent = (percent - 0.5f) * 2f;

				var tipAngle = petalGene.TipAngle * (1 - Mathf.Abs(percent));
				var wrapAngle = petalGene.WrapAngle * percent;

				newTip = Quaternion.AngleAxis(tipAngle, Vector3.left) * newTip;
				newTip = Quaternion.AngleAxis(wrapAngle, Vector3.up) * newTip;

				newTip.y += height;
				tips[i+1] = newTip;

				var angle = Mathf.Atan2(newTip.y, newTip.x);
				var normal = new Vector3(1 * Mathf.Cos(angle), 1 * Mathf.Sin(angle));
				tipNormals[i] = normal;
			}
			tips[tips.Length - 1] = new Vector3(0f, height, 0f);
			tipNormals[tips.Length - 1] = Vector3.forward;

			/* make the petals alter angle slightly so that the meshes do not
			* render exactly on top of each other */

			var posNeg = (petal % 2 == 0) ? 1 : 0;
			if (petal == petalGene.Count - 1 && petal % 2 == 0) {
				posNeg = 2;
			}

			var petalPitch = pitchPivotTuples[petal].First;
			var petalPivot = pitchPivotTuples[petal].Second;

			for (var i = 1; i < tips.Length; i++) {
				tips[i] = Quaternion.AngleAxis(petalPitch, Vector3.left) * tips[i];
				tips[i] = Quaternion.AngleAxis(petalPivot, Vector3.up) * tips[i];
			}

			var colorPercent = seededRandom.Range(0.0f, 1.0f);
            if (flowerColorGene.Gradient) {
                colorPercent = petal / (petalGene.Count - 1f);
                if (!petalGene.Spiraled) {
                    if (colorPercent <= 0.5f) {
                        colorPercent *= 2f;
                    }
                    else {
                        colorPercent = (1f - colorPercent) * 2f;
                    }
                }
            }

            var color = HSVColor.Lerp(tipColor, baseColor, colorPercent);

			var index = petal * VERTS_PER_PETAL;
			var centerIndex = tips.Length - 1;
			for (var i = 0; i <= centerIndex - 1; i++) {
				var j = (i == centerIndex - 1) ? 0 : i + 1;

				var tris = new int[]{i, j, centerIndex, centerIndex, j, i};
				var trianglePointIndex = petal * 36 + i * 6;

				foreach (var triIndex in tris) {
					triangles[trianglePointIndex] = triIndex + index;
					trianglePointIndex++;
				}
			}

			for (var i = 0; i < tips.Length; i++){
				vertices[index] = tips[i];

                normals[index] = plantPivot;

				var unpitchedVert = tips[i];
				unpitchedVert = Quaternion.AngleAxis(petalPivot, Vector3.down) * unpitchedVert;
				unpitchedVert = Quaternion.AngleAxis(petalPitch, Vector3.right) * unpitchedVert;
				unpitchedVert = Quaternion.AngleAxis(petalPivot, Vector3.up) * unpitchedVert;
				tangents[index] = new Vector3(unpitchedVert.x, unpitchedVert.y, unpitchedVert.z);
				//vertices[index] = new Vector3(unpitchedVert.x, unpitchedVert.y, unpitchedVert.z);
				colors[index] = color.RGBColor;
                index++;
            }
		}
		/*
		* save plantPivot into uv and uv2
		* This needs to be done since uvs won't be mutated by batching
		*/
		for (var i = 0; i < uvs.Length; i++) {
			uvs[i] = plantPivot;
			uv2s[i] = new Vector2(plantPivot.z, 0f);
		}
		/*
		* save the order of the petals so they can bloom in sequence
		*/
		for (var petal = petalGene.Count - 1; petal >= 0; petal--) {
			var startIndex = petal * VERTS_PER_PETAL;
			var endIndex = startIndex + VERTS_PER_PETAL;
			for (var index = startIndex; index < endIndex; index++) {
				uv3s[index] = new Vector2(petal, petalGene.Count);
			}
		}

		mesh.vertices = vertices;
		mesh.triangles = triangles;
		mesh.normals = normals; // each is set to flowerPivot
		mesh.tangents = tangents; // the halfway grown level of each petal
		mesh.colors = colors;
		mesh.uv = uvs; // x & y of flower pivot
		mesh.uv2 = uv2s; // z of flower pivot
		mesh.uv3 = uv3s; // order of petals in flower
		return mesh;
	}

	public static Mesh BuildStemMesh(
		Seed seed,
		Vector3 origin,
		int procSeed,
		out FlowerSpawn[] flowers,
        out int height
	) {
		var stemGene = seed.Genes[typeof(BaseStemGene)] as BaseStemGene;
		var seededRandom = new SeededRandom(procSeed);
		var flowerSpawns = new List<FlowerSpawn>();
		var rotationTracker = new GameObject().GetComponent<Transform>();

		var trunkDimensions = stemGene.BaseChunkSize;

		var firstBud = stemGene.BudSpacing;
		if (firstBud <= 2) {
            firstBud = Mathf.CeilToInt(stemGene.MaxDepth / 3f);
		}
        height = stemGene.MaxDepth;

		Func<Quaternion, bool, bool, float, Quaternion> adjustDirection = (direction, straight, trunk, twist) => {
			var scalar = (straight) ? 0.1f : 1f;
            if (trunk)
                scalar *= stemGene.Trunk;
			var angle = stemGene.BranchAngle * scalar;
			var random = stemGene.AngleRandom * scalar;
            var x = angle + seededRandom.Range(-random, random);
            var z = angle + seededRandom.Range(-random, random);
			return direction * Quaternion.Euler(x, twist, z);
		};

        var branchedDuringGrowth = false;

        var startStraight = stemGene.Straight || (!branchedDuringGrowth && !stemGene.CrookedBase);
		var startingDirection = adjustDirection(Quaternion.identity, startStraight, true, 0f);
        var branchHeight = Mathf.RoundToInt((stemGene.MaxDepth - 1) * stemGene.FirstBranch);
		var mainFoundation = CalculateVertexRing(Vector3.zero, Quaternion.identity, trunkDimensions);
		var meshStruct = new MeshStruct(0);
		BuildChunk(
			meshStruct
			, origin
			, new ChunkInfo(
				Vector3.zero,
				mainFoundation,
				startingDirection,
				0,
				branchHeight,
				firstBud,
				true,
				false,
				1f
			)
			, seed
			, seededRandom
			, rotationTracker
			, flowerSpawns
			);
		UnityEngine.Object.Destroy(rotationTracker.gameObject);
		flowers = flowerSpawns.ToArray();
		var returnMesh = new Mesh();
		returnMesh.SetVertices(meshStruct.Vertices);
		returnMesh.SetNormals(meshStruct.Normals);
		returnMesh.SetTangents(meshStruct.Tangents);
		returnMesh.triangles = meshStruct.Triangles.ToArray();
		returnMesh.SetColors(meshStruct.Colors);
		returnMesh.SetUVs(0, meshStruct.UVs);
		returnMesh.SetUVs(1, meshStruct.UV2s);
		returnMesh.SetUVs(2, meshStruct.UV3s);
		return returnMesh;
	}

	private static void BuildChunk(
		MeshStruct plantMesh
		, Vector3 origin
		, ChunkInfo info
		, Seed seed
		, SeededRandom seededRandom
		, Transform rotationTracker
		, List<FlowerSpawn> flowerSpawns
		) {
		var stemGene = seed.Genes[typeof(BaseStemGene)] as BaseStemGene;
		var stemColorGene = seed.Genes[typeof(BaseStemColorGene)] as BaseStemColorGene;
		var baseColor = stemColorGene.BaseColor;
		var tipColor = stemColorGene.TipColor;
		var trunkDimensions = stemGene.BaseChunkSize;
		var furthestBranch = stemGene.BranchSpacing + stemGene.BranchChange + 1;
        var closestBranch = stemGene.BranchSpacing - stemGene.BranchChange;
        if (closestBranch < 0) closestBranch = 0;

		Func<Quaternion, bool, bool, float, Quaternion> adjustDirection = (dir, straight, trunk, twist) => {
			var scalar = (straight) ? 0.1f : 1f;
            if (trunk)
                scalar *= stemGene.Trunk;
			var angle = stemGene.BranchAngle * scalar;
			var random = stemGene.AngleRandom * scalar;
            var x = angle + seededRandom.Range(-random, random);
            var z = angle + seededRandom.Range(-random, random);
			return dir * Quaternion.Euler(x, twist, z);
		};

		var center = info.center;
		var lowerRing = info.lowerRing;
		var direction = info.direction;
		var depth = info.depth;
		var nextBranch = info.nextBranch;
		var nextFlower = info.nextFlower;
		var rigidity = info.rigidity;

		var lowPercent = Mathf.Clamp01((float)(depth-1)/stemGene.MaxDepth);
		var highPercent = Mathf.Clamp01((float)depth/stemGene.MaxDepth);
		var dimensions = trunkDimensions * (1f - lowPercent);
		var upperDimensions = trunkDimensions * (1f - highPercent);

		var lowColor = HSVColor.Lerp(baseColor, tipColor, lowPercent).RGBColor;
		var highColor = HSVColor.Lerp(baseColor, tipColor, highPercent).RGBColor;

		var start = (float)(depth + 1) / (stemGene.MaxDepth + 1);
		// add 2 to ensure next chunk starts growing halfway through this one
		var end = (depth + 1 + 2f) / (stemGene.MaxDepth + 1);

		// default state for if a stunted branch exceeds MaxDepth
		if (depth >= stemGene.MaxDepth) {
			return;
		}
		// calculate the tip
		else if (depth >= stemGene.MaxDepth - 1) {
			var tip = center + (direction * (Vector3.up * dimensions.y));
			var vertices = new List<Vector3>(new Vector3[4]{tip, tip, tip, tip});
			var duplicatedVertices = new Vector3[lowerRing.Length * 2];
			for (var i = 0; i < lowerRing.Length; i++) {
				var j = (i != lowerRing.Length - 1) ? i + 1 : 0;
				duplicatedVertices[i*2] = lowerRing[i];
				duplicatedVertices[i*2+1] = lowerRing[j];
			}
			vertices = vertices.Combine(duplicatedVertices);
			plantMesh.ExpandCapcity(vertices.Count);

			for (var i = 0; i < 4; i++) {
				var r = seededRandom.Range(lowPercent, highPercent);
				var color = HSVColor.Lerp(baseColor, tipColor, r).RGBColor;
				var a = i + plantMesh.Vertices.Count;
				var b = i * 2 + 4 + plantMesh.Vertices.Count;
				var c = i * 2 + 4 + 1 + plantMesh.Vertices.Count;
				plantMesh.Triangles.AddRange(new int[]{a, b, c});
			}
			plantMesh.Vertices.AddRange(vertices);

			AddColors(plantMesh, vertices.Count, lowColor, highColor, seededRandom.Next());

			for (var i = 0; i < 12; i++) {
				if (i < 4) {
					plantMesh.Normals.Add(tip);
				}
				else {
					plantMesh.Normals.Add(vertices[i]);
				}
				plantMesh.Tangents.Add(center);
			}

			for (var i = 0; i < vertices.Count; i++) {
				plantMesh.UVs.Add(new Vector2(origin.x, origin.y));
				plantMesh.UV2s.Add(new Vector2(origin.z, rigidity));
				plantMesh.UV3s.Add(new Vector2(start, end));
			}

			//put a flower on the tip
			flowerSpawns.Add(new FlowerSpawn(tip, depth + 1, direction, rigidity));

			return;
		}
		// just make a plain old chunk
		else {
			var nextCenter = center + (direction * (Vector3.up * dimensions.y));
			var upperRing = CalculateVertexRing(nextCenter, direction, upperDimensions);
			var nextRigidity = rigidity - (1f / stemGene.MaxDepth);

			if (nextFlower == 0) {
				nextFlower = stemGene.BudSpacing;
				nextFlower += (seededRandom.Range(0f, 1f) > 0.5f) ? 1 : -1;

				// do not add extra flowers too close to the tip
				if (depth + nextFlower + 2 >= stemGene.MaxDepth - 1) {
					nextFlower = 10000;
				}

				var random = seededRandom.Next(4);
				var vertex = upperRing[random];
				rotationTracker.position = nextCenter;
				rotationTracker.LookAt(vertex, Vector3.up);
				// this rotation ensures the flower points away from the stem
				rotationTracker.Rotate(new Vector3(90, 0, 0));
				var flowerFacing = rotationTracker.rotation;

				var index = 0;
				while (index < flowerSpawns.Count && flowerSpawns[index].time < depth + 1) {
					index++;
				}
				flowerSpawns.Insert(index, new FlowerSpawn(vertex, depth + 1, flowerFacing, nextRigidity));
			}

			// determine if there are one or two children of this chunk
			// Store the vertices of those children inside plantMesh
			if (nextBranch == 0) {
				var stuntedBranch = seededRandom.Next(2);

				var leftTwist = stemGene.Twist;
				var adjust = 0;
				if (stemGene.Clockwise && stemGene.Counterclockwise) {
					adjust = (seededRandom.Next(2) == 1) ? 1 : -1;
				}
				else if (stemGene.Clockwise) {
					adjust = 1;
				}
				else if (stemGene.Counterclockwise) {
					adjust = -1;
				}
				else {
					leftTwist = 0;
					adjust = 1;
				}
				var rightTwist = (leftTwist + 180) * adjust;

				Func<bool, float, MeshStruct> makeBranch = (stunted, twist) => {
					var depthAdjust = 1;
					var trunk = true;
					var adjustedTwist = twist;
					if (stunted) {
						var branchRemaining = (float)(stemGene.MaxDepth - 1) - depth;
						branchRemaining *= (1f - stemGene.BranchStunting);
						depthAdjust += (int)Mathf.Round(branchRemaining);
						trunk = false;
						adjustedTwist = Mathf.RoundToInt(adjustedTwist * stemGene.Trunk);
					}
					var nextDirection = adjustDirection(direction, false, trunk, adjustedTwist);
					var branchGap = seededRandom.Next(closestBranch, furthestBranch);
					if (branchGap == 0 && stemGene.BranchSpacing == 1) {
						branchGap = 1;
					}
					var recurseProtectedNextFlower = nextFlower - 1;
					BuildChunk(
						plantMesh
						, origin
						, new ChunkInfo(
							nextCenter,
							upperRing,
							nextDirection,
							depth + depthAdjust,
							branchGap,
							recurseProtectedNextFlower,
							trunk,
							true,
							nextRigidity
						)
						, seed
						, seededRandom
						, rotationTracker
						, flowerSpawns
					);
					return plantMesh;
				};
				var leftMesh = makeBranch(stuntedBranch == 0, leftTwist);
				var rightMesh = makeBranch(stuntedBranch == 1, rightTwist);
			}
			else {
				var straight = stemGene.Straight || (!info.alreadyBranched && !stemGene.CrookedBase);
				var nextDirection = adjustDirection(direction, straight, info.trunk, 0f);
				BuildChunk(
					plantMesh
					, origin
					, new ChunkInfo(
						nextCenter,
						upperRing,
						nextDirection,
						depth+1,
						nextBranch-1,
						nextFlower  - 1,
						info.trunk,
						info.alreadyBranched,
						nextRigidity
					)
					, seed
					, seededRandom
					, rotationTracker
					, flowerSpawns
				);
			}
			var vertices = new List<Vector3>(lowerRing).Combine(upperRing);
			var mesh = GetChunkFromRings(
				plantMesh,
				vertices,
				rigidity,
				nextRigidity,
				center,
				nextCenter,
				lowColor,
				highColor,
				origin,
				start,
				end,
				seededRandom
			);
			CheckFlowerPointsInsideChunk(vertices, flowerSpawns);

			if (depth == 0) {
				var upDirection = Quaternion.Euler(Vector3.up);
				var lowerCenter = center + (upDirection * (Vector3.down * dimensions.y));
				var rootRing = CalculateVertexRing(lowerCenter, upDirection, dimensions);

				start = (float)depth / (stemGene.MaxDepth + 1);
				end = (depth + 2.0f) / (stemGene.MaxDepth + 1);
				var rootMesh = GetChunkFromRings(
					plantMesh,
					new List<Vector3>(rootRing).Combine(lowerRing),
					rigidity,
					rigidity,
					lowerCenter,
					center,
					lowColor,
					highColor,
					origin,
					start,
					end,
					seededRandom
				);
			}
			return;
		}
	}

    private static void CheckFlowerPointsInsideChunk(List<Vector3> vertices, List<FlowerSpawn> flowers) {
        var planes = new Plane[12];
        var planeIndex = 0;
        for (var i = 0; i < 4; i++) {
            var a = i;
            var b = (i < 4 - 1) ? i + 1 : 0;
            var aa = a + 4;
            var bb = b + 4;
            var indices = new int[] { a, b, aa, bb, aa, b };
            planes[planeIndex++] = new Plane(vertices[a], vertices[b], vertices[aa]);
            planes[planeIndex++] = new Plane(vertices[bb], vertices[aa], vertices[b]);
        }
        // calculate the top and bottom
        // the lower two planes must be reversed since the rings should face away
        planes[planeIndex++] = new Plane(vertices[2], vertices[1], vertices[1]);
        planes[planeIndex++] = new Plane(vertices[0], vertices[3], vertices[2]);
        planes[planeIndex++] = new Plane(vertices[4], vertices[5], vertices[6]);
        planes[planeIndex++] = new Plane(vertices[6], vertices[7], vertices[4]);
        for (var flowerIndex = flowers.Count - 1; flowerIndex >= 0; flowerIndex--) {
            var inside = true;
            foreach (var plane in planes) {
                if (plane.GetSide(flowers[flowerIndex].vertex)) {
                    inside = false;
                    break;
                }
            }
            if (inside) {
                flowers.RemoveAt(flowerIndex);
            }
        }
    }

	private static MeshStruct GetChunkFromRings(
		MeshStruct mesh,
        List<Vector3> ringVertices,
        float rigidity,
        float nextRigidity,
        Vector3 center,
        Vector3 nextCenter,
        Color lowColor,
        Color highColor,
        Vector3 origin,
        float start,
        float end,
        SeededRandom seededRandom
    ) {
		var newVertexCount = ringVertices.Count * 3;
		mesh.ExpandCapcity(newVertexCount);
        var ringSize = ringVertices.Count / 2;
        for (var i = 0; i < ringSize; i++) {
            var a = i;
            var b = (i < ringSize - 1) ? i + 1 : 0;
            var aa = a + ringSize;
            var bb = b + ringSize;
            var indices = new int[]{a, b, aa, bb, aa, b};
            for (var j = 0; j < indices.Length; j++) {
                mesh.Vertices.Add(ringVertices[indices[j]]);
                if (indices[j] < ringSize) {
                    mesh.Normals.Add(ringVertices[indices[j]]);
                    mesh.UV2s.Add(new Vector2(origin.z, rigidity));
                }
                else {
                    mesh.Normals.Add(nextCenter);
                    mesh.UV2s.Add(new Vector2(origin.z, nextRigidity));
                }
                mesh.Tangents.Add(center);
                mesh.Triangles.Add(mesh.Vertices.Count - 1);
            }
        }
		AddColors(mesh, newVertexCount, lowColor, highColor, seededRandom.Next());
        var uv = new List<Vector2>(new Vector2[mesh.Vertices.Count]);
		for (var i = 0; i < newVertexCount; i++) {
			mesh.UVs.Add(new Vector2(origin.x, origin.y));
			mesh.UV3s.Add(new Vector2(start, end));
		}
        return mesh;
    }

	private static void AddColors(MeshStruct mesh, int offset, Color lower, Color upper, int seed) {
		mesh.Colors.AddRange(new Color[offset]);
		var seededRandom = new SeededRandom(seed);
		for (var i = mesh.Triangles.Count - offset; i < mesh.Triangles.Count; i+=3) {
			var color = Color.Lerp(lower, upper, seededRandom.Sample());
			mesh.Colors[mesh.Triangles[i]] = color;
			mesh.Colors[mesh.Triangles[i+1]] = color;
			mesh.Colors[mesh.Triangles[i+2]] = color;
		}
	}

	private static Vector3[] CalculateVertexRing(Vector3 center, Quaternion direction, Vector3 edges) {
		var vertices = new Vector3[]{
			new Vector3(-edges.x/2, 0, -edges.z/2),
			new Vector3(-edges.x/2, 0, edges.z/2),
			new Vector3(edges.x/2, 0, edges.z/2),
			new Vector3(edges.x/2, 0, -edges.z/2)
		};
		for (var i = 0; i < vertices.Length; i++) {
			vertices[i] = vertices[i];
			vertices[i] = direction * vertices[i];
			vertices[i] += center;
		}
		return vertices;
	}

	struct ChunkInfo {
		public readonly Vector3 center;
		public readonly Vector3[] lowerRing;
		public readonly Quaternion direction;
		public readonly int depth;
		public readonly int nextBranch;
		public readonly int nextFlower;
		public readonly float rigidity;
        public readonly bool trunk;
		public readonly bool alreadyBranched;

		public ChunkInfo(
			Vector3 center,
			Vector3[] foundation,
			Quaternion direction,
			int depth,
			int nextBranch,
			int nextFlower,
			bool trunk,
			bool alreadyBranched,
            float rigidity
		) {
			this.center = center;
			this.lowerRing = foundation;
			this.direction = direction;
			this.depth = depth;
			this.nextBranch = nextBranch;
			this.nextFlower = nextFlower;
			this.rigidity = rigidity;
            this.trunk = trunk;
			this.alreadyBranched = alreadyBranched;
		}
	}

    public static void ChangeMeshOrigin(Mesh mesh, Vector3 origin) {
        var uv = mesh.uv;
        var uv2 = mesh.uv2;
        for (var i = 0; i < uv.Length; i++) {
            uv[i] = new Vector2(origin.x, origin.y);
            uv2[i] = new Vector2(origin.z, uv2[i].y);
        }
        mesh.uv = uv;
        mesh.uv2 = uv2;
    }

	public static void UpdateFlowerMeshForHeight(Mesh mesh, float height) {
        var uv2 = mesh.uv2;
        for (var i = 0; i < uv2.Length; i++) {
            uv2[i].y = height;
        }
        mesh.uv2 = uv2;
	}

    public static Mesh CloneMesh(Mesh baseMesh) {
        var mesh = new Mesh();
        mesh.vertices = baseMesh.vertices;
        mesh.triangles = baseMesh.triangles;
        mesh.normals = baseMesh.normals;
        mesh.colors = baseMesh.colors;
        mesh.tangents = baseMesh.tangents;
        mesh.uv = baseMesh.uv;
        mesh.uv2 = baseMesh.uv2;
        mesh.uv3 = baseMesh.uv3;
        mesh.uv4 = baseMesh.uv4;
        return mesh;
    }

    public static void CopyValuesIntoMesh(Mesh blueprint, Mesh output) {
        output.vertices = blueprint.vertices;
        output.triangles = blueprint.triangles;
        output.normals = blueprint.normals;
        output.colors = blueprint.colors;
        output.tangents = blueprint.tangents;
        output.uv = blueprint.uv;
        output.uv2 = blueprint.uv2;
        output.uv3 = blueprint.uv3;
        output.uv4 = blueprint.uv4;
    }
}

public struct FlowerSpawn {
	public Vector3 vertex;
	public float time;
	public Quaternion rotation;
	public float height;

	public FlowerSpawn(Vector3 vertex, float time, Quaternion rotation, float height) {
		this.vertex = vertex;
		this.time = time;
		this.rotation = rotation;
		this.height = height;
	}
}

public struct MeshStruct {
	public List<Vector3> Vertices;
	public List<int> Triangles;
	public List<Vector3> Normals;
	public List<Vector4> Tangents;
	public List<Color> Colors;
	public List<Vector2> UVs;
	public List<Vector2> UV2s;
	public List<Vector2> UV3s;

	public MeshStruct(int capacity) {
		Vertices = new List<Vector3>(capacity);
		Triangles = new List<int>(capacity);
		Normals = new List<Vector3>(capacity);
		Tangents = new List<Vector4>(capacity);
		Colors = new List<Color>(capacity);
		UVs = new List<Vector2>(capacity);
		UV2s = new List<Vector2>(capacity);
		UV3s = new List<Vector2>(capacity);
	}

	public void ExpandCapcity(int value) {
		Vertices.Capacity += value;
		Triangles.Capacity += value;
		Normals.Capacity += value;
		Tangents.Capacity += value;
		Colors.Capacity += value;
		UVs.Capacity += value;
		UV2s.Capacity += value;
		UV3s.Capacity += value;
	}
}