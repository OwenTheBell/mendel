﻿using UnityEngine;

public class PlantHybridizer : MonoBehaviour {

    public LayerMask GroundLayer;
    public float CastHeight;
    public GameObject PlantPrefab;

   private void OnEnable() {
        Events.instance.AddListener<HybridizePlantsEvent>(OnHybridzePlantsEvent);
    }

    private void OnDisable() {
        Events.instance.RemoveListener<HybridizePlantsEvent>(OnHybridzePlantsEvent);
    }

    private void OnHybridzePlantsEvent(HybridizePlantsEvent e) {
        var p = 0.5f;
        var position = Vector3.Lerp(e.position1, e.position2, p);
        position.y += CastHeight;
        var direction = (Random.Range(0, 2) == 0) ? -1 : 1;
        var ray = new Ray(position, Vector3.down);
        RaycastHit hitInfo;
        var layerIndex = (int)Mathf.Log(GroundLayer, 2);
        do {
            Physics.Raycast(ray, out hitInfo, Mathf.Infinity);
            p += 0.05f * direction;
            position = Vector3.Lerp(e.position1, e.position2, p);
            position.y += CastHeight;
            ray.origin = position;
        } while (hitInfo.collider.gameObject.layer != layerIndex && p > 0f && p < 1f);

        if (p < 0f || p > 1f) return;

        var plantMask = LayerMask.LayerToName(hitInfo.collider.gameObject.layer);

        var plant = PlantFunctions.GeneratePlant(PlantPrefab, e.id1, e.id2, hitInfo.point, false);
        if (e.immediate) {
            plant.GetComponent<StemBuilder>().ChunkGrowthTime = 0f;
            plant.GetComponent<FlowerCreator>().InstantFirstBloom = true;
        }
        plant.GetComponent<GeneManager>().Grow();
        DontDestroyOnLoad(plant);
    }
}