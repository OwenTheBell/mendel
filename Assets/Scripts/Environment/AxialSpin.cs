﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AxialSpin : MonoBehaviour {

    [Tooltip("degrees per second")]
    public Vector3 Speed;

	void Start () {
		
	}
	
	void Update () {
        var speed = Speed * Time.deltaTime;

        transform.Rotate(new Vector3(speed.x, speed.y, speed.z));
	}
}
