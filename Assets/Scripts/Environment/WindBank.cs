﻿using UnityEngine;
using System.Collections;

public class WindBank : MonoBehaviour {

    public AudioClip[] Clips;
    [Range(0, 1)]
    public float MinDelay;
    [Range(0, 1)]
    public float MaxDelay;

    private AudioSource[] _Sources;
    private int _LastSource;
    private int _LastClip;

    void Awake() {
        _Sources = GetComponents<AudioSource>();
        _LastSource = 0;
        _LastClip = Clips.Length;
    }

    private void Start() {
        StartCoroutine(PlayClip());
    }

    IEnumerator PlayClip() {
        do {
            var sourceIndex = (_LastSource == 0) ? 1 : 0;
            var clipIndex = Random.Range(0, Clips.Length - 1);
            if (clipIndex >= _LastClip) clipIndex++;
            _Sources[sourceIndex].clip = Clips[clipIndex];
            _Sources[sourceIndex].Play();
            _LastSource = sourceIndex;
            _LastClip = clipIndex;
            var length = Clips[_LastClip].length;
            var time = Random.Range(length * MinDelay, length * MaxDelay);
            yield return new WaitForSeconds(time);
        } while (true);
    }
}