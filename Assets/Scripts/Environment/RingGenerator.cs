﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RingGenerator : MonoBehaviour {

    public Vector2 DimensionRange;
    public Vector2 Spacing;
    public float Radius;
    public int Count;
    public float Scale;
    [Range(0, 1)]
    public float Distortion;

    private List<Vector3> _Centroids;

	void Start () {
        var vertices = new List<Vector3>();
        var normals = new List<Vector3>();
        var triangles = new List<int>();
        var tangents = new List<Vector4>();
        var colors = new List<Color>();

        float angle = 0f;
        float noisePoint = 0f;
        while (angle < Mathf.PI * 2) {
            var position = new Vector3(Mathf.Cos(angle) * Radius, Mathf.Sin(angle) * Radius, 0f);
            var dimension = Random.Range(DimensionRange.x, DimensionRange.y);
            GenerateChunk(position, dimension, vertices, normals, tangents, triangles, colors);

            noisePoint += Scale;
            var noise = Mathf.PerlinNoise(noisePoint, 0f);
            var spacing = Mathf.Lerp(Spacing.x, Spacing.y, noise);
            angle += Mathf.PI * 2 / spacing;
        }

        var mesh = new Mesh();
        mesh.SetVertices(vertices);
        mesh.SetNormals(normals);
        mesh.SetTangents(tangents);
        mesh.SetTriangles(triangles, 0);
        mesh.SetColors(colors);
        GetComponent<MeshFilter>().sharedMesh = mesh;
        GetComponent<MeshRenderer>().material.SetFloat("_Radius", Radius);

        _Centroids = normals;
	}

    void GenerateChunk(
                            Vector3 position,
                            float dimension,
                            List<Vector3> vertices,
                            List<Vector3> normals,
                            List<Vector4> tangents,
                            List<int> triangles,
                            List<Color> colors
                        ) {
        float a = dimension;
        float s = Mathf.Sqrt(a * a - Mathf.Pow(0.5f * a, 2));
        float h = Mathf.Sqrt(s * s - (1 / 12.0f) * (a * a));
        var chunckVertices = new Vector3[4] {
                                                new Vector3(0, 0, 0),
                                                new Vector3(a, 0, 0),
                                                new Vector3(a/2, 0, s),
                                                new Vector3(0.5f * a, h, s / 3)
                                            };
        var centroid = Vector3.zero;
        for (var index = 0; index < 4; index++) {
            var upper = dimension/2 * Distortion;
            var lower = -upper;
            chunckVertices[index] += new Vector3(
                                                    Random.Range(lower, upper),
                                                    Random.Range(lower, upper),
                                                    Random.Range(lower, upper)
                                                );
            centroid += chunckVertices[index];
        }
        centroid /= 4;

        for (var index = 0; index < 4; index++) {
            chunckVertices[index] += position;
        }
        centroid += position;

        var chunkTriangles = new int[12] { 0, 1, 2, 0, 3, 1, 1, 3, 2, 2, 3, 0 };
        var color = Color.black;
        var direction = Random.insideUnitSphere.normalized;
        for (var index = 0; index < chunkTriangles.Length; index++) {
            if (index % 3 == 0) {
                color = new Color(Random.value, 0, 0);
            }
            vertices.Add(chunckVertices[chunkTriangles[index]]);
            colors.Add(color);
            normals.Add(centroid);
            triangles.Add(vertices.Count - 1);
            tangents.Add(direction);
        }
    }
}
