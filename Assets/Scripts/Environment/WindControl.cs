﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WindControl : MonoBehaviour {

    public float WindSpeed = 1;
    public float WindStretch = 1;
    public float WindForce = 1;
    [Range(0, 360)]
    public float WindDirection = 0;
    public float HeightDelay = 1;
    [Range(0, 1)]
    public float BendBack = 0.25f;
    public float WindNoiseScale = 1;
    public float WindNoiseSpeed = 1;

    public int NoiseTextureWidth;

    private Texture2D _Noise;

    private List<AmbientSway> _swayers;

    private void Awake() {
        _swayers = new List<AmbientSway>();
        Services.WindControl = this;
        Events.instance.AddListener<RegisterAmbientSwayEvent>(OnRegisterAmbientSway);
        Events.instance.AddListener<UnregisterAmbientSwayEvent>(OnUnregisterAmbientSway);
    }

    void Start () {
        var map = Noise.GenerateTilableNoise(NoiseTextureWidth, NoiseTextureWidth, 20);
        var pixels = new Color[NoiseTextureWidth * NoiseTextureWidth];
        for (var i = 0; i < pixels.Length; i++) {
            var x = i / NoiseTextureWidth;
            var y = i % NoiseTextureWidth;
            pixels[i] = Color.Lerp(Color.black, Color.white, map[x, y]);
        }
        _Noise = new Texture2D(NoiseTextureWidth, NoiseTextureWidth);
        _Noise.wrapMode = TextureWrapMode.Repeat;
        _Noise.SetPixels(pixels);
        _Noise.Apply();
	}
	
	void Update () {
        Shader.SetGlobalFloat("_WindSpeed", WindSpeed);
        Shader.SetGlobalFloat("_WindStretch", WindStretch);
        Shader.SetGlobalFloat("_WindForce", WindForce);
        Shader.SetGlobalFloat("_WindDirection", WindDirection);
        Shader.SetGlobalTexture("_WindNoise", _Noise);
        Shader.SetGlobalFloat("_HeightDelay", HeightDelay);
        Shader.SetGlobalFloat("_BendBack", BendBack);
        Shader.SetGlobalFloat("_WindNoiseSpeed", WindNoiseSpeed);
        Shader.SetGlobalFloat("_WindNoiseScale", WindNoiseScale);
        Shader.SetGlobalFloat("_TestTime", Time.time);

        foreach (var swayer in _swayers) {
            swayer.WindSpeed = WindSpeed;
            swayer.WindStretch = WindStretch;
            swayer.WindForce = WindForce;
            swayer.WindDirection = WindDirection;
            swayer.Noise = _Noise;
            swayer.HeightDelay = HeightDelay;
            swayer.BendBack = BendBack;
            swayer.WindNoiseSpeed = WindNoiseSpeed;
            swayer.WindNoiseScale = WindNoiseScale;
            swayer.SwayTime = Time.time;
        }
	}

    private void OnDestroy() {
        Events.instance.RemoveListener<RegisterAmbientSwayEvent>(OnRegisterAmbientSway);
        Events.instance.RemoveListener<UnregisterAmbientSwayEvent>(OnUnregisterAmbientSway);
    }

    public void OnRegisterAmbientSway(RegisterAmbientSwayEvent e) {
        _swayers.Add(e.Sway);
    }

    public void OnUnregisterAmbientSway(UnregisterAmbientSwayEvent e) {
        _swayers.Remove(e.Sway);
    }
}
