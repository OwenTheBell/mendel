﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnvironmentColorer : MonoBehaviour {

    public MeshRenderer Sky;
    public MeshRenderer[] AsteroidRings;
    public MeshRenderer Ocean;

    public HSVColor PrimaryColor;
    [Range(0, 1)]
    public float ComplementaryOffset;
    [Range(0, 1)]
    public float AnalogousOffset;
    [Range(0, 1)]
    public float AsteroidSaturation;
    [Range(0, 1)]
    public float OceanSaturation;

    [Tooltip("seconds to complete a hull cycle of the h value")]
    public float CycleTime = 0f;
    private float _CurrentTime;

	void Start () {
		
	}
	
	void Update () {
        _CurrentTime += Time.deltaTime;
        if (_CurrentTime > CycleTime) {
            _CurrentTime -= CycleTime;
        }
        float percent = _CurrentTime / CycleTime;
        var color = new HSVColor(PrimaryColor.h + percent, PrimaryColor.s, PrimaryColor.v);
        SetColors(color);
	}

    void SetColors(HSVColor color) {
        var skybubbleAnalog = new HSVColor(color.h + AnalogousOffset, color.s, color.v);
        Sky.material.SetColor("_PeakColor", color.RGBColor);
        Sky.material.SetColor("_HorizonColor", skybubbleAnalog.RGBColor);

        var asteroidPrimary = new HSVColor(color.h + ComplementaryOffset, AsteroidSaturation, color.v).RGBColor;
        var asteroidSecondary = new HSVColor(skybubbleAnalog.h + ComplementaryOffset, AsteroidSaturation, skybubbleAnalog.v).RGBColor;
        foreach (var ring in AsteroidRings) {
            ring.material.SetColor("_PrimaryColor", asteroidPrimary);
            ring.material.SetColor("_SecondaryColor", asteroidSecondary);
            ring.material.SetColor("_AtmosphereColor", skybubbleAnalog.RGBColor);
        }

        var oceanPeak = new HSVColor(color.h, OceanSaturation, color.v).RGBColor;
        Ocean.material.SetColor("_PeakColor", oceanPeak);
    }
}
