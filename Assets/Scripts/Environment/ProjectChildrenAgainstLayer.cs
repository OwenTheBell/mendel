using UnityEngine;

public class ProjectChildrenAgainstLayer : MonoBehaviour {
	
	public LayerMask Layer;

	void Start() {
		/* gather all the GeneManagers first so any nonplant children
		* are ignored */
		var managers = GetComponentsInChildren<GeneManager>();
		var index = 0;
		foreach (var manager in managers) {
			var transform = manager.transform;
			var rayOrigin = transform.position;
			rayOrigin.y += 50;

			RaycastHit hit;
			if (Physics.Raycast(rayOrigin, Vector3.down, out hit)) {
				if (((1 << hit.transform.gameObject.layer) & Layer) != 0) {
					var position = hit.point;
					manager.transform.position = position;
				}
			}
		}
	}
}