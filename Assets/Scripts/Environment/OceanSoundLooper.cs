﻿using UnityEngine;
using System.Collections;

public class OceanSoundLooper : MonoBehaviour {

    public AudioClip Clip;
    public float TransitionTime;

    private AudioSource[] _Sources;
    private int _CurrentSource;
    private float _Volume;

    void Start() {
        _Sources = GetComponents<AudioSource>();
        _CurrentSource = 0;
        _Volume = _Sources[_CurrentSource].volume;
        _Sources[0].clip = Clip;
        _Sources[1].clip = Clip;
        _Sources[1].volume = 0;
        _Sources[_CurrentSource].Play();
        _Sources[_CurrentSource].time = Clip.length - TransitionTime - 3f;
        StartCoroutine(WaitToTransition());
    }

    IEnumerator WaitToTransition() {
        while (_Sources[_CurrentSource].time + TransitionTime < Clip.length) {
            yield return null;
        }
        StartCoroutine(SwitchClips());
    }

    IEnumerator SwitchClips() {
        var nextSource = (_CurrentSource == 0) ? 1 : 0;
        _Sources[nextSource].Play();
        var time = 0f;
        while (time < TransitionTime) {
            time += Time.deltaTime;
            var p = time / TransitionTime;
            _Sources[_CurrentSource].volume = _Volume * (1f - p);
            _Sources[nextSource].volume = _Volume * p;
            yield return null;
        }
        _CurrentSource = nextSource;
        StartCoroutine(WaitToTransition());
    }
}