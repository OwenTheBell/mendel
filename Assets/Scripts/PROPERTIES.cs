public static class PROPERTIES {

    /*
     * Game
     */
    public const string VersionNumber = "1.0.3";

	/*
	* Seed mixing variables
	*/
	public const float SpeciesCutoff = 0.95f;
    public const float MutationChance = 0.2f;

    public const string NoNameMessage = "Unnamed Species";
}