﻿using UnityEngine;
using System.Collections.Generic;

public class CancelStack : MonoBehaviour, ICancelStack {

    public PauseScreen Pause;
    public CameraFader Fader;
    private List<ICancelReciever> _Stack = new List<ICancelReciever>();

    public bool Occupied { get { return _Stack.Count > 0; } }
    public bool Locked { get; set; }

    private bool _CursorVisible = false;

    void Awake() {
        Services.CancelStack = this;
        Locked = false;
    }

    void Update() {
        if (Input.GetButtonDown("Cancel") && !Locked) {
            if (_Stack.Count > 0) {
                Remove();
            }
            else {
                Pause.OnPause();
            }
        }
        Cursor.visible = _CursorVisible;
        if (!_CursorVisible) {
            Cursor.lockState = CursorLockMode.Locked;
        }
    }

    public void Add(ICancelReciever reciever, bool fade = true) {
        if (!_Stack.Contains(reciever)) {
            _Stack.Add(reciever);
            if (fade) {
                _CursorVisible = true;
                Cursor.lockState = CursorLockMode.Confined;
                Fader.Show();
            }
        }
    }

    public void Remove() {
        if (_Stack.Count > 0) {
            var reciever = _Stack[_Stack.Count - 1];
            _Stack.RemoveAt(_Stack.Count - 1);
            if (_Stack.Count == 0) {
                _CursorVisible = false;
            }
            reciever.OnCancel();
            if (_Stack.Count == 0) {
                Fader.Hide();
            }
        }
    }

    public void Remove(ICancelReciever reciever) {
        if (_Stack.Contains(reciever)) {
            _Stack.Remove(reciever);
            if (_Stack.Count == 0) {
                _CursorVisible = false;
            }
            reciever.OnCancel();
            if (_Stack.Count == 0) {
                Fader.Hide();
            }
        }
    }

    public void RemoveAll() {
        var bufferStack = new List<ICancelReciever>(_Stack);
        _Stack.Clear();
        bufferStack.Act(r => r.OnCancel());
        _CursorVisible = false;
        Fader.Hide();
    }
}