﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class TriggerPlayerStartScript : MonoBehaviour {

    public bool TriggerNewGame;

	void Start () {
        // this should only every be true if the scene is launched on its own
        // from the editor
        if (SceneManager.sceneCount == 1) {
            if (TriggerNewGame) {
                Events.instance.Raise(new NewGameEvent());
            }
            Events.instance.Raise(new PlayerStartedEvent());
        }
	}
}
