using UnityEngine;

public abstract class LauncherViewport : MonoBehaviour{

	public bool Done { get; protected set; }

	public abstract void Open();
	public abstract void Release();
	
}