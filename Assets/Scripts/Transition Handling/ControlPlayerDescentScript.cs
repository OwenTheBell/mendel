﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class ControlPlayerDescentScript : MonoBehaviour {

	public float DescentTime;
	public float BreakingTime;
	[Range(0, 2)]
	public float ShakeIntensity;
    public AudioSource EngineAudio;
    public AudioSource MusicAudio;

	private GameObject mPlayerObject;
	private Transform mCameraTransform;
	private bool mShaking = false;
	private float mShakingIntensity;
	private bool mDescending = false;
    private float _EngineVolume;
    private bool _Decelerating = false;

	void Awake() {
		mShakingIntensity = ShakeIntensity;
	}

	void Start () {
		mCameraTransform = GetComponentInChildren<Camera>().transform;
        _EngineVolume = EngineAudio.volume;
	}

	void OnEnable() {
		ListenForEvents();
	}

	void OnDisable() {
		StopListeningForEvents();
	}

	void OnDestroy() {
		StopListeningForEvents();
	}

	void ListenForEvents() {
		Events.instance.AddListener<NewGameEvent>(OnNewGameEvent);
	}

	void StopListeningForEvents() {
		Events.instance.RemoveListener<NewGameEvent>(OnNewGameEvent);
	}

    public void StartDescent() {
		if (mDescending) {
			return;
		}
        mDescending = true;
        StartCoroutine(Descend());
    }

	IEnumerator IncreaseRocketVolume() {
		EngineAudio.Play();
		var time = 0f;
		while (time < BreakingTime) {
			time += Time.deltaTime;
			var easeInOut = Easing.EaseInOut(time/BreakingTime, 2);
			EngineAudio.volume = Mathf.Lerp(0, _EngineVolume, easeInOut);
			yield return 0;
		}
        yield return null;
	}

    IEnumerator DecreaseRocketVolume() {
		var time = 0f;
		while (time < BreakingTime) {
			time += Time.deltaTime;
			var easeInOut = Easing.EaseInOut(time/BreakingTime, 2);
            EngineAudio.volume = Mathf.Lerp(_EngineVolume, 0, easeInOut);
			EngineAudio.pitch = 1f - 0.5f * easeInOut;
			yield return 0;
		}
        yield return null;
    }

	IEnumerator Descend() {
		if (EngineAudio != null) {
			StartCoroutine(IncreaseRocketVolume());
		}

		mShaking = true;
		StartCoroutine(ScreenShake());

		var time = 0f;
		var startingObject = GameObject.FindGameObjectWithTag("Starting Point");
		var startPos = startingObject.transform.position;
		var targetPos = mPlayerObject.transform.position;
        var startRot = startingObject.transform.rotation;
        var targetRot = mPlayerObject.transform.rotation;
		transform.rotation = startingObject.transform.rotation;
		while (time < DescentTime) {
			time += Time.deltaTime;
			var easeOut = Easing.EaseOut(time/DescentTime, 2);
			var newPosition = Vector3.Lerp(startPos, targetPos, easeOut);
			transform.position = newPosition;
			if (DescentTime - time <= BreakingTime) {
                if (!_Decelerating) {
                    StartCoroutine(DecreaseRocketVolume());
                    _Decelerating = true;
                }
				var difference = DescentTime - time;
				var easeIn = Easing.EaseIn(difference/BreakingTime, 2);
				mShakingIntensity = ShakeIntensity * easeIn;
				var easeInOut = Easing.EaseInOut(1f - (difference/BreakingTime), 2);
				var newRotation = Quaternion.Lerp(startRot, targetRot, easeInOut);
				transform.rotation = newRotation;
			}
			yield return 0;
		}

		mShaking = false;
		mPlayerObject.SetActive(true);
		yield return new WaitForSeconds(0.1f);
		Events.instance.Raise(new PlayerStartedEvent());
        SceneManager.UnloadSceneAsync(SceneManager.GetActiveScene());
		yield return null;
	}

	IEnumerator ScreenShake() {
		var originalPos = mCameraTransform.localPosition;

		while (mShaking) {
			var x = Random.value * 2.0f - 1.0f;
			var y = Random.value * 2.0f - 1.0f;

			var position = originalPos + new Vector3(x, y, 0f) * mShakingIntensity;

			mCameraTransform.localPosition = position;

			yield return 0;
		}

		mCameraTransform.localPosition = originalPos;

		yield return null;	
	}

	void OnNewGameEvent(NewGameEvent e) {
		mPlayerObject = GameObject.FindWithTag("Player");
		mPlayerObject.SetActive(false);
	}
}
