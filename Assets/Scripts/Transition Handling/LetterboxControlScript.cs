﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class LetterboxControlScript : LauncherViewport {

	public Image TopBar;
	public Image LowerBar;
	public Image LeftBar;
	public Image RightBar;
	public CanvasRenderer CoverRenderer;

    public float FadeTime;
    public float ReleaseTime;
	[Range(0, 1)]
	public float OpenSize;

	// public bool Done { get; private set; }

	void Awake() {
		Done = false;
	}

	// Use this for initialization
	void Start () {
		var topBotTarget = new Vector3(1f, OpenSize, 1f);
		var leftRightTarget = new Vector3(OpenSize, 1f, 1f);

		StartCoroutine(OpenView(TopBar.rectTransform, topBotTarget, 0f));
		StartCoroutine(OpenView(LowerBar.rectTransform, topBotTarget, 0f));
		StartCoroutine(OpenView(LeftBar.rectTransform, leftRightTarget, 0f));
		StartCoroutine(OpenView(RightBar.rectTransform, leftRightTarget, 0f));	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	public override void Open() {

		// var topBotTarget = new Vector3(1f, OpenSize, 1f);
		// var leftRightTarget = new Vector3(OpenSize, 1f, 1f);

		// StartCoroutine(OpenView(TopBar.rectTransform, topBotTarget, OpenTime));
		// StartCoroutine(OpenView(LowerBar.rectTransform, topBotTarget, OpenTime));
		// StartCoroutine(OpenView(LeftBar.rectTransform, leftRightTarget, OpenTime));
		// StartCoroutine(OpenView(RightBar.rectTransform, leftRightTarget, OpenTime));

		StartCoroutine(FadeOutRenderer(CoverRenderer, FadeTime));
	}

	public override void Release() {
		if (GetComponent<AudioSource>() != null) {
			GetComponent<AudioSource>().Play();
		}
		StartCoroutine(CompletelyOpenView(ReleaseTime));
	}

	IEnumerator FadeOutRenderer(CanvasRenderer renderer, float duration) {
		var startAlpha = renderer.GetAlpha();

		var time = 0f;
		while (time < duration) {
			time += Time.deltaTime;

			var easeInOut = Easing.EaseInOut(time/duration, 2);
			renderer.SetAlpha(Mathf.Lerp(startAlpha, 0f, easeInOut));
			yield return 0;
		}

		renderer.SetAlpha(0f);

		yield return null;
	}

	IEnumerator OpenView(RectTransform transform, Vector3 target, float duration) {
		var time = 0f;

		var startScale = transform.localScale;
		// var targetScale = startScale;
		// targetScale.y = target;

		while (time < duration) {
			time += Time.deltaTime;
			var easeInOut = Easing.EaseInOut(time/duration, 2);	
			var scale = Vector3.Lerp(startScale, target, easeInOut);
			transform.localScale = scale;

			yield return 0;
		}

		transform.localScale = target;

		yield return null;
	}

	IEnumerator CompletelyOpenView(float duration) {
		var target = 0f;

		var topBotTarget = new Vector3(1f, 0f, 1f);
		var leftRightTarget = new Vector3(0f, 1f, 1f);

		StartCoroutine(OpenView(TopBar.rectTransform, topBotTarget, duration));
		StartCoroutine(OpenView(LowerBar.rectTransform, topBotTarget, duration));
		StartCoroutine(OpenView(LeftBar.rectTransform, leftRightTarget, duration));
		StartCoroutine(OpenView(RightBar.rectTransform, leftRightTarget, duration));

		yield return new WaitForSeconds(duration);

		Done = true;

		yield return null;
	}
}
