﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HideCanvasUntilPlayerReadyScript : MonoBehaviour {

	void Start() {
		GetComponent<Canvas>().scaleFactor = 0f;
	}

	void OnEnable() {
		Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnDisable() {
		Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnDestroy() {
		Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnPlayerStartedEvent(PlayerStartedEvent e) {
		GetComponent<Canvas>().scaleFactor = 1f;
	}
}
