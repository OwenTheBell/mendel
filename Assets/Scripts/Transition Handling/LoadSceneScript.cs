﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class LoadSceneScript : MonoBehaviour {

	public string SceneName;

	private Scene mActiveScene;
	private Scene mLoadedScene;

	void Start () {
		mLoadedScene = SceneManager.GetSceneByName(SceneName);
		mActiveScene = SceneManager.GetActiveScene();
		StartCoroutine(LoadScene());
	}
	
	void Update () {
	
	}

	void OnEnable() {
		Events.instance.AddListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnDisable() {
		Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnDestroy() {
		Events.instance.RemoveListener<PlayerStartedEvent>(OnPlayerStartedEvent);
	}

	void OnPlayerStartedEvent(PlayerStartedEvent e) {
		SceneManager.UnloadSceneAsync(mActiveScene.name);
	}

	IEnumerator LoadScene() {
		var operation = SceneManager.LoadSceneAsync(SceneName, LoadSceneMode.Additive);
		while (!operation.isDone) {
			yield return 0;
		}
        Events.instance.Raise(new NewGameEvent());
		yield return null;
	}
}
