﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UnityEngine;

public static class ComponentFilters {
    
    public static List<T> GetComponentsInList<T>(List<GameObject> gameObjects) {
        var values = new List<T>();
        foreach (var gameObject in gameObjects) {
            if (gameObject.HasComponent<T>()) {
                values.Add(gameObject.GetComponent<T>());
            }
        }
        return values;
    }

}