﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class GameObjectExtensions {
	public static bool HasComponent<T>(this GameObject gameObject) {
		return gameObject.GetComponent<T>() != null;
	}

	public static void SetLayers(this GameObject gameObject, int layer) {
		gameObject.layer = layer;
		foreach (Transform child in gameObject.transform) {
			child.gameObject.SetLayers(layer);
		}
	}

    public static void ActOnChildren<T>(this GameObject gameObject, Action<T> a) {
        var children = gameObject.GetComponentsInChildren<T>();
        //for (var i = 0; i < children.Length; i++) {
        //    a(children[i]);
        //}
        foreach (var child in children) {
            a(child);
        }
    }
}